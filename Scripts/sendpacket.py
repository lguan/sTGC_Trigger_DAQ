import socket
import binascii
from time import sleep
from target_info import *

cmd_dict={"InitSCA":0x0100,
	  "CfgASIC":0x0200,
	  "RstASIC":0x0300,
    	  "DoNothing":0xFFFF}

class SendPacket():

	def __init__(self):
		self.host_ip="192.168.0.16"
		self.host_port=7201
		self.general_packet_header= binascii.a2b_hex('decafbad') 

	def send(self,chip,cmd_string,target):
		# First Cfg Word [28:26]=board id [25:24]=aisc type [23:16]=VMMID Bit Mask [15:0] CMD
		idWord_bin = (bin(target(chip).board_id)[2:].zfill(3))+ \
				(bin(target(chip).asic_type)[2:].zfill(2))+ \
				(bin(target(chip).asic_id)[2:].zfill(8))

		idWord_hex = hex(int(idWord_bin,2))[2:].zfill(4)
		cmd_hex = hex(cmd_dict[cmd_string])[2:].zfill(4)

		command_word_32b = binascii.a2b_hex(idWord_hex+cmd_hex)

		print "~~~~~~",binascii.b2a_hex(command_word_32b)
		payload =binascii.a2b_hex(target(chip).cmd_content)
		
		message = self.general_packet_header + command_word_32b + payload 
		mes_vec = [message]

		sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		sock.bind((self.host_ip, self.host_port))

		
		for i in range(1):
			#print "My IP:     " + self.host_ip
			#print "My Port:   " + str(self.host_port)
			#print "Dest IP:   " + target(chip).guest_ip
			#print "Dest Port: " + str(target(chip).guest_port)
			print "Payload:   " + binascii.b2a_hex(mes_vec[i])
			sock.sendto(mes_vec[i], (target(chip).guest_ip, target(chip).guest_port))
			sleep(0.2)


if __name__ == "__main__":
	sp=SendPacket()
	#sp.send('','InitSCA',target)
	sp.send('vmm','CfgASIC',target)
	sp.send('tds','CfgASIC',target)
	#sp.send('vmm','RstASIC',target)
	#sp.send('tds','RstASIC',target)



