import xml.etree.cElementTree as ET

class XmlModifier():
	def __init__(self):
		None

	# check if any item needs to be modified

	def vmm_mod(self,vmm_xml,modlist):

		vmmxml=ET.ElementTree(file=vmm_xml)

		xml_file=open(vmm_xml,'w')
		
		xml_file.write("<?xml version=\"1.0\"?>\n")
		xml_file.write("<!-- Configuration Map for VMM3\n DO NOT (DOES NOT NEED TO) CHANGE REGISTER ORDER!!\n-->\n\n\n")
		xml_file.write("<vmm3config>\n")

		for i,child in enumerate(vmmxml.getroot()):
			#save child tag 
			
			addr=None
			xml_file.write("\t<"+child.tag)
			for key,value in child.attrib.items():
				#print key,value
				xml_file.write(" "+key+"=\""+value+"\"",)
				if (key=="addr"): addr=value #for checking in the modlist in the following
			xml_file.write(">\n")

			#change and save grand-child
			for j,elem in enumerate(child.iter()):
				if (j!=0):
					if any(addr in item for item in modlist) and any(elem.tag in item for item in modlist):
						for md in modlist:
							if md[0]==addr and md[1]==elem.tag:	
								xml_file.write('\t\t<'+elem.tag+'>'+str(md[2])+'</'+elem.tag+'>'+"\n")
					else:
						xml_file.write('\t\t<'+elem.tag+'>'+elem.text+'</'+elem.tag+'>'+"\n")

			#save child elem end tag 
			xml_file.write("\t</reg>\n")	

		xml_file.write("</vmm3config>")	
		return True	

	def tds_mod(self,tds_xml,modlist):

		tdsxml=ET.ElementTree(file=tds_xml)

		xml_file=open(tds_xml,'w')

		xml_file.write("<?xml version=\"1.0\"?>\n")
		xml_file.write("<!-- Configuration Map for TDSv2\n DO NOT (DOES NOT NEED TO) CHANGE REGISTER ORDER!!\n-->\n\n\n")
		xml_file.write("<tds2config>\n")

		reglist=[]
		reg_size=0
		bitStream=""

		for i,child in enumerate(tdsxml.getroot()):
			#print i,child.tag,child.attrib
			addr=None
			xml_file.write("\t<"+child.tag)
			for key,value in child.attrib.items():
				#print key,value
				xml_file.write(" "+key+"=\""+value+"\"",)
				if (key=="addr"): addr=value #for checking in the modlist in the following
			xml_file.write(">\n")

			for j,elem in enumerate(child.iter()): 
				if (j!=0): #exclude child itself 
					#print elem.tag,elem.text
					if any(addr in item for item in modlist) and any(elem.tag in item for item in modlist):
						for md in modlist:
							if md[0]==addr and md[1]==elem.tag:	
								xml_file.write('\t\t<'+elem.tag+'>'+str(md[2])+'</'+elem.tag+'>'+"\n")
					else:
						xml_file.write('\t\t<'+elem.tag+'>'+elem.text+'</'+elem.tag+'>'+"\n")
		
			xml_file.write("\t</reg>\n")	

		xml_file.write("</tds2config>")	
		return True


if __name__ == "__main__":
	xm=XmlModifier()
	#modlist=[['global1','smch',39],['global1','sdp',137]]
	#xm=XmlModifier()
	#xm.vmm_mod('vmm.xml',modlist)
	modlist=[['2','CHAN56','ON']]
	xm.tds_mod('tds.xml',modlist)
