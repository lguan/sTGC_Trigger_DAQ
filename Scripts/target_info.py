
## Place holder to store asic configuration parameters

from xmlparser import XmlParser
xp = XmlParser()


asic_type_dict={'NONE':0,'VMM':1,'TDS':2,'ROC':3}

class target(object):
	def __init__(self,chip):
		self.guest_ip = "192.168.0.1"
		self.guest_port = 6008
		self.board_id = 0
		if (chip=="vmm"):
			self.asic_type = asic_type_dict['VMM']
			self.asic_id = 1
			self.cmd_content=xp.vmm_hexgen("vmm.xml")
		elif (chip=="tds"):
			self.asic_type = asic_type_dict['TDS']
			self.asic_id = 7
			self.cmd_content=xp.tds_hexgen("tds.xml")
		else: 
			self.asic_type = asic_type_dict['NONE']
			self.asic_id = 0
			self.cmd_content=''
			
