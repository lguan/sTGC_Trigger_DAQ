#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import unicode_literals
import os
import pexpect
import psutil

from tds_decoder import *
from target_info import *
from xmlmodifier import *
from vmm_measlib import *
from vmm_mo_acq import *
from sendpacket import SendPacket
from hist_plotter import HistPlotter
from setOptVMMCfg import *


xm=XmlModifier()
sp=SendPacket()
#hp=HistPlotter()
NPOINTS=25 #number VMM analog amplitude measurement points
NSAMPLE=1 #number of ila_samples
NTRIG_WINDOW=64 #number triggers per ila_sample

#=============
# Change Log
#=============
# Version V1: 1. speed up test, now set up 64 trigger windows of depth 8 instead of 1 at 1024.
#	         the ila_capture, tds_decode, histplotter modules have been updated. 
#	      2. now will try to detect analog pulse before any measurement. If no pulse found,
#		 will not try to measure amplitude and take TDS data.


if __name__ == "__main__":

	'''
	#====================
	# VMM Calibraton Loop
	#====================
	start=time.time()
	scope=scope('141.211.99.202')
	setup_scope(scope,scope_channel=3)
	file_name="vmm_N_cali.dat"
	f=open(file_name,'w')
	f.write("************************\n")
	f.write("******  VMM ID:   ******\n")
	f.write("** Calibration File ****\n")
	f.write("*** DO NOT MODIFY!! ****\n")
	f.write("************************\n\n\n\n")
  	f.close()		

	global_threshold_scan(scope,file_name)
	pulser_scan(scope,file_name)
	baseline_scan(scope,file_name)
	channel_threshold_scan(scope,file_name)

	dt=time.time()-start
	print "Time lasped:",dt
	
	set_optimum_threshold('./vmmcalibration/vmmid_2_opt_channelthr.txt')
	sp.send('vmm',target)
	measure_trimmed_threshold(scope,file_name)
	'''


	scope=scope('141.211.99.202')

	for sc6b in range (3,4):
		
		#for sz6b in range(0,1):
		for sz6b in [2]:
		
			moni_dir="sc6b_"+str(sc6b)+"_sz6b_"+str(sz6b)
			if not os.path.exists(moni_dir):
				os.makedirs(moni_dir)
	
			conv_time=sc6b
			adc_offset=sz6b
			fdata=open("linearity_sc6b_"+str(conv_time)+"_sz6b_"+str(adc_offset)+".dat","a")

			# setup server to control the scope
			setup_scope(scope,scope_channel=3)

			#open vivado hw manager for ila operation
			pVivado=pexpect.spawn('vivado -mode tcl -nojou -nolog')
			pVivado.sendline('catch {source ila_capture.tcl -notrace}')
			pVivado.sendline('setup_hardware_server')
			is_hw_rdy = pVivado.expect(['RDY_FOR_DAQ',pexpect.EOF,pexpect.TIMEOUT],timeout=30)
			if is_hw_rdy ==0 :
				print "[INFO] OPEN HW SUCESS!"
				#time.sleep(0.2)
			else:
				print "[ERROR] OPEN HW ERROR"

			# settings for testing VMM with PT=25ns
			gain=1.0
			sdp=[]
			sdt=[]
			st=[1]*NPOINTS
			sth=[]
			if gain==0.5:
				sdp=[60]+[i*30 for i in range (3,27)]
				sdt=[220]*25
				sth=[1]*25
			elif gain==1.0:
				sdp=[i*15 for i in range (4,29)]
				sdt=[235]*NPOINTS
				sth=[1]*25
			elif gain==3.0:
				sth=[0]*1+[1]*24
				sdp=[150,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,95,100,105,110,115,120,125,130]
				sdt=[220]*25

			void_channel=[15,16,32,33,49,50] # no TDS readout in trigger bypassed mode

			for i in range (19,64): # scan entire ADC dynamic range
				tstart=time.time()
				print "\+=++++++++++++++++++++++++++++++++++++++++++++CHAN",i,'=============================='
				fdata.write(str(i)+' ')

				#check vmm channel aliveness
				tds_modlist=[]
				vmm_modlist=[['global1','scmx',1],['global1','smch',i],['global1','sdp',200],['global1','sc6b',conv_time]]
				#--channel settings (enable one at a time)
				for k in range (0,64):
					if k==i:
						tds_modlist.append(['2','CHAN'+str(k),'ON'])
						vmm_modlist.append(['ch'+str(k),'st',1])
						vmm_modlist.append(['ch'+str(k),'sth',1])
						vmm_modlist.append(['ch'+str(k),'sz6b',adc_offset])
					else:
						tds_modlist.append(['2','CHAN'+str(k),'OFF'])
						vmm_modlist.append(['ch'+str(k),'st',0])
						vmm_modlist.append(['ch'+str(k),'sth',0])
						vmm_modlist.append(['ch'+str(k),'sz6b',0])
				#--configure VMM & TDS
				xm.vmm_mod('vmm.xml',vmm_modlist)
				sp.send('vmm',target)
				xm.tds_mod('tds.xml',tds_modlist)
				sp.send('tds',target)
				isdead=is_deadchannel(scope,scope_channel=3,meas_id=2)

				if isdead is True: #-->dead channel
					print "dead channel!!"
					fdata.write('-1'+'\n')
					continue
				elif isdead is False and i not in void_channel: #-->Good channel
					fdata.write('255'+'\n')
					pass
				else:#-->no readout by TDS in trigger bypass mode
					print "void channell!!"
					fdata.write('0'+'\n')
					continue


				hp=HistPlotter(title=moni_dir+'/channel'+str(i))

				for j in range (NPOINTS):

					vmm_add_modlist=[['global1','sdt',sdt[j]],['global1','sdp',sdp[j]],['ch'+str(i),'st',st[j]],['ch'+str(i),'sth',sth[j]]]

					#configure VMM again to enable test pulse
					xm.vmm_mod('vmm.xml',vmm_add_modlist)
					sp.send('vmm',target)
			
					#measure analog amplitude
					#fscope='ch'+str(i)+'meas'+str(j)+'.png'
					amp_result=measure_amplitude(scope,scope_channel=3,meas_id=1) #save_screen='yes',filename=fscope)
					print "FINAL RESULTS (MEAN):",amp_result
					fdata.write(str(amp_result)+'\n')

					#run ila data capture
					for x in range (NSAMPLE):
						pVivado.sendline('capture_data')
						ret=pVivado.expect(['2e845da',pexpect.EOF,pexpect.TIMEOUT],timeout=1.5)
						if (ret==0): 
							#print "ila data acquisition finished"
							qlist = decode_charge('ila.csv')
							if type(qlist) is list and (len(qlist)!=0):
								#print "Sample",x,"charge list",qlist
								hp.update_hist(amp_result,qlist)
								fdata.write(" ".join(map(lambda x: str(x), qlist))+"\n")
							else: 
								fdata.write('-2 '*NTRIG_WINDOW+'\n')
								print pVivado.before,pVivado.after
						else:
							print pVivado.before,pVivado.after
							fdata.write('-3 '*NTRIG_WINDOW+'\n')

				hp.close_plot()
				tstop=time.time()
				delta_t=tstop-tstart
				print "channel scan duration:",str(delta_t),"seconds"

			fdata.close()

			print "============================"
			print " !!!!!!!!! DONE    !!!!!!!!!!"
			print "============================"


	'''
	# ------------SCAN 6b Charge Readout with Different CK6B PHASE-----------#

	#open vivado hw manager for ila operation
	pVivado=pexpect.spawn('vivado -mode tcl -nojou -nolog')
	pVivado.sendline('catch {source ila_capture.tcl -notrace}')
	pVivado.sendline('setup_hardware_server')
	is_hw_rdy = pVivado.expect(['RDY_FOR_DAQ',pexpect.EOF,pexpect.TIMEOUT],timeout=50)
	if is_hw_rdy ==0 :
		print "open hw success!!!!!!!!!!!!!!"
		time.sleep(0.2)
	else:
		print "ERROR"

	for kk in range (32): # scan 160 MHz epll clock phase
		filename="pha_"+str(kk)+".dat"
		fdata=open(filename,"w")

		dead_channel=[15,16,32,32,49,50,27,35,36,38,41,44,45,46,51,52]
		for i in range (58):

			if i in dead_channel:
				continue
			else:
				print "+=++++++++++++++++++CHAN",i,"==,phase:",kk,'============='

			fdata.write(str(i)+'\t')

			#construct xml moderation list
			#--global first
			tds_modlist=[['1','CK160_1_PHASE',kk],['1','CK160_0_PHASE',kk]]
			vmm_modlist=[['global1','scmx',1],['global1','smch',i]]
			#--channel settings (enable one at a time)
			for k in range (58):
				if k==i:
					tds_modlist.append(['2','CHAN'+str(k),'ON'])
					vmm_modlist.append(['ch'+str(k),'st',1])
					vmm_modlist.append(['ch'+str(k),'sth',1])
				else:
					tds_modlist.append(['2','CHAN'+str(k),'OFF'])
					vmm_modlist.append(['ch'+str(k),'st',0])
					vmm_modlist.append(['ch'+str(k),'sth',0])

			#configure VMM
			xm.vmm_mod('vmm.xml',vmm_modlist)
			sp.send('vmm',target)

			#configure TDS
			xm.tds_mod('tds.xml',tds_modlist)
			sp.send('tds',target)

			#run ila data capture
			for x in range (NSAMPLE):
				pVivado.sendline('capture_data')
				ret=pVivado.expect(['2e845da',pexpect.EOF,pexpect.TIMEOUT],timeout=5)
				if (ret==0): 
					print "ila data acquisition finished"
					q = decode_charge('ila.csv')
					print "sample",x,"charge[",q[0],"]"
					fdata.write(str(q[0])+' ')
					#print pVivado.before,pVivado.after
				else:
					print pVivado.before,pVivado.after
			fdata.write('\n')

		fdata.close()
	'''
