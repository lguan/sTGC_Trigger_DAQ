`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/30/2017 10:09:09 AM
// Design Name: 
// Module Name: gt_data2fifo
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: This module pushes TDS data packets input FIFO and wait for building UDP packet
//              Note TDS needs to send NULL packet at the very beginning for the correct
//              recognition of its first 30-bit data packet in each 120-bit data frame 
//              - FIFO DATA WIDTH: 28
//              - FIFO DEPTH: 64
//              - FIFO DATA WORD FORMAT:
//                2bit Header + 26 bit (TDS Data Packet without 4-bit header)
//                  2'b00   +
//
//              A NULL packet is inserted into the FIFO as event delimiter  
//                2'b11 + 26'b00              
//
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module gt_data2fifo(
   input rst,
   input gt_clk, //160 MHz
   input [29:0] gt_data,
   input gt_data_valid,
   input daq_start,
   input fifo_rd_clk, //80 MHz
   input fifo_rd_en,
   output [27:0] fifo_dout,
   output fifo_almost_full,
   output fifo_empty
);

   
    //--------------reg, wire definition-------------
    reg [2:0]st;
    reg [5:0]tds_packet_cnt;
    reg [27:0]fifo_din_r;
    reg fifo_wren_r;
    
    wire daq_start_w;
    wire [27:0] fifo_din_w;
    wire fifo_wren_w;   
    wire fifo_rd_en_w; 
 
    parameter idle=3'b0, st1=3'b1, st2=3'b10, st3=3'b11, st4=3'b100, st5=3'b101, st6=3'b110, st7=3'b111;
    parameter event_delimiter=28'hC000000;
   
   
   //--------------VIO to provide FIFO read write control--------
   
   wire daq_start_vio;
   wire rd_en_vio;
   reg  rd_en_vio_r;
   reg [2:0] vio_st_daq;
   reg [2:0] vio_st_rd;
   reg [3:0] rd_en_cnt;
   
   
   vio_gt_fifo_ctrl vio_gt_fifo_cctrl_inst(
     .clk(gt_clk),
     .probe_out0(daq_start_vio),
     .probe_out1(rd_en_vio)     
    );

   always @ (posedge gt_clk) begin
      case (vio_st_daq)
        3'b0: begin
            if (daq_start_vio==1'b1) vio_st_daq <=3'b1;
            else vio_st_daq<=3'b0;
        end
        3'b1: begin
            if (daq_start_vio==1'b0) vio_st_daq <=3'b0;
            else vio_st_daq <= 3'b1;
        end
        default: vio_st_daq<= 3'b0;
      endcase
   end
   

   always @ (posedge fifo_rd_clk) begin
      case (vio_st_rd)
        3'b0: begin
            rd_en_vio_r <=1'b0;
            if (rd_en_vio==1'b1) vio_st_rd <=3'b1;
            else vio_st_rd<=3'b0;
        end
        3'b1: begin
            if (fifo_empty!=1'b0) begin 
                rd_en_vio_r <= 1'b1;
                vio_st_rd <= 3'b1;
            end
            else begin
                rd_en_vio_r <= 1'b1;
                vio_st_rd <= 3'b10;
            end
        end
        3'b10: begin
            if (rd_en_vio==1'b0) vio_st_rd <=3'b0;
            else vio_st_rd <= 3'b10;
        end
        default: vio_st_rd<= 3'b0;
      endcase
   end

   
   
   assign daq_start_w = daq_start | daq_start_vio;
   assign fifo_rd_en_w = rd_en_vio_r | fifo_rd_en;
   
    //-------------state machine to control gt0 data flow ------------
    always @ (posedge gt_clk) begin
        if (rst==1'b1)  st <= idle;
        else case(st)
        idle: begin
            tds_packet_cnt <= 'b0;
            fifo_din_r <= 'b0;
            fifo_wren_r <= 1'b0;
            if (gt_data[29:26]==4'b1100 && gt_data_valid==1'b1 && daq_start_w==1'b1) st<= st1; //found NULL Packet
            else st <= idle;
        end
        st1: begin
            if (daq_start_w==1'b1) begin
                if (gt_data[29:26]==4'b1010) begin //found 1st data packet 
                    fifo_din_r <= {2'b00,gt_data[25:0]};
                    fifo_wren_r <= 1'b1;
                    tds_packet_cnt <= tds_packet_cnt + 1'b1;
                    st <= st2;
                end 
                else begin //found another null packet
                    fifo_wren_r <= 1'b0;
                    st <= st1;           
                end  
            end
            else st <= idle; //daq stopped...
        end
        st2: begin
            if (gt_data[29:26]==4'b1010) begin 
                fifo_din_r <= {2'b00,gt_data[25:0]};
                fifo_wren_r <= 1'b1;
                tds_packet_cnt <= tds_packet_cnt + 1'b1;
                st <= st2;
            end 
            else if (gt_data[29:26]==4'b1100) begin //found the end of data packet, insert delimiter
                fifo_din_r <= event_delimiter;
                fifo_wren_r <= 1'b1;   
                tds_packet_cnt <= 'b0;         
                st <= st1; 
            end 
            else st <= idle; // something went wrong! unrecognized packet
        end
        default: st <= idle;
        endcase
    end
     

    //-------------fifo to store TDS data packet ------------     
    assign fifo_din_w = fifo_din_r;
    assign fifo_wren_w = fifo_wren_r;

    fifo_gt_channel_data fifo_gt_channel_data_inst0
    (
        .rst(1'b0),
        .wr_clk(gt_clk),
        .rd_clk(fifo_rd_clk),
        .din(fifo_din_w),
        .wr_en(fifo_wren_w),
        .rd_en(fifo_rd_en_w),
        .dout(fifo_dout),
        .full(),
        .empty(fifo_empty),
        .valid(),
        .prog_full(fifo_almost_full)
     );


    //ila to monitor state machine operation
    ila_gt_data2fifo ila_gt_data2fifo_inst(
         .clk(gt_clk),
         .probe0(st),
         .probe1(gt_data),
         .probe2(fifo_din_w),
         .probe3(tds_packet_cnt)
    );

    //ila to monitor gt_fifo output
    ila_gt_channelFIFO_dout ila_gt_channelFIFO_dout_inst(
         .clk(fifo_rd_clk),
         .probe0(fifo_empty),
         .probe1(fifo_dout),
         .probe2(fifo_almost_full),
         .probe3(fifo_rd_en_w)
    );



endmodule
