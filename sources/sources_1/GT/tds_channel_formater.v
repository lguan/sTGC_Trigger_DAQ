`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/29/2017 08:22:51 PM
// Design Name: 
// Module Name: tds_channel_formater
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tds_channel_formater(
       input rst,
       input gt_clk, //160 MHz
       input [29:0] gt_data,
       input gt_data_valid,
       input fifo_rd_clk //200 MHz

    );
    
       
        // reg, wire definition
        reg [1:0] tds_frame_cnt; // 1 data frame = 120 bits
        reg [3:0] tds_packet_cnt; // 1 data packet = 30 bits
        reg [3:0] tds_word2fifo_cnt; // word cnts for tds0 data (4 or 8 or 12)   
        reg [3:0] udp_content_cnt;
        reg [3:0] udp_word_cnt;
        reg [4:0] gt_st;
        reg [127:0] tds_dframe0; 
        reg [127:0] tds_dframe1; 
        reg [127:0] tds_dframe2; 
        reg [31:0]  gt_fifo_din_r;
        reg gt_fifo_wren_r;
        
        wire [31:0] gt_fifo_din_w;
        wire gt_fifo_wren_w;
        
        parameter idle=4'b0, st1=4'b1, st2=4'b10, st3=4'b11, st4=4'b100, st5=4'b101, st6=4'b110, st7=4'b111, st8=4'b1000, st9=4'b1001, st10=4'b1010;
        
            
        //state machine to control gt0 data flow 
        //----------------------------------------------------------------------------
        // IMPORTANT NOTE: TDS NEEDS TO SEND NULL PACKET AT VERY BEGINNING FOR CORRECT
        // RECOGNITION of ITS FIRST 30-BIT DATA PACKET OF EACH 120-BIT DATA FRAME 
        // ALLOW TO STORE 3 CONSECUTIVE TDS DATA FRAMES (3 BCs)
        //----------------------------------------------------------------------------
        
        always @ (posedge gt_clk) begin
            if (rst==1'b1) begin
                tds_dframe0 <='b0;
                tds_dframe1 <='b0;
                tds_dframe2 <='b0;
                gt_fifo_din_r <='b0;
                gt_fifo_wren_r <=1'b0;
                tds_frame_cnt <= 'b0;
                tds_packet_cnt <='b0;
                tds_word2fifo_cnt <= 'b0;
                udp_word_cnt <='b0;
            end
            else begin 
            case(gt_st)
                idle: begin
                    tds_dframe0 <='b0;
                    tds_dframe1 <='b0;
                    tds_dframe2 <='b0;
                    gt_fifo_din_r <='b0;
                    gt_fifo_wren_r <=1'b0;
                    tds_frame_cnt <= 'b0;
                    tds_packet_cnt <='b0;  
                    tds_word2fifo_cnt <= 'b0;
                    udp_word_cnt <='b0;
                    if (gt_data_valid==1'b1) gt_st <= st1; 
                    else gt_st <= idle;
                end
                st1: begin //wait for a NULL packet
                    if (gt_data[29:26]==4'b1100) begin
                        gt_st <= st2; 
                    end
                    else gt_st <= st1; 
                end        
                st2: begin 
                    if (gt_data[29:26]==4'b1010) begin //found 1st data packet of a 120-bit TDS data frame
                            tds_dframe0[25:0] <= gt_data[25:0];
                            tds_dframe0[127:26] <= tds_dframe0[101:0];
                            tds_packet_cnt <= tds_packet_cnt + 1'b1; 
                            tds_word2fifo_cnt <= tds_word2fifo_cnt +1'b1;
                            gt_st <= st3;
                    end 
                    else if(gt_data[29:26]==4'b1100) gt_st <= st2; //found another NULL packet      
                    else gt_st<=idle; //something went wrong: tds data packet header is not 4'b1010/4'b1100  
                end
                st3: begin // write last 3 data packets in a tds data frame
                        if (tds_frame_cnt==2'b0) begin
                          tds_dframe0[25:0] <= gt_data[25:0];
                          tds_dframe0[127:26] <= tds_dframe0[101:0];
                        end
                        else if (tds_frame_cnt==2'b1) begin
                          tds_dframe1[25:0] <= gt_data[25:0];
                          tds_dframe1[127:26] <= tds_dframe1[101:0];
                        end                   
                        else if (tds_frame_cnt==2'b10) begin
                          tds_dframe2[25:0] <= gt_data[25:0];
                          tds_dframe2[127:26] <= tds_dframe2[101:0];
                        end                     
                
                        if (tds_packet_cnt < 4) begin
                            tds_packet_cnt <= tds_packet_cnt + 1'b1;
                            tds_word2fifo_cnt <= tds_word2fifo_cnt +1'b1;
                            gt_st <= st3;
                        end
                        else begin 
                            tds_packet_cnt <='b0;
                            tds_frame_cnt <= tds_frame_cnt + 1'b1;
                            gt_st <= st4;
                        end   
                end
                st4: begin  // looking for possible second/third tds data frame
                    if (gt_data[29:26]==4'b1010 && tds_frame_cnt<3) begin // found starting packet of 2nd/3rd 120-bit TDS data frame
                                tds_dframe1[25:0] <= gt_data[25:0];
                                tds_dframe1[127:26] <= tds_dframe1[101:0];
                                tds_packet_cnt <= tds_packet_cnt + 1'b1; 
                                tds_word2fifo_cnt <= tds_word2fifo_cnt +1'b1;
                                gt_st <= st3;
                    end
                    else if (gt_data[29:26]==4'b1100)  gt_st <= st5; // no more tds data frame
                    else gt_st<=idle; //something went wrong: tds data packet header is not 4'b1010/4'b1100  
                end
                st5: begin // build udp packet and write to FIFO
                     
                     if (udp_word_cnt==4'b0) begin // header
                        gt_fifo_din_r <= 32'hdecafbad; 
                        gt_st <= st5; 
                     end
                     else if (udp_word_cnt==4'b1) begin //reply command identifier
                        gt_fifo_din_r<=32'h000000F0; 
                        udp_content_cnt <= 'b0;
                        gt_st <= st5; 
                     end
                     else begin
                        if (udp_content_cnt<tds_word2fifo_cnt) begin
                            if(udp_content_cnt==4'b0) gt_fifo_din_r <= tds_dframe0[127:96];
                            else if (udp_content_cnt==4'b1) gt_fifo_din_r <= tds_dframe0[95:64];
                            else if (udp_content_cnt==4'b10) gt_fifo_din_r <= tds_dframe0[63:32];
                            else if (udp_content_cnt==4'b11) gt_fifo_din_r <= tds_dframe0[31:0];
                            else if (udp_content_cnt==4'b100) gt_fifo_din_r <= tds_dframe1[127:96];
                            else if (udp_content_cnt==4'b101) gt_fifo_din_r <= tds_dframe1[95:64];
                            else if (udp_content_cnt==4'b110) gt_fifo_din_r <= tds_dframe1[63:32];
                            else if (udp_content_cnt==4'b111) gt_fifo_din_r <= tds_dframe1[31:0];
                            else if (udp_content_cnt==4'b1000) gt_fifo_din_r <= tds_dframe2[127:96];
                            else if (udp_content_cnt==4'b1001) gt_fifo_din_r <= tds_dframe2[95:64];
                            else if (udp_content_cnt==4'b1010) gt_fifo_din_r <= tds_dframe2[63:32];
                            else  gt_fifo_din_r <= tds_dframe2[31:0];
                            udp_content_cnt <= udp_content_cnt +1'b1;
                            gt_st <= st5; 
                        end
                        else gt_st <= st6;
                     end
                     
                     udp_word_cnt <= udp_word_cnt +1'b1;
                     gt_fifo_din_r <= 1'b1; 
                end            
                st6: begin // one UDP packet assmebled
                    gt_st<=idle;
                end
                default: gt_st<=idle;
            endcase
            end
        end
            
 
 
           
    
        //ila to monitor state machine operation
        ila_gtdata2fifo ila_gtdata2fifo_inst(
         .clk(gt_clk),
         .probe0(gt_st),
         .probe1(gt_data),
         .probe2(tds_dframe0),
         .probe3(tds_dframe1),
         .probe4(tds_dframe2),
         .probe5(gt_fifo_din_w),
         .probe6(tds_word2fifo_cnt)
        );





        assign gt_fifo_din_w = gt_fifo_din_r;
        assign gt_fifo_wren_w = gt_fifo_wren_r;
    
//        gt_channel_fifo gt0_fifo_inst
//        (
//            .rst(1'b0),
//            .wr_clk(gt_clk),
//            .rd_clk(fifo_rd_clk),
//            .din(gt0_fifo_din_w),
//            .wr_en(gt0_fifo_wren_w),
//            .rd_en(),
//            .dout(),
//            .full(),
//            .almost_full(),
//            .empty()
//          );
        

    
    
    
endmodule
