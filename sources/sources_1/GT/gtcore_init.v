
`timescale 1ns / 1ps
`define DLY #1

(* DowngradeIPIdentifiedWarnings="yes" *)
//***********************************Entity Declaration************************
(* CORE_GENERATION_INFO = "gtcore,gtwizard_v3_6_3,{protocol_file=Start_from_scratch}" *)
module gtcore_init #
(
    parameter EXAMPLE_CONFIG_INDEPENDENT_LANES     =   1,//configuration for frame gen and check
    parameter EXAMPLE_LANE_WITH_START_CHAR         =   0,         // specifies lane with unique start frame char
    parameter EXAMPLE_WORDS_IN_BRAM                =   512,       // specifies amount of data in BRAM
    parameter EXAMPLE_SIM_GTRESET_SPEEDUP          =   "TRUE",    // simulation setting for GT SecureIP model
    parameter EXAMPLE_USE_CHIPSCOPE                =   0,         // Set to 1 to use Chipscope to drive resets
    parameter STABLE_CLOCK_PERIOD                  = 16

)
(
    input  wire  Q3_CLK1_GTREFCLK_PAD_N_IN,
    input  wire  Q3_CLK1_GTREFCLK_PAD_P_IN,
    input  wire  DRPCLK_IN,   
    input  wire [3:0]   RXN_IN,
    input  wire [3:0]   RXP_IN,
    output wire [3:0]   TXN_OUT,
    output wire [3:0]   TXP_OUT,
    input RST_CHECKER,
    input GTX_RST,
    
    //Ethernet Interface
    input CLK_FIFO //80MHz
    
);

    wire soft_reset_i;
    //(*mark_debug = "TRUE" *) wire soft_reset_vio_i;
    
//************************** Register Declarations ****************************
    wire            gt0_txfsmresetdone_i;
    wire            gt0_rxfsmresetdone_i;
    (* ASYNC_REG = "TRUE" *)reg             gt0_txfsmresetdone_r;
    (* ASYNC_REG = "TRUE" *)reg             gt0_txfsmresetdone_r2;
    (* ASYNC_REG = "TRUE" *)reg             gt0_rxresetdone_r;
    (* ASYNC_REG = "TRUE" *)reg             gt0_rxresetdone_r2;
    (* ASYNC_REG = "TRUE" *)reg             gt0_rxresetdone_r3;
    wire            gt1_txfsmresetdone_i;
    wire            gt1_rxfsmresetdone_i;
    (* ASYNC_REG = "TRUE" *)reg             gt1_txfsmresetdone_r;
    (* ASYNC_REG = "TRUE" *)reg             gt1_txfsmresetdone_r2;
    (* ASYNC_REG = "TRUE" *)reg             gt1_rxresetdone_r;
    (* ASYNC_REG = "TRUE" *)reg             gt1_rxresetdone_r2;
    (* ASYNC_REG = "TRUE" *)reg             gt1_rxresetdone_r3;
    wire            gt2_txfsmresetdone_i;
    wire            gt2_rxfsmresetdone_i;
    (* ASYNC_REG = "TRUE" *)reg             gt2_txfsmresetdone_r;
    (* ASYNC_REG = "TRUE" *)reg             gt2_txfsmresetdone_r2;
    (* ASYNC_REG = "TRUE" *)reg             gt2_rxresetdone_r;
    (* ASYNC_REG = "TRUE" *)reg             gt2_rxresetdone_r2;
    (* ASYNC_REG = "TRUE" *)reg             gt2_rxresetdone_r3;
    wire            gt3_txfsmresetdone_i;
    wire            gt3_rxfsmresetdone_i;
    (* ASYNC_REG = "TRUE" *)reg             gt3_txfsmresetdone_r;
    (* ASYNC_REG = "TRUE" *)reg             gt3_txfsmresetdone_r2;
    (* ASYNC_REG = "TRUE" *)reg             gt3_rxresetdone_r;
    (* ASYNC_REG = "TRUE" *)reg             gt3_rxresetdone_r2;
    (* ASYNC_REG = "TRUE" *)reg             gt3_rxresetdone_r3;


    reg [5:0] reset_counter = 0;
    reg [3:0] reset_pulse;


//**************************** Wire Declarations ******************************//
    //------------------------ GT Wrapper Wires ------------------------------
    //________________________________________________________________________
    //________________________________________________________________________
    //GT0  (X0Y12)
    //------------------------------- CPLL Ports -------------------------------
    wire            gt0_cpllfbclklost_i;
    wire            gt0_cplllock_i;
    //-------------------------- Channel - DRP Ports  --------------------------
    wire    [8:0]   gt0_drpaddr_i;
    wire    [15:0]  gt0_drpdi_i;
    wire    [15:0]  gt0_drpdo_i;
    wire            gt0_drpen_i;
    wire            gt0_drprdy_i;
    wire            gt0_drpwe_i;
    //------------------------- Digital Monitor Ports --------------------------
    wire    [7:0]   gt0_dmonitorout_i;
    //------------------------ RX Margin Analysis Ports ------------------------
    wire            gt0_eyescandataerror_i;
    //---------------- Receive Ports - FPGA RX interface Ports -----------------
    wire    [19:0]  gt0_rxdata_i;
    //----------------- Receive Ports - RX Buffer Bypass Ports -----------------
    //wire            gt0_rxdlysreset_i;
    //wire            gt0_rxdlysresetdone_i;
    //wire            gt0_rxphaligndone_i;
    //wire            gt0_rxphdlyreset_i;
    wire    [4:0]   gt0_rxphmonitor_i;
    wire    [4:0]   gt0_rxphslipmonitor_i;
    //------------------- Receive Ports - RX Equalizer Ports -------------------
    wire    [6:0]   gt0_rxmonitorout_i;
    //------------- Receive Ports - RX Fabric Output Control Ports -------------
    wire            gt0_rxoutclkfabric_i;
    //----------- Receive Ports - RX Initialization and Reset Ports ------------
    wire            gt0_rxpmareset_i;
    //------------ Receive Ports -RX Initialization and Reset Ports ------------
    wire            gt0_rxresetdone_i;
    //---------------- Transmit Ports - TX Data Path interface -----------------
    wire    [19:0]  gt0_txdata_i;
    //--------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
    wire            gt0_txoutclkfabric_i;
    wire            gt0_txoutclkpcs_i;
    //----------- Transmit Ports - TX Initialization and Reset Ports -----------
    wire            gt0_txresetdone_i;
    //________________________________________________________________________
    //GT1  (X0Y13)
    //------------------------------- CPLL Ports -------------------------------
    wire            gt1_cpllfbclklost_i;
    wire            gt1_cplllock_i;
    //-------------------------- Channel - DRP Ports  --------------------------
    wire    [8:0]   gt1_drpaddr_i;
    wire    [15:0]  gt1_drpdi_i;
    wire    [15:0]  gt1_drpdo_i;
    wire            gt1_drpen_i;
    wire            gt1_drprdy_i;
    wire            gt1_drpwe_i;
    //------------------------- Digital Monitor Ports --------------------------
    wire    [7:0]   gt1_dmonitorout_i;
    //------------------------ RX Margin Analysis Ports ------------------------
    wire            gt1_eyescandataerror_i;
    //---------------- Receive Ports - FPGA RX interface Ports -----------------
    wire    [19:0]  gt1_rxdata_i;
    //----------------- Receive Ports - RX Buffer Bypass Ports -----------------
    //wire            gt1_rxdlysreset_i;
    //wire            gt1_rxdlysresetdone_i;
    //wire            gt1_rxphaligndone_i;
    //wire            gt1_rxphdlyreset_i;
    wire    [4:0]   gt1_rxphmonitor_i;
    wire    [4:0]   gt1_rxphslipmonitor_i;   
    //------------------- Receive Ports - RX Equalizer Ports -------------------
    wire    [6:0]   gt1_rxmonitorout_i;
    //------------- Receive Ports - RX Fabric Output Control Ports -------------
    wire            gt1_rxoutclkfabric_i;
    //----------- Receive Ports - RX Initialization and Reset Ports ------------
    wire            gt1_rxpmareset_i;
    //------------ Receive Ports -RX Initialization and Reset Ports ------------
    wire            gt1_rxresetdone_i;
    //---------------- Transmit Ports - TX Data Path interface -----------------
    wire    [19:0]  gt1_txdata_i;
    //--------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
    wire            gt1_txoutclkfabric_i;
    wire            gt1_txoutclkpcs_i;
    //----------- Transmit Ports - TX Initialization and Reset Ports -----------
    wire            gt1_txresetdone_i;
    //________________________________________________________________________
    //GT2  (X0Y14)
    //------------------------------- CPLL Ports -------------------------------
    wire            gt2_cpllfbclklost_i;
    wire            gt2_cplllock_i;
    //-------------------------- Channel - DRP Ports  --------------------------
    wire    [8:0]   gt2_drpaddr_i;
    wire    [15:0]  gt2_drpdi_i;
    wire    [15:0]  gt2_drpdo_i;
    wire            gt2_drpen_i;
    wire            gt2_drprdy_i;
    wire            gt2_drpwe_i;
    //------------------------- Digital Monitor Ports --------------------------
    wire    [7:0]   gt2_dmonitorout_i;
    //------------------------ RX Margin Analysis Ports ------------------------
    wire            gt2_eyescandataerror_i;
    //---------------- Receive Ports - FPGA RX interface Ports -----------------
    wire    [19:0]  gt2_rxdata_i;    
    //----------------- Receive Ports - RX Buffer Bypass Ports -----------------
    //wire            gt2_rxdlysreset_i;
    //wire            gt2_rxdlysresetdone_i;
    //wire            gt2_rxphaligndone_i;
    //wire            gt2_rxphdlyreset_i;
    wire    [4:0]   gt2_rxphmonitor_i;
    wire    [4:0]   gt2_rxphslipmonitor_i;
    //------------------- Receive Ports - RX Equalizer Ports -------------------
    wire    [6:0]   gt2_rxmonitorout_i;
    //------------- Receive Ports - RX Fabric Output Control Ports -------------
    wire            gt2_rxoutclkfabric_i;
    //----------- Receive Ports - RX Initialization and Reset Ports ------------
    wire            gt2_rxpmareset_i;
    //------------ Receive Ports -RX Initialization and Reset Ports ------------
    wire            gt2_rxresetdone_i;
    //---------------- Transmit Ports - TX Data Path interface -----------------
    wire    [19:0]  gt2_txdata_i;
    //--------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
    wire            gt2_txoutclkfabric_i;
    wire            gt2_txoutclkpcs_i;
    //----------- Transmit Ports - TX Initialization and Reset Ports -----------
    wire            gt2_txresetdone_i;
    //________________________________________________________________________
    //GT3  (X0Y15)
    //------------------------------- CPLL Ports -------------------------------
    wire            gt3_cpllfbclklost_i;
    wire            gt3_cplllock_i;
    //-------------------------- Channel - DRP Ports  --------------------------
    wire    [8:0]   gt3_drpaddr_i;
    wire    [15:0]  gt3_drpdi_i;
    wire    [15:0]  gt3_drpdo_i;
    wire            gt3_drpen_i;
    wire            gt3_drprdy_i;
    wire            gt3_drpwe_i;
    //------------------------- Digital Monitor Ports --------------------------
    wire    [7:0]   gt3_dmonitorout_i;
    //------------------------ RX Margin Analysis Ports ------------------------
    wire            gt3_eyescandataerror_i;
    //---------------- Receive Ports - FPGA RX interface Ports -----------------
    wire    [19:0]  gt3_rxdata_i;
    //----------------- Receive Ports - RX Buffer Bypass Ports -----------------
    //wire            gt3_rxdlysreset_i;
    //wire            gt3_rxdlysresetdone_i;
    //wire            gt3_rxphaligndone_i;
    //wire            gt3_rxphdlyreset_i;
    wire    [4:0]   gt3_rxphmonitor_i;
    wire    [4:0]   gt3_rxphslipmonitor_i;
    //------------------- Receive Ports - RX Equalizer Ports -------------------
    wire    [6:0]   gt3_rxmonitorout_i;
    //------------- Receive Ports - RX Fabric Output Control Ports -------------
    wire            gt3_rxoutclkfabric_i;
    //----------- Receive Ports - RX Initialization and Reset Ports ------------
    wire            gt3_rxpmareset_i;
    //------------ Receive Ports -RX Initialization and Reset Ports ------------
    wire            gt3_rxresetdone_i;
    //---------------- Transmit Ports - TX Data Path interface -----------------
    wire    [19:0]  gt3_txdata_i;
    //--------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
    wire            gt3_txoutclkfabric_i;
    wire            gt3_txoutclkpcs_i;
    //----------- Transmit Ports - TX Initialization and Reset Ports -----------
    wire            gt3_txresetdone_i;

    //____________________________COMMON PORTS________________________________
    //----------------------------- Global Signals -----------------------------
    wire            gt0_tx_system_reset_c;
    wire            gt0_rx_system_reset_c;
    wire            gt1_tx_system_reset_c;
    wire            gt1_rx_system_reset_c;
    wire            gt2_tx_system_reset_c;
    wire            gt2_rx_system_reset_c;
    wire            gt3_tx_system_reset_c;
    wire            gt3_rx_system_reset_c;
    wire            tied_to_ground_i;
    wire    [63:0]  tied_to_ground_vec_i;
    wire            tied_to_vcc_i;
    wire    [7:0]   tied_to_vcc_vec_i;
    wire            GTTXRESET_IN;
    wire            GTRXRESET_IN;
    wire            CPLLRESET_IN;
    wire            QPLLRESET_IN;

     //--------------------------- User Clocks ---------------------------------
    wire            gt0_txusrclk_i; 
    wire            gt0_txusrclk2_i; 
    wire            gt0_rxusrclk_i; 
    wire            gt0_rxusrclk2_i;
    wire            gt0_txmmcm_reset_i;
    
    wire            gt1_txusrclk_i; 
    wire            gt1_txusrclk2_i; 
    wire            gt1_rxusrclk_i; 
    wire            gt1_rxusrclk2_i;
    wire            gt1_txmmcm_reset_i;
    
    wire            gt2_txusrclk_i; 
    wire            gt2_txusrclk2_i; 
    wire            gt2_rxusrclk_i; 
    wire            gt2_rxusrclk2_i;
    wire            gt2_txmmcm_reset_i;
    
    wire            gt3_txusrclk_i; 
    wire            gt3_txusrclk2_i; 
    wire            gt3_rxusrclk_i; 
    wire            gt3_rxusrclk2_i;
    wire            gt3_txmmcm_reset_i;
    //--------------------- Frame check/gen Module Signals --------------------
    wire            gt0_matchn_i;
    wire    [5:0]   gt0_txcharisk_float_i;
    wire    [15:0]  gt0_txdata_float16_i;
    wire    [43:0]  gt0_txdata_float_i;
    wire            gt0_block_sync_i;
    wire            gt0_track_data_i;
    wire    [7:0]   gt0_error_count_i;
    wire            gt0_frame_check_reset_i;
    wire            gt0_inc_out_i;
    wire    [19:0]  gt0_unscrambled_data_i;

    wire            gt1_matchn_i;
    wire    [5:0]   gt1_txcharisk_float_i;
    wire    [15:0]  gt1_txdata_float16_i;
    wire    [43:0]  gt1_txdata_float_i;
    wire            gt1_block_sync_i;
    wire            gt1_track_data_i;
    wire    [7:0]   gt1_error_count_i;
    wire            gt1_frame_check_reset_i;
    wire            gt1_inc_out_i;
    wire    [19:0]  gt1_unscrambled_data_i;
    
    wire            gt2_matchn_i;
    wire    [5:0]   gt2_txcharisk_float_i;
    wire    [15:0]  gt2_txdata_float16_i;
    wire    [43:0]  gt2_txdata_float_i;
    wire            gt2_block_sync_i;
    wire            gt2_track_data_i;
    wire    [7:0]   gt2_error_count_i;
    wire            gt2_frame_check_reset_i;
    wire            gt2_inc_out_i;
    wire    [19:0]  gt2_unscrambled_data_i;

    wire            gt3_matchn_i;
    wire    [5:0]   gt3_txcharisk_float_i;
    wire    [15:0]  gt3_txdata_float16_i;
    wire    [43:0]  gt3_txdata_float_i;
    wire            gt3_block_sync_i;
    wire            gt3_track_data_i;
    wire    [7:0]   gt3_error_count_i;
    wire            gt3_frame_check_reset_i;
    wire            gt3_inc_out_i;
    wire    [19:0]  gt3_unscrambled_data_i;

    wire            reset_on_data_error_i;
    
    
    
    //--------------------------- Custom declared ports, wires ---------------------------------
    wire gt0_clk160;
    wire [29:0]gt0_data_out;
    wire [29:0]gt1_data_out;
    wire [29:0]gt2_data_out;
    wire [29:0]gt3_data_out;
    wire gt0_data_valid_o;
    wire gt1_data_valid_o;
    wire gt2_data_valid_o;
    wire gt3_data_valid_o;        

 
//**************************** Main Body of Code *******************************

    //  Static signal Assigments    
    assign tied_to_ground_i             = 1'b0;
    assign tied_to_ground_vec_i         = 64'h0000000000000000;
    assign tied_to_vcc_i                = 1'b1;
    assign tied_to_vcc_vec_i            = 8'hff;

    assign zero_vector_rx_80 = 0;
    assign zero_vector_rx_8 = 0;



    //***********************************************************************//
    //                                                                       //
    //------------------------ TDS DATA DRIVER  -----------------------------//
    //                                                                       //
    //***********************************************************************//
    tds_packet_formater tds_packet_formater_inst(
        .rst(1'b0),
        .gt0_clk160(gt0_clk160),
        .fifo_rd_clk(CLK_FIFO),
        .gt0_data_out(gt0_data_out),
        .gt0_data_valid_o(gt0_data_valid_o),
        .gt1_data_out(gt1_data_out),
        .gt1_data_valid_o(gt1_data_valid_o),
        .gt2_data_out(gt2_data_out),
        .gt2_data_valid_o(gt2_data_valid_o),
        .gt3_data_out(gt3_data_out),
        .gt3_data_valid_o(gt3_data_valid_o)
          
        //Ethernet Interface
 
    
    );



    //***********************************************************************//
    //                                                                       //
    //--------------------------- The GT Wrapper ----------------------------//
    //                                                                       //
    //***********************************************************************//
    
    // Use the instantiation template in the example directory to add the GT wrapper to your design.
    // In this example, the wrapper is wired up for basic operation with a frame generator and frame 
    // checker. The GTs will reset, then attempt to align and transmit data. If channel bonding is 
    // enabled, bonding should occur after alignment.



    gtcore_tds4 gtcore_tds4 
    (
        .soft_reset_tx_in               (soft_reset_i),
        .soft_reset_rx_in               (soft_reset_i||GTX_RST),
        .dont_reset_on_data_error_in    (tied_to_ground_i),
        .q3_clk1_gtrefclk_pad_n_in(Q3_CLK1_GTREFCLK_PAD_N_IN),
        .q3_clk1_gtrefclk_pad_p_in(Q3_CLK1_GTREFCLK_PAD_P_IN),
        .gt0_tx_mmcm_lock_out           (gt0_txmmcm_lock_i),
        .gt0_tx_fsm_reset_done_out      (gt0_txfsmresetdone_i),
        .gt0_rx_fsm_reset_done_out      (gt0_rxfsmresetdone_i),
        .gt0_data_valid_in              (gt0_track_data_i),
        .gt1_tx_mmcm_lock_out           (gt1_txmmcm_lock_i),
        .gt1_tx_fsm_reset_done_out      (gt1_txfsmresetdone_i),
        .gt1_rx_fsm_reset_done_out      (gt1_rxfsmresetdone_i),
        .gt1_data_valid_in              (gt1_track_data_i),
        .gt2_tx_mmcm_lock_out           (gt2_txmmcm_lock_i),
        .gt2_tx_fsm_reset_done_out      (gt2_txfsmresetdone_i),
        .gt2_rx_fsm_reset_done_out      (gt2_rxfsmresetdone_i),
        .gt2_data_valid_in              (gt2_track_data_i),
        .gt3_tx_mmcm_lock_out           (gt3_txmmcm_lock_i),
        .gt3_tx_fsm_reset_done_out      (gt3_txfsmresetdone_i),
        .gt3_rx_fsm_reset_done_out      (gt3_rxfsmresetdone_i),
        .gt3_data_valid_in              (gt3_track_data_i),
     
        .gt0_txusrclk_out(gt0_txusrclk_i),
        .gt0_txusrclk2_out(gt0_txusrclk2_i),
        .gt0_rxusrclk_out(gt0_rxusrclk_i),
        .gt0_rxusrclk2_out(gt0_rxusrclk2_i),
     
        .gt1_txusrclk_out(gt1_txusrclk_i),
        .gt1_txusrclk2_out(gt1_txusrclk2_i),
        .gt1_rxusrclk_out(gt1_rxusrclk_i),
        .gt1_rxusrclk2_out(gt1_rxusrclk2_i),
     
        .gt2_txusrclk_out(gt2_txusrclk_i),
        .gt2_txusrclk2_out(gt2_txusrclk2_i),
        .gt2_rxusrclk_out(gt2_rxusrclk_i),
        .gt2_rxusrclk2_out(gt2_rxusrclk2_i),
     
        .gt3_txusrclk_out(gt3_txusrclk_i),
        .gt3_txusrclk2_out(gt3_txusrclk2_i),
        .gt3_rxusrclk_out(gt3_rxusrclk_i),
        .gt3_rxusrclk2_out(gt3_rxusrclk2_i),


        //_____________________________________________________________________
        //_____________________________________________________________________
        //GT0  (X1Y12)

        //------------------------------- CPLL Ports -------------------------------
        .gt0_cpllfbclklost_out          (gt0_cpllfbclklost_i),
        .gt0_cplllock_out               (gt0_cplllock_i),
        .gt0_cpllreset_in               (tied_to_ground_i),
        //-------------------------- Channel - DRP Ports  --------------------------
        .gt0_drpaddr_in                 (gt0_drpaddr_i),
        .gt0_drpdi_in                   (gt0_drpdi_i),
        .gt0_drpdo_out                  (gt0_drpdo_i),
        .gt0_drpen_in                   (gt0_drpen_i),
        .gt0_drprdy_out                 (gt0_drprdy_i),
        .gt0_drpwe_in                   (gt0_drpwe_i),
        //------------------------- Digital Monitor Ports --------------------------
        .gt0_dmonitorout_out            (gt0_dmonitorout_i),
        //------------------- RX Initialization and Reset Ports --------------------
        .gt0_eyescanreset_in            (tied_to_ground_i),
        .gt0_rxuserrdy_in               (tied_to_ground_i),
        //------------------------ RX Margin Analysis Ports ------------------------
        .gt0_eyescandataerror_out       (gt0_eyescandataerror_i),
        .gt0_eyescantrigger_in          (tied_to_ground_i),
        //---------------- Receive Ports - FPGA RX interface Ports -----------------
        .gt0_rxdata_out                 (gt0_rxdata_i),
        //------------------------- Receive Ports - RX AFE -------------------------
        .gt0_gtxrxp_in                  (RXP_IN[0]),
        //---------------------- Receive Ports - RX AFE Ports ----------------------
        .gt0_gtxrxn_in                  (RXN_IN[0]),
        //----------------- Receive Ports - RX Buffer Bypass Ports -----------------
        .gt0_rxphmonitor_out            (gt0_rxphmonitor_i),
        .gt0_rxphslipmonitor_out        (gt0_rxphslipmonitor_i),
        //------------------- Receive Ports - RX Equalizer Ports -------------------
        .gt0_rxdfelpmreset_in           (tied_to_ground_i),
        .gt0_rxmonitorout_out           (gt0_rxmonitorout_i),
        .gt0_rxmonitorsel_in            (2'b00),
        //------------- Receive Ports - RX Fabric Output Control Ports -------------
        .gt0_rxoutclkfabric_out         (gt0_rxoutclkfabric_i),
        //----------- Receive Ports - RX Initialization and Reset Ports ------------
        .gt0_gtrxreset_in               (tied_to_ground_i),
        .gt0_rxpmareset_in              (gt0_rxpmareset_i),
        //------------ Receive Ports -RX Initialization and Reset Ports ------------
        .gt0_rxresetdone_out            (gt0_rxresetdone_i),
        //------------------- TX Initialization and Reset Ports --------------------
        .gt0_gttxreset_in               (tied_to_ground_i),
        .gt0_txuserrdy_in               (tied_to_ground_i),
        //---------------- Transmit Ports - TX Data Path interface -----------------
        .gt0_txdata_in                  (gt0_txdata_i),
        //-------------- Transmit Ports - TX Driver and OOB signaling --------------
        .gt0_gtxtxn_out                 (TXN_OUT[0]),
        .gt0_gtxtxp_out                 (TXP_OUT[0]),
        //--------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        .gt0_txoutclkfabric_out         (gt0_txoutclkfabric_i),
        .gt0_txoutclkpcs_out            (gt0_txoutclkpcs_i),
        //----------- Transmit Ports - TX Initialization and Reset Ports -----------
        .gt0_txresetdone_out            (gt0_txresetdone_i),



        //_____________________________________________________________________
        //_____________________________________________________________________
        //GT1  (X1Y13)

        //------------------------------- CPLL Ports -------------------------------
        .gt1_cpllfbclklost_out          (gt1_cpllfbclklost_i),
        .gt1_cplllock_out               (gt1_cplllock_i),
        .gt1_cpllreset_in               (tied_to_ground_i),
        //-------------------------- Channel - DRP Ports  --------------------------
        .gt1_drpaddr_in                 (gt1_drpaddr_i),
        .gt1_drpdi_in                   (gt1_drpdi_i),
        .gt1_drpdo_out                  (gt1_drpdo_i),
        .gt1_drpen_in                   (gt1_drpen_i),
        .gt1_drprdy_out                 (gt1_drprdy_i),
        .gt1_drpwe_in                   (gt1_drpwe_i),
        //------------------------- Digital Monitor Ports --------------------------
        .gt1_dmonitorout_out            (gt1_dmonitorout_i),
        //------------------- RX Initialization and Reset Ports --------------------
        .gt1_eyescanreset_in            (tied_to_ground_i),
        .gt1_rxuserrdy_in               (tied_to_ground_i),
        //------------------------ RX Margin Analysis Ports ------------------------
        .gt1_eyescandataerror_out       (gt1_eyescandataerror_i),
        .gt1_eyescantrigger_in          (tied_to_ground_i),
        //---------------- Receive Ports - FPGA RX interface Ports -----------------
        .gt1_rxdata_out                 (gt1_rxdata_i),
        //------------------------- Receive Ports - RX AFE -------------------------
        .gt1_gtxrxp_in                  (RXP_IN[1]),
        //---------------------- Receive Ports - RX AFE Ports ----------------------
        .gt1_gtxrxn_in                  (RXN_IN[1]),
        //----------------- Receive Ports - RX Buffer Bypass Ports -----------------
        .gt1_rxphmonitor_out            (gt1_rxphmonitor_i),
        .gt1_rxphslipmonitor_out        (gt1_rxphslipmonitor_i),
        //------------------- Receive Ports - RX Equalizer Ports -------------------
        .gt1_rxdfelpmreset_in           (tied_to_ground_i),
        .gt1_rxmonitorout_out           (gt1_rxmonitorout_i),
        .gt1_rxmonitorsel_in            (2'b00),
        //------------- Receive Ports - RX Fabric Output Control Ports -------------
        .gt1_rxoutclkfabric_out         (gt1_rxoutclkfabric_i),
        //----------- Receive Ports - RX Initialization and Reset Ports ------------
        .gt1_gtrxreset_in               (tied_to_ground_i),
        .gt1_rxpmareset_in              (gt1_rxpmareset_i),
        //------------ Receive Ports -RX Initialization and Reset Ports ------------
        .gt1_rxresetdone_out            (gt1_rxresetdone_i),
        //------------------- TX Initialization and Reset Ports --------------------
        .gt1_gttxreset_in               (tied_to_ground_i),
        .gt1_txuserrdy_in               (tied_to_ground_i),
        //---------------- Transmit Ports - TX Data Path interface -----------------
        .gt1_txdata_in                  (gt1_txdata_i),
        //-------------- Transmit Ports - TX Driver and OOB signaling --------------
        .gt1_gtxtxn_out                 (TXN_OUT[1]),
        .gt1_gtxtxp_out                 (TXP_OUT[1]),
        //--------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        .gt1_txoutclkfabric_out         (gt1_txoutclkfabric_i),
        .gt1_txoutclkpcs_out            (gt1_txoutclkpcs_i),
        //----------- Transmit Ports - TX Initialization and Reset Ports -----------
        .gt1_txresetdone_out            (gt1_txresetdone_i),



        //_____________________________________________________________________
        //_____________________________________________________________________
        //GT2  (X1Y14)

        //------------------------------- CPLL Ports -------------------------------
        .gt2_cpllfbclklost_out          (gt2_cpllfbclklost_i),
        .gt2_cplllock_out               (gt2_cplllock_i),
        .gt2_cpllreset_in               (tied_to_ground_i),
        //-------------------------- Channel - DRP Ports  --------------------------
        .gt2_drpaddr_in                 (gt2_drpaddr_i),
        .gt2_drpdi_in                   (gt2_drpdi_i),
        .gt2_drpdo_out                  (gt2_drpdo_i),
        .gt2_drpen_in                   (gt2_drpen_i),
        .gt2_drprdy_out                 (gt2_drprdy_i),
        .gt2_drpwe_in                   (gt2_drpwe_i),
        //------------------------- Digital Monitor Ports --------------------------
        .gt2_dmonitorout_out            (gt2_dmonitorout_i),
        //------------------- RX Initialization and Reset Ports --------------------
        .gt2_eyescanreset_in            (tied_to_ground_i),
        .gt2_rxuserrdy_in               (tied_to_ground_i),
        //------------------------ RX Margin Analysis Ports ------------------------
        .gt2_eyescandataerror_out       (gt2_eyescandataerror_i),
        .gt2_eyescantrigger_in          (tied_to_ground_i),
        //---------------- Receive Ports - FPGA RX interface Ports -----------------
        .gt2_rxdata_out                 (gt2_rxdata_i),
        //------------------------- Receive Ports - RX AFE -------------------------
        .gt2_gtxrxp_in                  (RXP_IN[2]),
        //---------------------- Receive Ports - RX AFE Ports ----------------------
        .gt2_gtxrxn_in                  (RXN_IN[2]),
        //----------------- Receive Ports - RX Buffer Bypass Ports -----------------
        .gt2_rxphmonitor_out            (gt2_rxphmonitor_i),
        .gt2_rxphslipmonitor_out        (gt2_rxphslipmonitor_i),
        //------------------- Receive Ports - RX Equalizer Ports -------------------
        .gt2_rxdfelpmreset_in           (tied_to_ground_i),
        .gt2_rxmonitorout_out           (gt2_rxmonitorout_i),
        .gt2_rxmonitorsel_in            (2'b00),
        //------------- Receive Ports - RX Fabric Output Control Ports -------------
        .gt2_rxoutclkfabric_out         (gt2_rxoutclkfabric_i),
        //----------- Receive Ports - RX Initialization and Reset Ports ------------
        .gt2_gtrxreset_in               (tied_to_ground_i),
        .gt2_rxpmareset_in              (gt2_rxpmareset_i),
        //------------ Receive Ports -RX Initialization and Reset Ports ------------
        .gt2_rxresetdone_out            (gt2_rxresetdone_i),
        //------------------- TX Initialization and Reset Ports --------------------
        .gt2_gttxreset_in               (tied_to_ground_i),
        .gt2_txuserrdy_in               (tied_to_ground_i),
        //---------------- Transmit Ports - TX Data Path interface -----------------
        .gt2_txdata_in                  (gt2_txdata_i),
        //-------------- Transmit Ports - TX Driver and OOB signaling --------------
        .gt2_gtxtxn_out                 (TXN_OUT[2]),
        .gt2_gtxtxp_out                 (TXP_OUT[2]),
        //--------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        .gt2_txoutclkfabric_out         (gt2_txoutclkfabric_i),
        .gt2_txoutclkpcs_out            (gt2_txoutclkpcs_i),
        //----------- Transmit Ports - TX Initialization and Reset Ports -----------
        .gt2_txresetdone_out            (gt2_txresetdone_i),



        //_____________________________________________________________________
        //_____________________________________________________________________
        //GT3  (X1Y15)

        //------------------------------- CPLL Ports -------------------------------
        .gt3_cpllfbclklost_out          (gt3_cpllfbclklost_i),
        .gt3_cplllock_out               (gt3_cplllock_i),
        .gt3_cpllreset_in               (tied_to_ground_i),
        //-------------------------- Channel - DRP Ports  --------------------------
        .gt3_drpaddr_in                 (gt3_drpaddr_i),
        .gt3_drpdi_in                   (gt3_drpdi_i),
        .gt3_drpdo_out                  (gt3_drpdo_i),
        .gt3_drpen_in                   (gt3_drpen_i),
        .gt3_drprdy_out                 (gt3_drprdy_i),
        .gt3_drpwe_in                   (gt3_drpwe_i),
        //------------------------- Digital Monitor Ports --------------------------
        .gt3_dmonitorout_out            (gt3_dmonitorout_i),
        //------------------- RX Initialization and Reset Ports --------------------
        .gt3_eyescanreset_in            (tied_to_ground_i),
        .gt3_rxuserrdy_in               (tied_to_ground_i),
        //------------------------ RX Margin Analysis Ports ------------------------
        .gt3_eyescandataerror_out       (gt3_eyescandataerror_i),
        .gt3_eyescantrigger_in          (tied_to_ground_i),
        //---------------- Receive Ports - FPGA RX interface Ports -----------------
        .gt3_rxdata_out                 (gt3_rxdata_i),
        //------------------------- Receive Ports - RX AFE -------------------------
        .gt3_gtxrxp_in                  (RXP_IN[3]),
        //---------------------- Receive Ports - RX AFE Ports ----------------------
        .gt3_gtxrxn_in                  (RXN_IN[3]),
        //----------------- Receive Ports - RX Buffer Bypass Ports -----------------
        .gt3_rxphmonitor_out            (gt3_rxphmonitor_i),
        .gt3_rxphslipmonitor_out        (gt3_rxphslipmonitor_i),
        //------------------- Receive Ports - RX Equalizer Ports -------------------
        .gt3_rxdfelpmreset_in           (tied_to_ground_i),
        .gt3_rxmonitorout_out           (gt3_rxmonitorout_i),
        .gt3_rxmonitorsel_in            (2'b00),
        //------------- Receive Ports - RX Fabric Output Control Ports -------------
        .gt3_rxoutclkfabric_out         (gt3_rxoutclkfabric_i),
        //----------- Receive Ports - RX Initialization and Reset Ports ------------
        .gt3_gtrxreset_in               (tied_to_ground_i),
        .gt3_rxpmareset_in              (gt3_rxpmareset_i),
        //------------ Receive Ports -RX Initialization and Reset Ports ------------
        .gt3_rxresetdone_out            (gt3_rxresetdone_i),
        //------------------- TX Initialization and Reset Ports --------------------
        .gt3_gttxreset_in               (tied_to_ground_i),
        .gt3_txuserrdy_in               (tied_to_ground_i),
        //---------------- Transmit Ports - TX Data Path interface -----------------
        .gt3_txdata_in                  (gt3_txdata_i),
        //-------------- Transmit Ports - TX Driver and OOB signaling --------------
        .gt3_gtxtxn_out                 (TXN_OUT[3]),
        .gt3_gtxtxp_out                 (TXP_OUT[3]),
        //--------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        .gt3_txoutclkfabric_out         (gt3_txoutclkfabric_i),
        .gt3_txoutclkpcs_out            (gt3_txoutclkpcs_i),
        //----------- Transmit Ports - TX Initialization and Reset Ports -----------
        .gt3_txresetdone_out            (gt3_txresetdone_i),


    //____________________________COMMON PORTS________________________________
    .gt0_qplloutclk_out(),
    .gt0_qplloutrefclk_out(),
    .sysclk_in(DRPCLK_IN)
    );

 
 

    //***********************************************************************//
    //                                                                       //
    //--------------------------- User Module Resets-------------------------//
    //                                                                       //
    //***********************************************************************//
    // All the User Modules i.e. FRAME_GEN, FRAME_CHECK and the sync modules
    // are held in reset till the RESETDONE goes high. 
    // The RESETDONE is registered a couple of times on *USRCLK2 and connected 
    // to the reset of the modules
        
    always @(posedge gt0_rxusrclk2_i or negedge gt0_rxresetdone_i)
    begin
        if (!gt0_rxresetdone_i)
        begin
            gt0_rxresetdone_r    <=   `DLY 1'b0;
            gt0_rxresetdone_r2   <=   `DLY 1'b0;
            gt0_rxresetdone_r3   <=   `DLY 1'b0;
        end
        else
        begin
            gt0_rxresetdone_r    <=   `DLY gt0_rxresetdone_i;
            gt0_rxresetdone_r2   <=   `DLY gt0_rxresetdone_r;
            gt0_rxresetdone_r3   <=   `DLY gt0_rxresetdone_r2;
        end
    end
    
    always @(posedge gt1_rxusrclk2_i or negedge gt1_rxresetdone_i)
    begin
        if (!gt1_rxresetdone_i)
        begin
            gt1_rxresetdone_r    <=   `DLY 1'b0;
            gt1_rxresetdone_r2   <=   `DLY 1'b0;
            gt1_rxresetdone_r3   <=   `DLY 1'b0;
        end
        else
        begin
            gt1_rxresetdone_r    <=   `DLY gt0_rxresetdone_i;
            gt1_rxresetdone_r2   <=   `DLY gt0_rxresetdone_r;
            gt1_rxresetdone_r3   <=   `DLY gt0_rxresetdone_r2;
        end
    end
    
    always @(posedge gt2_rxusrclk2_i or negedge gt2_rxresetdone_i)
    begin
        if (!gt2_rxresetdone_i)
        begin
            gt2_rxresetdone_r    <=   `DLY 1'b0;
            gt2_rxresetdone_r2   <=   `DLY 1'b0;
            gt2_rxresetdone_r3   <=   `DLY 1'b0;
        end
        else
        begin
            gt2_rxresetdone_r    <=   `DLY gt0_rxresetdone_i;
            gt2_rxresetdone_r2   <=   `DLY gt0_rxresetdone_r;
            gt2_rxresetdone_r3   <=   `DLY gt0_rxresetdone_r2;
        end
    end
    
    always @(posedge gt3_rxusrclk2_i or negedge gt3_rxresetdone_i)
    begin
        if (!gt3_rxresetdone_i)
        begin
            gt3_rxresetdone_r    <=   `DLY 1'b0;
            gt3_rxresetdone_r2   <=   `DLY 1'b0;
            gt3_rxresetdone_r3   <=   `DLY 1'b0;
        end
        else
        begin
            gt3_rxresetdone_r    <=   `DLY gt0_rxresetdone_i;
            gt3_rxresetdone_r2   <=   `DLY gt0_rxresetdone_r;
            gt3_rxresetdone_r3   <=   `DLY gt0_rxresetdone_r2;
        end
    end
        
        
    always @(posedge  gt0_txusrclk2_i or negedge gt0_txfsmresetdone_i)
    begin
        if (!gt0_txfsmresetdone_i)
        begin
            gt0_txfsmresetdone_r    <=   `DLY 1'b0;
            gt0_txfsmresetdone_r2   <=   `DLY 1'b0;
        end
        else
        begin
            gt0_txfsmresetdone_r    <=   `DLY gt0_txfsmresetdone_i;
            gt0_txfsmresetdone_r2   <=   `DLY gt0_txfsmresetdone_r;
        end
    end
    
    always @(posedge  gt1_txusrclk2_i or negedge gt1_txfsmresetdone_i)
    begin
        if (!gt1_txfsmresetdone_i)
        begin
            gt1_txfsmresetdone_r    <=   `DLY 1'b0;
            gt1_txfsmresetdone_r2   <=   `DLY 1'b0;
        end
        else
        begin
            gt1_txfsmresetdone_r    <=   `DLY gt0_txfsmresetdone_i;
            gt1_txfsmresetdone_r2   <=   `DLY gt0_txfsmresetdone_r;
        end
    end
    
    always @(posedge  gt2_txusrclk2_i or negedge gt2_txfsmresetdone_i)
    begin
        if (!gt2_txfsmresetdone_i)
        begin
            gt2_txfsmresetdone_r    <=   `DLY 1'b0;
            gt2_txfsmresetdone_r2   <=   `DLY 1'b0;
        end
        else
        begin
            gt2_txfsmresetdone_r    <=   `DLY gt0_txfsmresetdone_i;
            gt2_txfsmresetdone_r2   <=   `DLY gt0_txfsmresetdone_r;
        end
    end
    
    always @(posedge  gt3_txusrclk2_i or negedge gt3_txfsmresetdone_i)
    begin
        if (!gt3_txfsmresetdone_i)
        begin
            gt3_txfsmresetdone_r    <=   `DLY 1'b0;
            gt3_txfsmresetdone_r2   <=   `DLY 1'b0;
        end
        else
        begin
            gt3_txfsmresetdone_r    <=   `DLY gt0_txfsmresetdone_i;
            gt3_txfsmresetdone_r2   <=   `DLY gt0_txfsmresetdone_r;
        end
    end



    //***********************************************************************//
    //                                                                       //
    //------------------------  Frame Generators  ---------------------------//
    //                                                                       //
    //***********************************************************************//
    // NOT USED FOR TDSV2 TESTING

    gtcore_GT_FRAME_GEN #(.WORDS_IN_BRAM(EXAMPLE_WORDS_IN_BRAM))
    gt0_frame_gen(.TX_DATA_OUT ({gt0_txdata_float_i,gt0_txdata_i,gt0_txdata_float16_i}),.TXCTRL_OUT(),.USER_CLK (gt0_txusrclk2_i),.SYSTEM_RESET (gt0_tx_system_reset_c));
 
    gtcore_GT_FRAME_GEN #(.WORDS_IN_BRAM(EXAMPLE_WORDS_IN_BRAM))
    gt1_frame_gen(.TX_DATA_OUT ({gt1_txdata_float_i,gt1_txdata_i,gt1_txdata_float16_i}),.TXCTRL_OUT(),.USER_CLK (gt1_txusrclk2_i),.SYSTEM_RESET (gt1_tx_system_reset_c));

    gtcore_GT_FRAME_GEN #(.WORDS_IN_BRAM(EXAMPLE_WORDS_IN_BRAM))
    gt2_frame_gen(.TX_DATA_OUT ({gt2_txdata_float_i,gt2_txdata_i,gt2_txdata_float16_i}),.TXCTRL_OUT(),.USER_CLK (gt2_txusrclk2_i),.SYSTEM_RESET (gt2_tx_system_reset_c));

    gtcore_GT_FRAME_GEN #(.WORDS_IN_BRAM(EXAMPLE_WORDS_IN_BRAM))
    gt3_frame_gen(.TX_DATA_OUT ({gt3_txdata_float_i,gt3_txdata_i,gt3_txdata_float16_i}),.TXCTRL_OUT(),.USER_CLK (gt3_txusrclk2_i),.SYSTEM_RESET (gt3_tx_system_reset_c));
 

    //***********************************************************************//
    //                                                                       //
    //------------------------  Frame Checkers  -----------------------------//
    //                                                                       //
    //***********************************************************************//

    assign gt0_frame_check_reset_i = (EXAMPLE_CONFIG_INDEPENDENT_LANES==0)?reset_on_data_error_i:gt0_matchn_i;
    assign gt1_frame_check_reset_i = (EXAMPLE_CONFIG_INDEPENDENT_LANES==0)?reset_on_data_error_i:gt1_matchn_i;
    assign gt2_frame_check_reset_i = (EXAMPLE_CONFIG_INDEPENDENT_LANES==0)?reset_on_data_error_i:gt2_matchn_i;
    assign gt3_frame_check_reset_i = (EXAMPLE_CONFIG_INDEPENDENT_LANES==0)?reset_on_data_error_i:gt3_matchn_i;

    //-------------GTX RX 1st Lane---------------//
    gtcore_GT_FRAME_CHECK #
    (
        .RX_DATA_WIDTH ( 20 ),
        .RXCTRL_WIDTH ( 2 ),
        .WORDS_IN_BRAM(EXAMPLE_WORDS_IN_BRAM),
        .START_OF_PACKET_CHAR ( 20'h0027c )
    )
    gt0_frame_check(
         // GT Interface
        .RX_DATA_IN           (gt0_rxdata_i),
	
        // System Interface
        .USER_CLK             (gt0_rxusrclk2_i),
        .SYSTEM_RESET         (gt0_rx_system_reset_c),
        .MODE_SEL             (),
        .GT_DATA_OUT          (gt0_data_out),
        .GT_DATA_VALID_o      (gt0_data_valid_o),
        .GT_CLK160            (gt0_clk160)
    );

    //-------------GTX RX 2nd Lane---------------//
    gtcore_GT_FRAME_CHECK #
    (
        .RX_DATA_WIDTH ( 20 ),
        .RXCTRL_WIDTH ( 2 ),
        .WORDS_IN_BRAM(EXAMPLE_WORDS_IN_BRAM),
        .START_OF_PACKET_CHAR ( 20'h0027c )
    )
    gt1_frame_check(
         // GT Interface
        .RX_DATA_IN           (gt1_rxdata_i),
    
        // System Interface
        .USER_CLK             (gt1_rxusrclk2_i),
        .SYSTEM_RESET         (gt1_rx_system_reset_c),
        .MODE_SEL             (),
        .GT_DATA_OUT          (gt1_data_out),
        .GT_DATA_VALID_o      (gt1_data_valid_o),
        .GT_CLK160            ()
    );


    //-------------GTX RX 3rd Lane---------------//
    gtcore_GT_FRAME_CHECK #
    (
        .RX_DATA_WIDTH ( 20 ),
        .RXCTRL_WIDTH ( 2 ),
        .WORDS_IN_BRAM(EXAMPLE_WORDS_IN_BRAM),
        .START_OF_PACKET_CHAR ( 20'h0027c )
    )
    gt2_frame_check(
         // GT Interface
        .RX_DATA_IN           (gt2_rxdata_i),
    
        // System Interface
        .USER_CLK             (gt2_rxusrclk2_i),
        .SYSTEM_RESET         (gt2_rx_system_reset_c),
        .MODE_SEL             (),
        .GT_DATA_OUT          (gt2_data_out),
        .GT_DATA_VALID_o      (gt2_data_valid_o),
        .GT_CLK160            ()        
    );


    //-------------GTX RX 4th Lane---------------//
    gtcore_GT_FRAME_CHECK #
    (
        .RX_DATA_WIDTH ( 20 ),
        .RXCTRL_WIDTH ( 2 ),
        .WORDS_IN_BRAM(EXAMPLE_WORDS_IN_BRAM),
        .START_OF_PACKET_CHAR ( 20'h0027c )
    )
    gt3_frame_check(
         // GT Interface
        .RX_DATA_IN           (gt3_rxdata_i),
    
        // System Interface
        .USER_CLK             (gt3_rxusrclk2_i),
        .SYSTEM_RESET         (gt3_rx_system_reset_c),
        .MODE_SEL             (),
        .GT_DATA_OUT          (gt3_data_out),
        .GT_DATA_VALID_o      (gt3_data_valid_o),
        .GT_CLK160            ()
    );



//-------------------------Debug Signals assignment--------------------

    //------------ optional Ports assignments --------------
    //------GTH/GTP
    assign  gt0_rxpmareset_i                     =  tied_to_ground_i;
    assign  gt1_rxpmareset_i                     =  tied_to_ground_i;
    assign  gt2_rxpmareset_i                     =  tied_to_ground_i;
    assign  gt3_rxpmareset_i                     =  tied_to_ground_i;
    //------------------------------------------------------
    // assign resets for frame_gen modules
    assign  gt0_tx_system_reset_c = !gt0_txfsmresetdone_r2;
    assign  gt1_tx_system_reset_c = !gt1_txfsmresetdone_r2;
    assign  gt2_tx_system_reset_c = !gt2_txfsmresetdone_r2;
    assign  gt3_tx_system_reset_c = !gt3_txfsmresetdone_r2;

    // assign resets for frame_check modules
    assign  gt0_rx_system_reset_c = !gt0_rxresetdone_r3;
    assign  gt1_rx_system_reset_c = !gt1_rxresetdone_r3;
    assign  gt2_rx_system_reset_c = !gt2_rxresetdone_r3;
    assign  gt3_rx_system_reset_c = !gt3_rxresetdone_r3;

    assign gt0_drpaddr_i = 9'd0;
    assign gt0_drpdi_i = 16'd0;
    assign gt0_drpen_i = 1'b0;
    assign gt0_drpwe_i = 1'b0;
    assign gt1_drpaddr_i = 9'd0;
    assign gt1_drpdi_i = 16'd0;
    assign gt1_drpen_i = 1'b0;
    assign gt1_drpwe_i = 1'b0;
    assign gt2_drpaddr_i = 9'd0;
    assign gt2_drpdi_i = 16'd0;
    assign gt2_drpen_i = 1'b0;
    assign gt2_drpwe_i = 1'b0;
    assign gt3_drpaddr_i = 9'd0;
    assign gt3_drpdi_i = 16'd0;
    assign gt3_drpen_i = 1'b0;
    assign gt3_drpwe_i = 1'b0;
    assign soft_reset_i = tied_to_ground_i;
    
    //** DO NOT MODIFY**  PREVENT AUTOMATIC RX FMS RESET (otherwise will reset rxusrclk!) 
    assign gt0_track_data_i = 1'b1;
    assign gt1_track_data_i = 1'b1;
    assign gt2_track_data_i = 1'b1;
    assign gt3_track_data_i = 1'b1;
    
    
    

endmodule