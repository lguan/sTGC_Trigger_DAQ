`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Original Author: Jinhong Wang 
// 
// Create Date: 03/31/2017 06:37:35 PM
// Design Name: 
// Module Name: strip_mode
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// TDS data format (without header)
//  [103:91] BANDID+PHIID
//  [90:85] Lower 6 bits of BCID
//  [84] HIGH/LOW BAND SELECTED
//  [83:0] 6-bit Charge x14 
//////////////////////////////////////////////////////////////////////////////////


module strip_mode(
   input USER_CLK,
   input clk160,
   input plllocked,
   input SYSTEM_RESET,
   input [19:0] RX_DATA_IN,
   output [29:0]tds_data_packet,  //4'b packet header + 26'b descrambled data
   output tds_link_sync_done //tds data link synchronization done
    );


 //***************************Internal Register Declarations******************** 
    
 (* ASYNC_REG = "TRUE" *) (* keep = "true" *)reg             system_reset_r;
 (* ASYNC_REG = "TRUE" *) (* keep = "true" *)reg             system_reset_r2;
        
////----------------------------------------------------------------------------------------------
(* keep = "TRUE" *)   reg     [19:0] rx_data_r1,rx_data_r1_r;
(* keep = "TRUE" *)   reg     [19:0] rx_data_r2,rx_data_r2_r;
(* keep = "TRUE" *)   reg     [19:0] rx_data_r3,rx_data_r3_r;
(* keep = "TRUE" *)   reg     [19:0] rx_data_r4,rx_data_r4_r;
(* keep = "TRUE" *)   reg     [19:0] rx_data_r5,rx_data_r5_r;

(* keep = "TRUE" *)  reg [1:0] align_cnt, align_cnt_s,align_cnt_s_r;
(* keep = "TRUE" *)  reg [1:0] edge_decode;
(* keep = "TRUE" *)  reg align_flag;
(* keep = "TRUE" *)  reg packet_trace;
 
reg tds_link_sync_done_r;  // in clk240(USER_CLK) domain 
reg en_r;   
reg [3 :0] state;
reg [4:0] err_cnt;
reg [15:0] syn_cnt;
reg [4:0] pos;
reg [29:0] tdsdata_r;
reg [3:0] header,header_r;
     
(* keep = "TRUE" *) wire [59:0] tds2xdata;
(* keep = "TRUE" *) wire [25:0] tdsdata_dscr;

parameter idle =4'b0, st0=4'b1,st1=4'b10,st2=4'b11,st3=4'b100,st4=4'b101,st5=4'b110,st6=4'b111,st7=4'b1000,st8=4'b1001,st_rxrst=4'b1010;
(* keep = "TRUE" *) reg [103 :0] frmdata;


     
  //___________ synchronizing the async reset for ease of timing simulation ________
  always@(posedge USER_CLK)
    begin
      system_reset_r <=  SYSTEM_RESET;    
      system_reset_r2 <=  system_reset_r; 
  end   
 
  function [59:0] data_trace(input [19:0] rx_data_r1,rx_data_r2,rx_data_r3,rx_data_r4,rx_data_r5,
                                     input [4:0] pos);
          begin
            if(pos==5'b0)
                 data_trace ={rx_data_r5,rx_data_r4,rx_data_r3};
            else if(pos==5'b1)
                 data_trace ={rx_data_r5[18:0],rx_data_r4,rx_data_r3,rx_data_r2[19]};
            else if(pos==5'b10)
                 data_trace ={rx_data_r5[17:0],rx_data_r4,rx_data_r3,rx_data_r2[19:18]};
            else if(pos==5'b11)
                 data_trace ={rx_data_r5[16:0],rx_data_r4,rx_data_r3,rx_data_r2[19:17]};
            else if(pos==5'b100)
                 data_trace ={rx_data_r5[15:0],rx_data_r4,rx_data_r3,rx_data_r2[19:16]};
            else if(pos==5'b101)
                 data_trace ={rx_data_r5[14:0],rx_data_r4,rx_data_r3,rx_data_r2[19:15]};
            else if(pos==5'b110)
                 data_trace ={rx_data_r5[13:0],rx_data_r4,rx_data_r3,rx_data_r2[19:14]};
            else if(pos==5'b111)
                 data_trace ={rx_data_r5[12:0],rx_data_r4,rx_data_r3,rx_data_r2[19:13]};
            else if(pos==5'b1000)
                 data_trace ={rx_data_r5[11:0],rx_data_r4,rx_data_r3,rx_data_r2[19:12]};
            else if(pos==5'b1001)
                 data_trace ={rx_data_r5[10:0],rx_data_r4,rx_data_r3,rx_data_r2[19:11]};
            else if(pos==5'b1010)
                 data_trace ={rx_data_r5[9:0],rx_data_r4,rx_data_r3,rx_data_r2[19:10]};
            else if(pos==5'b1011)
                 data_trace ={rx_data_r5[8:0],rx_data_r4,rx_data_r3,rx_data_r2[19:9]};
            else if(pos==5'b1100)
                 data_trace ={rx_data_r5[7:0],rx_data_r4,rx_data_r3,rx_data_r2[19:8]};
            else if(pos==5'b1101)
                 data_trace ={rx_data_r5[6:0],rx_data_r4,rx_data_r3,rx_data_r2[19:7]};
            else if(pos==5'b1110)
                 data_trace ={rx_data_r5[5:0],rx_data_r4,rx_data_r3,rx_data_r2[19:6]};
            else if(pos==5'b1111)
                 data_trace ={rx_data_r5[4:0],rx_data_r4,rx_data_r3,rx_data_r2[19:5]};
            else if(pos==5'b10000)
                 data_trace ={rx_data_r5[3:0],rx_data_r4,rx_data_r3,rx_data_r2[19:4]};
            else if(pos==5'b10001)
                 data_trace ={rx_data_r5[2:0],rx_data_r4,rx_data_r3,rx_data_r2[19:3]};
            else if(pos==5'b10010)
                 data_trace ={rx_data_r5[1:0],rx_data_r4,rx_data_r3,rx_data_r2[19:2]};
            else if(pos==5'b10011)
                 data_trace ={rx_data_r5[0],rx_data_r4,rx_data_r3,rx_data_r2[19:1]};
            else if(pos==5'b10100)
                 data_trace ={rx_data_r4,rx_data_r3,rx_data_r2};
            else if(pos==5'b10101)
                 data_trace ={rx_data_r4[18:0],rx_data_r3,rx_data_r2,rx_data_r1[19]};
            else if(pos==5'b10110)
                 data_trace ={rx_data_r4[17:0],rx_data_r3,rx_data_r2,rx_data_r1[19:18]};
            else if(pos==5'b10111)
                 data_trace ={rx_data_r4[16:0],rx_data_r3,rx_data_r2,rx_data_r1[19:17]};
            else if(pos==5'b11000)
                 data_trace ={rx_data_r4[15:0],rx_data_r3,rx_data_r2,rx_data_r1[19:16]};
            else if(pos==5'b11001)
                 data_trace ={rx_data_r4[14:0],rx_data_r3,rx_data_r2,rx_data_r1[19:15]};
            else if(pos==5'b11010)
                 data_trace ={rx_data_r4[13:0],rx_data_r3,rx_data_r2,rx_data_r1[19:14]};
            else if(pos==5'b11011)
                 data_trace ={rx_data_r4[12:0],rx_data_r3,rx_data_r2,rx_data_r1[19:13]};
            else if(pos==5'b11100)
                 data_trace ={rx_data_r4[11:0],rx_data_r3,rx_data_r2,rx_data_r1[19:12]};
            else if(pos==5'b11101)
                 data_trace ={rx_data_r4[10:0],rx_data_r3,rx_data_r2,rx_data_r1[19:11]};
            else 
                data_trace = 'b0;
           end
  endfunction
         
    always @(negedge clk160 )begin
         if(system_reset_r ==1'b1) en_r <= 1'b0;
         else  en_r <= 1'b1; end
           
    always @(posedge USER_CLK) begin   
      if(system_reset_r == 1'b1)
          align_cnt <= 2'b0;
      else begin
          if(align_cnt == 2'b10) align_cnt <= 2'b0;
          else if(en_r == 1'b1)  align_cnt <= align_cnt + 1'b1;
          else align_cnt <= 2'b0; 
      end
     end 
                 
      always @(negedge clk160) begin
          if(system_reset_r == 1'b1) begin
             align_cnt_s <= 2'b0;
             align_cnt_s_r <= 2'b0; end
          else begin
             align_cnt_s <= align_cnt;
             align_cnt_s_r <= align_cnt_s; end
      end
                 
      always @(*)
        begin
          if((align_cnt_s == 2'b0 && align_cnt_s_r ==2'b10) ||(align_cnt_s==2'b10 && align_cnt_s_r ==2'b0))
             edge_decode <= 2'b10;
          else if((align_cnt_s == 2'b0 && align_cnt_s_r ==2'b01) ||(align_cnt_s==2'b01 && align_cnt_s_r ==2'b0))
             edge_decode <= 2'b01;
          else
             edge_decode <= 2'b0;
       end
                  
      always @(*) begin
         if((align_cnt==2'b10 && edge_decode==2'b10) ||(align_cnt==2'b00 && edge_decode==2'b01))
              align_flag <= 1'b1;
         else
              align_flag <= 1'b0;
      end
                 
      //buffer input data 
      always @(posedge USER_CLK )
        begin
         if(system_reset_r== 1'b1) begin
           rx_data_r1 <= 'b0;
           rx_data_r2 <= 'b0;
           rx_data_r3 <= 'b0;
           rx_data_r4 <= 'b0;
           rx_data_r5 <= 'b0; end
         else begin
           rx_data_r1 <= RX_DATA_IN; 
           rx_data_r2 <= rx_data_r1;
           rx_data_r3 <= rx_data_r2;
           rx_data_r4 <= rx_data_r3;
           rx_data_r5 <= rx_data_r4; end
      end
                  
      always@(posedge USER_CLK)begin //align_flag should appear at every third tick of 240 MHz clock 
        if(align_flag == 1'b1) begin
           rx_data_r1_r <= RX_DATA_IN;
           rx_data_r2_r <= rx_data_r1;
           rx_data_r3_r <= rx_data_r2;
           rx_data_r4_r <= rx_data_r3;
           rx_data_r5_r <= rx_data_r4;end
      end
                  
              
    assign tds2xdata = data_trace(rx_data_r1_r,rx_data_r2_r,rx_data_r3_r,rx_data_r4_r,rx_data_r5_r,pos);
    
    
  //state machine for synchronization
         always @(posedge USER_CLK)
         begin
         if(system_reset_r ==1'b1) begin
            state <= idle;
            syn_cnt <= 'b0;
            err_cnt <= 'b0;
            pos  <= 5'b0;
            packet_trace <= 1'b0;
            tds_link_sync_done_r<=1'b0;
            end
         else begin
            case(state)
            idle: begin
                  if(align_flag == 1'b1 ) begin // when pll0 is locked
                   state <= st1;
                   syn_cnt <= 'b0;
                   err_cnt <= 'b0;
                   packet_trace <= 1'b0;
                   tds_link_sync_done_r<=1'b0;
                   end
            end
            st1:  begin
                  if(align_flag == 1'b1) begin
                   state <= st2;
                   end
                  end
            st2:  begin
                  if((tds2xdata[29:26] == 4'b1010 ||tds2xdata[29:26] == 4'b1100) && (tds2xdata[59:56] == 4'b1010 ||tds2xdata[59:56] == 4'b1100) )
                   syn_cnt <= syn_cnt + 1'b1;
                  else
                   err_cnt <= err_cnt + 1'b1;
                  
                  state <= st3;
                  end
            st3:  begin
                  if(syn_cnt == 16'b1111111111111100 && err_cnt < 5'b11) begin // syn established
                            state <= st4;
                            end
                          else if (err_cnt > 5'b100) begin // to many errors established
                              pos <= pos +1'b1;
                              state <= idle;               // ------>> got some errors at the very beginning
                            end
                          else begin
                            state <= st1;//->>
                            end
            end
            st4:  begin // begin to align the phase of user clock,calculate the amount of phase shift
                  //add a reset to select pos less than 17
                        err_cnt <= 'b0;
                        state <= st6; 
            end      
            st6:  begin           
                  if(align_flag == 1'b1) begin
                    packet_trace <= 1'b1;
                    tds_link_sync_done_r<=1'b1;
                    state <= st7;  
                    end
                  end
            st7:  begin
                       if((tds2xdata[29:26] == 4'b1010 ||tds2xdata[29:26] == 4'b1100) && (tds2xdata[59:56] == 4'b1010 ||tds2xdata[59:56] == 4'b1100) )
                         state <= st8;
                       else begin
                         err_cnt <= err_cnt + 1'b1;
                         state <= st8;
                         end
                  end
            st8: begin
                 if(err_cnt <8) begin
                 state <= st6;
                 packet_trace <= 1'b0; 
                 tds_link_sync_done_r<=1'b1;
                 end
                 else
                 state <= idle;
                 end
            default:  state <= idle;
          endcase;
         end
         end
         
         
  always@(posedge clk160)
     begin
     if(packet_trace==1'b1)
        tdsdata_r <= tds2xdata[59:30];
     else
        tdsdata_r <= tds2xdata[29:0];
   end
         
         
  strip_descrambler strip_descr_inst( .datain(tdsdata_r[25:0]), .clk(clk160), .bypass(1'b0), .framein(1'b0), .rst(1'b0),
                         .dataout(tdsdata_dscr[25:0]), .frameout());
    
    
   FlagAck_CrossDomain FlagAck_CrossDomain_inst(
         .clkA(USER_CLK),
         .FlagIn_clkA(tds_link_sync_done_r),
         .Busy_clkA(),
         .clkB(clk160),
         .FlagOut_clkB(tds_link_sync_done)
    );

      
    //gt data debug and probe
    (*mark_debug = "TRUE" *) wire [29:0] tdsdata_w;
    (*mark_debug = "TRUE" *) wire [25:0] tdsdata_dscr_w;
    (*mark_debug = "TRUE" *) wire [12:0] bandphi;
    (*mark_debug = "TRUE" *) wire [5:0]bcid;
    (*mark_debug = "TRUE" *) wire high_low;
    (*mark_debug = "TRUE" *) wire [5:0]q13;
    (*mark_debug = "TRUE" *) wire [5:0]q12;
    (*mark_debug = "TRUE" *) wire [5:0]q11;
    (*mark_debug = "TRUE" *) wire [5:0]q10;
    (*mark_debug = "TRUE" *) wire [5:0]q9;
    (*mark_debug = "TRUE" *) wire [5:0]q8;
    (*mark_debug = "TRUE" *) wire [5:0]q7;
    (*mark_debug = "TRUE" *) wire [5:0]q6;
    (*mark_debug = "TRUE" *) wire [5:0]q5;
    (*mark_debug = "TRUE" *) wire [5:0]q4;
    (*mark_debug = "TRUE" *) wire [5:0]q3;
    (*mark_debug = "TRUE" *) wire [5:0]q2;
    (*mark_debug = "TRUE" *) wire [5:0]q1;
    (*mark_debug = "TRUE" *) wire [5:0]q0;           
       
    
    
    assign tdsdata_w=tdsdata_r;
    assign tdsdata_dscr_w=tdsdata_dscr[25:0];

    always @(posedge clk160)  begin
     header <= tdsdata_r[29:26];
     header_r <= header;
    end

    assign tds_data_packet={header,tdsdata_dscr[25:0]};
    assign bandphi=frmdata[103:91];
    assign bcid=frmdata[90:85];
    assign high_low=frmdata[84];
    assign q13=frmdata[83:78];
    assign q12=frmdata[77:72];
    assign q11=frmdata[71:66];
    assign q10=frmdata[65:60];
    assign q9=frmdata[59:54];
    assign q8=frmdata[53:48];
    assign q7=frmdata[47:42];
    assign q6=frmdata[41:36];
    assign q5=frmdata[35:30];
    assign q4=frmdata[29:24];
    assign q3=frmdata[23:18];
    assign q2=frmdata[17:12];
    assign q1=frmdata[11:6];
    assign q0=frmdata[5:0];           
   
    always @(posedge clk160) begin
     if(header == 4'b1010) begin
       frmdata[25:0] <= tdsdata_dscr[25:0];
       frmdata[103:26] <= frmdata[77:0];
     end
    end
    
    ila_gt_stripTDS ila_gt_stripTDS_inst(
     .clk(USER_CLK),
     //.probe0(tdsdata_w),
     .probe0(tds_data_packet),
     .probe1(tdsdata_dscr_w),   //--
     .probe2(edge_decode),      //--
     .probe3(packet_trace),     //--
     .probe4(state),            //--
     .probe5(err_cnt),//5       //--
     .probe6(tds2xdata),        //--
     .probe7(pos),              //--
     .probe8(bandphi),           //--frmdata
     .probe9(bcid),
     .probe10(high_low),
     .probe11(q13),
     .probe12(q12),
     .probe13(q11),
     .probe14(q10),
     .probe15(q9),
     .probe16(q8),
     .probe17(q7),
     .probe18(q6),
     .probe19(q5),
     .probe20(q4),
     .probe21(q3),
     .probe22(q2),
     .probe23(q1),
     .probe24(q0)
    );

endmodule



//module to synchronize flag pulses in two clock domains
module FlagAck_CrossDomain(
    input clkA,
    input FlagIn_clkA,
    output Busy_clkA,
    input clkB,
    output FlagOut_clkB
);

reg FlagToggle_clkA;
always @(posedge clkA) FlagToggle_clkA <= FlagToggle_clkA ^ (FlagIn_clkA & ~Busy_clkA);

reg [2:0] SyncA_clkB;
always @(posedge clkB) SyncA_clkB <= {SyncA_clkB[1:0], FlagToggle_clkA};

reg [1:0] SyncB_clkA;
always @(posedge clkA) SyncB_clkA <= {SyncB_clkA[0], SyncA_clkB[2]};

assign FlagOut_clkB = (SyncA_clkB[2] ^ SyncA_clkB[1]);
assign Busy_clkA = FlagToggle_clkA ^ SyncB_clkA[1];
endmodule
