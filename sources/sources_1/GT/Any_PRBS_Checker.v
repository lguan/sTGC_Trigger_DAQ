`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/12/2016 09:24:50 AM
// Design Name: 
// Module Name: Any_PRBS_Checker
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Any_PRBS_Checker (RST, CLK, DATA_IN, EN, DATA_OUT, ERR_CNT,RST_ERR_CNT);

  //--------------------------------------------		
  // Configuration parameters
  //--------------------------------------------		
   parameter CHK_MODE = 1; //CHECK MODE
   parameter INV_PATTERN = 1; // INVert it!!
   parameter POLY_LENGHT = 31; // PRBS 31
   parameter POLY_TAP = 28; // prbs 31
   parameter NBITS = 20; // bus width 20 

  //--------------------------------------------		
  // Input/Outputs
  //--------------------------------------------		

   input RST;
   input CLK;
   input [NBITS - 1:0] DATA_IN;
   input EN;
   output [NBITS - 1:0] DATA_OUT;
   input RST_ERR_CNT; // modified_4_TDS
   output [15:0] ERR_CNT; // modified_4_TDS

   reg [NBITS - 1:0] DATA_OUT_r = {NBITS{1'b1}};
   reg [15:0] ERR_CNT_r= {15{1'b0}}; // modified_4_TDS

  //--------------------------------------------		
  // Internal variables
  //--------------------------------------------		

   wire [1:POLY_LENGHT] prbs[NBITS:0];
   wire [NBITS - 1:0] data_in_i;
   wire [NBITS - 1:0] prbs_xor_a;
   wire [NBITS - 1:0] prbs_xor_b;
   wire [NBITS:1] prbs_msb;
   reg  [1:POLY_LENGHT]prbs_reg = {(POLY_LENGHT){1'b1}};


  //--------------------------------------------		
  // Implementation
  //--------------------------------------------		
   //modified by jinhong for RC grammar check
   assign DATA_OUT = DATA_OUT_r;
   assign ERR_CNT = ERR_CNT_r;
   
   assign data_in_i = INV_PATTERN == 0 ? DATA_IN : ( ~DATA_IN);
   assign prbs[0] = prbs_reg; 
   
   genvar I;
   generate for (I=0; I<NBITS; I=I+1) begin : g1
      assign prbs_xor_a[I] = prbs[I][POLY_TAP] ^ prbs[I][POLY_LENGHT];
      assign prbs_xor_b[I] = prbs_xor_a[I] ^ data_in_i[I];
      assign prbs_msb[I+1] = CHK_MODE == 0 ? prbs_xor_a[I]  :  data_in_i[I];  
      assign prbs[I+1] = {prbs_msb[I+1] , prbs[I][1:POLY_LENGHT-1]};
   end
   endgenerate

   always @(posedge CLK) begin
      if(RST == 1'b 1) begin
         prbs_reg <= {POLY_LENGHT{1'b1}};
         DATA_OUT_r <= {NBITS{1'b1}};
      end
      else if(EN == 1'b 1) begin
         DATA_OUT_r <= prbs_xor_b;
         prbs_reg <= prbs[NBITS];
      end
  end

   always @(negedge CLK) begin
        if (RST_ERR_CNT == 1) ERR_CNT_r <= 'b0;
        else  
         ERR_CNT_r <=  ERR_CNT_r +DATA_OUT_r[19]+ DATA_OUT_r[18]+ DATA_OUT_r[17]+ DATA_OUT_r[16]
                                 +DATA_OUT_r[15]+ DATA_OUT_r[14]+ DATA_OUT_r[13]+ DATA_OUT_r[12]
                                 +DATA_OUT_r[11]+ DATA_OUT_r[10]+ DATA_OUT_r[9]+ DATA_OUT_r[8]
                                 +DATA_OUT_r[7]+ DATA_OUT_r[6]+ DATA_OUT_r[5]+ DATA_OUT_r[4]   
                                 +DATA_OUT_r[3]+ DATA_OUT_r[2]+ DATA_OUT_r[1]+ DATA_OUT_r[0];   
         end


endmodule
