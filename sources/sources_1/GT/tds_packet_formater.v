`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/29/2017 02:19:36 PM
// Design Name: 
// Module Name: tds_packet_formater
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: build udp packets to send out TDS data
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tds_packet_formater(

    //GT Interface
    input rst,
    input gt0_clk160,
    input fifo_rd_clk, //80MHz
    input [29:0] gt0_data_out,
    input        gt0_data_valid_o,
    input [29:0] gt1_data_out,
    input        gt1_data_valid_o,
    input [29:0] gt2_data_out,
    input        gt2_data_valid_o,
    input [29:0] gt3_data_out,
    input        gt3_data_valid_o
    
    //Ethernet Interface
    
//    --    -- Ethernet signals
//    --    packet_recv         => to_SCA.packet_recv,
//    --    command_in          => to_SCA.command,
//    --    dout                => to_SCA.dout,
//    --    addr_eth            => from_SCA.addr,
//    --    finished            => from_SCA.done,
//    --    send_request        => from_SCA_tx.request,
//    --    send_data           => from_SCA_tx.data_in,
//    --    send_end_packet     => from_SCA_tx.end_packet,
//    --    send_wr_en          => from_SCA_tx.wr_en,
//    --    send_grant          => grant_SCA
    
          
);










    gt_data2fifo gt_data2fifop_inst0(
        .rst(rst),
        .gt_clk(gt0_clk160),
        .gt_data(gt0_data_out),
        .gt_data_valid(gt0_data_valid_o),
        .daq_start(),
        .fifo_rd_clk(fifo_rd_clk),
        .fifo_rd_en(1'b0),
        .fifo_dout(),
        .fifo_almost_full(),
        .fifo_empty()
    );



endmodule
