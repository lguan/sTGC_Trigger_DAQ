----------------------------------------------------------------------------------
-- Company: Michigan
-- Engineer: Reid Pinkham
-- 
-- Create Date: 21.10.2016
-- Module Name: UDP_TX - Behavioral
-- Target Devices: Virtex-7 VC707
-- Tool Versions: Vivado 2016.2
-- Changelog:
-- 21.10.2016 Created the file (Reid Pinkham)
----------------------------------------------------------------------------------

library unisim;
use unisim.vcomponents.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.axi.all;
use work.ipv4_types.all;
use work.arp_types.all;
use work.bus_types.all;

entity ETH_RX is
    port(
        clk_udp             : in  std_logic; --125 MHz (Write UDP Packet Data to FIFO)
        clk_fifo            : in  std_logic; --80 MHz (Read from Ethernet Recev FIFO) 
        reset               : in  std_logic;

        udp_rx              : in  udp_rx_type;

        fifo_rd_en          : in  std_logic;
        fifo_empty          : out std_logic;
        fifo_valid          : out std_logic;
        fifo_dout           : out std_logic_vector(8 downto 0);
        
        -- full info to rxmux 
        fifo_full_rd_en          : in  std_logic;
        fifo_full_empty          : out std_logic
    );
    
    
end ETH_RX;

architecture Behavioral of ETH_RX is

constant src_port_expected  : std_logic_vector(15 downto 0) := x"1c21"; -- coming from port 7201

signal dest_port            : std_logic_vector(15 downto 0);
signal src_port             : std_logic_vector(15 downto 0);
signal src_ip               : std_logic_vector(31 downto 0);

signal data_in              : std_logic_vector(7 downto 0);
signal packet_ready         : std_logic;
signal data_last            : std_logic;

signal is_state             : std_logic_vector(7 downto 0);
signal send_reply           : std_logic;
signal i                    : integer range 0 to 3;
signal reply_state          : std_logic_vector(3 downto 0);
signal reply_done           : std_logic;
signal reply_cnt            : integer range 0 to 2;

signal data_hmark           : std_logic := '0';

signal header_in            : std_logic_vector(31 downto 0);
signal command_in           : std_logic_vector(31 downto 0);

signal valid                : std_logic;

signal fifo_dout_r          : std_logic_vector(8 downto 0);


-----------------TETST SIGNAL

signal rst                  : std_logic;
signal fifo_wen             : std_logic;
signal fifo_din             : std_logic_vector(8 downto 0);
signal fifo_full            : std_logic;
signal fifo_empty_r         : std_logic;
signal fifo_prog_full_r     : std_logic;
signal fifo_prog_full       : std_logic;
signal fifo_valid_r         : std_logic;
signal fifo_full_mark    : std_logic_vector(1 downto 0);



signal fifo_full_wen             : std_logic;
signal fifo_full_din             : std_logic;
signal fifo_full_dout            : std_logic;
signal fifo_full_full            : std_logic;
signal fifo_full_empty_r         : std_logic;



signal sanity_din           : std_logic_vector(8 downto 0);

type rx_state is (idle, data_recv, end_recv,header, sanity, command, check, ext_cntrl, int_cntrl, reply_1, reply_2);
signal state                : rx_state;


component fifo_eth_rx_data IS
  PORT (
    rst                     : IN STD_LOGIC;
    wr_clk                  : IN STD_LOGIC;
    rd_clk                  : IN STD_LOGIC;
    din                     : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
    wr_en                   : IN STD_LOGIC;
    rd_en                   : IN STD_LOGIC;
    dout                    : OUT STD_LOGIC_VECTOR(8 DOWNTO 0);
    full                    : OUT STD_LOGIC;
    empty                   : OUT STD_LOGIC;
    valid                   : OUT STD_LOGIC;
    prog_full               : OUT STD_LOGIC
  );
end component;

component fifo_eth_rx_full IS
  PORT (
    rst                     : IN STD_LOGIC;
    wr_clk                  : IN STD_LOGIC;
    rd_clk                  : IN STD_LOGIC;
    din                     : IN STD_LOGIC;
    wr_en                   : IN STD_LOGIC;
    rd_en                   : IN STD_LOGIC;
    dout                    : OUT STD_LOGIC;
    full                    : OUT STD_LOGIC;
    empty                   : OUT STD_LOGIC
  );
end component;

component ila_eth_rx_udp2fifo
    port (
        clk             : in std_logic;
        probe0          : in std_logic_vector(15 downto 0);
        probe1          : in std_logic_vector(15 downto 0);
        probe2          : in std_logic_vector(7 downto 0);
        probe3          : in std_logic;
        probe4          : in std_logic;
        probe5          : in std_logic_vector(7 downto 0);
        probe6          : in std_logic;
        probe7          : in std_logic_vector(8 downto 0);
        probe8          : in std_logic_vector(8 downto 0);
        probe9          : in std_logic;
        probe10         : in std_logic;
        probe11         : in std_logic_vector(15 downto 0)

    );
end component;

begin

recv_fsm: process (clk_udp, reset, state, packet_ready, data_last, i)
begin
if rising_edge(clk_udp) then
    if (reset = '1') then
        state <= idle;

        data_hmark <= '0';
        
    else
        case state is -- Main FSM for recieving
            when idle =>
                is_state <= x"00";
                if (packet_ready = '1') then -- New packet has arrived                                
                    if (src_port = src_port_expected) then  --filter-shoule come from right port
                        fifo_wen <= '1';
                        fifo_din <= data_hmark & data_in;
                        state <= data_recv;
                    else 
                        sanity_din <= data_hmark & data_in;
                        state <= sanity;
                    end if;
                else
                    state <= idle;
                end if;

            when data_recv =>
                is_state <= x"01";
                if (packet_ready = '1') then -- data valid
                    fifo_wen <= '1';
                    fifo_din <= data_hmark & data_in;        
                    if (data_last = '1') then -- Last bit of data
                        data_hmark <= not data_hmark;
                        state <= end_recv;                
                    else
                        state <= data_recv;
                    end if;
                else
                    fifo_wen <= '0'; -- prevent writes
                    state <= data_recv;
                end if;

            when sanity => -- Clear junk data
                is_state <= x"02";
                if (packet_ready = '1') then -- data valid
                    sanity_din <= '0' & data_in;        
                    if (data_last = '1') then -- Last bit of data
                        state <= end_recv;                
                    else
                        state <= sanity;
                    end if;
                else
                    state <= data_recv;
                end if;
            
            when end_recv => --Finished! go back to idle stat. 
                is_state <=x"03";
                fifo_wen <= '0';
                state <= idle;
            
            when others => state <= idle;

        end case;
    end if;
end if;
end process;



-- Assign signals BRAM internal/external access

data_in             <= udp_rx.data.data_in;
dest_port           <= udp_rx.hdr.dst_port;
src_port            <= udp_rx.hdr.src_port;
src_ip              <= udp_rx.hdr.src_ip_addr;
packet_ready        <= udp_rx.data.data_in_valid;
data_last           <= udp_rx.data.data_in_last;

fifo_dout           <= fifo_dout_r;


fifo_eth_rx_data_inst: fifo_eth_rx_data
  port map (
    rst                 => '0',
    wr_clk              => clk_udp,
    rd_clk              => clk_fifo,
    din                 => fifo_din,
    wr_en               => fifo_wen,
    rd_en               => fifo_rd_en,
    dout                => fifo_dout_r,
    full                => fifo_full,
    empty               => fifo_empty_r,
    valid               => fifo_valid_r,
    prog_full           => fifo_prog_full_r    
  );

fifo_empty  <= fifo_empty_r;
fifo_valid  <= fifo_valid_r;

fifo_eth_rx_full_inst: fifo_eth_rx_full
  port map (
    rst                 => '0',
    wr_clk              => clk_udp,
    rd_clk              => clk_fifo,
    din                 => fifo_full_din,
    wr_en               => fifo_full_wen,
    rd_en               => fifo_full_rd_en,
    dout                => fifo_full_dout,
    full                => fifo_full_full,
    empty               => fifo_full_empty_r
  );

fifo_full_empty  <= fifo_full_empty_r;

--fifo_prog_full  <= fifo_prog_full_r; ---found timing problem;


fifo_full_moniter: process (clk_udp,fifo_full_mark) -- flag at the rising edge
begin
    if rising_edge(clk_udp) then
        fifo_full_mark (0) <= fifo_prog_full_r;
        fifo_full_mark (1) <= fifo_full_mark (0);
    end if;
    
    
    if (fifo_full_mark (0) = '1') and (fifo_full_mark (1) = '0') then 
        fifo_prog_full<= '1';
    else 
        fifo_prog_full<= '0';
    end if;
end process;


full_fsm: process (clk_udp, reset)
begin
if rising_edge(clk_udp) then
    if (fifo_prog_full = '1') then
        fifo_full_wen <= '1';
        fifo_full_din <= '1';     
    else
        fifo_full_wen <= '0';
    end if;
end if;
end process;



--ila_eth_rx_udp2fifo_inst : ila_eth_rx_udp2fifo
--port map (
--    clk         => clk_udp,
--    probe0      => dest_port,
--    probe1      => src_port,
--    probe2      => data_in,
----    probe3      => packet_ready,
----    probe4      => data_last,
--    probe3      => fifo_full_empty_r,
--    probe4      =>fifo_full_rd_en,
--    probe5      => is_state,
--    probe6      => fifo_wen,
--    probe7      => fifo_din,
--    probe8      => fifo_dout_r,
--    probe9      => fifo_prog_full_r,
--    probe10     => fifo_full_wen,
--    probe11     => src_port_expected
--);

end Behavioral;
