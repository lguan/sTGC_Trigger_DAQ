-- (c) Copyright 2009 Xilinx, Inc. All rights reserved.
--
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
--
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
--
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
--
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES. 
-- 
-- 
--------------------------------------------------------------------------------
-- Description: This is the top level vhdl example design for the
--              Ethernet 1000BASE-X PCS/PMA core.
--
--              This design example instantiates IOB flip-flops
--              and input/output buffers on the GMII.
--
--              A Transmitter Elastic Buffer is instantiated on the Tx
--              GMII path to perform clock compenstation between the
--              core and the external MAC driving the Tx GMII.
--
--              This design example can be synthesised.
--
--
--
--    ----------------------------------------------------------------
--    |                             Example Design                   |
--    |                                                              |
--    |             ----------------------------------------------   |
--    |             |           Core Block (wrapper)             |   |
--    |             |                                            |   |
--    |             |   --------------          --------------   |   |
--    |             |   |    Core    |          | tranceiver |   |   |
--    |             |   |            |          |            |   |   |
--    |  ---------  |   |            |          |            |   |   |
--    |  |       |  |   |            |          |            |   |   |
--    |  |  Tx   |  |   |            |          |            |   |   |
--  ---->|Elastic|----->| GMII       |--------->|        TXP |--------->
--    |  |Buffer |  |   | Tx         |          |        TXN |   |   |
--    |  |       |  |   |            |          |            |   |   |
--    |  ---------  |   |            |          |            |   |   |
--    | GMII        |   |            |          |            |   |   |
--    | IOBs        |   |            |          |            |   |   |
--    |             |   |            |          |            |   |   |
--    |             |   | GMII       |          |        RXP |   |   |
--  <-------------------| Rx         |<---------|        RXN |<---------
--    |             |   |            |          |            |   |   |
--    |             |   --------------          --------------   |   |
--    |             |                                            |   |
--    |             ----------------------------------------------   |
--    |                                                              |
--    ----------------------------------------------------------------
--
--


library unisim;
use unisim.vcomponents.all;

library ieee;
use ieee.std_logic_1164.all;


--------------------------------------------------------------------------------
-- The entity declaration for the example design
--------------------------------------------------------------------------------

entity gig_ethernet_wrapper is
port(
    --An independent clock source used as the reference clock for an
    --IDELAYCTRL (if present) and for the main GT transceiver reset logic.
    --This example design assumes that this is of frequency 200MHz.
    independent_clock    : in std_logic;
    
    -- Tranceiver Interface
    -----------------------
    
    gtrefclk_p           : in std_logic;                     -- Differential +ve of reference clock for tranceiver: , very high quality
    gtrefclk_n           : in std_logic;                     -- Differential -ve of reference clock for tranceiver: , very high quality
    rxuserclk2           : out std_logic;                     -- Differential -ve of reference clock for tranceiver:, very high quality
    txp                  : out std_logic;                    -- Differential +ve of serial transmission from PMA to PMD.
    txn                  : out std_logic;                    -- Differential -ve of serial transmission from PMA to PMD.
    rxp                  : in std_logic;                     -- Differential +ve for serial reception from PMD to PMA.
    rxn                  : in std_logic;                     -- Differential -ve for serial reception from PMD to PMA.
    
    -- GMII Interface (client MAC <=> PCS)
    --------------------------------------
    sgmii_clk            : out std_logic;                    -- Clock for client MAC
    clk125_o             : out std_logic;
    sgmii_clk_en         : out std_logic;
    gmii_txd             : in std_logic_vector(7 downto 0);  -- Transmit data from client MAC.
    gmii_tx_en           : in std_logic;                     -- Transmit control signal from client MAC.
    gmii_tx_er           : in std_logic;                     -- Transmit control signal from client MAC.
    gmii_rxd             : out std_logic_vector(7 downto 0); -- Received Data to client MAC.
    gmii_rx_dv           : out std_logic;                    -- Received control signal to client MAC.
    gmii_rx_er           : out std_logic;                    -- Received control signal to client MAC.
    -- Management: Alternative to MDIO Interface
    --------------------------------------------
    
    configuration_vector : in std_logic_vector(4 downto 0);  -- Alternative to MDIO interface.
    
    an_interrupt         : out std_logic;                    -- Interrupt to processor to signal that Auto-Negotiation has completed
    an_adv_config_vector : in std_logic_vector(15 downto 0); -- Alternate interface to program REG4 (AN ADV)
    an_restart_config    : in std_logic;                     -- Alternate signal to modify AN restart bit in REG0
    
    -- Speed Control
    ----------------
    speed_is_10_100      : in std_logic;                     -- Core should operate at either 10Mbps or 100Mbps speeds
    speed_is_100         : in std_logic;                     -- Core should operate at 100Mbps speed
    
    -- General IO's
    ---------------
    status_vector        : out std_logic_vector(15 downto 0); -- Core status.
    reset                : in std_logic;                     -- Asynchronous reset for entire core.
    signal_detect        : in std_logic                      -- Input from PMD to indicate presence of optical input.
      );
end gig_ethernet_wrapper;



architecture top_level of gig_ethernet_wrapper is
   attribute DowngradeIPIdentifiedWarnings: string;
   attribute DowngradeIPIdentifiedWarnings of top_level : architecture is "yes";



  ------------------------------------------------------------------------------
  -- Component Declaration for the Core Block (core wrapper).
  ------------------------------------------------------------------------------
   component gig_ethernet_pcs_pma_0
  port (
      -- Transceiver Interface
  ------------------------


  gtrefclk_p           : in std_logic;                     -- Very high quality clock for GT transceiver
  gtrefclk_n           : in std_logic;                    
  gtrefclk_out         : out std_logic;                  
  gtrefclk_bufg_out    : out std_logic;                           
  
  txp                  : out std_logic;                    -- Differential +ve of serial transmission from PMA to PMD.
  txn                  : out std_logic;                    -- Differential -ve of serial transmission from PMA to PMD.
  rxp                  : in std_logic;                     -- Differential +ve for serial reception from PMD to PMA.
  rxn                  : in std_logic;                     -- Differential -ve for serial reception from PMD to PMA.

  mmcm_locked_out      : out std_logic;                     -- Locked signal from MMCM
  userclk_out          : out std_logic;                  
  userclk2_out         : out std_logic;                 
  rxuserclk_out          : out std_logic;               
  rxuserclk2_out         : out std_logic;               
  independent_clock_bufg : in std_logic;                
  pma_reset_out         : out std_logic;                     -- transceiver PMA reset signal
  resetdone             :out std_logic;

  -- GMII Interface
  -----------------
  sgmii_clk_r            : out std_logic;                 
  sgmii_clk_f            : out std_logic;                 
  sgmii_clk_en           : out std_logic;                    -- Clock enable for client MAC
  gmii_txd             : in std_logic_vector(7 downto 0);  -- Transmit data from client MAC.
  gmii_tx_en           : in std_logic;                     -- Transmit control signal from client MAC.
  gmii_tx_er           : in std_logic;                     -- Transmit control signal from client MAC.
  gmii_rxd             : out std_logic_vector(7 downto 0); -- Received Data to client MAC.
  gmii_rx_dv           : out std_logic;                    -- Received control signal to client MAC.
  gmii_rx_er           : out std_logic;                    -- Received control signal to client MAC.
  gmii_isolate         : out std_logic;                    -- Tristate control to electrically isolate GMII.
  -- Management: Alternative to MDIO Interface
  --------------------------------------------

  configuration_vector : in std_logic_vector(4 downto 0);  -- Alternative to MDIO interface.


  an_interrupt         : out std_logic;                    -- Interrupt to processor to signal that Auto-Negotiation has completed
  an_adv_config_vector : in std_logic_vector(15 downto 0); -- Alternate interface to program REG4 (AN ADV)
  an_restart_config    : in std_logic;                     -- Alternate signal to modify AN restart bit in REG0

  -- Speed Control
  ----------------
  speed_is_10_100      : in std_logic;                     -- Core should operate at either 10Mbps or 100Mbps speeds
  speed_is_100         : in std_logic;                      -- Core should operate at 100Mbps speed


  -- General IO's
  ---------------
  status_vector        : out std_logic_vector(15 downto 0); -- Core status.
  reset                : in std_logic;                     -- Asynchronous reset for entire core.
  signal_detect        : in std_logic;                      -- Input from PMD to indicate presence of optical input.
  gt0_qplloutclk_out     : out std_logic;
  gt0_qplloutrefclk_out  : out std_logic
  );

end component;



------------------------------------------------------------------------------
-- internal signals used in this top level example design.
------------------------------------------------------------------------------

-- clock generation signals for tranceiver
signal gtrefclk_bufg_out     : std_logic;

signal userclk               : std_logic;                    
signal userclk2              : std_logic;                    
signal rxuserclk2_i          : std_logic;                    


-- An independent clock source used as the reference clock for an
-- IDELAYCTRL (if present) and for the main GT transceiver reset logic.
signal independent_clock_bufg: std_logic;

-- GMII signals
signal gmii_isolate          : std_logic;                    -- Internal gmii_isolate signal.
signal gmii_txd_int          : std_logic_vector(7 downto 0); -- Internal gmii_txd signal (between core and SGMII adaptation module).
signal gmii_tx_en_int        : std_logic;                    -- Internal gmii_tx_en signal (between core and SGMII adaptation module).
signal gmii_tx_er_int        : std_logic;                    -- Internal gmii_tx_er signal (between core and SGMII adaptation module).
signal gmii_rxd_int          : std_logic_vector(7 downto 0); -- Internal gmii_rxd signal (between core and SGMII adaptation module).
signal gmii_rx_dv_int        : std_logic;                    -- Internal gmii_rx_dv signal (between core and SGMII adaptation module).
signal gmii_rx_er_int        : std_logic;                    -- Internal gmii_rx_er signal (between core and SGMII adaptation module).
signal sgmii_clk_r : std_logic;
signal sgmii_clk_f : std_logic;


begin



-----------------------------------------------------------------------------
-- An independent clock source used as the reference clock for an
-- IDELAYCTRL (if present) and for the main GT transceiver reset logic.
-----------------------------------------------------------------------------

-- Route independent_clock input through a BUFG
bufg_independent_clock : BUFG
port map (
  I         => independent_clock,
  O         => independent_clock_bufg
);


------------------------------------------------------------------------------
-- Instantiate the Core Block (core wrapper).
------------------------------------------------------------------------------
core_wrapper_i : gig_ethernet_pcs_pma_0

port map (

  gtrefclk_p             => gtrefclk_p,
  gtrefclk_n             => gtrefclk_n,
  gtrefclk_out           => open,
  gtrefclk_bufg_out      => gtrefclk_bufg_out,
  
  txp                  => txp,
  txn                  => txn,
  rxp                  => rxp,
  rxn                  => rxn,
  mmcm_locked_out          => open,
  userclk_out              => userclk,
  userclk2_out             => userclk2,
  rxuserclk_out              => open,
  rxuserclk2_out             => rxuserclk2_i,
  independent_clock_bufg => independent_clock_bufg,
  pma_reset_out              => open,
  resetdone                  => open,
  
  sgmii_clk_r            => sgmii_clk_r,
  sgmii_clk_f            => sgmii_clk_f,
  sgmii_clk_en           => sgmii_clk_en,
  gmii_txd             => gmii_txd_int,
  gmii_tx_en           => gmii_tx_en_int,
  gmii_tx_er           => gmii_tx_er_int,
  gmii_rxd             => gmii_rxd_int,
  gmii_rx_dv           => gmii_rx_dv_int,
  gmii_rx_er           => gmii_rx_er_int,
  gmii_isolate         => gmii_isolate,
  configuration_vector => configuration_vector,
  an_interrupt         => an_interrupt,
  an_adv_config_vector => an_adv_config_vector,
  an_restart_config    => an_restart_config,
  speed_is_10_100      => speed_is_10_100,
  speed_is_100         => speed_is_100,

  status_vector        => status_vector,
  reset                => reset,


  signal_detect        => signal_detect,
  gt0_qplloutclk_out     => open,
  gt0_qplloutrefclk_out  => open
  );



-----------------------------------------------------------------------------
-- GMII transmitter data logic
-----------------------------------------------------------------------------


-- Drive input GMII signals through IOB input flip-flops (inferred).
process (userclk2)
begin
  if userclk2'event and userclk2 = '1' then
     gmii_txd_int    <= gmii_txd;
     gmii_tx_en_int  <= gmii_tx_en;
     gmii_tx_er_int  <= gmii_tx_er;

  end if;
end process;


-----------------------------------------------------------------------------
-- GMII receiver data logic
-----------------------------------------------------------------------------


-- Drive input GMII signals through IOB output flip-flops (inferred).
process (userclk2)
begin
  if userclk2'event and userclk2 = '1' then
     gmii_rxd    <= gmii_rxd_int;
     gmii_rx_dv  <= gmii_rx_dv_int;
     gmii_rx_er  <= gmii_rx_er_int;

  end if;
end process;


-----------------------------------------------------------------------------
-- SGMII clock logic
-----------------------------------------------------------------------------

-- All GMII transmitter input signals must be synchronous to this
-- clock.

-- All GMII receiver output signals are synchrounous to this clock.

-- This instantiates a DDR output register.  This is a nice way to
-- drive the output clock since the clock-to-PAD delay will the
-- same as that of data driven from an IOB Ouput flip-flop.


sgclk_ddr_iob : ODDR
port map(
  Q  => sgmii_clk,
  C  => userclk2,
  CE => '1',
  D1 => sgmii_clk_r,
  D2 => sgmii_clk_f,
  R  => '0',
  S  => '0'
);


rxuserclk2 <= rxuserclk2_i;
clk125_o <= userclk2;

    
end top_level;
