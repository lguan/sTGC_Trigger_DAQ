----------------------------------------------------------------------------------
-- Company: NTU ATHNENS - BNL - Michigan
-- Engineer: Reid Pinkham, Paris Moschovakos, Panagiotis Gkountoumis
-- 
-- Create Date: 18.04.2016 13:00:21
-- Module Name: FIFO2UDP
-- Target Devices: Virtex-7 VC707
-- Tool Versions: Vivado 2016.2
-- Changelog:
-- 07.10.2016 Adapting from mmfe8 to TDS application (Reid Pinkham)
-- 18.10.2016 Removed end_packet synchronization, same clock domain (Reid Pinkham)
----------------------------------------------------------------------------------

library unisim;
use unisim.vcomponents.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.axi.all;
use work.ipv4_types.all;
use work.arp_types.all;

entity FIFO2UDP is
    Port (
        clk_fifo                    : in std_logic; -- Clock used for writing to fifo 80Mhz
        clk_udp                     : in std_logic; -- Clock used for UDP interface
        reset                       : in  std_logic;
        destinationIP               : in std_logic_vector(31 downto 0);
        data_in                     : in  std_logic_vector(31 downto 0); -- Data to be written to fifo with wr_en
        wr_en                       : in  std_logic;
        end_packet                  : in  std_logic; -- Indicates this is the last word in the packet
        udp_txi		                : out udp_tx_type; -- type to go into the UDP block
        udp_tx_start                : out std_logic; -- Signal to form packet header in UDP block
        control					    : out std_logic; -- Control to UDP
        udp_tx_data_out_ready       : in  std_logic; -- Signal from UDP to indicate it can accept data
        sending_o                   : out std_logic; -- Signal to indicate this module is completing a transaction
        fifo_full_info              : out std_logic_vector(1 downto 0)
        
    );
end FIFO2UDP;

architecture Behavioral of FIFO2UDP is

signal st_send                     : unsigned(3 downto 0) := x"0";
signal count_length                : unsigned(15 downto 0) := x"0000";
signal daq_fifo_re                 : std_logic := '0';
signal fifo_empty_UDP              : std_logic := '0';
signal fifo_full_UDP               : std_logic := '0';
signal fifo_full_len               : std_logic := '0';
signal prog_fifo_empty             : std_logic := '0';
signal data_out                    : std_logic_vector(7 downto 0) := x"00";
signal data_out_valid              : std_logic := '0';
signal packet_length               : unsigned(15 downto 0) := x"0000";
signal data_out_last               : std_logic := '0';
signal sending                     : std_logic := '0';
signal udp_tx_start_i              : std_logic := '0';
signal fifo_len_wr_en              : std_logic := '0';
signal fifo_len_rd_en              : std_logic := '0';
signal packet_length_in            : std_logic_vector(11 downto 0);
signal packet_length_cnt           : unsigned(11 downto 0);
signal packet_len_r                : std_logic_vector(11 downto 0);
signal fifo_empty_len              : std_logic;
signal state                       : std_logic_vector(3 downto 0) := "0000";
signal daq_data_out                : std_logic_vector(7 downto 0) := x"00";
signal fifo_full_info_r              : std_logic_vector(1 downto 0) := b"00";

signal fifo_prog_full_UDP               : std_logic := '0';
signal fifo_prog_full_len               : std_logic := '0';


signal data_fifo_valid             : std_logic;
signal len_fifo_valid              : std_logic;

component fifo_eth_tx_data is

port(
    rst         : in std_logic;
    wr_clk      : in std_logic;
    rd_clk      : in std_logic;
    din         : in std_logic_vector(31 downto 0);
    wr_en       : in std_logic;
    rd_en       : in std_logic;
    dout        : out std_logic_vector(7 downto 0);
    full        : out std_logic;
    empty       : out std_logic;
    prog_full   : out std_logic;
    valid       : out std_logic
);
end component;

component fifo_eth_tx_packet_length
port (
    wr_clk      : in std_logic;
    rd_clk      : in std_logic;
    rst         : in std_logic;
    din         : in std_logic_vector(11 downto 0);
    wr_en       : in std_logic;
    rd_en       : in std_logic;
    dout        : out std_logic_vector(11 downto 0);
    full        : out std_logic;
    empty       : out std_logic;
    prog_full   : out std_logic;
    valid       : out std_logic
);
end component;

component ila_fifo2udp
port (
    clk         : in std_logic;
    probe0      : in std_logic_vector(3 downto 0);
    probe1      : in std_logic;
    probe2      : in std_logic;
    probe3      : in std_logic;
    probe4      : in std_logic_vector(15 downto 0);
    probe5      : in std_logic;
    probe6      : in std_logic_vector(11 downto 0);
    probe7      : in std_logic;
    probe8      : in std_logic;
    probe9      : in std_logic_vector(11 downto 0);
    probe10     : in std_logic;
    probe11     : in std_logic;
    probe12     : in std_logic;
    probe13     : in unsigned(15 downto 0);
    probe14     : in std_logic_vector(3 downto 0);
    probe15     : in std_logic_vector(7 downto 0)
);
end component;

begin

--ila_eth_tx_fifo2udp_inst: ila_eth_tx_fifo2udp
--    port map(
--        clk         => clk_udp,
--        probe0      => std_logic_vector(st_send),
--        probe1      => udp_tx_data_out_ready,
--        probe2      => fifo_full_UDP,
--        probe3      => fifo_full_len,
--        probe4      => std_logic_vector(packet_length),
--        probe5      => udp_tx_start_i,
--        probe6      => packet_len_r,
--        probe7      => fifo_empty_UDP,
--        probe8      => fifo_empty_len,
--        probe9      => std_logic_vector(packet_length_cnt),
--        probe10     => end_packet,
--        probe11     => wr_en,
--        probe12     => end_packet,
--        probe13     => count_length,
--        probe14     => state,
--        probe15     => daq_data_out

--    );


fifo_eth_tx_data_inst: fifo_eth_tx_data
    port map(
        rst         => reset,
        wr_clk      => clk_fifo,
        rd_clk      => clk_udp,
        din         => data_in,
        wr_en       => wr_en,
        rd_en       => daq_fifo_re,
        dout        => daq_data_out,
        full        => fifo_full_UDP,
        empty       => fifo_empty_UDP,
        prog_full   => fifo_prog_full_UDP,
        valid       => data_fifo_valid 
    );

fifo_eth_tx_packet_length_inst: fifo_eth_tx_packet_length
    port map (
        wr_clk      => clk_fifo,
        rd_clk      => clk_udp,
        rst         => reset,
        din         => packet_length_in,
        wr_en       => fifo_len_wr_en,
        rd_en       => fifo_len_rd_en,
        dout        => packet_len_r,
        full        => fifo_full_len,
        empty       => fifo_empty_len,
        prog_full   => fifo_prog_full_len,
        valid       => len_fifo_valid
    );


fill_packet_len: process (clk_fifo, state, wr_en, end_packet) -- small state machine to write packet_len to fifo
begin
    if rising_edge(clk_fifo) then
        if reset = '1' then
            packet_length_cnt <= (others => '0');
            fifo_len_wr_en <= '0';
            state <= "0000";
        else
            case state is
                when "0000" => -- idle
                    fifo_len_wr_en <= '0';
                    if (wr_en = '1' and end_packet = '1') then -- latch the packet_len into the fifo
                        packet_length_cnt   <= packet_length_cnt + 1;
                        state               <= "0001";
                    elsif (wr_en = '1') then -- increment packet
                        packet_length_cnt <= packet_length_cnt + 1;
                        state           <= "0000";
                    end if;
    
                when "0001" => -- write to the fifo, save value
                    fifo_len_wr_en <= '1';
                    packet_length_in <= std_logic_vector(packet_length_cnt);
                    state <= "0010";

                when "0010" => -- erase vector, stop write
                    fifo_len_wr_en <= '0';
                    packet_length_cnt <= (others => '0');
                    state <= "0000";

                when others =>
                    state <= "0000";
            end case;
        end if;
    end if;
end process;


process (clk_udp, st_send, udp_tx_data_out_ready, fifo_empty_UDP, prog_fifo_empty, data_out_valid)
begin
    if rising_edge(clk_udp) then
        if reset = '1' then -- IF statement to read from length fifo and initiate a packet send
            sending             <= '0';
            data_out_last       <= '0';    
            data_out_valid      <= '0';     
            udp_tx_start_i      <= '0';
            st_send               <= x"0";
        else
            case st_send is
                when x"0" =>
                    if fifo_empty_len = '0' then -- Send packets until FIFO is empty
                        fifo_len_rd_en  <= '1';
                        st_send         <= x"2";
                        sending         <= '1';
                    end if;

--                when x"1" => -- state to allow fifo time to respond
--                    st_send <= x"2";
--                    fifo_len_rd_en  <= '0';

                when x"2" =>
                    if len_fifo_valid = '1' then    
                        packet_length   <= resize(unsigned("0000" & packet_len_r) * 4 + 4, 16);
                        count_length    <= resize(unsigned("0000" & packet_len_r) * 4, 16);
                        fifo_len_rd_en  <= '0';
                        st_send <= x"3";
                    else 
                        st_send         <= x"2";
                    end if;

                when x"3" =>
                      data_out_last     <= '0';    
                      data_out_valid    <= '0';
                      data_out          <= (others => '0');
                      udp_tx_start_i    <= '0';
                      st_send           <= x"4";

                when x"4" =>
                      udp_tx_start_i            <= '1';
                      udp_txi.hdr.dst_ip_addr   <= destinationIP;         -- set a generic ip adrress (192.168.0.255)
                      udp_txi.hdr.src_port      <= x"19CB";                -- set src and dst ports
                      udp_txi.hdr.dst_port      <= x"1778";                     -- x"6af0"; 
                      udp_txi.hdr.data_length   <= std_logic_vector(packet_length); -- defined to be 16 bits in UDP
                      daq_fifo_re               <= '0';                           
                      udp_txi.hdr.checksum      <= x"0000";
                      st_send                   <= x"5";

                when x"5" =>
                    if udp_tx_data_out_ready = '1' then     
                      udp_tx_start_i            <= '0';
                      daq_fifo_re               <= '1';
                      st_send                   <= x"6";
                    end if;

                when x"6" =>
                    if udp_tx_data_out_ready = '1' then   
                      count_length      <= count_length - 1;      
                      udp_tx_start_i    <= '0'; 
                      data_out          <= daq_data_out;
                      st_send           <= x"7";
                    end if;

                when x"7" =>
                    if udp_tx_data_out_ready = '1' then
                        if count_length = 1 then
                            daq_fifo_re                 <= '0';
                        elsif count_length = 0 then
                            st_send                       <= x"8"; 
                            daq_fifo_re                 <= '0';
                        else
                            daq_fifo_re                 <= '1';
                        end if; 
                        count_length    <= count_length - 1;    
                        udp_tx_start_i  <= '0';                
                        data_out_valid  <= '1';   
                        control         <= '0';         
                        data_out_last   <= '0';       
                        data_out        <= daq_data_out;
                    else
                        daq_fifo_re     <= '0';
                    end if;

                when x"8" =>
                  if udp_tx_data_out_ready = '1' then    
                      daq_fifo_re                 <= '0';
                      udp_tx_start_i              <= '0';
                      data_out_last               <= '0';
                      data_out <= x"ff";
                      st_send <= x"9";
                  end if;

                when x"9" =>
                  if udp_tx_data_out_ready = '1' then    
                      daq_fifo_re                 <= '0';
                      udp_tx_start_i              <= '0';
                      data_out_last               <= '0';
                      data_out <= x"ff";
                      st_send <= x"a";
                  end if;

                when x"a" =>
                  if udp_tx_data_out_ready = '1' then    
                      daq_fifo_re                 <= '0';
                      udp_tx_start_i              <= '0';
                      data_out_last               <= '0';
                      data_out <= x"ff";
                      st_send <= x"b";
                  end if;

                when x"b" =>
                    if udp_tx_data_out_ready = '1' then
                        daq_fifo_re                 <= '0';    
                        udp_tx_start_i              <= '0';
                        data_out_last               <= '1';
                        data_out <= x"ff";
                        st_send <= x"c";
                    end if;

                when x"c" =>
                      data_out_last     <= '0';    
                      data_out_valid    <= '0';
                      data_out          <= (others => '0');
                      udp_tx_start_i    <= '0';
                      st_send           <= x"d";

                when x"d" =>
                      st_send           <= x"0";
                      sending           <= '0';
                      count_length      <= x"0000";
                      data_out_last     <= '0';    
                      data_out_valid    <= '0';                  
                      udp_tx_start_i    <= '0';

                when others =>
                    st_send             <= x"0";                      
            end case;
        end if;
    end if;
end process;

       
udp_tx_start                <= udp_tx_start_i;
udp_txi.data.data_out_last  <= data_out_last;
udp_txi.data.data_out_valid <= data_out_valid ;
udp_txi.data.data_out       <= data_out;

sending_o                   <= sending;

fifo_full_info_r              <=  fifo_prog_full_len & fifo_prog_full_UDP;
fifo_full_info               <= fifo_full_info_r;



end Behavioral;----------------------------------------------------------------------------------
