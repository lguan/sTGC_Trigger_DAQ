library unisim;
use unisim.vcomponents.all;
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.axi.all;
use work.ipv4_types.all;
use work.arp_types.all;
use work.bus_types.all;

entity ethernet_wrapper is
    port(
        clk_80                   : in std_logic;
        clk_200                 : in  std_logic;
        clk_125                 : out std_logic;
        glbl_rst		        : in  std_logic;

        -- Signals to the tx MUX
        to_eth_tx               : in  TX_mux2eth;
        eth_tx_hold             : out std_logic;

        -- Tranceiver Interface
        -----------------------
        refclk125_p             : in  std_logic;                     -- Differential +ve of reference clock for tranceiver: 125MHz, very high quality
        refclk125_n             : in  std_logic;                     -- Differential -ve of reference clock for tranceiver: 125MHz, very high quality
        txp                     : out std_logic;                     -- Differential +ve of serial transmission from PMA to PMD.
        txn                     : out std_logic;                     -- Differential -ve of serial transmission from PMA to PMD.
        rxp                     : in  std_logic;                     -- Differential +ve for serial reception from PMD to PMA.
        rxn                     : in  std_logic;                     -- Differential -ve for serial reception from PMD to PMA.
        phy_int                 : out std_logic;
        phy_rstn_out 		    : out std_logic;
        
        rxr_fifo_rd_en          : in  std_logic;
        rxr_fifo_empty          : out std_logic;
        rxr_fifo_dout           : out std_logic_vector(8 downto 0);
       
        rxr_fifo_valid          : out std_logic;        
        
        txt_fifo_full_info          : out std_logic_vector(1 downto 0);
        
        rxr_fifo_full_rd_en          : in std_logic;
        rxr_fifo_full_empty          : out std_logic
	  );
end ethernet_wrapper;

architecture Behavioral of ethernet_wrapper is

-- clock generation signals for tranceiver
signal mmcm_reset            : std_logic;                    -- MMCM reset signal.
signal clk125_core           : std_logic;                    -- 125MHz clock for core reference clock.

-- GMII signals
signal gmii_txd              : std_logic_vector(7 downto 0); -- Internal gmii_txd signal (between core and SGMII adaptation module).
signal gmii_tx_en            : std_logic;                    -- Internal gmii_tx_en signal (between core and SGMII adaptation module).
signal gmii_tx_er            : std_logic;                    -- Internal gmii_tx_er signal (between core and SGMII adaptation module).
signal gmii_rxd              : std_logic_vector(7 downto 0); -- Internal gmii_rxd signal (between core and SGMII adaptation module).
signal gmii_rx_dv            : std_logic;                    -- Internal gmii_rx_dv signal (between core and SGMII adaptation module).
signal gmii_rx_er            : std_logic;                    -- Internal gmii_rx_er signal (between core and SGMII adaptation module).

-- Extra registers to ease IOB placement
signal status_vector : std_logic_vector(15 downto 0);

----------------------------panos---------------------------------
signal speed_is_10_100		     : std_logic;
signal speed_is_100			     : std_logic;
signal tx_axis_mac_tready      : std_logic;
signal rx_axis_mac_tlast       : std_logic;
signal rx_axis_mac_tdata       : std_logic_vector(7 downto 0);
signal rx_axis_mac_tvalid      : std_logic;
signal local_gtx_reset		        : std_logic;
signal rx_reset		 			   : std_logic;
signal tx_reset		 			   : std_logic;
signal gtx_pre_resetn              : std_logic := '0';
signal tx_axis_mac_tdata             : std_logic_vector(7 downto 0);	
signal tx_axis_mac_tvalid            : std_logic;
signal tx_axis_mac_tlast             : std_logic;  
signal gtx_resetn				     : std_logic;
signal glbl_rstn        	         : std_logic;
signal glbl_rst_i        	         : std_logic;
signal gtx_clk_reset		     : std_logic;
signal an_restart_config       : std_logic;
signal rx_axis_mac_tready      : std_logic;
signal rx_configuration_vector : std_logic_vector(79 downto 0);
signal tx_configuration_vector : std_logic_vector(79 downto 0);
signal data_monitor_out        : std_logic_vector(7 downto 0);
signal vector_resetn               : std_logic := '0';
signal vector_pre_resetn           : std_logic := '0';
signal vector_reset             : std_logic;
signal clk_enable   			   : std_logic;
signal udp_txi      			   : udp_tx_type;
signal control                     : udp_control_type;
signal udp_rx                      : udp_rx_type;
signal ip_rx_hdr                   : ipv4_rx_header_type;
signal udp_tx_data_out_ready       : std_logic;
signal udp_tx_start	   		       : std_logic;
signal reset                       : std_logic := '0';
signal udp_header         : std_logic := '0';

signal udp_data              : std_logic_vector(31 downto 0);
signal udp_wr_en             : std_logic;
signal udp_wr_last           : std_logic;

--------------- Signals for Ethernet Test Sender --------------
type tx_state is (idle, st1, st2, st3, st4, st5, st6, st7, st8, st9, st10, st11, st12, st13, st14, st15);
signal st               : tx_state;

--signal send             : std_logic := '0';
signal myIP             : std_logic_vector(31 downto 0) := x"c0a80001";
signal myMAC            : std_logic_vector(47 downto 0) := x"002320212223";
signal destIP           : std_logic_vector(31 downto 0) := x"c0a80010";
--signal counter          : unsigned(32 downto 0) := (others => '0');
signal st_is            : std_logic_vector(3 downto 0);
signal phy_rstn_out_i   : std_logic;

--signal bind             : std_logic;
--signal bind_ip          : std_logic_vector(31 downto 0);

signal send_cnt         : unsigned(6 downto 0);


component gig_ethernet_wrapper is
port(
    --An independent clock source used as the reference clock for an
    --IDELAYCTRL (if present) and for the main GT transceiver reset logic.
    --This example design assumes that this is of frequency 200MHz.
    independent_clock    : in std_logic;
    
    -- Tranceiver Interface
    -----------------------
    
    gtrefclk_p           : in std_logic;                     -- Differential +ve of reference clock for tranceiver: , very high quality
    gtrefclk_n           : in std_logic;                     -- Differential -ve of reference clock for tranceiver: , very high quality
    rxuserclk2           : out std_logic;                     -- Differential -ve of reference clock for tranceiver:, very high quality
    txp                  : out std_logic;                    -- Differential +ve of serial transmission from PMA to PMD.
    txn                  : out std_logic;                    -- Differential -ve of serial transmission from PMA to PMD.
    rxp                  : in std_logic;                     -- Differential +ve for serial reception from PMD to PMA.
    rxn                  : in std_logic;                     -- Differential -ve for serial reception from PMD to PMA.
    
    -- GMII Interface (client MAC <=> PCS)
    --------------------------------------
    sgmii_clk            : out std_logic;                    -- Clock for client MAC
    clk125_o             : out std_logic;
    sgmii_clk_en         : out std_logic;
    gmii_txd             : in std_logic_vector(7 downto 0);  -- Transmit data from client MAC.
    gmii_tx_en           : in std_logic;                     -- Transmit control signal from client MAC.
    gmii_tx_er           : in std_logic;                     -- Transmit control signal from client MAC.
    gmii_rxd             : out std_logic_vector(7 downto 0); -- Received Data to client MAC.
    gmii_rx_dv           : out std_logic;                    -- Received control signal to client MAC.
    gmii_rx_er           : out std_logic;                    -- Received control signal to client MAC.
    -- Management: Alternative to MDIO Interface
    --------------------------------------------
    
    configuration_vector : in std_logic_vector(4 downto 0);  -- Alternative to MDIO interface.
    
    an_interrupt         : out std_logic;                    -- Interrupt to processor to signal that Auto-Negotiation has completed
    an_adv_config_vector : in std_logic_vector(15 downto 0); -- Alternate interface to program REG4 (AN ADV)
    an_restart_config    : in std_logic;                     -- Alternate signal to modify AN restart bit in REG0
    
    -- Speed Control
    ----------------
    speed_is_10_100      : in std_logic;                     -- Core should operate at either 10Mbps or 100Mbps speeds
    speed_is_100         : in std_logic;                     -- Core should operate at 100Mbps speed
    
    -- General IO's
    ---------------
    status_vector        : out std_logic_vector(15 downto 0); -- Core status.
    reset                : in std_logic;                     -- Asynchronous reset for entire core.
    signal_detect        : in std_logic                      -- Input from PMD to indicate presence of optical input.
);
end component;

component UDP_Complete_nomac
   Port (
        -- UDP TX signals
        udp_tx_start			: in std_logic;							    -- indicates req to tx UDP
        udp_txi					: in udp_tx_type;							-- UDP tx cxns
        udp_tx_result			: out std_logic_vector (1 downto 0);        -- tx status (changes during transmission)
        udp_tx_data_out_ready   : out std_logic;							-- indicates udp_tx is ready to take data
        -- UDP RX signals
        udp_rx_start			: out std_logic;							-- indicates receipt of udp header
        udp_rxo					: out udp_rx_type;
        -- IP RX signals
        ip_rx_hdr				: out ipv4_rx_header_type;
        -- system signals
        rx_clk					: in  STD_LOGIC;
        tx_clk					: in  STD_LOGIC;
        reset 					: in  STD_LOGIC;
        our_ip_address 		    : in std_logic_vector (31 downto 0);
        our_mac_address 		: in std_logic_vector (47 downto 0);
        control					: in udp_control_type;
        -- status signals
        arp_pkt_count			: out std_logic_vector(7 downto 0);			-- count of arp pkts received
        ip_pkt_count			: out std_logic_vector(7 downto 0);			-- number of IP pkts received for us
        -- MAC Transmitter
        mac_tx_tdata            : out  std_logic_vector(7 downto 0);	    -- data byte to tx
        mac_tx_tvalid           : out  std_logic;							-- tdata is valid
        mac_tx_tready           : in std_logic;							    -- mac is ready to accept data
        mac_tx_tfirst           : out  std_logic;							-- indicates first byte of frame
        mac_tx_tlast            : out  std_logic;							-- indicates last byte of frame
        -- MAC Receiver
        mac_rx_tdata            : in std_logic_vector(7 downto 0);	        -- data byte received
        mac_rx_tvalid           : in std_logic;							    -- indicates tdata is valid
        mac_rx_tready           : out  std_logic;							-- tells mac that we are ready to take data
        mac_rx_tlast            : in std_logic);							-- indicates last byte of the trame
end component;

component temac_10_100_1000_fifo_block
port(
        gtx_clk                    : in  std_logic;
        -- asynchronous reset
        glbl_rstn                  : in  std_logic;
        rx_axi_rstn                : in  std_logic;
        tx_axi_rstn                : in  std_logic;
        -- Receiver Statistics Interface
        -----------------------------------------
        rx_reset                   : out std_logic;
        rx_statistics_vector       : out std_logic_vector(27 downto 0);
        rx_statistics_valid        : out std_logic;
        -- Receiver (AXI-S) Interface
        ------------------------------------------
        rx_fifo_clock              : in  std_logic;
        rx_fifo_resetn             : in  std_logic;
        rx_axis_fifo_tdata         : out std_logic_vector(7 downto 0);
        rx_axis_fifo_tvalid        : out std_logic;
        rx_axis_fifo_tready        : in  std_logic;
        rx_axis_fifo_tlast         : out std_logic;
        -- Transmitter Statistics Interface
        --------------------------------------------
        tx_reset                   : out std_logic;
        tx_ifg_delay               : in  std_logic_vector(7 downto 0);
        tx_statistics_vector       : out std_logic_vector(31 downto 0);
        tx_statistics_valid        : out std_logic;
        -- Transmitter (AXI-S) Interface
        ---------------------------------------------
        tx_fifo_clock              : in  std_logic;
        tx_fifo_resetn             : in  std_logic;
        tx_axis_fifo_tdata         : in  std_logic_vector(7 downto 0);
        tx_axis_fifo_tvalid        : in  std_logic;
        tx_axis_fifo_tready        : out std_logic;
        tx_axis_fifo_tlast         : in  std_logic;
        -- MAC Control Interface
        --------------------------
        pause_req                  : in  std_logic;
        pause_val                  : in  std_logic_vector(15 downto 0);
        -- GMII Interface
        -------------------
        gmii_txd                  : out std_logic_vector(7 downto 0);
        gmii_tx_en                : out std_logic;
        gmii_tx_er                : out std_logic;
        gmii_rxd                  : in  std_logic_vector(7 downto 0);
        gmii_rx_dv                : in  std_logic;
        gmii_rx_er                : in  std_logic;
        clk_enable                : in  std_logic;
        speedis100                : out std_logic;
        speedis10100              : out std_logic;
        -- Configuration Vector
        -------------------------
        rx_configuration_vector   : in  std_logic_vector(79 downto 0);
        tx_configuration_vector   : in  std_logic_vector(79 downto 0);
        data_monitor_out          : out std_logic_vector(7 downto 0));
end component;

component temac_10_100_1000_reset_sync
    port ( 
        reset_in           : in  std_logic;    -- Active high asynchronous reset
        enable             : in  std_logic;
        clk                : in  std_logic;    -- clock to be sync'ed to
        reset_out          : out std_logic);     -- "Synchronised" reset signal
end component;

component temac_10_100_1000_config_vector_sm is
port(
  gtx_clk                 : in  std_logic;
  gtx_resetn              : in  std_logic;
  mac_speed               : in  std_logic_vector(1 downto 0);
  update_speed            : in  std_logic;
  rx_configuration_vector : out std_logic_vector(79 downto 0);
  tx_configuration_vector : out std_logic_vector(79 downto 0));
end component;

component phy_reset is
port(  
    clk_in       		  : in    std_logic;		
    phy_rstn_out 		  : out   std_logic
);	
end component;

component FIFO2UDP is
port(
    clk_fifo                    : in std_logic; -- Clock used for writing to fifo
    clk_udp                     : in std_logic; -- Clock used for UDP interface
    reset                       : in  std_logic;
    destinationIP               : in std_logic_vector(31 downto 0);
    data_in                     : in  std_logic_vector(31 downto 0); -- Data to be written to fifo with wr_en
    wr_en                       : in  std_logic;
    end_packet                  : in  std_logic; -- Indicates this is the last word in the packet
    udp_txi                     : out udp_tx_type; -- type to go into the UDP block
    udp_tx_start                : out std_logic; -- Signal to form packet header in UDP block
    control                     : out std_logic; -- Control vector to UDP
    udp_tx_data_out_ready       : in  std_logic; -- Signal from UDP to indicate it can accept data
    sending_o                   : out std_logic; -- Signal to indicate this module is completing a transaction
    fifo_full_info              : out std_logic_vector(1 downto 0)
);
end component;

component ETH_RX is
port(
    clk_udp             : in  std_logic;
    clk_fifo            : in  std_logic;
    reset               : in  std_logic;
    
    udp_rx              : in  udp_rx_type;
    
--    -- Signals to bind IP
--    bind                : out std_logic;
--    bind_ip             : out std_logic_vector(31 downto 0);
      
    fifo_rd_en          : in  std_logic;
    fifo_empty          : out std_logic;
    fifo_valid          : out std_logic;
    fifo_dout           : out std_logic_vector(8 downto 0); 
    fifo_full_rd_en     : in  std_logic;
    fifo_full_empty     : out std_logic
);
end component;

begin

	glbl_rstn	     <= not glbl_rst_i;
	phy_int          <= '1';
	
gen_vector_reset: process (clk125_core)
    begin
     if clk125_core'event and clk125_core = '1' then
       if vector_reset = '1' then
         vector_pre_resetn  <= '0';
         vector_resetn      <= '0';
       else
         vector_pre_resetn  <= '1';
         vector_resetn      <= vector_pre_resetn;
       end if;
     end if;
    end process gen_vector_reset;

    mmcm_reset <= glbl_rst_i; -- reset;


ethernet_core: gig_ethernet_wrapper
port map(
    independent_clock       => clk_200, -- CHANGED from 125 MHz
    gtrefclk_p              => refclk125_p,
    gtrefclk_n              => refclk125_n,
    rxuserclk2              => open,
    txp                     => txp,
    txn                     => txn,
    rxp                     => rxp,
    rxn                     => rxn,
    sgmii_clk               => open, --clk125_core, -- 125 MHz Ref clock
    clk125_o                => clk125_core, -- 125 MHz Ref clock
    sgmii_clk_en            => clk_enable, -- needed by temac core
    gmii_txd                => gmii_txd,
    gmii_tx_en              => gmii_tx_en,
    gmii_tx_er              => gmii_tx_er,
    gmii_rxd                => gmii_rxd,
    gmii_rx_dv              => gmii_rx_dv,
    gmii_rx_er              => gmii_rx_er,
    configuration_vector    => "10000",
    an_interrupt            => open, -- maybe can be used?
    an_adv_config_vector    => "1111111000000001",-- Alternate interface to program REG4 (AN ADV)
    an_restart_config       => an_restart_config, -- Alternate signal to modify AN restart bit in REG0
    speed_is_10_100         => speed_is_10_100,
    speed_is_100            => speed_is_100,
    status_vector           => status_vector,
    reset                   => glbl_rst_i,
    signal_detect           => '1'
);

glbl_rst_i <= glbl_rst;

process(clk125_core, local_gtx_reset)
    begin
        if (local_gtx_reset = '1') then 
            an_restart_config <= '1';
        else
            an_restart_config <= '0';
        end if;
end process;

tri_fifo: temac_10_100_1000_fifo_block
    port map(
      gtx_clk                    => clk125_core,
      -- asynchronous reset
      glbl_rstn                  => glbl_rstn,
      rx_axi_rstn                => '1',
      tx_axi_rstn                => '1',
      -- Receiver Statistics Interface
      -----------------------------------------
      rx_reset                   => rx_reset,
      rx_statistics_vector       => open,
      rx_statistics_valid        => open,
      -- Receiver (AXI-S) Interface
      ------------------------------------------
      rx_fifo_clock              => clk125_core,
      rx_fifo_resetn             => gtx_resetn,
      rx_axis_fifo_tdata         => rx_axis_mac_tdata,
      rx_axis_fifo_tvalid        => rx_axis_mac_tvalid,
      rx_axis_fifo_tready        => rx_axis_mac_tready,
      rx_axis_fifo_tlast         => rx_axis_mac_tlast,
      -- Transmitter Statistics Interface
      --------------------------------------------
      tx_reset                   => tx_reset,
      tx_ifg_delay               => x"00",
      tx_statistics_vector       => open,
      tx_statistics_valid        => open,
      -- Transmitter (AXI-S) Interface
      ---------------------------------------------
      tx_fifo_clock              => clk125_core,
      tx_fifo_resetn             => gtx_resetn,
      tx_axis_fifo_tdata         => tx_axis_mac_tdata,
      tx_axis_fifo_tvalid        => tx_axis_mac_tvalid,
      tx_axis_fifo_tready        => tx_axis_mac_tready,
      tx_axis_fifo_tlast         => tx_axis_mac_tlast,
      -- MAC Control Interface
      --------------------------
      pause_req                  => '0',
      pause_val                  => x"0000",
      -- GMII Interface
      -------------------
      gmii_txd                  => gmii_txd,
      gmii_tx_en                => gmii_tx_en,
      gmii_tx_er                => gmii_tx_er,
      gmii_rxd                  => gmii_rxd,
      gmii_rx_dv                => gmii_rx_dv,
      gmii_rx_er                => gmii_rx_er,
	  clk_enable			    => clk_enable,
      speedis100                => speed_is_100,
      speedis10100              => speed_is_10_100,
      -- Configuration Vector
      -------------------------
      rx_configuration_vector   => rx_configuration_vector, -- x"0605_0403_02da_0000_2022",
      tx_configuration_vector   => tx_configuration_vector, -- x"0605_0403_02da_0000_2022"
      data_monitor_out          => data_monitor_out  
);
	
	-- Control vector reset
axi_lite_reset_gen: temac_10_100_1000_reset_sync
   port map (
       clk                      => clk125_core,
       enable                   => '1',
       reset_in                 => glbl_rst_i,
       reset_out                => vector_reset);	
		
config_vector: temac_10_100_1000_config_vector_sm
    port map(
      gtx_clk                   => clk125_core,
      gtx_resetn                => vector_resetn,    
      mac_speed                 => status_vector(11 downto 10), -- "10",
      update_speed              => '1',    
      rx_configuration_vector   => rx_configuration_vector,
      tx_configuration_vector   => tx_configuration_vector);

   -----------------------------------------------------------------------------
   -- GMII transmitter data logic
   -----------------------------------------------------------------------------		
	local_gtx_reset <= glbl_rst_i or rx_reset or tx_reset;
	
gtx_reset_gen: temac_10_100_1000_reset_sync
    port map (
       clk              => clk125_core,
       enable           => '1',
       reset_in         => local_gtx_reset,
       reset_out        => gtx_clk_reset);

gen_gtx_reset: process (clk125_core)
   begin
     if clk125_core'event and clk125_core = '1' then
       if gtx_clk_reset = '1' then
         gtx_pre_resetn   <= '0';
         gtx_resetn       <= '0';
       else
         gtx_pre_resetn   <= '1';
         gtx_resetn       <= gtx_pre_resetn;
       end if;
     end if;
   end process gen_gtx_reset;

UDP_block: UDP_Complete_nomac
	Port map(
			udp_tx_start				=> udp_tx_start,
			udp_txi						=> udp_txi, 
			udp_tx_result				=> open,
			udp_tx_data_out_ready	    => udp_tx_data_out_ready, 
			udp_rx_start				=> udp_header,		-- indicates receipt of udp header
			udp_rxo						=> udp_rx,
			ip_rx_hdr					=> ip_rx_hdr,	
			rx_clk						=> clk125_core,
			tx_clk						=> clk125_core,
			reset 						=> glbl_rst_i,
			our_ip_address 			    => myIP,
			our_mac_address 			=> myMAC,
			control						=> control,
			arp_pkt_count				=> open,
			ip_pkt_count				=> open,
			mac_tx_tdata         	    => tx_axis_mac_tdata,
			mac_tx_tvalid        	    => tx_axis_mac_tvalid,
			mac_tx_tready        	    => tx_axis_mac_tready,
			mac_tx_tfirst        	    => open,
			mac_tx_tlast         	    => tx_axis_mac_tlast,
			mac_rx_tdata         	    => rx_axis_mac_tdata,
			mac_rx_tvalid        	    => rx_axis_mac_tvalid,
			mac_rx_tready        	    => rx_axis_mac_tready,
			mac_rx_tlast         	    => rx_axis_mac_tlast);

phy_reset_module: phy_reset
	   port map(  clk_in       			=> clk_80,
		          phy_rstn_out 			=> phy_rstn_out_i
              );

UDP_Sending: FIFO2UDP
port map(
    clk_fifo                => clk_80, -- Clock used for writing to fifo
    clk_udp                 => clk125_core, -- Clock used for UDP interface
    reset                   => glbl_rst_i,
    destinationIP           => destIP,
    data_in                 => to_eth_tx.data_in, -- Data to be written to fifo with wr_en
    wr_en                   => to_eth_tx.wr_en,
    end_packet              => to_eth_tx.end_packet, -- Indicates this is the last word in the packet
    udp_txi                 => udp_txi, -- type to go into the UDP block
    udp_tx_start            => udp_tx_start, -- Signal to form packet header in UDP block
    control                 => control.ip_controls.arp_controls.clear_cache, -- Control to UDP
    udp_tx_data_out_ready   => udp_tx_data_out_ready, -- Signal from UDP to indicate it can accept data
    sending_o               => open, -- Signal to indicate this module is completing a transaction
    fifo_full_info          => txt_fifo_full_info    
    

);

recieve: ETH_RX
    port map (
        clk_udp         => clk125_core,
        clk_fifo        => clk_80,
        reset           => reset,
        udp_rx          => udp_rx,
   
--        bind            => bind,
--        bind_ip         => bind_ip,
        
        fifo_rd_en      => rxr_fifo_rd_en,
        fifo_empty      => rxr_fifo_empty,
        fifo_valid      => rxr_fifo_valid,
        
        fifo_dout       => rxr_fifo_dout,
        
        
        fifo_full_rd_en => rxr_fifo_full_rd_en,
        fifo_full_empty => rxr_fifo_full_empty
        
    );



---- Process to bind the IP adress
--bind_process: process(clk125_core, bind, bind_ip)
--begin
--    if rising_edge(clk125_core) then
--        if (bind = '1') then
--            destIP <= bind_ip;
--        end if;
--    end if;
--end process;

phy_rstn_out                <= phy_rstn_out_i;
clk_125                     <= clk125_core;


end Behavioral;
