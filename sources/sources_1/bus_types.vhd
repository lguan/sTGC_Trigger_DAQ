--	Purpose: This package defines types for use for internal communication


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

package bus_types is


    -- Define type for interfacing between modules and the TX MUX
    type TX_mod2mux is record
        request             : std_logic;
        data_in             : std_logic_vector(31 downto 0);
        end_packet          : std_logic;
        wr_en               : std_logic;
    end record;

    -- Define type for interfacing between the TX MUX and fifo2udp
    type TX_mux2eth is record
        data_in             : std_logic_vector(31 downto 0);
        end_packet          : std_logic;
        wr_en               : std_logic;
    end record;


end bus_types;

