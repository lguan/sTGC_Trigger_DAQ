----------------------------------------------------------------------------------
-- Company: Michigan
-- Engineer: Reid Pinkham
-- 
-- Create Date: 11/09/2016 04:25:38 PM
-- Module Name: tx_mux
-- Target Devices: Virtex-7 VC707
-- Tool Versions: Vivado 2016.2
-- Changelog:
-- 09.11.2016 Created File (Reid Pinkham)
-- 15.03.2017 Modified for SCA (Reid Pinkham)
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.bus_types.all;

entity tx_mux is
    Port (
        clk                     : in  std_logic;
        reset                   : in  std_logic;

        -- Signals to the ethernet components
        to_eth_tx               : out TX_mux2eth;
        hold                    : in std_logic;

        -- Signals to each module
        from_rxmux_tx              : in  TX_mod2mux;
        grant_rxmux             : out std_logic;
        from_SCA_tx             : in  TX_mod2mux;
        grant_SCA               : out std_logic;
        txt_fifo_full_info          : in std_logic_vector(1 downto 0)
    );
end tx_mux;

architecture Behavioral of tx_mux is

signal st_is            : std_logic_vector(3 downto 0);
signal mod_sel          : std_logic_vector(3 downto 0);
signal grant            : std_logic:='0';
signal grant_rxmux_r    : std_logic:='0';
signal grant_SCA_r      : std_logic:='0';
signal end_packet_i     : std_logic;
signal wr_en_i          : std_logic;
signal data_in_i        : std_logic_vector(31 downto 0);
signal st_full          : std_logic_vector(1 downto 0);


signal txt_fifo_full_mark    :std_logic_vector(1 downto 0);
signal txt_fifo_full    :std_logic;
signal fifo_full_record          : std_logic_vector(1 downto 0);
signal        from_txinner_tx         : TX_mod2mux;
signal        grant_txinner           : std_logic;
signal reply_cnt             : integer range 0 to 2;

signal tx_full_request  :std_logic;


type state is (idle, sending, end_packet);
signal st               : state;


component ila_tx_mux
    port (
        clk             : in std_logic;
        probe0          : in std_logic_vector(31 downto 0);
        probe1          : in std_logic_vector(31 downto 0);
        probe2          : in std_logic_vector(31 downto 0);
        probe3          : in std_logic;
        probe4          : in std_logic;
        probe5          : in std_logic;
        probe6          : in std_logic;
        probe7          : in std_logic;
        probe8          : in std_logic;
        probe9          : in std_logic_vector(3 downto 0);
        probe10         : in std_logic_vector(3 downto 0);
        probe11         : in std_logic;
        probe12         : in std_logic_vector(1 downto 0)
    );
end component;

begin

-- Main FSM to control the recieveing of packets
main_FSM: process(clk, reset, st, hold, from_rxmux_tx.request, end_packet_i,txt_fifo_full_info)
begin
    if rising_edge(clk) then
        if (reset = '1') then -- give control back to rx logic
            st                  <= idle;
        else
            case st is
                when idle =>
                    st_is <= x"0";
                    if (hold = '1') then
                        st <= idle;
                    else
                        if (txt_fifo_full_info = "00") then
                        -- Hierarchy of inputs
                            if (from_rxmux_tx.request = '1') then -- reply packet
                                mod_sel     <= x"1";
                                grant       <= '1';
                                st          <= sending;
                            elsif (from_SCA_tx.request = '1') then -- SCA
                                mod_sel     <= x"2";
                                grant       <= '1';
                                st          <= sending;
                            else
                                mod_sel     <= x"0";
                                st          <= idle;
                            end if;
                        else
                            if (tx_full_request = '1') then 
                                mod_sel     <= x"3";
                                grant       <= '1';
                                st          <= sending;                            
                            else                               
                                mod_sel     <= x"0";
                                st          <= idle;
                            end if;
                         end if;
                    end if;

                when sending => -- wait for last packet signal
                    st_is <= x"1";
                    if (end_packet_i = '1') then -- end of packet
                        grant   <= '0';
                        mod_sel <= x"0";
                        st      <= end_packet;
                    else
                        grant   <= '1';
                        mod_sel <= mod_sel;
                        st      <= sending;
                    end if;
    
                when end_packet => -- make sure the request is low
                    st_is <= x"2";
                    st      <= idle;
                    mod_sel <= x"0"; -- idle state

                when others =>
                    st <= idle;

            end case;
        end if;
    end if;
end process;


-- Main FSM to control the recieveing of packets
Full_monitering_FSM: process(clk, reset, st_full,grant_txinner)
begin
    if rising_edge(clk) then
        if (reset = '1') then -- give control back to rx logic
            st_full    <= "00";
        else
            case st_full is
                when "00" =>    
                    from_txinner_tx.wr_en       <= '0';
                    from_txinner_tx.end_packet  <= '0';
                    if txt_fifo_full= '1' then
                       st_full    <= "01";
                       tx_full_request <= '1';
                       fifo_full_record <= txt_fifo_full_info;
                    else 
                        st_full    <= "00";
                        tx_full_request <= '0';
                        fifo_full_record <= "00";
                    end if;
                 
                when "01" =>
                    reply_cnt <= 0;
                    if (grant_txinner = '1') then
                        tx_full_request <= '0';
                        st_full    <= "10";
                    else 
                        tx_full_request <= '1';
                        st_full    <= "01";
                    end if;
                    
                when "10" =>
                    tx_full_request <= '0';
                    from_txinner_tx.wr_en   <= '1';
                    reply_cnt <= reply_cnt + 1;
                    if reply_cnt = 0 then
                        from_txinner_tx.data_in     <= x"22222bad";
                        st_full    <= "10";
                        from_txinner_tx.end_packet  <= '0';
                    elsif reply_cnt = 1 then
                        from_txinner_tx.data_in     <= x"badbad" & "000000"& fifo_full_record ; -- reply
                        st_full    <= "10";
                        from_txinner_tx.end_packet  <= '0';
                    else
                        from_txinner_tx.data_in     <= x"badbad" & "000000"& fifo_full_record ;
                        st_full    <= "00";
                        from_txinner_tx.end_packet  <= '1';
                    end if;   
                
                when others =>     st_full    <= "00";    
            end case;
        end if;
    end if;
    
   
end process;



fifo_full_marker: process (clk,txt_fifo_full_mark) -- flag at the rising edge
begin
    if rising_edge(clk) then
        txt_fifo_full_mark(0) <= txt_fifo_full_info(0) or txt_fifo_full_info(1);
        txt_fifo_full_mark(1) <= txt_fifo_full_mark(0);
    end if;

    if (txt_fifo_full_mark (0) = '1') and (txt_fifo_full_mark (1) = '0') then 
        txt_fifo_full<= '1';
    else 
        txt_fifo_full<= '0';
    end if;
end process;


--===================================================================


-- Implement the multiplexer
mux: process(mod_sel,from_rxmux_tx,from_SCA_tx,from_txinner_tx,grant)
begin
    case mod_sel is
        when x"0" => -- idle state
            end_packet_i    <= '0';
            wr_en_i         <= '0';
            data_in_i       <= x"00000000";
            grant_rxmux     <= '0';
            grant_sca       <= '0';
            grant_txinner   <= '0';

        when x"1" => -- rxmux 
            end_packet_i    <= from_rxmux_tx.end_packet;
            wr_en_i         <= from_rxmux_tx.wr_en;
            data_in_i       <= from_rxmux_tx.data_in;
            grant_rxmux     <= grant;
            grant_sca       <= '0';
            grant_txinner   <= '0';

        when x"2" => -- SCA
            end_packet_i    <= from_SCA_tx.end_packet;
            wr_en_i         <= from_SCA_tx.wr_en;
            data_in_i       <= from_SCA_tx.data_in;
            grant_SCA       <= grant;
            grant_rxmux     <= '0';
            grant_txinner   <= '0';

        when x"3" => -- inner tx (sending fifo full)
            end_packet_i    <= from_txinner_tx.end_packet;
            wr_en_i         <= from_txinner_tx.wr_en;
            data_in_i       <= from_txinner_tx.data_in;
            grant_txinner   <= grant;
            
        when others =>
            end_packet_i    <= '0';
            wr_en_i         <= '0';
            data_in_i       <= x"00000000";
            grant_rxmux     <= '0';
            grant_SCA       <= '0';
            grant_txinner     <= '0';

    end case;
end process;


--ila_TX_mux_inst: ila_tx_mux
--port map (
--    clk         => clk,
--    probe0      => data_in_i,
--    probe1      => from_rxmux_tx.data_in,
  
--    probe2      => from_SCA_tx.data_in,
--    probe3      => from_SCA_tx.wr_en,
--    probe4      => from_rxmux_tx.wr_en,
   
--    probe5      => from_SCA_tx.end_packet,
--    probe6      => from_rxmux_tx.end_packet,

--    probe7      => grant,
--    probe8      => from_rxmux_tx.request,
--    probe9      => st_is,
--    probe10     => mod_sel,
   
--    probe11     => from_SCA_tx.request,
--    probe12     => txt_fifo_full_info
--);



-- Assign outputs
to_eth_tx.end_packet <= end_packet_i;
to_eth_tx.wr_en      <= wr_en_i;
to_eth_tx.data_in    <= data_in_i;

end Behavioral;
