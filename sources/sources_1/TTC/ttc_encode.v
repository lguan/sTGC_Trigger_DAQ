`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: U of M
// Engineer: Liang Guan
// 
// Create Date: 05/15/2017 03:37:51 PM
// Design Name: 
// Module Name: ttc_encode
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: Encoding/serializing 8 bit TTC information
// 
// Dependencies: This module encodes the trigger input, test pulse into 320 Mbps TTC_stream 
//               needed by ROC
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ttc_encode(

    input CLK40,
    input CLK320,
    input wire EC0R, // Reset L0 ID, not used in ROC
    input wire SCA_RST,
    input wire TRIG_i,//L0,L1 trigger
    input wire BCR_OCR, 
    input wire ECR, //reset L1/trigger counter
    input wire VMM_TESTPULSE_ena,
    input wire ROC_SoftRST,
    
    input wire TTC_ena,//block trigger during initialization or set mannual deadtime
    output wire TTC_STREAM_o
    );
    
    
    
    reg [7:0] ttc_stream_r;
    reg [0:0] ttc_data_r;
    reg [0:0] load;
    
    
    always @ (posedge CLK40) begin
        if (TRIG_i==1'b1) load <=1'b1;
    end
    
    always @ (posedge CLK320) begin
        if (TTC_ena==1'b0) begin
            ttc_stream_r<='b0;
        end
        else begin
            if (load==1'b1) begin
                ttc_data_r<='b0;
            end
            else begin
                ttc_data_r <= ttc_stream_r[7];
                ttc_stream_r <= {ttc_stream_r[6:0],1'b0};
            end
        end
    end
    
    
   assign  TTC_STREAM_o = ttc_data_r;
    
    
    
endmodule
