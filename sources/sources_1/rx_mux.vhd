----------------------------------------------------------------------------------
-- Company: Michigan
-- Engineer: Reid Pinkham
-- 
-- Create Date: 10/24/2016 04:23:29 PM
-- Module Name: rx_mux
-- Target Devices: Virtex-7 VC705
-- Tool Versions: Vivado 2016.2
-- Changelog:
-- 24.10.2016 Created File (Reid Pinkham)
-- 15.03.2017 Modified for SCA (Reid Pinkham)
-- 08.06.2017 Changed clock to 80Mhz with hold command and fifo full warining function (Xiong)
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.bus_types.all;

entity rx_mux is
    Port (
        clk                     : in  std_logic;
        reset                   : in  std_logic;
        
        -- data comes from ethernet. 
        rxr_fifo_rd_en          : out std_logic;
        rxr_fifo_empty          : in  std_logic;
        rxr_fifo_dout           : in  std_logic_vector(8 downto 0);
        rxr_fifo_full_rd_en     : out std_logic;
        rxr_fifo_full_empty     : in std_logic;
        rxr_fifo_valid          : in std_logic;
        
        
        -- interface  between tx mux
        from_rxmux_tx         : out TX_mod2mux;
        grant_rxmux           : in  std_logic;
        
        -- interface between SCA
        cfgFIFO_wren        : out  std_logic;    
        cfgFIFO_din         : out  std_logic_vector(7 downto 0);
        cfgFIFO_data_length : out  std_logic_vector(7 downto 0);
        cfgFIFO_wr_done     : out  std_logic;    
        cfg_busy            : in  std_logic    
        
);
end rx_mux;

architecture Behavioral of rx_mux is

signal st_is            : std_logic_vector(5 downto 0);
signal mod_sel          : std_logic_vector(3 downto 0);
signal packet_recv      : std_logic;
signal module_done      : std_logic;
signal cnt              : unsigned(3 downto 0);

signal crap_din         : std_logic_vector(8 downto 0);
signal header_r         : std_logic_vector(31 downto 0);
signal header           : std_logic_vector(31 downto 0);
signal command_r        : std_logic_vector(31 downto 0);
signal command          : std_logic_vector(31 downto 0);
signal sca_command      : std_logic_vector(31 downto 0);
signal hmark            : std_logic;


signal sca_dlength_cnt       : unsigned(7 downto 0);
signal cfgFIFO_wren_r        : std_logic;    
signal cfgFIFO_din_r         : std_logic_vector(7 downto 0);
signal cfgFIFO_data_length_r : std_logic_vector(7 downto 0);
signal cfgFIFO_wr_done_r     : std_logic;    
signal unknow_cmd            : std_logic;  

signal rxr_fifo_rd_en_r      : std_logic;   

signal burst_packet          : std_logic:='0';  
signal reply_cnt             : integer range 0 to 2;

signal send_reply            : std_logic;   
signal reply_done            : std_logic;   
signal reply_state           : std_logic_vector(3 downto 0);


--signal rxr_fifo_full_mark    : std_logic_vector(1 downto 0);

signal rxr_fifo_full         : std_logic;
signal rxr_fifo_full_r       : std_logic;

signal full_state            : std_logic;
signal rxr_fifo_full_request : std_logic;
signal rxr_fifo_full_ack     : std_logic;
signal send_cmd_ack          : std_logic;
signal send_cmd_request      : std_logic;
signal send_time_out_ack          : std_logic;
signal send_time_out_request      : std_logic;

signal timer1_state          : std_logic;
signal timer2_state          : std_logic;
signal timer_cnt1            : unsigned(11 downto 0);
signal timer_cnt2            : unsigned(11 downto 0);
signal time_out              : std_logic;
signal timer_start           : std_logic;


component ila_rx_mux
    port (
        clk             : in std_logic;
        probe0          : in std_logic;
        probe1          : in std_logic_vector(7 downto 0);
        probe2          : in std_logic_vector(7 downto 0);
        probe3          : in std_logic;
        probe4          : in std_logic;
        probe5          : in std_logic_vector(31 downto 0);
        probe6          : in std_logic_vector(31 downto 0);
        probe7          : in std_logic_vector(31 downto 0);
        probe8          : in std_logic_vector(31 downto 0);
        probe9          : in std_logic_vector(5 downto 0);
        probe10         : in std_logic;
        probe11         : in std_logic;
        probe12         : in std_logic_vector(8 downto 0);
        probe13         : in std_logic;
        probe14         : in unsigned(3 downto 0);
        probe15         : in std_logic_vector(8 downto 0);
        probe16         : in std_logic;
        probe17         : in std_logic;
        probe18         : in unsigned(7 downto 0);
        probe19         : in std_logic_vector(3 downto 0);
        probe20         : in std_logic;
        probe21         : in std_logic;
        probe22         : in std_logic;
        probe23         : in std_logic;
        probe24         : in std_logic;
        probe25         : in std_logic;
        probe26         : in unsigned(11 downto 0);
        probe27         : in unsigned(11 downto 0)
    );
end component;


type state is (idle, st_command,st_filter,st_header_start, st_header,st_process,st_SCA_c,st_SCA_s,st_SCA_h,st_SCA_f,st_crap,st_clearing,st_reply_r,st_reply_d,st_time_out_r,st_time_out_d);
signal st               : state;

begin
-- Main FSM to control the recieveing of packets
main_FSM: process(clk, reset, st, rxr_fifo_empty)
begin
    if rising_edge(clk) then
        if (reset = '1') then -- give control back to rx logic
            st                  <= idle;
            packet_recv         <= '0';
            mod_sel             <= (others => '0');

        else
            case st is
                when idle =>
                    st_is <= "000000";--0                    
                    if rxr_fifo_empty = '0' then  -- Monitor if date is coming     
                         st <= st_header_start;
                         rxr_fifo_rd_en_r <='1';                                                              
                    else
                        st <= idle;
                        rxr_fifo_rd_en_r <='0';
                    end if;
                when st_header_start =>  --recive the first 8bit of the header
                    st_is <= "100000";--should re-order later                    
                    
                    if rxr_fifo_valid = '1' then
                        header_r(7 downto 0) <= rxr_fifo_dout(7 downto 0);
                        hmark <= rxr_fifo_dout(8);
                        st <= st_header;
                        rxr_fifo_rd_en_r <='1';
                        cnt<= x"0";
                    else 
                        st <= st_header_start;
                        rxr_fifo_rd_en_r <='0';
                    end if;

                when st_header => -- Receiving header
                    
                    st_is <= "000001";--1
                    cfgFIFO_wr_done_r <= '0';
                    
                    if rxr_fifo_empty = '1' then --crap
                        rxr_fifo_rd_en_r <='0';
                        st <= idle;
                    elsif rxr_fifo_valid = '1' then
                        if rxr_fifo_dout(8) = not hmark then
                            header_r(7 downto 0) <= rxr_fifo_dout(7 downto 0); -- the previos is crap and with another packet right behind it!
                            hmark <= rxr_fifo_dout(8);
                            rxr_fifo_rd_en_r <='1';
                            cnt<= x"0";
                            st <= st_header;
                        elsif rxr_fifo_valid = '1' then 
                            if (cnt <= x"2") then 
                                rxr_fifo_rd_en_r <='1';
                                header_r(7 downto 0) <= rxr_fifo_dout(7 downto 0);
                                header_r(31 downto 8) <= header_r(23 downto 0);
                                st <= st_header;
                                cnt<= cnt+1;
                            else
                                command_r(7 downto 0) <= rxr_fifo_dout(7 downto 0);
                                header <= header_r;
                                st <= st_command;
                            cnt<= x"0";
                            end if;
                        end if;
                     else 
                        st <= st_header;
                        rxr_fifo_rd_en_r <='0';
                     end if;

                when st_command => -- Receiving command
                    st_is <= "000010";--2                             
                    if rxr_fifo_empty = '1' then 
                        if (cnt = x"3") then     --cmd without following data
                            rxr_fifo_rd_en_r <='0';
                            command <= command_r;
                            st <= st_filter;
                            cnt<= x"0";                            
                        else ---crap
                            rxr_fifo_rd_en_r <='0';
                            st <= idle;
                        end if;
                    elsif rxr_fifo_valid = '1' then
                        if rxr_fifo_dout(8) = not hmark then
                            header_r(7 downto 0) <= rxr_fifo_dout(7 downto 0); -- the previos is crap and with another packet right behind it!
                            hmark <= rxr_fifo_dout(8);
                            rxr_fifo_rd_en_r <='1';
                            cnt<= x"0";
                            st <= st_header;
                        elsif (cnt <= x"1") then 
                            rxr_fifo_rd_en_r <='1';
                            command_r(7 downto 0) <= rxr_fifo_dout(7 downto 0);
                            command_r(31 downto 8)<= command_r(23 downto 0);
                            st <= st_command;
                            cnt<= cnt+1;
                        elsif (cnt = x"2") then     
                            rxr_fifo_rd_en_r <='0';
                            command_r(7 downto 0) <= rxr_fifo_dout(7 downto 0);
                            command_r(31 downto 8)<= command_r(23 downto 0);
                            st <= st_command;
                            cnt<= cnt+1;                               
                        else 
                            rxr_fifo_rd_en_r <='0';
                            command <= command_r;
                            st <= st_filter;
                            cnt<= x"0";                      
                        end if ;
                    else 
                        st <= st_command;
                        rxr_fifo_rd_en_r <='0';
                    end if;
                  
                  
             when st_filter => -- Processing command
                   st_is <= "000011";--3
                   case command(15 downto 0) is
                        when  x"0100"|x"0200"|x"0300" =>  --need to add all the commands here
                             st <= st_reply_r;
                             send_cmd_request  <= '1';
                             unknow_cmd <= '0';
                         when others  =>   
                             st <= st_reply_r;
                             unknow_cmd <= '1';
                   end case;                 
                   
               when st_reply_r => --echo request
                   st_is <="000100";--4
                   send_cmd_request  <= '1';
                   if (send_cmd_ack = '1') then 
                       st    <= st_reply_d;
                   else
                        st    <= st_reply_r;
                   end if;
     
               when st_reply_d => --waiting for echo finish
                   st_is <="000101";--5
                   send_cmd_request  <= '0'; 
                   if (reply_done = '1') then -- finished sending
                       if unknow_cmd = '1'  then 
                         st   <= st_crap;                      
                       else
                          st   <= st_process;
                       end if;
                       
                   else
                       st   <= st_reply_d;
                   end if;                    
                                                        
                when st_process => -- Processing command
                    st_is <= "000110"; --6
                    case command(15 downto 0) is
                        when x"0100"|x"0200"|x"0300" =>   st <= st_SCA_c; timer_start <= '1';
                                          
                        
                        
                        when others  =>   st <= st_crap;
                                          rxr_fifo_rd_en_r <='1';
                    end case;
 
                when st_crap => -- crap packet 
                    st_is <= "000111";--7
                    if rxr_fifo_empty = '1' then 
                          rxr_fifo_rd_en_r <='0';
                          st <= st_clearing;
                    else        
                      if rxr_fifo_dout(8) = (not hmark) then
                             header_r(7 downto 0) <= rxr_fifo_dout(7 downto 0);
                             hmark <= rxr_fifo_dout(8);
                             rxr_fifo_rd_en_r <='1';
                             st <= st_header;
                      else
                             rxr_fifo_rd_en_r <='1';
                             crap_din <= rxr_fifo_dout;
                             st <= st_crap;
                      end if;
                    end if;
                    
                 when st_SCA_c => -- Check if SCA is busy
                    st_is <= "001000";--8
                    timer_start <= '0';
                    if  time_out = '1' then
                        st <= st_time_out_r;                  
                    elsif cfg_busy = '1' then 
                        st <= st_SCA_c;                             
                    else 
                        st <= st_SCA_h;
                        sca_dlength_cnt <= x"00";
                        sca_command <= command;
                    end if;
               
                 
                 when st_SCA_h => -- sending header
                    st_is <="001001";--9
                    if cnt <= x"2" then 
                        cfgFIFO_din_r <= sca_command(31 downto 24);
                        cfgFIFO_wren_r <='1';
                        sca_command(31 downto 8) <=  sca_command (23 downto 0 );
                        cnt <= cnt +1;
                        st <= st_SCA_h;
                    else   
                        cfgFIFO_din_r <= sca_command(31 downto 24);
                        cfgFIFO_wren_r <='1';
                        rxr_fifo_rd_en_r <='1';
                        cnt <= x"0"; 
                        st <= st_SCA_s;
                    end if;
                 
                 when st_SCA_s => -- Sending data to SCA
                    st_is <="001010";--a
                    if rxr_fifo_empty = '1' then 
                          cfgFIFO_wr_done_r <= '1';
                          rxr_fifo_rd_en_r <='0';
                          cfgFIFO_wren_r <='0';
                          st <= st_clearing;  -- finished sending. 
                          cfgFIFO_data_length_r <= std_logic_vector(sca_dlength_cnt);
                    elsif rxr_fifo_valid = '1' then
                        if (rxr_fifo_dout(8) = not hmark) then
                              cfgFIFO_wr_done_r <= '1';
                              header_r(7 downto 0) <= rxr_fifo_dout(7 downto 0);
                              hmark <= rxr_fifo_dout(8);
                              rxr_fifo_rd_en_r <='1';
                              cfgFIFO_wren_r <='0';
                              cfgFIFO_data_length_r <= std_logic_vector(sca_dlength_cnt);
                              st <= st_header;
                        else
                              sca_dlength_cnt <= sca_dlength_cnt + 1;
                              rxr_fifo_rd_en_r <='1';
                              cfgFIFO_wren_r <='1';
                              cfgFIFO_din_r  <= rxr_fifo_dout(7 downto 0);
                              st <= st_SCA_s;
                             end if;
                    else 
                        rxr_fifo_rd_en_r <='0';
                        st <= st_SCA_s;                                
                    end if;
                    
                when st_time_out_r => --echo request
                        st_is <="100001";--x21
                        send_time_out_request  <= '1';
                        if (send_time_out_ack = '1') then 
                            st    <= st_time_out_d;
                        else
                             st    <= st_time_out_r;
                        end if;
          
                when st_time_out_d => --waiting for echo finish
                        st_is <="100011";--x22
                        send_time_out_request  <= '0'; 
                        if (reply_done = '1') then -- finished sending
                              st   <= st_crap;                                   
                        else
                            st   <= st_time_out_d;
                        end if;  
                        
                  when st_clearing => --clear and reset signal and register
                    st_is <="001100";--c
                    cfgFIFO_wr_done_r <= '0';
                    header_r  <= (others=>'0');
                    command_r <= (others=>'0');
                    header    <= (others=>'0');
                    command   <= (others=>'0');
                    st <= idle;

                when others =>
                    st <= idle;

            end case;
        end if;
    end if;
end process;


cfgFIFO_wren        <= cfgFIFO_wren_r;
cfgFIFO_din         <= cfgFIFO_din_r;
cfgFIFO_data_length <= cfgFIFO_data_length_r;
cfgFIFO_wr_done     <= cfgFIFO_wr_done_r;

rxr_fifo_rd_en      <= rxr_fifo_rd_en_r;


rxr_fifo_full_moniter: process (clk, rxr_fifo_full_empty)
begin

if rising_edge(clk) then
    
    if (reset = '1') then
        full_state         <= '0';
    else 
        case full_state is 
            when '0' =>
                if rxr_fifo_full_empty = '0' then 
                    rxr_fifo_full_request <= '1';
                    rxr_fifo_full_rd_en <= '1';
                    full_state <= '1';
                else 
                    rxr_fifo_full_request <= '0';
                    rxr_fifo_full_rd_en <= '0';
                    full_state <= '0';
                end if;
            
            when '1' =>
                rxr_fifo_full_rd_en <= '0';
                if (rxr_fifo_full_ack = '1') then 
                    rxr_fifo_full_request <= '0';
                    full_state <= '0';
                else 
                    rxr_fifo_full_request <= '1';
                    full_state <= '1';
                end if;
                
            when others => full_state <= '0';
        end case;
    end if;            
end if;

end process;



timer: process (clk, timer_start)
begin

if rising_edge(clk) then   
    if (reset = '1') then
        timer1_state    <= '0';
        timer_cnt1 <= x"000";
    else 
        case timer1_state is 
            when '0' =>        
                if timer_start = '1' then 
                    timer_cnt1 <= timer_cnt1 +1;          
                    timer1_state <= '1';
                else      
                    timer_cnt1 <= x"000";
                    timer1_state <= '0';
                end if;
            
            when '1' =>
                if (time_out = '1') then 
                    timer1_state <= '0';
                else 
                    timer_cnt1 <= timer_cnt1 +1; 
                    timer1_state <= '1';
                end if;
                
            when others => full_state <= '0';
        end case;
    end if;            
end if;


if rising_edge(clk) then 
    if (reset = '1') then
        timer_cnt2 <= x"000";
        timer2_state <= '0';
    else
        case timer2_state is 
        when '0' =>
            time_out <= '0';
            if timer_cnt1 = x"fff" then 
                 timer_cnt2 <= timer_cnt2 +1;
                 timer2_state <= '1';
            else    
                 timer_cnt2 <= x"000";
                 timer2_state <= '0';
            end if;
        when '1' =>
            if timer_cnt2 = x"7a1" then -- undering 80Mhz clocking, it's about 0.1s time out holding time.
                time_out <= '1';
                timer2_state <= '0';
            elsif timer_cnt1 = x"fff" then 
                timer_cnt2 <= timer_cnt2 +1;
                timer2_state <= '1';
            else 
                timer2_state <= '1';
            end if;
        when others => timer2_state <= '0';
        end case;
            
    end if;
end if;
end process;


reply_fsm: process (clk, reply_state, grant_rxmux,send_reply)
begin
if rising_edge(clk) then
    if (reset = '1') then
        reply_state         <= x"0";
        from_rxmux_tx.request <= '0';
    else
        case reply_state is -- FSM to send reply packet
            when x"0" => -- Wait for signal to begin  
                reply_done      <= '0';
                if (rxr_fifo_full_request = '1') then
                    rxr_fifo_full_ack <= '1';
                    reply_state <= x"1";               
                elsif (send_cmd_request = '1') then -- start
                    reply_state <= x"1";
                    send_cmd_ack <= '1';
                    from_rxmux_tx.request <= '1'; -- request access
                elsif (send_time_out_request = '1') then -- start
                    reply_state <= x"1";
                    send_time_out_ack <= '1';
                    from_rxmux_tx.request <= '1'; -- request access
                else
                    reply_state <= x"0";
                end if;

            when x"1" => -- Wait for access to be granted
                reply_cnt <= 0;
                if (grant_rxmux = '1') then -- granted, can start transaction
                    if (send_cmd_ack = '1') then
                        reply_state <= x"2";
                    elsif (rxr_fifo_full_ack = '1') then    
                        reply_state <= x"3";
                    else 
                        reply_state <= x"4";
                    end if;
                else
                    from_rxmux_tx.request <= '1'; -- request access
                    reply_state <= x"1";
                end if;

            when x"2" => -- Send data to fifo with data_out_last
                from_rxmux_tx.request <= '0';
                from_rxmux_tx.wr_en   <= '1';
                reply_cnt <= reply_cnt + 1;
                if reply_cnt = 0 then
                    if unknow_cmd = '1'  then 
                        from_rxmux_tx.data_in     <= x"11111bad";
                    else
                        from_rxmux_tx.data_in     <= x"11111111";
                    end if;
                    reply_state             <= x"2";
                    from_rxmux_tx.end_packet  <= '0';
                elsif reply_cnt = 1 then
                    from_rxmux_tx.data_in     <= header; -- reply
                    reply_state             <= x"2";
                    from_rxmux_tx.end_packet  <= '0';
                else
                    from_rxmux_tx.data_in     <= command;
                    reply_state             <= x"5";
                    from_rxmux_tx.end_packet  <= '1';
                end if;
                
            
            when x"3" => -- fifo full
                from_rxmux_tx.request <= '0';
                from_rxmux_tx.wr_en   <= '1';
                reply_cnt <= reply_cnt + 1;
                if reply_cnt = 0 then
                    from_rxmux_tx.data_in     <= x"11111bad";
                    reply_state             <= x"3";
                    from_rxmux_tx.end_packet  <= '0';
                elsif reply_cnt = 1 then
                    from_rxmux_tx.data_in     <= x"badbad00"; -- reply
                    reply_state             <= x"3";
                    from_rxmux_tx.end_packet  <= '0';
                else
                    from_rxmux_tx.data_in     <= x"badbad00";
                    reply_state             <= x"5";
                    from_rxmux_tx.end_packet  <= '1';
                end if;            
            
           when x"4" => -- time out
                    from_rxmux_tx.request <= '0';
                    from_rxmux_tx.wr_en   <= '1';
                    reply_cnt <= reply_cnt + 1;
                    if reply_cnt = 0 then
                        from_rxmux_tx.data_in     <= x"11111bad";
                        reply_state             <= x"4";
                        from_rxmux_tx.end_packet  <= '0';
                    elsif reply_cnt = 1 then
                        from_rxmux_tx.data_in     <= x"badbad01"; -- reply
                        reply_state             <= x"4";
                        from_rxmux_tx.end_packet  <= '0';
                    else
                        from_rxmux_tx.data_in     <= x"badbad01";
                        reply_state             <= x"5";
                        from_rxmux_tx.end_packet  <= '1';
                    end if; 
                        
                        
            when x"5" => --zero signals
                rxr_fifo_full_ack <= '0';
                send_cmd_ack <= '0';
                send_time_out_ack <= '0';
                from_rxmux_tx.wr_en       <= '0';
                from_rxmux_tx.end_packet  <= '0';
                reply_state             <= x"0";
                reply_done      <= '1';


            when others => reply_state <= x"0";
        end case;
    end if;
end if;
end process;


--ila_RX_mux_inst: ila_rx_mux
--port map (
--    clk         => clk,
--    probe0      => cfgFIFO_wren_r,
--    probe1      => cfgFIFO_din_r,
--    probe2      => cfgFIFO_data_length_r,
--    probe3      => cfgFIFO_wr_done_r,
--    probe4      => cfg_busy,
--    probe5      => command,
--    probe6      => command_r,
--    probe7      => header,
--    probe8      => header_r,
--    probe9      => st_is,
--    probe10     => rxr_fifo_empty,
--    probe11     => rxr_fifo_rd_en_r,
--    probe12      => rxr_fifo_dout,
--    probe13      => hmark,
--    probe14      => cnt,
--    probe15      => crap_din,
--    probe16      => rxr_fifo_dout(8),
--    probe17      => burst_packet,
--    probe18      => sca_dlength_cnt,   
--    probe19      => reply_state,
--    probe20      => full_state,
--    probe21      => rxr_fifo_full_empty,
--    probe22      => timer1_state,
--    probe23      => timer2_state, 
--    probe24      => time_out,
--    probe25      => timer_start,
--    probe26      => timer_cnt1,
--    probe27      => timer_cnt2
--);

end Behavioral;
