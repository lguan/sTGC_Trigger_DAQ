`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/22/2017 10:24:45 PM
// Design Name: 
// Module Name: SCA_RX
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SCA_RX(
	Reset_ni,
	CLK80M_i,
	BitStream_i,
	SCARX_FIFO_WEn_o,
	SCARX_FIFO_WData_o
);


input wire	Reset_ni;
input wire	CLK80M_i;
input wire	BitStream_i;
output wire	SCARX_FIFO_WEn_o;
output wire	[9:0] SCARX_FIFO_WData_o;

wire	Ph2_BitStream;
wire	Ph2_BitValid;
wire	Ph4_BitValid;
wire	[7:0] Ph4_ByteStream;
wire	Ph4_Start;
wire	Ph4_Stop;
wire	[2:0] Ph6_BitCounter;
wire	Ph6_FrameValid;





SCA_RX_Ph2_BitValid	b2v_inst(
	.Reset_ni(Reset_ni),
	.CLK80M_i(CLK80M_i),
	.BitStream_i(BitStream_i),
	.Ph2_BitStream_o(Ph2_BitStream),
	.Ph2_BitValid_o(Ph2_BitValid));


SCA_RX_Ph6_FrameValid	b2v_inst4(
	.Reset_ni(Reset_ni),
	.CLK80M_i(CLK80M_i),
	.Ph4_BitValid_i(Ph4_BitValid),
	.Ph4_Start_i(Ph4_Start),
	.Ph4_Stop_i(Ph4_Stop),
	.Ph6_FrameValid_o(Ph6_FrameValid),
	.Ph6_BitCounter_o(Ph6_BitCounter));


SCA_RX_Ph4_Start	b2v_inst5(
	.Reset_ni(Reset_ni),
	.CLK80M_i(CLK80M_i),
	.Ph2_BitStream_i(Ph2_BitStream),
	.Ph2_BitValid_i(Ph2_BitValid),
	.Ph4_BitValid_o(Ph4_BitValid),
	.Ph4_Start_o(Ph4_Start),
	.Ph4_Stop_o(Ph4_Stop),
	.Ph4_ByteStream_o(Ph4_ByteStream));


SCA_RX_Ph6_WriteFIFO	b2v_inst7(
	.Reset_ni(Reset_ni),
	.CLK80M_i(CLK80M_i),
	.Ph4_Start_i(Ph4_Start),
	.Ph4_Stop_i(Ph4_Stop),
	.Ph6_FrameValid_i(Ph6_FrameValid),
	.Ph4_ByteStream_i(Ph4_ByteStream),
	.Ph6_BitCounter_i(Ph6_BitCounter),
	.Ph6_FIFO_WEn_o(SCARX_FIFO_WEn_o),
	.Ph6_FIFO_WData_o(SCARX_FIFO_WData_o));


endmodule



module SCA_RX_Ph2_BitValid
(
	input Reset_ni,
	input CLK80M_i,
	input BitStream_i,
	
	output Ph2_BitStream_o,	
	output Ph2_BitValid_o
);

	assign Ph2_BitStream_o =Ph2Sha_StuffShiftReg[0];
	assign Ph2_BitValid_o  =Ph2Ent_BitValid;
	
	
	reg[6:0] Ph1Ent_StuffShiftReg;
	reg[6:0] Ph2Sha_StuffShiftReg;
	
	reg Ph2Ent_BitValid;
	
	
	//////******Ph1Ent_StuffShiftReg******//////
	always@(posedge CLK80M_i or negedge Reset_ni)
	begin
		if(~Reset_ni)
		begin
			Ph1Ent_StuffShiftReg[6:0]<=7'b000_0000;
		end
		else
		begin
			Ph1Ent_StuffShiftReg[6:0]<={Ph2Sha_StuffShiftReg[5:0],BitStream_i};
		end
	end
	always@(negedge CLK80M_i or negedge Reset_ni)
	begin
		if(~Reset_ni)
		begin
			Ph2Sha_StuffShiftReg[6:0]<=7'b000_0000;
		end
		else
		begin
			Ph2Sha_StuffShiftReg[6:0]<=Ph1Ent_StuffShiftReg[6:0];
		end
	end
	
	//////******Ph2Ent_BitValid******//////
	always@(negedge CLK80M_i or negedge Reset_ni)
	begin
		if(~Reset_ni)
		begin
			Ph2Ent_BitValid<=1'b0;
		end
		else
		begin
			if(Ph1Ent_StuffShiftReg[6:0]==7'b011_1110)
			begin
				Ph2Ent_BitValid<=1'b0;
			end
			else
			begin
				Ph2Ent_BitValid<=1'b1;
			end
		end
	end
	
	
	
endmodule









module SCA_RX_Ph4_Start
(
	input Reset_ni,
	input CLK80M_i,
	
	input Ph2_BitStream_i,	
	input Ph2_BitValid_i,
	
	output [7:0] Ph4_ByteStream_o,
	output       Ph4_BitValid_o,
	output       Ph4_Start_o,
	output       Ph4_Stop_o
);

	
	
	
	reg[14:0] Ph3Ent_MainShiftReg;
	reg[14:0] Ph4Sha_MainShiftReg;
	
	reg[7:0] Ph3Ent_ByteShiftReg;
	reg[7:0] Ph4Sha_ByteShiftReg;
	
	wire Ph3_wMainShiftReg_Idle;
	wire Ph3_wMainShiftReg_SOF;
	
	assign Ph3_wMainShiftReg_Idle=(Ph3Ent_MainShiftReg[14:8]==7'b111_1111)?1'b1:1'b0;
	assign Ph3_wMainShiftReg_SOF =(Ph3Ent_MainShiftReg[7:0] ==8'b0111_1110)?1'b1:1'b0;
	
	reg Ph3Ent_BitValid;	
	reg Ph4Sha_BitValid;
	
	reg Ph4Ent_Start;	
	reg Ph4Ent_Stop;	
	
	assign Ph4_ByteStream_o[7:0]=Ph4Sha_ByteShiftReg[7:0];
	assign Ph4_BitValid_o =Ph4Sha_BitValid;	
	assign Ph4_Start_o =Ph4Ent_Start;
	assign Ph4_Stop_o  =Ph4Ent_Stop;
	
	
	//////******Ph3Ent_MainShiftReg******//////
	always@(posedge CLK80M_i or negedge Reset_ni)
	begin
		if(~Reset_ni)
		begin
			Ph3Ent_MainShiftReg[14:0]<=15'b000_0000_0000_0000;
		end
		else
		begin
		   Ph3Ent_MainShiftReg[14:0]<={Ph4Sha_MainShiftReg[13:0],Ph2_BitStream_i};			
		end
	end
	always@(negedge CLK80M_i or negedge Reset_ni)
	begin
		if(~Reset_ni)
		begin
			Ph4Sha_MainShiftReg[14:0]<=15'b000_0000_0000_0000;
		end
		else
		begin
			Ph4Sha_MainShiftReg[14:0]<=Ph3Ent_MainShiftReg[14:0];
		end
	end
	
	//////******Ph3Ent_ByteShiftReg******//////
	always@(posedge CLK80M_i or negedge Reset_ni)
	begin
		if(~Reset_ni)
		begin
			Ph3Ent_ByteShiftReg[7:0]<=8'b0000_0000;
		end
		else
		begin
		   if(Ph2_BitValid_i)
			begin
				Ph3Ent_ByteShiftReg[7:0]<={Ph4Sha_ByteShiftReg[6:0],Ph2_BitStream_i};
			end
			else
			begin
				Ph3Ent_ByteShiftReg[7:0]<=Ph4Sha_ByteShiftReg[7:0];
			end
		end
	end
	always@(negedge CLK80M_i or negedge Reset_ni)
	begin
		if(~Reset_ni)
		begin
			Ph4Sha_ByteShiftReg[7:0]<=8'b0000_0000;
		end
		else
		begin
			Ph4Sha_ByteShiftReg[7:0]<=Ph3Ent_ByteShiftReg[7:0];
		end
	end
	
	//////******Ph4Ent_Start******//////
	always@(negedge CLK80M_i or negedge Reset_ni)
	begin
		if(~Reset_ni)
		begin
			Ph4Ent_Start<=1'b0;
		end
		else
		begin			
			Ph4Ent_Start<=(Ph3_wMainShiftReg_Idle&Ph3_wMainShiftReg_SOF);
		end
	end
	
	
	//////******Ph4Ent_Stop******//////
	always@(negedge CLK80M_i or negedge Reset_ni)
	begin
		if(~Reset_ni)
		begin
			Ph4Ent_Stop<=1'b0;
		end
		else
		begin			
			Ph4Ent_Stop<=((~Ph3_wMainShiftReg_Idle)&Ph3_wMainShiftReg_SOF);
		end
	end
	
	
	
	//////******Ph3Ent_BitValid******//////
	always@(posedge CLK80M_i or negedge Reset_ni)
	begin
		if(~Reset_ni)
		begin
			Ph3Ent_BitValid<=1'b0;
		end
		else
		begin
			Ph3Ent_BitValid<=Ph2_BitValid_i;
		end
	end
	always@(negedge CLK80M_i or negedge Reset_ni)
	begin
		if(~Reset_ni)
		begin
			Ph4Sha_BitValid<=1'b0;
		end
		else
		begin
			Ph4Sha_BitValid<=Ph3Ent_BitValid;
		end
	end
	
	
endmodule






module SCA_RX_Ph6_FrameValid
(
	input Reset_ni,
	input CLK80M_i,
	
	input Ph4_BitValid_i,
	input Ph4_Start_i,
	input Ph4_Stop_i,
	
	output       Ph6_FrameValid_o,
	output [2:0] Ph6_BitCounter_o
);


	reg Ph5Ent_FrameValid;
	reg Ph6Sha_FrameValid;
	
	
	reg Ph6Ent_FrameTrueValid;
	
	reg Ph5Ent_BitValid;
	
	reg [2:0] Ph6Ent_BitCounter;
	reg [2:0] Ph7Sha_BitCounter;
	
	assign Ph6_FrameValid_o =Ph6Ent_FrameTrueValid;
	assign Ph6_BitCounter_o[2:0] =Ph6Ent_BitCounter[2:0];
	
	
	//////******Ph5Ent_FrameValid******//////
	always@(posedge CLK80M_i or negedge Reset_ni)
	begin
		if(~Reset_ni)
		begin
			Ph5Ent_FrameValid<=1'b0;
		end
		else
		begin
		   case({Ph4_Stop_i,Ph4_Start_i})
			2'b00:begin
				Ph5Ent_FrameValid<=Ph6Sha_FrameValid;
			end
			
			2'b01:begin
				Ph5Ent_FrameValid<=1'b1;
			end
			
			2'b10:begin
				Ph5Ent_FrameValid<=1'b0;
			end
			
			2'b11:begin
				Ph5Ent_FrameValid<=1'b0;
			end
			endcase			
		end
	end
	always@(negedge CLK80M_i or negedge Reset_ni)
	begin
		if(~Reset_ni)
		begin
			Ph6Sha_FrameValid<=1'b0;
		end
		else
		begin
			Ph6Sha_FrameValid<=Ph5Ent_FrameValid;
		end
	end
	
	
	//////******Ph5Ent_BitValid******//////
	always@(posedge CLK80M_i or negedge Reset_ni)
	begin
		if(~Reset_ni)
		begin
			Ph5Ent_BitValid<=1'b0;
		end
		else
		begin
		Ph5Ent_BitValid<=Ph4_BitValid_i;
		end
	end
	
	
	//////******Ph6Ent_BitCounter******//////
	always@(negedge CLK80M_i or negedge Reset_ni)
	begin
		if(~Reset_ni)
		begin
			Ph6Ent_BitCounter[2:0]<=3'b000;
		end
		else
		begin
			if(Ph5Ent_FrameValid)
			begin
				Ph6Ent_BitCounter[2:0]<=Ph7Sha_BitCounter[2:0]+Ph5Ent_BitValid;
			end
			else
			begin
				Ph6Ent_BitCounter[2:0]<=3'b000;
			end
		end
	end
	always@(posedge CLK80M_i or negedge Reset_ni)
	begin
		if(~Reset_ni)
		begin
			Ph7Sha_BitCounter[2:0]<=3'b000;
		end
		else
		begin
		   Ph7Sha_BitCounter[2:0]<=Ph6Ent_BitCounter[2:0];		
		end
	end
	
	
	//////******Ph6Ent_FrameTrueValid******//////
	always@(negedge CLK80M_i or negedge Reset_ni)
	begin
		if(~Reset_ni)
		begin
			Ph6Ent_FrameTrueValid<=1'b0;
		end
		else
		begin
			Ph6Ent_FrameTrueValid<=Ph5Ent_BitValid & Ph5Ent_FrameValid;
		end
	end
	
endmodule










module SCA_RX_Ph6_WriteFIFO
(
	input Reset_ni,
	input CLK80M_i,
	
	input [7:0]  Ph4_ByteStream_i,
	input        Ph4_Start_i,
	input        Ph4_Stop_i,
	input        Ph6_FrameValid_i,
	input [2:0]  Ph6_BitCounter_i,
	
	output       Ph6_FIFO_WEn_o,
	output [9:0] Ph6_FIFO_WData_o
);
	
	
	
	reg       Ph5Ent_FrameValid;	
	reg [9:0] Ph5Ent_Data;
	reg       Ph5Ent_ByteValid;	
	
	reg       Ph6Ent_FIFO_WEn;
	reg [9:0] Ph6Ent_FIFO_WData;
	reg [9:0] Ph7Sha_FIFO_WData;
	
	assign Ph6_FIFO_WEn_o=Ph6Ent_FIFO_WEn;
	assign Ph6_FIFO_WData_o[9:0] =Ph6Ent_FIFO_WData[9:0];
	
	
	//////******Ph5Ent_FrameValid******//////
	always@(posedge CLK80M_i or negedge Reset_ni)
	begin
		if(~Reset_ni)
		begin
			Ph5Ent_FrameValid<=1'b0;
		end
		else
		begin
			Ph5Ent_FrameValid<=(Ph4_Start_i|Ph6_FrameValid_i);
		end
	end
	
	
	
	//////******Ph5Ent_Data******//////
	always@(posedge CLK80M_i or negedge Reset_ni)
	begin
		if(~Reset_ni)
		begin
			Ph5Ent_Data[9:0]<=10'b00_0000_0000;
		end
		else
		begin
			Ph5Ent_Data[9:0]<={Ph4_Stop_i,Ph4_Start_i,Ph4_ByteStream_i[7:0]};
		end
	end
	
	
	//////******Ph5Ent_ByteValid******//////
	always@(posedge CLK80M_i or negedge Reset_ni)
	begin
		if(~Reset_ni)
		begin
			Ph5Ent_ByteValid<=1'b0;
		end
		else
		begin
			if(Ph6_BitCounter_i==3'b000)
			begin
				Ph5Ent_ByteValid<=1'b1;
			end
			else
			begin
				Ph5Ent_ByteValid<=1'b0;
			end
		end
	end
	
	
	//////******Ph6Ent_FIFO_WEn******//////
	always@(negedge CLK80M_i or negedge Reset_ni)
	begin
		if(~Reset_ni)
		begin
			Ph6Ent_FIFO_WEn<=1'b0;
		end
		else
		begin
			if(Ph5Ent_FrameValid & Ph5Ent_ByteValid)
			begin
				Ph6Ent_FIFO_WEn<=1'b1;
			end
			else
			begin
				Ph6Ent_FIFO_WEn<=1'b0;
			end
		end
	end
	
	
	//////******Ph6Ent_FIFO_WData******//////
	always@(negedge CLK80M_i or negedge Reset_ni)
	begin
		if(~Reset_ni)
		begin
			Ph6Ent_FIFO_WData[9:0]<=10'b00_0000_0000;
		end
		else
		begin
			if(Ph5Ent_FrameValid & Ph5Ent_ByteValid)
			begin
				Ph6Ent_FIFO_WData[9:0]<=Ph5Ent_Data[9:0];
			end
			else
			begin
				Ph6Ent_FIFO_WData[9:0]<=Ph7Sha_FIFO_WData[9:0];
			end
		end
	end
	always@(posedge CLK80M_i or negedge Reset_ni)
	begin
		if(~Reset_ni)
		begin
			Ph7Sha_FIFO_WData[9:0]<=10'b00_0000_0000;
		end
		else
		begin
			Ph7Sha_FIFO_WData[9:0]<=Ph6Ent_FIFO_WData[9:0];
		end
	end
	
	
endmodule