`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/06/2017 09:30:35 AM
// Design Name: 
// Module Name: SCA_RX_DWORD
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments: Pulls and formats word from SCA_RX fifo
// 
//////////////////////////////////////////////////////////////////////////////////


module SCA_RX_DWORD
(
    input           clk80,
    input           fifo_full,
    input           fifo_empty,
    input   [9:0]   fifo_dout,
    input           fifo_valid,
    output          fifo_rd_en,
    output          new_hdlc_word,
    output  [7:0]   hdlc_rx_error_o,
    output  [31:0]  hdlc_rx_data,
    input [4:0] st_cfg_debug
);


reg                 fifo_rd_en_r;
reg     [139:0]     dout_buf; // buffer to hold dynamic fifo data. Max length of 14 bytes
reg     [7:0]       hdlc_byte [13:0]; // buffer to hold bytes of the HDLC frame
reg     [3:0]       hdlc_len;
reg     [7:0]       hdlc_control;
reg     [7:0]       hdlc_channel;
reg     [7:0]       hdlc_error;
reg                 hdlc_valid;
reg     [1:0]       hdlc_valid_vec;

assign fifo_rd_en = fifo_rd_en_r;
assign new_hdlc_word = (hdlc_valid_vec == 2'b10) ? 1'b1 : 1'b0; // if rising edge, assign one
assign hdlc_rx_error_o = hdlc_error;  
assign hdlc_rx_data = {hdlc_byte[5], hdlc_byte[6], hdlc_byte[3], hdlc_byte[4]};


// Process to write contents to buffer
always @(posedge clk80)
begin
    if (fifo_valid == 1'b1) // fifo is reading (HDLC reply frame: LSByte first), data is valid 
        begin
            dout_buf <= {fifo_dout, dout_buf[139:10]};
        end
    else
        dout_buf <= dout_buf;
end

// Process to read from fifo
always @(posedge clk80)
begin
    if (fifo_empty == 1'b0) // data present
        fifo_rd_en_r <= 1'b1;
    else
        fifo_rd_en_r <= 1'b0;
end


// Need to minitor FIFO to find the location of the HDLC word
always @(posedge clk80)
begin
    if (dout_buf[139:130] == 10'h27E) // buffer has a complete packet in it, save to byte array (8'h7E is SOF/EOF character)
        begin
            hdlc_valid <= 1'b1;
            hdlc_byte[0] <= dout_buf[137:130];
            hdlc_byte[1] <= dout_buf[127:120];
            hdlc_byte[2] <= dout_buf[117:110];
            hdlc_byte[3] <= dout_buf[107:100];
            hdlc_byte[4] <= dout_buf[97:90];
            hdlc_byte[5] <= dout_buf[87:80];
            hdlc_byte[6] <= dout_buf[77:70];
            hdlc_byte[7] <= dout_buf[67:60];
            hdlc_byte[8] <= dout_buf[57:50];
            hdlc_byte[9] <= dout_buf[47:40];
            hdlc_byte[10] <= dout_buf[37:30];
            hdlc_byte[11] <= dout_buf[27:20];
            hdlc_byte[12] <= dout_buf[17:10];
            hdlc_byte[13] <= dout_buf[7:0];

            // Need to determine the length of the packet. Find the 2-bit marker (2'b01 indicate the start of a HDLC frame data)
            if (dout_buf[89:88] == 2'b01) // Smallest packet size
                hdlc_len <= 4'h6;
            else if (dout_buf[49:48] == 2'b01) // No data contents
                hdlc_len <= 4'ha;
            else if (dout_buf[39:38] == 2'b01) // 1 byte data
                hdlc_len <= 4'hb;
            else if (dout_buf[29:28] == 2'b01) // 2 byte data
                hdlc_len <= 4'hc;
            else if (dout_buf[19:18] == 2'b01) // 3 byte data
                hdlc_len <= 4'hd;
            else if (dout_buf[9:8] == 2'b01) // 4 byte data
                hdlc_len <= 4'he;
            else // Error
                hdlc_len <= 4'h0;
        end
    else
        hdlc_valid <= 1'b0;
end


// Use the size of the packet and update content regs based on that
always @(posedge clk80)
begin
    case (hdlc_len)
        4'h0 : // error, fill the error field
            begin
                hdlc_error <= 8'hFF;
                hdlc_control <= 8'h0;
                hdlc_channel <= 8'h0;
            end
        4'h6 : // smallest packet size
            begin
                hdlc_error <= 8'h0; // no real error field, just fill it with zeros
                hdlc_channel <= 8'h0; // same as error
                hdlc_control <= hdlc_byte[3];
            end
        4'ha : // no data
            begin
                hdlc_error <= hdlc_byte[4];
                hdlc_channel <= hdlc_byte[5];
                hdlc_control <= hdlc_byte[7];
            end
        4'hb : // 1 byte
            begin
                hdlc_error <= hdlc_byte[5];
                hdlc_channel <= hdlc_byte[6];
                hdlc_control <= hdlc_byte[8];
            end
        4'hc : // 2 bytes
            begin
                hdlc_error <= hdlc_byte[6];
                hdlc_channel <= hdlc_byte[7];
                hdlc_control <= hdlc_byte[9];
            end
        4'hd : // 3 bytes
            begin
                hdlc_error <= hdlc_byte[7];
                hdlc_channel <= hdlc_byte[8];
                hdlc_control <= hdlc_byte[10];
            end
        4'he : // 4 bytes
            begin
                hdlc_error <= hdlc_byte[8];
                hdlc_channel <= hdlc_byte[9];
                hdlc_control <= hdlc_byte[11];
            end

        default : // Error
            hdlc_error <= 8'h0; // no real error field, just fill it with zeros
    endcase
end

// process to generate a pulse for every new frame
always @(posedge clk80)
begin
    hdlc_valid_vec <= {hdlc_valid, hdlc_valid_vec[1]};
end




//ILA to monitor data
//ila_sca_rx_dword ila_sca_rx_dword_inst
//(
//    .clk(clk80),
//    .probe0(fifo_full),
//    .probe1(fifo_empty),
//    .probe2(fifo_valid),
//    .probe3(fifo_rd_en_r),
//    .probe4(fifo_dout),
//    .probe5(dout_buf),
//    .probe6({hdlc_byte[13],hdlc_byte[12],hdlc_byte[11],hdlc_byte[10],hdlc_byte[9],hdlc_byte[8],hdlc_byte[7],hdlc_byte[6],hdlc_byte[5],hdlc_byte[4],hdlc_byte[3],hdlc_byte[2],hdlc_byte[1],hdlc_byte[0]}),
//    .probe7(hdlc_len),
//    .probe8(hdlc_error),
//    .probe9(hdlc_channel),
//    .probe10(hdlc_control),
//    .probe11(new_hdlc_word),
//    .probe12(st_cfg_debug)
//);


endmodule