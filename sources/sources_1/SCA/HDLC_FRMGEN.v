`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Liang Guan guanl@umich.edu
// 
// Create Date: 02/20/2017 04:23:00 PM
// Design Name: 
// Module Name: HDLC_FRMGEN
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description:  Prepares 16-bit words to be pushed to the FIFO in order to assemble a HDLC data frame to GBT-SCA.
//               The 16-bit word consists of a data byte (ADDRESS,CONTROL,CHANNEL,etc.), required to assemble a HDLC frame,
//               and two extra bits at MSB end to indicate whether the data byte is the first or last byte to be used
//               for assembly    
//
//               10-bit word structure is shown as below:
//                    9     8      7-0
//                   STOP  START  [8-bit Data]
//
//               Refer to SCA Mannaul and wikipedia for HDLC protocol informaiton                 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module HDLC_FRMGEN(

    input wire clk80,
    input wire rst_b,
    input wire start,
    input wire [99:0] hdlc_word,
    
    output wire [9:0] FIFO_Data_o,
    output wire WEn_o,
    output wire done
    );
    

    reg [2:0] st;   
    reg [3:0] WR_NBytes_r;  //number of bytes to write into upstream FIFO
    reg [0:0] WEn_o_r;
(* KEEP = "TRUE" *) reg [9:0] FIFO_Data_o_r;
(* KEEP = "TRUE" *) reg [99:0] HDLC_WORD_r;

    reg DONE_r;
          
    assign done=DONE_r;
    
    assign WEn_o = WEn_o_r;
    assign FIFO_Data_o = FIFO_Data_o_r;


    parameter st_idle=3'b000, st1=3'b001, st2=3'b010, st3=3'b011, st4=3'b100, st5=3'b101, st6=3'b110;
    
    always @ (negedge clk80 ) begin //Write data on the next posedge after WEn HIGH
        if (rst_b == 1'b0) begin
            FIFO_Data_o_r <= 'b0;
            HDLC_WORD_r <= 'b0;
            WR_NBytes_r <= 'b0;
            WEn_o_r <= 1'b0;
        end
        else begin
        case (st)
            st_idle:begin 
                    FIFO_Data_o_r <= 'b0;
                    HDLC_WORD_r <= 'b0;
                    WR_NBytes_r <= 'b0;
                    WEn_o_r <= 1'b0;
                    DONE_r <=1'b0;
                    if (start == 1'b1) st <= st1;
                    else st <= st_idle; 
                    end
            st1:begin
                DONE_r <=1'b0;
                HDLC_WORD_r <= hdlc_word;
                st <= st2;
            end        
            st2:begin
                DONE_r <=1'b0;
                if (HDLC_WORD_r[97:90]== 8'hFF) WR_NBytes_r <=4'h2;  //Supervisor Frame, Send 2 Bytes
                else if (HDLC_WORD_r[97:90]<= 8'h16) begin 
                    WR_NBytes_r <= 4'hA; //Information Frame, Send 10 Bytes
                    end
                else WR_NBytes_r <= 4'h0;
                st <=st3;    
                end              
            st3:begin //load Bytes to FIFO Write
                if (WR_NBytes_r!= 'b0) begin
                FIFO_Data_o_r <= HDLC_WORD_r[99:90];
                HDLC_WORD_r[99:90] <= HDLC_WORD_r[89:80];
                HDLC_WORD_r[89:80] <= HDLC_WORD_r[79:70];
                HDLC_WORD_r[79:70] <= HDLC_WORD_r[69:60];
                HDLC_WORD_r[69:60] <= HDLC_WORD_r[59:50];
                HDLC_WORD_r[59:50] <= HDLC_WORD_r[49:40];
                HDLC_WORD_r[49:40] <= HDLC_WORD_r[39:30];
                HDLC_WORD_r[39:30] <= HDLC_WORD_r[29:20];
                HDLC_WORD_r[29:20] <= HDLC_WORD_r[19:10];
                HDLC_WORD_r[19:10] <= HDLC_WORD_r[9:0]; 
                WEn_o_r <= 1'b1;       
                st <= st4;
                end
                else st <= st5;
                end    
            st4:begin 
                WEn_o_r <= 1'b0;
                WR_NBytes_r <= WR_NBytes_r - 1'b1;
                st <= st5;
                end
            st5:begin
                WEn_o_r <= 1'b0;
                if (WR_NBytes_r != 'b0) st <= st3;
                else st <= st6;
                end
            st6:begin
                DONE_r <=1'b1;
                WEn_o_r <= 1'b0;                    
                st <= st6;
                //State Machine Stops Here
                if (start == 1'b0) st <=st_idle;
                end
            default: st <=st_idle;
        endcase
        end
    end

endmodule


//      vio_0 vio_0_inst (
//            .clk(clk80),
////          .probe_in0(HDLC_Recv),
//            .probe_out0(START),
//            .probe_out1(RST_B),
//            .probe_out2(HDLC_WORD)
//      );
