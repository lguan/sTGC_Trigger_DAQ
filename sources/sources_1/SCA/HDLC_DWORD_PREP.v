`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: U of Michigan
// Engineer: Liang Guan (guanl@umich.edu) and Reid Pinkham (pinkhamr@umich.edu)
// Create Date: 03/15/2017 10:38:49 AM
// Module Name: HDLC_DWORD_PREP
// Target Devices: 
// Tool Versions: vivado 2016.2
// Description: [] This module pulls out ASIC configuration bitstream from FIFO and prepares data words for assembling 
//                 HDLC frames needed by the SCA chip to control VMM, TDS, ROC etc. 
//              [] procedures:  
//                  init SCA --> write cfg data to cfgFIFO --> load to cfg BRAM --> Start SCA self configuration --> 
//                  load ASIC cfg data to SCA transmit buffer --> Issue cfg cmd to each reg --> check SCA RX error--> 
//                  Reply error to Etherent Tx module
//
//              [] Total number of configuration bits:
//                          VMM   18*96=1726
//                          TDS   1296 (RW)
//                          ROC   64*8=512
//    
//              [] SCA HDLC FRAME FORMAT (w/o SOF,EOF,FCS)--> Ad Ct ID Ch Le Co D1 D0 D3 D2
//                                                      (EACH BYTE SHOULD BE WRITTEN AS LSB->MSB!!!)            
//              [] error code definition
//                  [15:14] 2'b11: SCA Time Out 
//                  [13:12] 2'b11: SCA Init Error
//                  [9:8]  2'b1:SCA Error 2'b10:I2C Error 2'b11:Unrecognized ASIC Type
//                  [7:0]   SCA RX ERROR FLAG Byte(See GTB-SCA mannual)
// Revisions:
// 0.0 - File Created
// 1.1 - TDSv2 configuration verified. (20 March 2017)
// 1.2 - VMM3 configuration initial development done (Need verification) 
// 1.3 - Added SCA Rx checking: check against each HDLC TX frame for a reply from Rx (12 April 2017)
// 2.0 - Major Change: All logics, interface with Ethernet MUX in 80 MHz clock domain 
//                     Including blocks to init sca, send cfg data and reset asic (12 June 2017)
// 2.1 - Soft Reset for VMM3 works. GPIO Dout all set to 1 after SCA Init (14 June 2017) 
//
// Additional Comments:
// Note Sepcial acquisition reset needed for VMM3 (CS negative pulse followed by ENA negative pulse) 
// Still need to add one gpio bitmask register to change GPIO values from individual lines 
//////////////////////////////////////////////////////////////////////////////////

module HDLC_DWORD_PREP
(
    input clk80,
    input reset,
    
    //communicate with ethernet module
    output          send_request,
    output [31:0]   send_data,
    output          send_end_packet,
    output          send_wr_en,
    input           send_grant,
        
    input cfgFIFO_wren,
    input cfgFIFO_wr_done,    
    input [7:0] cfgFIFO_din,
    input [7:0] cfgFIFO_data_length,
    output cfg_busy,

    //communicate with HDLC frame assembler
    input           hdlcfrm_gen_grant,
    output          hdlcfrm_gen_request,
    output  [99:0]  hdlc_word,
    
    //commnunicate with SCA RX checker
    input           new_rx_word_i,
    input   [7:0]   hdlc_rx_error_i,
    input   [31:0]  hdlc_rx_data_i,

    //sca elink port multiplxer 
    output [2:0] sca_port_mux,
    
    output [4:0] st_cfg_debug
);
    
//------------------------------------------------// 
//------------ Wire, Reg Declaration -------------//
//------------------------------------------------// 

// -- Interface with Ethernet RX
reg cfg_busy_r;
reg cfgFIFO_rst_r;
reg cfgFIFO_rd_en_r;

wire [15:0] cfgFIFO_dout;
wire cfgFIFO_rd_en;
wire cfgFIFO_empty;
wire cfgFIFO_data_valid;

// -- Interface with CONFIG DATA BRAM
reg [6:0] read16b_cnt;
reg [6:0] read16b_cnt_max;
reg [2:0] st_bram;
reg [15:0]cfgCMD;
reg [15:0]config_din[0:107];

reg scaInit_start;
reg asicCfg_start;
reg asicRst_start;
reg scaInit_start_ack;
reg asicCfg_start_ack;
reg asicRst_start_ack;
reg scaInit_done;
reg asicCfg_done;
reg asicRst_done;

// -- configuration inputs
reg [2:0] sca_port_mux_r;
reg [2:0] board_id;
reg [1:0] asic_type; // 01-VMM, 10-TDS, 11-ROC
reg [7:0] asic_id; //VMM:id is bitmask  TDS:I2C ADDRESS, ROC: I2C ADDRESS

// -- sca frmgen cache parameters
wire [7:0]  spi_cs_rev;
wire [2:0]  hdlc_frmseq_rev;
wire [7:0]  sca_chanid_rev;
wire [127:0]sca_cmd_data_rev;

reg [31:0] gpiodout; 
reg [7:0]  spi_cs; //vmm_id to spi_cs chan #
reg [2:0]  hdlc_frmseq;  //0-7
reg [7:0]  sca_chanid;
reg [127:0]sca_cmd_data; // holds sca cmd data up to 4 bytes  
reg [2:0]  sca_n_32bit; //nb.of 32-bit (1 HDLC frame) words in each asic reg


genvar j,k;   // reverse bit order of a BYTE to accommodate SCA LSB-first protocol
for (j=0;j<3;j=j+1) 
begin: hdlc_frmseq_inversion
        assign hdlc_frmseq_rev[j]=hdlc_frmseq[2-j]; 
end
for (k=0;k<8;k=k+1)
begin: sca_cmd
    assign spi_cs_rev[k]=spi_cs[7-k];   
    assign sca_chanid_rev[k] = sca_chanid[7-k];
    assign sca_cmd_data_rev[127-k] =  sca_cmd_data[120+k]; 
    assign sca_cmd_data_rev[119-k] =  sca_cmd_data[112+k]; 
    assign sca_cmd_data_rev[111-k] =  sca_cmd_data[104+k]; 
    assign sca_cmd_data_rev[103-k] =  sca_cmd_data[96+k]; 
    assign sca_cmd_data_rev[95-k] =  sca_cmd_data[88+k]; 
    assign sca_cmd_data_rev[87-k] =  sca_cmd_data[80+k]; 
    assign sca_cmd_data_rev[79-k] =  sca_cmd_data[72+k]; 
    assign sca_cmd_data_rev[71-k] =  sca_cmd_data[64+k]; 
    assign sca_cmd_data_rev[63-k] =  sca_cmd_data[56+k]; 
    assign sca_cmd_data_rev[55-k] =  sca_cmd_data[48+k]; 
    assign sca_cmd_data_rev[47-k] =  sca_cmd_data[40+k];
    assign sca_cmd_data_rev[39-k] =  sca_cmd_data[32+k];
    assign sca_cmd_data_rev[31-k] =  sca_cmd_data[24+k];
    assign sca_cmd_data_rev[23-k] =  sca_cmd_data[16+k];
    assign sca_cmd_data_rev[15-k] =  sca_cmd_data[8+k];
    assign sca_cmd_data_rev[7-k]  =  sca_cmd_data[k];          
end
   
// -- sca cmd parameters;
parameter CTRL_W_CRB_rev=8'h40,         // CTRL_W_CRB = 8'h02,
          CTRL_W_CRC_rev=8'h20,         // CTRL_W_CRC = 8'h04,
          GPIO_W_INTSEL_rev=8'h0C,    // GPIO_W_INTSEL = 8'h30,
          GPIO_W_CLKSEL_rev=8'h01,    // GPIO_W_CLKSEL = 8'h80,
          GPIO_W_DIRECTION_rev=8'h04, // GPIO_W_DIRECTION = 8'h20,
          GPIO_W_DATAOUT_rev=8'h08,   // GPIO_W_DATAOUT = 8'h10,
          SPI_W_FREQ_rev = 8'h0A,     // SPI_W_FREQ = 8'h50,
          SPI_W_SS_rev=8'h06,         // SPI_W_SS =8'h60,
          SPI_W_CTRL_rev=8'h02,       // SPI_W_CTRL = 8'h40,
          SPI_W_MOSI0_rev=8'h00,      // SPI_W_MOSI0=8'h00,
          SPI_W_MOSI1_rev=8'h08,      // SPI_W_MOSI1=8'h10,
          SPI_W_MOSI2_rev=8'h04,      // SPI_W_MOSI2=8'h20,
          SPI_GO_rev=8'h4E,           // SPI_GO=8'h72,
          I2C_W_CTRL_rev=8'h0C,       // I2C_W_CTRL = 8'h30,
          I2C_W_DATA0_rev=8'h02,      // I2C_W_DATA0 = 8'h40,
          I2C_W_DATA1_rev=8'h0A,      // I2C_W_DATA1 = 8'h50,
          I2C_W_DATA2_rev=8'h06,      // I2C_W_DATA2 = 8'h60,
          I2C_W_DATA3_rev=8'h0E,      // I2C_W_DATA3 = 8'h70,
          I2C_M_7B_W_rev=8'h5B;       // I2C_M_7B_W=8'hDA,

parameter isVMM=2'b01,isTDS=2'b10,isROC=2'b11;

// -- vmm,tds,roc control registers
reg [4:0] asic_reg_addr; //0-17 for VMM3, 0-12 for TDS2
reg [2:0] vmm_ctrl_stat; //[0]-ENA_HI [1] ENA_LO [2]SS_LO [3]WR VMM CONFIG [4] SS_HI 

// -- 10-bit words to HDLC frame assmebler
reg hdlcfrm_gen_request_r;
reg [99:0] hdlc_word_scaInit; 
reg [99:0] hdlc_word_asicCfg;
reg [99:0] hdlc_word_asicRst;

assign hdlc_word = (scaInit_start_ack==1'b1)? hdlc_word_scaInit:
                   (asicCfg_start_ack==1'b1)? hdlc_word_asicCfg:
                   (asicRst_start_ack==1'b1)? hdlc_word_asicRst:100'b0;
                   
assign hdlcfrm_gen_request= hdlcfrm_gen_request_r;
assign sca_port_mux = sca_port_mux_r;

// -- interface with SCA init block 
reg [3:0] st_sca_init;
reg [3:0] st_sca_init_next;

// -- interface with HDLC frame gen module
reg [1:0]st_hdlc_frm_gen;
reg abort_w_error;
reg reset_hdlc_frm_seq;
reg genHDLCfrm4SCAInit;
reg genHDLCfrm4ASICCfg;
reg genHDLCfrm4ASICRst;
reg assemble_new_hdlc_frm_done; //present only for one clock cycle
wire assemble_new_hdlc_frm;

// -- interface with SCA RX checker 
reg [17:0]  sca_rx_timeout_cnt;
reg         new_rx_word_reset;
reg         new_rx_word;
reg [7:0]   hdlc_rx_error;
reg [31:0]  hdlc_rx_data;
reg [15:0]  error_code;

// -- Interface with Ethernet TX
reg error_reply_done;
reg [1:0]st_eth_tx;
reg [1:0]send_cnt; 
reg [31:0]send_data_r;
reg send_wr_en_r;
reg send_end_packet_r;
reg send_request_r;

wire operation_done;

assign send_wr_en=send_wr_en_r;
assign send_end_packet=send_end_packet_r;
assign send_request=send_request_r;
assign send_data=send_data_r;

// -- cfg FSM parameters
reg [4:0]   st_cfg;
reg [4:0] st_cfg_prev; 
reg [4:0] st_cfg_next;

parameter   idle=           5'h0,     
            st_vmm_entry=   5'h1,
            st_gpioDOUT=    5'h2,                                           
            st_spiSS=       5'h3,
            st_vmm_iterReg= 5'h4,  
            st_vmm_loadReg= 5'h5,
            st_vmm_issueGo= 5'h6,  
            st_vmm_exit   = 5'h7,                       
            st_tds_entry=   5'h8,
            st_i2cCTRL=     5'h9,
            st_tds_iterReg= 5'hA,
            st_tds_loadReg= 5'hB,
            st_tds_issueGo= 5'hC,
            st_roc_entry=   5'hD,
            st_roc_iterReg= 5'hE,
            st_roc_loadReg= 5'hF,
            st_roc_issueGo= 5'h10,
            st_gen_hdlcFRM= 5'h1D,
            st_chk_hdlcFRM= 5'h1E,
            st_finish=      5'h1F;

//------------------------------------------------// 
//------------ ASIC CONFIGURATION DATA FIFO-------//
//------------------------------------------------// 

  fifo_sca_cfgdata fifo_sca_cfgdata_inst (
    .clk(clk80),
    .srst(cfgFIFO_rst),
    .din(cfgFIFO_din),
    .wr_en(cfgFIFO_wren),
    .rd_en(cfgFIFO_rd_en),
    .dout(cfgFIFO_dout),
    .full(),
    .empty(cfgFIFO_empty),
    .valid(cfgFIFO_data_valid)
  );
  
assign cfg_busy = cfg_busy_r;
assign cfgFIFO_rd_en = cfgFIFO_rd_en_r;
assign cfgFIFO_rst = cfgFIFO_rst_r;

//-------------------------------------------------------//
//--------- CfgFIFO To BRAM & GENERAL CONTROL  --------- //
//-------------------------------------------------------//
//--config_din : 16x108 BRAM

always @ (posedge clk80) begin
    if (reset == 1'b1) st_bram <= 3'b0;
    else begin   
        case(st_bram)
        3'b0: begin
            {board_id,asic_type,asic_id}<={13'b0};
            cfgCMD <='b0;
            read16b_cnt <= 'b0;
            read16b_cnt_max <= 'b0;
            cfgFIFO_rst_r <= 1'b0;
            if (cfgFIFO_wr_done == 1'b1 && cfgFIFO_data_length<=8'hDC) begin //new data ready in cfgFIFO 
                cfg_busy_r <= 1'b1;
                cfgFIFO_rd_en_r <=1'b1;
                st_bram <= 3'b1;
            end
            else begin
                cfg_busy_r <= 1'b0; 
                st_bram <= 3'b0;
            end
        end
        3'b1: begin
            if (cfgFIFO_data_valid == 1'b1) begin
               case(cfgFIFO_dout[9:8])
               isVMM:read16b_cnt_max <=7'h6C;
               isTDS:read16b_cnt_max <=7'h51;
               isROC:read16b_cnt_max <=7'h08;
               default:read16b_cnt_max <='b0;
               endcase
               {board_id,asic_type,asic_id} <= cfgFIFO_dout[12:0];
               cfgFIFO_rd_en_r <= 1'b1;
               st_bram <= 3'b10;             
            end 
            else begin
                cfgFIFO_rd_en_r <= 1'b0; 
                st_bram <= 3'b1;
            end
        end
        3'b10: begin
            cfgFIFO_rd_en_r <= 1'b0;
            if (cfgFIFO_data_valid == 1'b1) begin
                cfgCMD <= cfgFIFO_dout;
                st_bram <= 3'b11;
            end
            else st_bram <= 3'b10;
        end
        3'b11: begin
            if (read16b_cnt < read16b_cnt_max) begin 
                cfgFIFO_rd_en_r <= 1'b1;
                st_bram <= 3'b100;
            end
            else st_bram <= 3'b101;
        end
        3'b100: begin
            cfgFIFO_rd_en_r <= 1'b0;
            if (cfgFIFO_data_valid == 1'b1) begin
                config_din[read16b_cnt] <= cfgFIFO_dout;
                read16b_cnt <= read16b_cnt + 1'b1;
                st_bram <= 3'b11;
            end
            else st_bram <= 3'b100;
        end
        3'b101:begin
            case (cfgCMD)
            16'h0100: begin scaInit_start <= 1'b1; st_bram <= 3'b110;end //SCA Init Request
            16'h0200: begin asicCfg_start <= 1'b1; st_bram <= 3'b110;end //NSW ASIC Configure Request 
            16'h0300: begin asicRst_start <= 1'b1; st_bram <= 3'b110;end //NSW ASIC Reset Request 
            default begin cfgFIFO_rst_r <= 1'b1; st_bram <= 3'b0;end //Unrecognized CMD, Reset FIFO, Abort
            endcase
        end
        3'b110:begin
            if ((scaInit_start_ack|asicCfg_start_ack|asicRst_start_ack) ==1'b1) begin
                {scaInit_start,asicCfg_start,asicRst_start}<=3'b0;
                st_bram <= 3'b111;
            end
            else st_bram <= 3'b110;
        end
        3'b111: begin
            cfgFIFO_rst_r <= 1'b1;
            if (error_reply_done == 1'b1) st_bram <= 3'b0;
            else st_bram <= 3'b111;
        end
        default: st_bram <= 3'b0;
        endcase
    end
end


//------------------------------------------------// 
//------------ SCA GPIO PORT Mapping -------------//
//------------------------------------------------// 
// DIRECTION:  0(INOPUT) 1(OUTPUT)
// pFEB v2.1 Configuration:
// GPIO#  Signal    Dir
//  0     GFZ_ID     1
//  1     GFZ_SDA    1
//  2     VMM_ENA0   1
//  3     VMM_ENA1   1
//  5     VMM_ENA2   1
//  6     ROC_RST    1
//  16    TDS_RST    1
//  27    PGood0     0
//  29    PGood1     0
//  30    PGood2     0
wire [7:0]sca_gpioDIR_Byte0;
wire [7:0]sca_gpioDIR_Byte1;
wire [7:0]sca_gpioDIR_Byte2;
wire [7:0]sca_gpioDIR_Byte3;
wire [7:0]sca_gpioDIR_revByte0;
wire [7:0]sca_gpioDIR_revByte1;
wire [7:0]sca_gpioDIR_revByte2;
wire [7:0]sca_gpioDIR_revByte3;

//assign sca_gpioDIR_Byte0 = 8'b11111100; 
assign sca_gpioDIR_Byte0 = 8'hFF; 
assign sca_gpioDIR_Byte1 = 8'hFF;
assign sca_gpioDIR_Byte2 = 8'hFF;
assign sca_gpioDIR_Byte3 = 8'hFF;
//assign sca_gpioDIR_Byte3 = 8'b10010111;

genvar m;
for (m=0;m<8;m=m+1)
begin: sca_gpio_dir_func
    assign sca_gpioDIR_revByte0[m] = sca_gpioDIR_Byte0[7-m];   
    assign sca_gpioDIR_revByte1[m] = sca_gpioDIR_Byte1[7-m];
    assign sca_gpioDIR_revByte2[m] = sca_gpioDIR_Byte2[7-m]; 
    assign sca_gpioDIR_revByte3[m] = sca_gpioDIR_Byte3[7-m];        
end

//-------------------------------------------------------//
//-------------------- SCA Initilization  -------------- //
//-------------------------------------------------------//

always @ (posedge clk80) begin
    if (reset==1'b1) st_sca_init <=3'b0;
    else case (st_sca_init)
        4'd0: begin 
            scaInit_done <=1'b0;
            genHDLCfrm4SCAInit <= 1'b0;
            reset_hdlc_frm_seq <=1'b0;
            st_sca_init_next<='b0;
            if (scaInit_start == 1'b1) begin              
                scaInit_start_ack <= 1'b1;  
                st_sca_init <= 4'd1; end
            else begin 
                scaInit_start_ack <= 1'b0; 
                st_sca_init <= 4'd0; end
        end
        4'd1: begin // CONNECT to SCA pri e-port 
            hdlc_word_scaInit[99:0] <= {20'h7FEF4,80'h0};
            reset_hdlc_frm_seq <=1'b1;
            st_sca_init_next <=4'd2;
            st_sca_init<=4'd14; 
        end 
        4'd2: begin // RESET SCA
            hdlc_word_scaInit[99:0] <= {20'h7FEF1,80'h0}; 
            reset_hdlc_frm_seq <=1'b1;
            st_sca_init_next <=4'd3;
            st_sca_init<=4'd14;   
        end    
        4'd3: begin // CONFIG SCA CONTROL REG B
            hdlc_word_scaInit[99:90]<= {2'b1,8'h0}; // ADDR
            hdlc_word_scaInit[89:80]<= {2'b0,1'b0,hdlc_frmseq_rev,1'b0,hdlc_frmseq_rev}; //CONTROL
            hdlc_word_scaInit[79:70]<= {2'b0,8'h80}; // Trans. ID
            hdlc_word_scaInit[69:60]<= {2'b0,8'h0}; // CHAN  (0x00: SCA control reg address)
            hdlc_word_scaInit[59:50]<= {2'b0,8'h80}; // LEN (1 byte)
            hdlc_word_scaInit[49:40]<= {2'b0,CTRL_W_CRB_rev}; //CMD 
            hdlc_word_scaInit[39:0] <= {2'b0,8'h0,2'b0,8'hFF,2'b0,8'h0,2'b10,8'h0}; //DATA: D1 D0 D3 D2  (D0: enabled channel)
            reset_hdlc_frm_seq <=1'b0;           
            st_sca_init_next <=4'd4;
            st_sca_init<=4'd14; 
        end    
        4'd4: begin // CONFIG SCA CONTROL REG C
            hdlc_word_scaInit[89:80]<= {2'b0,1'b0,hdlc_frmseq_rev,1'b0,hdlc_frmseq_rev}; //CONTROL
            hdlc_word_scaInit[69:60]<= {2'b0,8'h0}; // CHAN  (0x00: SCA control reg address)
            hdlc_word_scaInit[59:50]<= {2'b0,8'h80}; // LEN (1 byte)
            hdlc_word_scaInit[49:40]<= {2'b0,CTRL_W_CRC_rev}; //CMD 
            hdlc_word_scaInit[39:0] <= {2'b0,8'h0,2'b0,8'hFF,2'b0,8'h0,2'b10,8'h0};  //DATA: D1 D0 D3 D2  (D0: enabled channel)         
            st_sca_init_next <=4'd5;
            st_sca_init<=4'd14; 
        end   
        4'd5: begin// GPIO CLOCK SELECT
            hdlc_word_scaInit[89:80]<= {2'b0,1'b0,hdlc_frmseq_rev,1'b0,hdlc_frmseq_rev}; 
            hdlc_word_scaInit[69:60]<= {2'b0,8'h40}; // (CH=0x02)
            hdlc_word_scaInit[59:50]<= {2'b0,8'h20}; // (LEN=4)
            hdlc_word_scaInit[49:40]<= {2'b0,GPIO_W_CLKSEL_rev}; 
            hdlc_word_scaInit[39:0] <= {2'b0,8'h0,2'b0,8'h0,2'b0,8'h0,2'b10,8'h0};  //(USE INT. 40 MHz)
            st_sca_init_next <=4'd6;
            st_sca_init<=4'd14;  
        end
        4'd6:begin  // GPIO DIRECTION
            hdlc_word_scaInit[89:80]<= {2'b0,1'b0,hdlc_frmseq_rev,1'b0,hdlc_frmseq_rev}; 
            hdlc_word_scaInit[69:60]<= {2'b0,8'h40}; // (CH=0x02)
            hdlc_word_scaInit[59:50]<= {2'b0,8'h20}; // (LEN=4)
            hdlc_word_scaInit[49:40]<= {2'b0,GPIO_W_DIRECTION_rev}; 
            hdlc_word_scaInit[39:0] <= {2'b0,sca_gpioDIR_revByte3,2'b0,sca_gpioDIR_revByte2,2'b0,sca_gpioDIR_revByte0,2'b10,sca_gpioDIR_revByte1};  
            st_sca_init_next <=4'd7;
            st_sca_init<=4'd14; 
        end
        4'd7:begin  // GPIO DOUT (SET DEFAULT TO 1)
            hdlc_word_scaInit[89:80]<= {2'b0,1'b0,hdlc_frmseq_rev,1'b0,hdlc_frmseq_rev}; 
            hdlc_word_scaInit[69:60]<= {2'b0,8'h40}; // (CH=0x02)
            hdlc_word_scaInit[59:50]<= {2'b0,8'h20}; // (LEN=4)
            hdlc_word_scaInit[49:40]<= {2'b0,GPIO_W_DATAOUT_rev}; 
            hdlc_word_scaInit[39:0] <={2'b0,8'hFF,2'b0,8'hFF,2'b0,8'hFF,2'b10,8'hFF}; 
            st_sca_init_next <=4'd8;
            st_sca_init<=4'd14; 
        end          
        4'd8:begin // SPI CONTROL REG
            hdlc_word_scaInit[89:80]<= {2'b0,1'b0,hdlc_frmseq_rev,1'b0,hdlc_frmseq_rev}; 
            hdlc_word_scaInit[69:60]<= {2'b0,8'h80}; // (CH=0x01)
            hdlc_word_scaInit[59:50]<= {2'b0,8'h20}; // (LEN=4)
            hdlc_word_scaInit[49:40]<= {2'b0,SPI_W_CTRL_rev}; 
            hdlc_word_scaInit[39:0] <= {2'b0,8'h0,2'b0,8'h0,2'b0,8'h06,2'b10,8'h18}; 
              //[6:0]=7'b1100000 (Nbits=96) [7]=1 (SCK IDLE LOW),
              //[8]=0 GO/BUSY OFF [10]=0 TXEDGE Rising, [11]=1 (SPI LSB First) [12]=1(Always), [13]=0 (SS Manual),
            st_sca_init_next <=4'd9;
            st_sca_init<=4'd14; 
        end   
        4'd9: begin // SPI FREQUENCY
            hdlc_word_scaInit[89:80]<= {2'b0,1'b0,hdlc_frmseq_rev,1'b0,hdlc_frmseq_rev}; 
            hdlc_word_scaInit[69:60]<= {2'b0,8'h80}; // (CH=0x01)
            hdlc_word_scaInit[59:50]<= {2'b0,8'h20}; // (LEN=4)
            hdlc_word_scaInit[49:40]<= {2'b0,SPI_W_FREQ_rev}; 
            hdlc_word_scaInit[39:0] <= {2'b0,8'h0,2'b0,8'h0,2'b0,8'hC8,2'b10,8'h0}; // DATA: D2 D3 D0 D1 (SPI freq=1MHz,div_cnt=19, f=2e7/(div+1))  
            st_sca_init_next <=4'd10;
            st_sca_init<=4'd14; 
        end
        4'd10: begin
            scaInit_done <=1'b1;
            st_sca_init <= 4'd0;        
        end
        4'd14:begin
            genHDLCfrm4SCAInit <= 1'b1;
            st_sca_init <=4'd15;
        end
        4'd15: begin // check hdlc frame sent
            if (assemble_new_hdlc_frm_done==1'b1) begin
                if (abort_w_error==1'b1) st_sca_init <= 4'd10; //-->Abort with Error
                else st_sca_init <= st_sca_init_next;                  
            end
            else begin genHDLCfrm4SCAInit <= 1'b0; st_sca_init <= 4'd15; end   
        end
        default: st_sca_init<=4'd0;
        endcase 
end

//------------------------------------------------// 
//------------ Board ID to SCA Port Mapping ------//
//------------------------------------------------// 

    //assumes sca_port_mux == board id;
    always @(posedge clk80) begin sca_port_mux_r <= board_id; end

//------------------------------------------------// 
//------------ ASIC CONFIGURATION BLOCK ----------//
//------------------------------------------------//

always @ (posedge clk80) begin
    if (reset == 1'b1) st_cfg <= idle;    
    else begin case (st_cfg)
        idle: begin
            asicCfg_done <= 1'b0;
            sca_chanid <='b0;
            sca_n_32bit <='b0;
            sca_cmd_data <='b0;
            asic_reg_addr <='b0;
            vmm_ctrl_stat <= 'b0;
            genHDLCfrm4ASICCfg <='b0;
            {st_cfg_prev,st_cfg_next} <='b0;
            if (asicCfg_start == 1'b1) begin // new configure command arrives
              asicCfg_start_ack <= 1'b1;
              hdlc_word_asicCfg[99:90]<= {2'b1,8'h0}; // ADDR
              hdlc_word_asicCfg[79:70]<= {2'b0,8'h80}; // Trans. ID
              case (asic_type)
              isVMM: st_cfg <= st_vmm_entry;  // --SET PATH FOR VMM     
              isTDS: st_cfg <= st_tds_entry; // --SET PATH FOR TDS
              isROC: st_cfg <= st_roc_entry;  // !!!WORK IN PROGRESS!!!
              default: st_cfg <= idle;
              endcase
            end
            else begin
                asicCfg_start_ack <= 1'b0; 
                hdlc_word_asicCfg <='b0;
                st_cfg <= idle; end
        end
        st_gpioDOUT:begin
            hdlc_word_asicCfg[89:80]<= {2'b0,1'b0,hdlc_frmseq_rev,1'b0,hdlc_frmseq_rev}; 
            hdlc_word_asicCfg[69:60]<= {2'b0,sca_chanid_rev}; 
            hdlc_word_asicCfg[59:50]<= {2'b0,8'h20}; // (LEN=4)
            hdlc_word_asicCfg[49:40]<= {2'b0,GPIO_W_DATAOUT_rev}; 
            hdlc_word_asicCfg[39:0] <={2'b0,gpiodout[31:24],2'b0,gpiodout[23:16],2'b0,gpiodout[15:8],2'b10,gpiodout[7:0]};
            st_cfg <= st_gen_hdlcFRM; 
        end
        st_spiSS: begin
            hdlc_word_asicCfg[89:80]<= {2'b0,1'b0,hdlc_frmseq_rev,1'b0,hdlc_frmseq_rev};
            hdlc_word_asicCfg[69:60]<= {2'b0,sca_chanid_rev}; 
            hdlc_word_asicCfg[59:50]<= {2'b0,8'h20}; // (LEN=4)
            hdlc_word_asicCfg[49:40]<= {2'b0,SPI_W_SS_rev}; 
            hdlc_word_asicCfg[39:0] <={2'b0,8'h0,2'b0,8'h0,2'b0,spi_cs_rev[7:0],2'b10,8'h0};          
            st_cfg <= st_gen_hdlcFRM;
        end      
        st_i2cCTRL: begin // I2C CHANNEL CONTROL REG
            hdlc_word_asicCfg[89:80]<= {2'b0,1'b0,hdlc_frmseq_rev,1'b0,hdlc_frmseq_rev}; 
            hdlc_word_asicCfg[69:60]<= {2'b0,sca_chanid_rev}; 
            hdlc_word_asicCfg[59:50]<= {2'b0,8'h40}; // (LEN=2)
            hdlc_word_asicCfg[49:40]<= {2'b0,I2C_W_CTRL_rev}; 
            hdlc_word_asicCfg[39:0] <= {2'b0,8'h0,2'b0,8'b10000010,2'b0,8'h0,2'b10,8'h0}; 
            // D0 [1:0] I2C freq (00-100kHz [v]01-200kHz 10-400kHz 11-1MHz) [6:2] NBYTE to transmit    
            st_cfg <= st_gen_hdlcFRM;    
        end      
       
        //*************//
        st_vmm_entry:begin //-- CONFIG VMM3 step 1: Toggle VMM_ENA(connected to GPIO) to LOW, Set SS to Mannual Low (channel selected)
            case (vmm_ctrl_stat)
            3'd0: begin
                sca_chanid <= 8'h2; //SCA GPIO CHAN #
                gpiodout <=32'hFFFFFFFF;
                st_cfg <= st_gpioDOUT;
                st_cfg_next <= st_vmm_entry;
                vmm_ctrl_stat <= vmm_ctrl_stat + 1'b1;
            end
            3'd1: begin
                sca_chanid <= 8'h2;
                gpiodout <=32'h0;
                st_cfg <= st_gpioDOUT;
                st_cfg_next <= st_vmm_entry;
                vmm_ctrl_stat <= vmm_ctrl_stat + 1'b1;
            end
            3'd2: begin
                spi_cs <= asic_id;
                sca_chanid <= 8'h1; //SCA SPI CHAN #
                st_cfg <= st_spiSS;
                st_cfg_next <= st_vmm_entry;
                vmm_ctrl_stat <= vmm_ctrl_stat + 1'b1;
            end
            3'd3: begin
                st_cfg <= st_vmm_iterReg;
                vmm_ctrl_stat <= vmm_ctrl_stat + 1'b1;
            end
            endcase
        end
        st_vmm_iterReg: begin //-- CONFIG VMM3 step 2: pull out vmm config data   
            if (asic_reg_addr < 5'h12) begin 
                case (asic_reg_addr)
                5'h0: sca_cmd_data <= {config_din[0],config_din[1],config_din[2],config_din[3],config_din[4],config_din[5],32'h0};
                5'h1: sca_cmd_data <= {config_din[6],config_din[7],config_din[8],config_din[9],config_din[10],config_din[11],32'h0};
                5'h2: sca_cmd_data <= {config_din[12],config_din[13],config_din[14],config_din[15],config_din[16],config_din[17],32'h0};
                5'h3: sca_cmd_data <= {config_din[18],config_din[19],config_din[20],config_din[21],config_din[22],config_din[23],32'h0};
                5'h4: sca_cmd_data <= {config_din[24],config_din[25],config_din[26],config_din[27],config_din[28],config_din[29],32'h0};
                5'h5: sca_cmd_data <= {config_din[30],config_din[31],config_din[32],config_din[33],config_din[34],config_din[35],32'h0};
                5'h6: sca_cmd_data <= {config_din[36],config_din[37],config_din[38],config_din[39],config_din[40],config_din[41],32'h0};
                5'h7: sca_cmd_data <= {config_din[42],config_din[43],config_din[44],config_din[45],config_din[46],config_din[47],32'h0};
                5'h8: sca_cmd_data <= {config_din[48],config_din[49],config_din[50],config_din[51],config_din[52],config_din[53],32'h0};
                5'h9: sca_cmd_data <= {config_din[54],config_din[55],config_din[56],config_din[57],config_din[58],config_din[59],32'h0};
                5'ha: sca_cmd_data <= {config_din[60],config_din[61],config_din[62],config_din[63],config_din[64],config_din[65],32'h0};
                5'hb: sca_cmd_data <= {config_din[66],config_din[67],config_din[68],config_din[69],config_din[70],config_din[71],32'h0};
                5'hc: sca_cmd_data <= {config_din[72],config_din[73],config_din[74],config_din[75],config_din[76],config_din[77],32'h0};
                5'hd: sca_cmd_data <= {config_din[78],config_din[79],config_din[80],config_din[81],config_din[82],config_din[83],32'h0};
                5'he: sca_cmd_data <= {config_din[84],config_din[85],config_din[86],config_din[87],config_din[88],config_din[89],32'h0};                    
                5'hf: sca_cmd_data <= {config_din[90],config_din[91],config_din[92],config_din[93],config_din[94],config_din[95],32'h0};
                5'h10: sca_cmd_data <= {config_din[96],config_din[97],config_din[98],config_din[99],config_din[100],config_din[101],32'h0};
                5'h11: sca_cmd_data <= {config_din[102],config_din[103],config_din[104],config_din[105],config_din[106],config_din[107],32'h0};
                default: sca_cmd_data <='b0;
                endcase   
                st_cfg <= st_vmm_loadReg; 
            end
            else begin 
                asic_reg_addr <= 'b0;
                spi_cs <='b0; //(toggle SPI CS to high to finish transcation)
                st_cfg <= st_spiSS;
                st_cfg_next <= st_vmm_exit; 
                vmm_ctrl_stat <= vmm_ctrl_stat + 1'b1;
            end                     
        end
        st_vmm_loadReg: begin  //-- CONFIG VMM3 step 3: load 32-bits per HDLC frame to SCA SPI buffer  
            if (sca_n_32bit <= 3'h2) begin
                hdlc_word_asicCfg[89:80]<= {2'b0,1'b0,hdlc_frmseq_rev,1'b0,hdlc_frmseq_rev}; 
                hdlc_word_asicCfg[69:60]<= {2'b0,sca_chanid_rev};  
                hdlc_word_asicCfg[59:50]<= {2'b0,8'h20}; // (LEN=4)
                if (sca_n_32bit == 3'h0) begin 
                    hdlc_word_asicCfg[49:40]<= {2'b0,SPI_W_MOSI0_rev}; 
                    hdlc_word_asicCfg[39:0] <= {2'b0,sca_cmd_data[111:104],2'b0,sca_cmd_data[103:96],
                                         2'b0,sca_cmd_data[127:120],2'b10,sca_cmd_data[119:112]}; end //(DATA: D2 D3 D0 D1)                                            
                else if (sca_n_32bit == 3'h1) begin 
                    hdlc_word_asicCfg[49:40]<= {2'b0,SPI_W_MOSI1_rev}; 
                    hdlc_word_asicCfg[39:0] <= {2'b0,sca_cmd_data[79:72],2'b0,sca_cmd_data[71:64],
                                          2'b0,sca_cmd_data[95:88],2'b10,sca_cmd_data[87:80]}; end  
                else begin hdlc_word_asicCfg[49:40]<= {2'b0,SPI_W_MOSI2_rev}; 
                            hdlc_word_asicCfg[39:0] <= {2'b0,sca_cmd_data[47:40],2'b0,sca_cmd_data[39:32],
                                                  2'b0,sca_cmd_data[63:56],2'b10,sca_cmd_data[55:48]}; end   
                sca_n_32bit <= sca_n_32bit + 1'b1;
                st_cfg <= st_gen_hdlcFRM;
                st_cfg_next <= st_vmm_loadReg;        
            end
            else begin 
                sca_n_32bit <= 'b0;
                st_cfg <= st_vmm_issueGo;                    
            end              
        end
        st_vmm_issueGo:begin //-- CONFIG VMM3 step 4: Issue "GO" Command  
            hdlc_word_asicCfg[89:80]<= {2'b0,1'b0,hdlc_frmseq_rev,1'b0,hdlc_frmseq_rev}; 
            hdlc_word_asicCfg[69:60]<= {2'b0,sca_chanid_rev};  
            hdlc_word_asicCfg[59:50]<= {2'b0,8'h20}; // (LEN=4)
            hdlc_word_asicCfg[49:40]<= {2'b0,SPI_GO_rev}; 
            hdlc_word_asicCfg[39:0] <= {2'b0,8'h0,2'b0,8'h0,2'b0,8'h0,2'b10,8'h0}; 
            asic_reg_addr <= asic_reg_addr + 1'b1;
            st_cfg <= st_gen_hdlcFRM;
            st_cfg_next <= st_vmm_iterReg;                          
        end
        st_vmm_exit: begin //-- CONFIG VMM3 step 5: Toggle VMM_ENA HIGH to start acquisition
            sca_chanid <= 8'h2;
            gpiodout <=32'hFFFFFFFF;
            st_cfg <= st_gpioDOUT;
            st_cfg_next <= st_finish; 
        end
                   
        //*************//
        st_tds_entry:begin
            case (asic_id)
            8'h0: sca_chanid <= 8'h0E; //pFEB TDS0
            8'h1: sca_chanid <= 8'h03; //sFEB TDS?
            8'h2: sca_chanid <= 8'h04; //sFEB TDS?
            8'h3: sca_chanid <= 8'h05; //sFEB TDS?
            8'h7:sca_chanid <= 8'h06;
            default: sca_chanid <= 8'h06;
            endcase       
            st_cfg <= st_i2cCTRL;
            st_cfg_next <= st_tds_iterReg;
        end
        st_tds_iterReg:begin  // -- CONFIG TDS step 1: pull out config data for each TDS reg.     
            if (asic_reg_addr < 5'hd) begin 
                case (asic_reg_addr)
                5'h0: sca_cmd_data <= {config_din[0],config_din[1],96'h0};
                5'h1: sca_cmd_data <= {config_din[2],112'h0};
                5'h2: sca_cmd_data <= {config_din[3],config_din[4],config_din[5],config_din[6],config_din[7],config_din[8],config_din[9],config_din[10]};
                5'h3: sca_cmd_data <= {config_din[11],config_din[12],config_din[13],config_din[14],config_din[15],config_din[16],config_din[17],config_din[18]};
                5'h4: sca_cmd_data <= {config_din[19],config_din[20],config_din[21],config_din[22],config_din[23],config_din[24],config_din[25],config_din[26]};
                5'h5: sca_cmd_data <= {config_din[27],config_din[28],config_din[29],config_din[30],config_din[31],config_din[32],config_din[33],config_din[34]};
                5'h6: sca_cmd_data <= {config_din[35],config_din[36],config_din[37],config_din[38],config_din[39],config_din[40],config_din[41],config_din[42]};
                5'h7: sca_cmd_data <= {config_din[43],config_din[44],config_din[45],config_din[46],config_din[47],config_din[48],config_din[49],config_din[50]};
                5'h8: sca_cmd_data <= {config_din[51],config_din[52],config_din[53],config_din[54],config_din[55],config_din[56],config_din[57],config_din[58]};
                5'h9: sca_cmd_data <= {config_din[59],config_din[60],config_din[61],config_din[62],config_din[63],config_din[64],config_din[65],config_din[66]};
                5'ha: sca_cmd_data <= {config_din[67],config_din[68],config_din[69],config_din[70],config_din[71],config_din[72],config_din[73],config_din[74]};
                5'hb: sca_cmd_data <= {config_din[75],config_din[76],config_din[77],config_din[78],64'h0};
                5'hc: sca_cmd_data <= {config_din[79],config_din[80],96'h0};
                default: sca_cmd_data <='b0;   
                endcase
                st_cfg <= st_tds_loadReg; 
                st_cfg_prev <= st_tds_iterReg;
            end
            else begin
                asic_reg_addr <='b0;
                st_cfg <= st_finish;
            end
        end
        st_tds_loadReg:begin //-- CONFIG TDS step 2: load 32-bits per HDLC frame to SCA I2C buffer  
            if (sca_n_32bit <= 3'h3) begin
                hdlc_word_asicCfg[89:80]<= {2'b0,1'b0,hdlc_frmseq_rev,1'b0,hdlc_frmseq_rev}; 
                hdlc_word_asicCfg[69:60]<= {2'b0,sca_chanid_rev};  
                hdlc_word_asicCfg[59:50]<= {2'b0,8'h20}; // (LEN=4)
                if (sca_n_32bit == 3'h0) begin 
                    hdlc_word_asicCfg[49:40]<= {2'b0,I2C_W_DATA0_rev}; 
                    hdlc_word_asicCfg[39:0] <= {2'b0,sca_cmd_data_rev[119:112],2'b0,sca_cmd_data_rev[127:120],
                                                2'b0,sca_cmd_data_rev[103:96],2'b10,sca_cmd_data_rev[111:104]}; end //(DATA: D1 D0 D3 D2)                                            
                else if (sca_n_32bit == 3'h1) begin 
                    hdlc_word_asicCfg[49:40]<= {2'b0,I2C_W_DATA1_rev}; 
                    hdlc_word_asicCfg[39:0] <= {2'b0,sca_cmd_data_rev[87:80],2'b0,sca_cmd_data_rev[95:88],
                                                2'b0,sca_cmd_data_rev[71:64],2'b10,sca_cmd_data_rev[79:72]}; end  
                else if (sca_n_32bit == 3'h2) begin 
                    hdlc_word_asicCfg[49:40]<= {2'b0,I2C_W_DATA2_rev}; 
                    hdlc_word_asicCfg[39:0] <= {2'b0,sca_cmd_data_rev[55:48],2'b0,sca_cmd_data_rev[63:56],
                                                2'b0,sca_cmd_data_rev[39:32],2'b10,sca_cmd_data_rev[47:40]}; end  
                else begin 
                    hdlc_word_asicCfg[49:40]<= {2'b0,I2C_W_DATA3_rev}; 
                    hdlc_word_asicCfg[39:0] <= {2'b0,sca_cmd_data_rev[23:16],2'b0,sca_cmd_data_rev[31:24],
                                                2'b0,sca_cmd_data_rev[7:0],2'b10,sca_cmd_data_rev[15:8]}; end 
               sca_n_32bit <= sca_n_32bit + 1'b1;
               st_cfg <= st_gen_hdlcFRM;
               st_cfg_prev <= st_tds_loadReg;
               st_cfg_next <= st_tds_loadReg;        
               end
               else begin 
                   sca_n_32bit <= 'b0;
                   st_cfg <= st_tds_issueGo;                    
               end                             
        end
        st_tds_issueGo:begin  //-- CONFIG TDS step 3: Issue "GO" Command  
            hdlc_word_asicCfg[89:80]<= {2'b0,1'b0,hdlc_frmseq_rev,1'b0,hdlc_frmseq_rev}; 
            hdlc_word_asicCfg[69:60]<= {2'b0,sca_chanid_rev};  
            hdlc_word_asicCfg[59:50]<= {2'b0,8'h40}; // LEN=2
            hdlc_word_asicCfg[49:40]<= {2'b0,I2C_M_7B_W_rev}; 
            hdlc_word_asicCfg[39:0] <={2'b0,8'h0,2'b0,asic_reg_addr[0],asic_reg_addr[1],asic_reg_addr[2],asic_reg_addr[3],asic_id[0],asic_id[1],asic_id[2],
                                 1'b0,2'b0,8'h0,2'b10,8'h0}; // (D0: 1'b0 + 3'bCHIPID + 4'bREGID)                
            asic_reg_addr <= asic_reg_addr + 1'b1;
            st_cfg <= st_gen_hdlcFRM;
            st_cfg_prev <= st_tds_issueGo;
            st_cfg_next <= st_tds_iterReg; 
        end
       
        //*************//
        st_gen_hdlcFRM: begin
            genHDLCfrm4ASICCfg <= 1'b1;
            st_cfg <=st_chk_hdlcFRM;
        end
        st_chk_hdlcFRM: begin
            if (assemble_new_hdlc_frm_done==1'b1) begin
                if (abort_w_error==1'b1) st_cfg <= st_finish; //-->Abort with Error
                else st_cfg <= st_cfg_next;                  
            end
            else begin genHDLCfrm4ASICCfg <= 1'b0; st_cfg <= st_chk_hdlcFRM; end 
        end

        //*************//
        st_finish:begin
            asicCfg_done <= 1'b1;
            st_cfg<=idle;
        end
        default: st_cfg <= idle;
        endcase 
    end
end


//------------------------------------------------// 
//-------------- ASIC Soft Reset BLOCK -----------//
//------------------------------------------------//

reg [3:0] st_asicRst;
reg [3:0] st_asicRst_next;
reg [7:0] st_timer_cnt;


always @(posedge clk80) begin
    if (reset==1'b1) st_asicRst <=3'd0;
    else case (st_asicRst)
    4'd0: begin
        genHDLCfrm4ASICRst <= 1'b0; 
        asicRst_done <= 1'b0;
        st_asicRst_next <= 'b0;
        st_timer_cnt <='b0;
        if (asicRst_start==1'b1) begin
            asicRst_start_ack <=1'b1;
            hdlc_word_asicRst[99:90]<= {2'b1,8'h0}; // ADDR
            hdlc_word_asicRst[79:70]<= {2'b0,8'h80}; // Trans. ID 
            case (asic_type)
            isVMM: st_asicRst <= 4'd1;  //assumed asic_id is VMM bit mask and spi_cs==asic_id (check FEB map!!)
            isTDS: st_asicRst <= 4'd6;
            isROC: st_asicRst <= 4'd7;
            default: st_asicRst <= 4'd0;
            endcase
        end
        else begin
            asicRst_start_ack <=1'b0;
            hdlc_word_asicRst <='b0;
            st_asicRst <= 4'd0;
        end
    end
    
    /**********VMM********/
   4'd1: begin  // -- (ENA LOW)
        st_timer_cnt <= 'b0;
        hdlc_word_asicRst[89:80]<= {2'b0,1'b0,hdlc_frmseq_rev,1'b0,hdlc_frmseq_rev}; 
        hdlc_word_asicRst[69:60]<= {2'b0,8'h40}; // GPIO on SCA CHAN #2
        hdlc_word_asicRst[59:50]<= {2'b0,8'h20}; // (LEN=4)
        hdlc_word_asicRst[49:40]<= {2'b0,GPIO_W_DATAOUT_rev}; 
        hdlc_word_asicRst[39:0] <= {2'b0,8'h0,2'b0,8'h0,2'b0,8'h0,2'b10,8'h0}; // Check VMM ENA is connected to which SCA GPIO on FEB!!
        st_asicRst <= 4'd14; 
        st_asicRst_next <=4'd2;
    end
    4'd2:begin  //-- acquisition reset entry (CS Low)
        hdlc_word_asicRst[89:80]<= {2'b0,1'b0,hdlc_frmseq_rev,1'b0,hdlc_frmseq_rev};
        hdlc_word_asicRst[69:60]<= {2'b0,8'h80}; // SPI on SCA CHAN #1
        hdlc_word_asicRst[59:50]<= {2'b0,8'h20}; // (LEN=4)
        hdlc_word_asicRst[49:40]<= {2'b0,SPI_W_SS_rev}; 
        hdlc_word_asicRst[39:0] <= {2'b0,8'h0,2'b0,8'h0,2'b0,asic_id[0],asic_id[1],asic_id[2],asic_id[3],asic_id[4],asic_id[5],
                                    asic_id[6],asic_id[7],2'b10,8'h0};          // Check VMM SS MAP on FEB!!
        st_asicRst <= 4'd14; 
        st_asicRst_next <=4'd3; 
    end
    4'd3: begin  // -- (CS HIGH)
        if (st_timer_cnt < 8'hFF) begin st_timer_cnt <= st_timer_cnt + 1'b1; st_asicRst <= 4'd3; end
        else begin
            st_timer_cnt <= 'b0;
            hdlc_word_asicRst[89:80]<= {2'b0,1'b0,hdlc_frmseq_rev,1'b0,hdlc_frmseq_rev};
            hdlc_word_asicRst[69:60]<= {2'b0,8'h80}; // SPI on SCA CHAN #1
            hdlc_word_asicRst[59:50]<= {2'b0,8'h20}; // (LEN=4)
            hdlc_word_asicRst[49:40]<= {2'b0,SPI_W_SS_rev}; 
            hdlc_word_asicRst[39:0] <= {2'b0,8'h0,2'b0,8'h0,2'b0,8'h0,2'b10,8'h0};         
            st_asicRst <= 4'd14;
            st_asicRst_next <=4'd4;
        end
    end
    4'd4: begin  //-- (ENA HIGH)
        hdlc_word_asicRst[89:80]<= {2'b0,1'b0,hdlc_frmseq_rev,1'b0,hdlc_frmseq_rev}; 
        hdlc_word_asicRst[69:60]<= {2'b0,8'h40}; // GPIO on SCA CHAN #2
        hdlc_word_asicRst[59:50]<= {2'b0,8'h20}; // (LEN=4)
        hdlc_word_asicRst[49:40]<= {2'b0,GPIO_W_DATAOUT_rev}; 
        hdlc_word_asicRst[39:0] <= {2'b0,8'hFF,2'b0,8'hFF,2'b0,8'hFF,2'b10,8'hFF};  // Check VMM ENA is connected to which SCA GPIO on FEB!!
        st_asicRst <= 4'd14; 
        st_asicRst_next <=4'd9;
    end        

    /**********TDS***********/
    4'd6: begin  // -- (ENA LOW)
        hdlc_word_asicRst[89:80]<= {2'b0,1'b0,hdlc_frmseq_rev,1'b0,hdlc_frmseq_rev}; 
        hdlc_word_asicRst[69:60]<= {2'b0,8'h40}; // GPIO on SCA CHAN #2
        hdlc_word_asicRst[59:50]<= {2'b0,8'h20}; // (LEN=4)
        hdlc_word_asicRst[49:40]<= {2'b0,GPIO_W_DATAOUT_rev}; 
        hdlc_word_asicRst[39:0] <= {2'b0,8'hFF,2'b0,8'hFF,2'b0,8'hFF,2'b10,8'hFF}; // Check TDS RST is connected to which SCA GPIO on FEB!!
        st_asicRst <= 4'd14; 
        st_asicRst_next <=4'd7;
    end
    4'd7: begin  //-- (ENA HIGH)
        hdlc_word_asicRst[89:80]<= {2'b0,1'b0,hdlc_frmseq_rev,1'b0,hdlc_frmseq_rev}; 
        hdlc_word_asicRst[69:60]<= {2'b0,8'h40}; // GPIO on SCA CHAN #2
        hdlc_word_asicRst[59:50]<= {2'b0,8'h20}; // (LEN=4)
        hdlc_word_asicRst[49:40]<= {2'b0,GPIO_W_DATAOUT_rev}; 
        hdlc_word_asicRst[39:0] <= {2'b0,8'hFF,2'b0,8'hFF,2'b0,8'hFF,2'b10,8'hFF};  // Check TDS RST is connected to which SCA GPIO on FEB!!
        st_asicRst <= 4'd14; 
        st_asicRst_next <= 4'd9;
    end      
        
    /**********ROC***********/
    // pending to be implemented
    4'd8: begin
        st_asicRst <= 4'd9;   
    end
    
    4'd9: begin
        asicRst_done <= 1'b1;
        st_asicRst <= 4'd0;        
    end
    4'd14:begin
        genHDLCfrm4ASICRst <= 1'b1;
        st_asicRst <=4'd15;
    end
    4'd15: begin // check hdlc frame sent
        if (assemble_new_hdlc_frm_done==1'b1) begin
            if (abort_w_error==1'b1) st_asicRst <= 4'd9; //-->Abort with Error
            else st_asicRst <= st_asicRst_next;                  
        end
        else begin genHDLCfrm4ASICRst <= 1'b0; st_asicRst <= 4'd15; end   
    end
    endcase
end



//--------------------------------------------------// 
//--- SCA HDLC Frame Generation and Rx Monitor -----//
//--------------------------------------------------// 
// Two Level Reset > global reset- reset everything (need to re-init SCA)
//                 > cfg reset- reset error code upon receiving new cfg request (NO HDLC FRM SEQ RESET!!) 

assign assemble_new_hdlc_frm = (scaInit_start_ack==1'b1)? genHDLCfrm4SCAInit:
                               (asicCfg_start_ack==1'b1)? genHDLCfrm4ASICCfg:
                               (asicRst_start_ack==1'b1)? genHDLCfrm4ASICRst:1'b0;


always @ (posedge clk80) begin
     if (reset==1'b1) begin 
        hdlc_frmseq <= 'b0;
        error_code <= 16'h0;
        st_hdlc_frm_gen <= 2'b0;
     end
     else if (cfgFIFO_wr_done==1'b1) begin
        error_code <= 16'h0; 
        st_hdlc_frm_gen <= 2'b0;
     end
     else case (st_hdlc_frm_gen)
        2'b0: begin  // REQUEST NEW HDLC FRM GEN
            assemble_new_hdlc_frm_done <=1'b0;
            abort_w_error <= 1'b0;
            new_rx_word_reset <= 1'b1;
            if (assemble_new_hdlc_frm==1'b1) begin 
                hdlcfrm_gen_request_r <= 1'b1;
                st_hdlc_frm_gen <= 2'b1; end
            else begin
                hdlcfrm_gen_request_r <= 1'b0;
                st_hdlc_frm_gen <= 2'b0;end
        end
        2'b1: begin  // WAIT FOR HDLC FRM GEN GRANTED 
            new_rx_word_reset <= 1'b0;
            if (hdlcfrm_gen_grant == 1'b1)begin
                hdlcfrm_gen_request_r <= 1'b0;
                st_hdlc_frm_gen <= 2'b10; end
            else st_hdlc_frm_gen <= 2'b1;
        end
        2'b10:begin  // SCA RX ERROR CHECKING
            if (sca_rx_timeout_cnt[17] == 1'b1) begin// SCA RX Timeout    
                sca_rx_timeout_cnt <= 'b0;
                assemble_new_hdlc_frm_done <=1'b1;
                error_code <= {4'hF,4'h0,8'h0};
                abort_w_error <= 1'b1;
                hdlc_frmseq <= hdlc_frmseq + 1'b1;
                st_hdlc_frm_gen <= 2'b0; 
            end
            else if (new_rx_word == 1'b1) begin// RX packet recieved, good transmision (kinda)
                if (|hdlc_rx_error == 1'b1) begin 
                    abort_w_error <= 1'b1; 
                    error_code <= {4'h0,4'b0001,hdlc_rx_error}; end// sca error
                else if (asic_type==isTDS && st_cfg_prev==st_tds_issueGo && hdlc_rx_data[31:24]!= 8'h20) begin 
                    abort_w_error <= 1'b1; 
                    error_code <={4'h0,4'b0010,hdlc_rx_error};end // I2C Error
                else begin 
                    abort_w_error <= 1'b0; 
                    error_code<='b0; end
                sca_rx_timeout_cnt <= 'b0;
                assemble_new_hdlc_frm_done <=1'b1;
                
                if (reset_hdlc_frm_seq==1'b1) hdlc_frmseq <= 'b0;
                else hdlc_frmseq <= hdlc_frmseq + 1'b1;
                st_hdlc_frm_gen <= 2'b0; 
            end
            else begin
                sca_rx_timeout_cnt <= sca_rx_timeout_cnt + 1'b1;
                st_hdlc_frm_gen <= 2'b10;
            end
        end
        default:st_hdlc_frm_gen<=2'b0;
      endcase
end        

//-------------------------------------------------// 
//-- SCA RX INTERFACE AND ERROR REPLY To HOST PC --//
//-------------------------------------------------// 
assign operation_done = (scaInit_start_ack==1'b1)? scaInit_done:
                        (asicCfg_start_ack==1'b1)? asicCfg_done:
                        (asicRst_start_ack==1'b1)? asicRst_done:1'b0;


always @(posedge clk80) begin
    if (new_rx_word_reset == 1'b1)
        new_rx_word <= 1'b0;
    else if (new_rx_word_i == 1'b1) begin
        new_rx_word <= 1'b1;
        hdlc_rx_error <= hdlc_rx_error_i;
        hdlc_rx_data <= hdlc_rx_data_i; end
    else new_rx_word <= 1'b0;
end

// TX FIFO for reply and error reporting
always @(posedge clk80) begin
    if (reset == 1'b1)
        st_eth_tx <= 2'b0;
    else case (st_eth_tx)
        2'b0 : begin
            send_wr_en_r <= 1'b0;
            send_end_packet_r <= 1'b0; // End of packet
            send_cnt <= 2'b0;
            error_reply_done <='b0;
            if (operation_done == 1'b1) begin // new error
                send_request_r <= 1'b1;
                st_eth_tx <= 2'b1; end
            else st_eth_tx <= 2'b0;    
        end
        2'b1 : begin // wait for granting, then send one data packet
            if (send_grant == 1'b1) begin // granted
                send_request_r <= 1'b0;
                st_eth_tx <= 2'b10;end
            else begin
                send_request_r <= 1'b1;
                st_eth_tx <= 2'b1;end
        end
        2'b10 :begin // Send one set of data
            send_request_r <= 1'b0;
            send_wr_en_r <= 1'b1;
            send_cnt <= send_cnt + 1'b1;
            if (send_cnt == 2'b0) // header
                send_data_r <= 32'hdecafbad; // header
            else if (send_cnt == 2'b1) // reply
                send_data_r <= 32'h0F; 
            else begin
                if (|error_code ==1'b0) send_data_r <= 32'b0;
                else send_data_r <= {4'h0,12'hbad, error_code};
                send_end_packet_r <= 1'b1; // End of packet
                st_eth_tx <= 2'b11; end
        end
        2'b11: begin
            send_wr_en_r <= 1'b0;
            error_reply_done <=1'b1;
            if (cfgFIFO_wr_done==1'b1) st_eth_tx <= 2'b0;
            else st_eth_tx <=2'b11;
        end            
        default : st_eth_tx <= 2'b0;
        endcase
end


//------------------------------------------------// 
//--------------- Debugging BLOCK ----------------//
//------------------------------------------------// 

// Debug module for checking the write of configuration data into cfgFIFO

(* mark_debug = "true" *) wire [95:0]vmmreg[0:17];
 
assign vmmreg[0]= {config_din[0],config_din[1],config_din[2],config_din[3],config_din[4],config_din[5]};
assign vmmreg[1]= {config_din[6],config_din[7],config_din[8],config_din[9],config_din[10],config_din[11]};
assign vmmreg[2]= {config_din[12],config_din[13],config_din[14],config_din[15],config_din[16],config_din[17]};
assign vmmreg[3]= {config_din[18],config_din[19],config_din[20],config_din[21],config_din[22],config_din[23]};
assign vmmreg[4]= {config_din[24],config_din[25],config_din[26],config_din[27],config_din[28],config_din[29]};
assign vmmreg[5]= {config_din[30],config_din[31],config_din[32],config_din[33],config_din[34],config_din[35]};
assign vmmreg[6]= {config_din[36],config_din[37],config_din[38],config_din[39],config_din[40],config_din[41]};
assign vmmreg[7]= {config_din[42],config_din[43],config_din[44],config_din[45],config_din[46],config_din[47]};
assign vmmreg[8]= {config_din[48],config_din[49],config_din[50],config_din[51],config_din[52],config_din[53]};
assign vmmreg[9]= {config_din[54],config_din[55],config_din[56],config_din[57],config_din[58],config_din[59]};
assign vmmreg[10]={config_din[60],config_din[61],config_din[62],config_din[63],config_din[64],config_din[65]};
assign vmmreg[11]={config_din[66],config_din[67],config_din[68],config_din[69],config_din[70],config_din[71]};
assign vmmreg[12]={config_din[72],config_din[73],config_din[74],config_din[75],config_din[76],config_din[77]};        
assign vmmreg[13]={config_din[78],config_din[79],config_din[80],config_din[81],config_din[82],config_din[83]};  
assign vmmreg[14]={config_din[84],config_din[85],config_din[86],config_din[87],config_din[88],config_din[89]};
assign vmmreg[15]={config_din[90],config_din[91],config_din[92],config_din[93],config_din[94],config_din[95]};  
assign vmmreg[16]={config_din[96],config_din[97],config_din[98],config_din[99],config_din[100],config_din[101]};  
assign vmmreg[17]={config_din[102],config_din[103],config_din[104],config_din[105],config_din[106],config_din[107]}; 


ila_sca_cfgFIFO2BRAM ila_sca_cfgFIFO2BRAM_inst(
    .clk(clk80),
    .probe0(st_bram),//3
    .probe1(cfgFIFO_rst_r),//1
    .probe2(cfgFIFO_rd_en_r),//1
    .probe3(cfgFIFO_data_valid),//1
    .probe4(cfgFIFO_empty),//1
    .probe5(vmmreg[0]),
    .probe6(vmmreg[1]),
    .probe7(vmmreg[2]),
    .probe8(vmmreg[3]),
    .probe9(vmmreg[4]),
    .probe10(vmmreg[5]),
    .probe11(vmmreg[6]),
    .probe12(vmmreg[7]),
    .probe13(vmmreg[8]),
    .probe14(vmmreg[9]),
    .probe15(vmmreg[10]),
    .probe16(vmmreg[11]),
    .probe17(vmmreg[12]),        
    .probe18(vmmreg[13]),  
    .probe19(vmmreg[14]),
    .probe20(vmmreg[15]),  
    .probe21(vmmreg[16]),  
    .probe22(vmmreg[17]) 
);


// >> debug variables

(* mark_debug = "true" *) wire [2:0]  stat_bram;
(* mark_debug = "true" *) wire [4:0]  stat_Cfg;
(* mark_debug = "true" *) wire [3:0]  stat_scaInit;
(* mark_debug = "true" *) wire [1:0]  stat_hdlcFRM_gen;
(* mark_debug = "true" *) wire [1:0]  stat_eth_tx;
(* mark_debug = "true" *) wire [4:0]  RegAddr;
(* mark_debug = "true" *) wire [2:0]  frmSEQ;
(* mark_debug = "true" *) wire [127:0]scaCMDData;
(* mark_debug = "true" *) wire [99:0] hdlcWORD;
(* mark_debug = "true" *) wire [7:0]asic_id_w;
(* mark_debug = "true" *) wire [1:0]asic_type_w;

(* mark_debug = "true" *) wire scaInit_Start;
(* mark_debug = "true" *) wire asicCfg_Start;
(* mark_debug = "true" *) wire scaInit_Start_ack;
(* mark_debug = "true" *) wire asicCfg_Start_ack;
(* mark_debug = "true" *) wire asicRst_Start_ack;
(* mark_debug = "true" *) wire scaInit_Done;
(* mark_debug = "true" *) wire asicCfg_Done;
(* mark_debug = "true" *) wire asicRst_Done;

(* mark_debug = "true" *) wire assemble_new_hdlcFRM;
(* mark_debug = "true" *) wire assemble_new_hdlcFRM_done;

assign stat_bram = st_bram;
assign stat_Cfg = st_cfg;
assign stat_scaInit = st_sca_init;
assign stat_hdlcFRM_gen = st_hdlc_frm_gen;
assign stat_eth_tx = st_eth_tx;

assign RegAddr = asic_reg_addr;
assign frmSEQ = hdlc_frmseq;
assign scaCMDData = sca_cmd_data;
assign hdlcWORD = hdlc_word;
assign hdlcfrm_gen_request_w= hdlcfrm_gen_request_r;
assign asic_id_w = asic_id;
assign asic_type_w= asic_type;
assign scaInit_Start=scaInit_start;
assign asicCfg_Start=asicCfg_start;
assign scaInit_Start_ack=scaInit_start_ack;
assign asicCfg_Start_ack=asicCfg_start_ack;
assign asicRst_Start_ack=asicRst_start_ack;
assign scaInit_Done=scaInit_done;
assign asicCfg_Done=asicCfg_done;
assign asicRst_Done=asicRst_done;
assign assemble_new_hdlcFRM=assemble_new_hdlc_frm;
assign assemble_new_hdlcFRM_done= assemble_new_hdlc_frm_done;

ila_sca_HDLC_dword_prep ila_sca_hdlc_dword_prep_inst(
    .clk(clk80),
    .probe0(stat_bram),
    .probe1(stat_Cfg),
    .probe2(stat_scaInit),
    .probe3(stat_hdlcFRM_gen),
    .probe4(stat_eth_tx),
    .probe5(cfg_busy_r),
    .probe6(hdlcfrm_gen_request_r),
    .probe7(error_reply_done),
    .probe8(scaInit_Start),
    .probe9(asicCfg_Start),
    .probe10(scaInit_Start_ack),
    .probe11(asicCfg_Start_ack),
    .probe12(asicRst_Start_ack),
    .probe13(scaInit_Done),
    .probe14(asicCfg_Done),
    .probe15(asicRst_Done),
    .probe16(RegAddr),
    .probe17(frmSEQ),
    .probe18(scaCMDData),
    .probe19(hdlcWORD),
    .probe20(asic_id_w),
    .probe21(asic_type_w),
    .probe22(hdlc_rx_error),
    .probe23(hdlc_rx_data[31:24]),
    .probe24(error_code),
    .probe25(assemble_new_hdlcFRM),
    .probe26(assemble_new_hdlcFRM_done),
    .probe27(abort_w_error),
    .probe28(st_asicRst)
);

endmodule
