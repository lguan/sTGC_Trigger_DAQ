`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/20/2017 01:13:24 PM
// Design Name: 
// Module Name: SCA_DRIVE
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SCA_DRIVE
(
    input           clk80, // 80 MHz clock
    input           ElinkRX_i,
    output          ElinkTX_o,
    output          ElinkCK_o,
    output [2:0]    sca_port_mux,
    
    //Communicate with Ethernet TX,RX
    input cfgFIFO_wren,
    input cfgFIFO_wr_done,    
    input [7:0] cfgFIFO_din,
    input [7:0] cfgFIFO_data_length,
    output cfg_busy,
    output          send_request,
    output [31:0]   send_data,
    output          send_end_packet,
    output          send_wr_en,
    input           send_grant
);


// SCA Signals
wire            FIFO_upstream_WEn;
wire    [9:0]   FIFO_upstream_WR_Byte;
wire    [9:0]   FIFO_upstream_RD_Byte;
wire            FIFO_upstream_Empt;
wire            FIFO_upstream_REn_o;
wire            BitStream_o;
wire            BitValid_o;
wire            EmptyGate_o;
wire            FIFO_downstream_RData_o;
wire            FIFO_downstream_Empty_o;
wire            FIFO_downstream_REn_i;
wire            FIFO_Empt_last;
wire            SCARX_FIFO_WEn_o;
wire    [9:0]   SCARX_FIFO_WData_o;
wire            SCARX_FIFO_REn;
wire    [9:0]   SCARX_FIFO_DOUT;
wire            SCARX_FIFO_empty;
wire            SCARX_FIFO_full;
wire            SCARX_FIFO_valid;
wire    [99:0]  HDLC_Word;
wire            hdlcfrm_gen_request;
wire            hdlcfrm_gen_grant;
wire            new_rx_word;
wire    [7:0]   hdlc_rx_error;
wire    [31:0]  hdlc_rx_data;

// Ethernet Signals
wire   reset;
assign reset = 1'b0;

assign FIFO_Empt_last = EmptyGate_o | FIFO_downstream_Empty_o;


wire [4:0] st_cfg_debug;

// -- receive config. data from Ethernet
//  prepare HDLC assembler words
HDLC_DWORD_PREP hdlc_dword_prep
(
    .clk80(clk80),
    .reset(reset),

    //communicate with ethernet TX (old)
    .send_request(send_request),
    .send_data(send_data),
    .send_end_packet(send_end_packet),
    .send_wr_en(send_wr_en),
    .send_grant(send_grant),
     
    //communicate with ethernet RX
    .cfgFIFO_wren(cfgFIFO_wren),    
    .cfgFIFO_wr_done(cfgFIFO_wr_done),
    .cfgFIFO_din(cfgFIFO_din),
    .cfgFIFO_data_length(cfgFIFO_data_length),
    .cfg_busy(cfg_busy),

    //communicate with HDLC frame assembler
    .hdlcfrm_gen_grant(hdlcfrm_gen_grant),
    .hdlcfrm_gen_request(hdlcfrm_gen_request),
    .hdlc_word(HDLC_Word),
    
    //communicate with SCA RX checker
    .new_rx_word_i(new_rx_word),
    .hdlc_rx_error_i(hdlc_rx_error),
    .hdlc_rx_data_i(hdlc_rx_data),
    
    //sca elink port multiplxer 
    .sca_port_mux(sca_port_mux),
    
    //debug
    .st_cfg_debug(st_cfg_debug) 
);

//============ upstream FIFO Control  =============
// -- push HDLC data bytes to upstream FIFO
HDLC_FRMGEN hdlc_frmgen
(
    .clk80(clk80),
    .rst_b(1'b1),
    .start(hdlcfrm_gen_request),
    .hdlc_word(HDLC_Word),
    .FIFO_Data_o(FIFO_upstream_WR_Byte),
    .WEn_o(FIFO_upstream_WEn),
    .done(hdlcfrm_gen_grant)
);


//============ SCA Tx Core modules ===============
fifo_sca_hdlc_10b fifo_sca_hdlc_10b_instTxUpStream
(
    .clk (clk80),
    .srst(reset),
    .din(FIFO_upstream_WR_Byte),
    .wr_en(FIFO_upstream_WEn),
    .rd_en(FIFO_upstream_REn_o),
    .dout(FIFO_upstream_RD_Byte),
    .full(),
    .empty(FIFO_upstream_Empt),
    .valid()
);
 

// -- Add SOF, FCS, EOF and prepare a HDLC frame
SCA_ByteTXnew SCA_ByteTxnew_inst0
(
    .Reset_ni(1'b1),
    .CLK_i(clk80),
    
    .FIFO_RData_i(FIFO_upstream_RD_Byte),
    .FIFO_Empty_i(FIFO_upstream_Empt),
    
    .FIFO_REn_o(FIFO_upstream_REn_o),
    .EmptyGate_o(EmptyGate_o),
    .BitStream_o(BitStream_o),
    .BitValid_o(BitValid_o)
);


fifo_sca_hdlc_1b fifo_sca_hdlc_1b_instDwnStream
(
    .clk (clk80),
    .srst(reset),
    .din(BitStream_o),
    .wr_en(BitValid_o),
    .rd_en(FIFO_downstream_REn_i),
    .dout(FIFO_downstream_RData_o),
    .full(),
    .empty(FIFO_downstream_Empty_o)
);


// -- Prepare for sending bit stream (stuff bits in data field)   
SCA_BitTX SCA_BitTX_inst0
(
    .Reset_ni(1'b1),
    .CLK80M_i(clk80),
    .FIFO_Empty_i(FIFO_Empt_last),
    .FIFO_Data_i(FIFO_downstream_RData_o),
    
    .FIFO_REn_o(FIFO_downstream_REn_i),    
    .ElinkTX_o(ElinkTX_o),
    .ElinkCK_o(ElinkCK_o)
);


//============ SCA Rx Core modules ===============
SCA_RX SCA_RX_inst
(
   .Reset_ni(1'b1),
   .CLK80M_i(clk80),
   .BitStream_i(ElinkRX_i),
   .SCARX_FIFO_WEn_o(SCARX_FIFO_WEn_o),
   .SCARX_FIFO_WData_o(SCARX_FIFO_WData_o)
);


fifo_sca_hdlc_10b fifo_sca_hdlc_10b_rx
(
    .clk (clk80),
    .srst(reset),
    .din(SCARX_FIFO_WData_o),
    .wr_en(SCARX_FIFO_WEn_o),
    .rd_en(SCARX_FIFO_REn),
    .dout(SCARX_FIFO_DOUT),
    .full(SCARX_FIFO_full),
    .empty(SCARX_FIFO_empty),
    .valid(SCARX_FIFO_valid)
);

SCA_RX_DWORD SCA_RX_DWORD_inst
(
    .clk80(clk80),
    .fifo_full(SCARX_FIFO_full),
    .fifo_empty(SCARX_FIFO_empty),
    .fifo_dout(SCARX_FIFO_DOUT),
    .fifo_valid(SCARX_FIFO_valid),
    .fifo_rd_en(SCARX_FIFO_REn),
    .new_hdlc_word(new_rx_word),
    .hdlc_rx_error_o(hdlc_rx_error),
    .hdlc_rx_data(hdlc_rx_data),
    .st_cfg_debug(st_cfg_debug)
);



//ila_sca_port ila_sca_port_inst(
//    .clk(clk80),
//    .probe0(FIFO_upstream_WEn),
//    .probe1(FIFO_upstream_RD_Byte),
//    .probe2(ElinkCK_o),
//    .probe3(ElinkTX_o),
//    .probe4(SCARX_FIFO_WData_o),
//    .probe5(SCARX_FIFO_WEn_o),
//    .probe6(SCARX_FIFO_empty),
//    .probe7(push2upstrmFIFO_start),
//    .probe8(HDLC_Word),
//    .probe9(ElinkRX_i)
//);


endmodule