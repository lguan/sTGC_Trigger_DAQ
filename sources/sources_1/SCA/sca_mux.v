`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/13/2017 06:10:03 PM
// Design Name: 
// Module Name: sca_mux
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sca_mux(
    input wire [2:0] sca_port_mux,
    input wire SCA_ElinkCk_o,              
    output wire SCA_ElinkRX_i,             
    input wire SCA_ElinkTX_o,         
    output wire [7:0]to_FEB_SCA_CLK,      
    output wire [7:0]to_FEB_SCA_DATA,     
    input wire [7:0]from_FEB_SCA_DATA 
    );
    
    genvar i; 
    generate 
    for (i=0;i<8;i=i+1) begin : sca_elink 
        assign to_FEB_SCA_CLK[i] = (sca_port_mux==i)? SCA_ElinkCk_o:1'b0;
        assign to_FEB_SCA_DATA[i] = (sca_port_mux==i)? SCA_ElinkTX_o:1'b0;
     end
    endgenerate
   
    assign SCA_ElinkRX_i = from_FEB_SCA_DATA[sca_port_mux];
   
   
endmodule
