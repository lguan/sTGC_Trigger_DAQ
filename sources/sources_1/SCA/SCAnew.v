
module SCA_BitTX
(
	input Reset_ni,
	input CLK80M_i,
	input FIFO_Empty_i,
	input FIFO_Data_i,
	
	
	output FIFO_REn_o,	
	output ElinkTX_o,
	output ElinkCK_o
);

	
	
reg Ph1Ent_ElinkCK;
reg Ph2Sha_ElinkCK;

reg Ph0Ent_ElinkTX;

reg Ph0Ent_FIFO_REn;


reg[4:0] Ph0Ent_Status;
reg[4:0] Ph1Sha_Status;

reg[3:0] Ph1Ent_4b;


assign FIFO_REn_o=Ph0Ent_FIFO_REn;
assign ElinkTX_o=Ph0Ent_ElinkTX;
assign ElinkCK_o=Ph1Ent_ElinkCK;	
	
	
	//////******Ph1Ent_4b******//////
	always@(posedge CLK80M_i or negedge Reset_ni)
	begin
		if(~Reset_ni)
		begin
			Ph1Ent_ElinkCK<=1'b0;
		end
		else
		begin
			Ph1Ent_ElinkCK<=~Ph2Sha_ElinkCK;
		end
	end
	always@(negedge CLK80M_i or negedge Reset_ni)
	begin
		if(~Reset_ni)
		begin
			Ph2Sha_ElinkCK<=1'b0;
		end
		else
		begin
			Ph2Sha_ElinkCK<=Ph1Ent_ElinkCK;
		end
	end
	
	
	
	
	
	//////******Ph1Ent_4b******//////
	always@(posedge CLK80M_i or negedge Reset_ni)
	begin
		if(~Reset_ni)
		begin
			Ph1Ent_4b[3:0]<=4'b0000;
		end
		else
		begin
			Ph1Ent_4b[3:0]<={Ph1Ent_4b[2:0],Ph0Ent_ElinkTX};
		end
	end
	
	
	
	//////******Ph0Ent_Status******//////
	//////******Ph1Sha_Status******//////
	always@(posedge CLK80M_i or negedge Reset_ni)
	begin
		if(~Reset_ni)
		begin
			Ph1Sha_Status[4:0]<=5'b0_0000;
		end
		else
		begin
			Ph1Sha_Status[4:0]<=Ph0Ent_Status[4:0];
		end
	end
	always@(negedge CLK80M_i or negedge Reset_ni)
	begin
		if(~Reset_ni)
		begin
			Ph0Ent_Status[4:0]<=5'b0_0000;
		end
		else
		begin
			case(Ph1Sha_Status[4:0])
			//send Idle
			5'd0:begin
				Ph0Ent_ElinkTX<=1'b0;
				Ph0Ent_FIFO_REn<=1'b0;
				Ph0Ent_Status[4:0]<=Ph1Sha_Status[4:0]+1'b1;
			end
			
			5'd1,2,3,4,5,6:begin
				Ph0Ent_ElinkTX<=1'b1;
				Ph0Ent_FIFO_REn<=1'b0;
				Ph0Ent_Status[4:0]<=Ph1Sha_Status[4:0]+1'b1;
			end
			
			5'd7:begin
				Ph0Ent_ElinkTX<=1'b1;
				Ph0Ent_FIFO_REn<=1'b0;
				if(FIFO_Empty_i)
				begin
					Ph0Ent_Status[4:0]<=5'b0_0000;
				end
				else
				begin
					Ph0Ent_Status[4:0]<=Ph1Sha_Status[4:0]+1'b1;
				end
			end
			
			//send SOF
			5'd8:begin
				Ph0Ent_ElinkTX<=1'b0;
				Ph0Ent_FIFO_REn<=1'b0;
				Ph0Ent_Status[4:0]<=Ph1Sha_Status[4:0]+1'b1;
			end
			
			5'd9,10,11,12,13,14:begin
				Ph0Ent_ElinkTX<=1'b1;
				Ph0Ent_FIFO_REn<=1'b0;
				Ph0Ent_Status[4:0]<=Ph1Sha_Status[4:0]+1'b1;
			end
			
			5'd15:begin
				Ph0Ent_ElinkTX<=1'b0;
				Ph0Ent_FIFO_REn<=1'b1;
				Ph0Ent_Status[4:0]<=Ph1Sha_Status[4:0]+1'b1;
			end
			
			//send Data bit
			5'd16:begin
				Ph0Ent_ElinkTX<=FIFO_Data_i;
				
				if({Ph1Ent_4b[3:0],FIFO_Data_i}==5'b1_1111)
				begin
					Ph0Ent_FIFO_REn<=1'b0;
					Ph0Ent_Status[4:0]<=Ph1Sha_Status[4:0]+1'b1;	
				end
				else
				begin
					if(FIFO_Empty_i)
					begin
						Ph0Ent_Status[4:0]<=5'b1_0010;//jump to case18				
						Ph0Ent_FIFO_REn<=1'b0;
					end
					else
					begin
						Ph0Ent_Status[4:0]<=5'b1_0000;//stay case16
						Ph0Ent_FIFO_REn<=1'b1;
					end
				end				
			end	
			
			//send stuff bit
			5'd17:begin
				Ph0Ent_ElinkTX<=1'b0;
				if(FIFO_Empty_i)
				begin
					Ph0Ent_Status[4:0]<=Ph1Sha_Status[4:0]+1'b1;					
					Ph0Ent_FIFO_REn<=1'b0;
				end
				else
				begin
					Ph0Ent_Status[4:0]<=5'b1_0000;//jump to case16
					Ph0Ent_FIFO_REn<=1'b1;
				end
			end	
			
			
			//send EOF
			5'd18:begin
				Ph0Ent_ElinkTX<=1'b0;
				Ph0Ent_FIFO_REn<=1'b0;
				Ph0Ent_Status[4:0]<=Ph1Sha_Status[4:0]+1'b1;
			end
			
			5'd19,20,21,22,23,24:begin
				Ph0Ent_ElinkTX<=1'b1;
				Ph0Ent_FIFO_REn<=1'b0;
				Ph0Ent_Status[4:0]<=Ph1Sha_Status[4:0]+1'b1;
			end
			
			5'd25:begin
				Ph0Ent_ElinkTX<=1'b0;
				Ph0Ent_FIFO_REn<=1'b0;
				Ph0Ent_Status[4:0]<=5'b0_0000;
			end
			
			endcase
		end
	end
	
	
endmodule











//module SCA_CRC16_CCITTfalse(

//input CLK_i,
//input ENA_i,
//input BitStream_i,
//input BitValid_i,

//output reg [15:0]CRC16_o
//);

//reg [15:0]CRC16_shadow;
//wire INV1 = CRC16_shadow[15] ^ BitStream_i;
//wire INV2 = CRC16_shadow[4]  ^ CRC16_shadow[15] ^ BitStream_i;
//wire INV3 = CRC16_shadow[11] ^ CRC16_shadow[15] ^ BitStream_i;

//always@(negedge CLK_i)
//begin
//	if(~ENA_i)
//	begin
//		CRC16_shadow[15:0] <= 16'b1111_1111_1111_1111;
//	end
//	else
//	begin
//		CRC16_shadow[15:0] <= CRC16_o[15:0];
//	end
//end

		
//always@(posedge CLK_i)
//begin
//	if(~ENA_i)
//	begin
//		CRC16_o[15:0] <= 16'b1111_1111_1111_1111;
//	end
//	else
//	begin
//		if(BitValid_i)
//		begin
//			CRC16_o[15:0] <= {CRC16_shadow[14:12],INV3,CRC16_shadow[10:5],INV2,CRC16_shadow[3:0],INV1};
//		end
//		else
//		begin
//			CRC16_o[15:0] <= CRC16_shadow[15:0];
//		end
//	end
//end
		
//endmodule





//module SCA_Manager(
//input        Reset_ni,
//input        CLK_i,

//input [71:0] FIFO_RData_i,
//input        FIFO_Empty_i,
//input [15:0] CRC16_i,
//input        GotResp_i,

//output FIFO_REn_o,
//output CRC_ENA_o,
//output CRC_BitValid_o,

//output EmptyGate_o,
//output BitStream_o
//);


//reg Ph0Ent_FIFO_REn    ;
//reg Ph1Ent_CRC_ENA     ;
//reg Ph1Ent_CRC_BitValid;
//reg Ph1Ent_EmptyGate   ;
//reg Ph1Ent_BitStream   ;

//reg [7:0] Ph0Ent_Data;

//reg [3:0] Ph0Ent_Status;
//reg [3:0] Ph1Sha_Status;

//reg [7:0] Ph0Ent_Ring;
//reg [7:0] Ph1Sha_Ring;

//always@(negedge CLK_i or negedge Reset_ni)
//begin
//	if(~Reset_ni)
//	begin
//		Ph0Ent_Ring[7:0] <= 8'b0000_0000;
//	end
//	else
//	begin
//		Ph0Ent_Ring[7:0] <= Ph1Sha_Ring[];
//	end
//end

		
//always@(posedge CLK_i or negedge Reset_ni)
//begin
//	if(~Reset_ni)
//	begin
//		Ph1Sha_Ring[7:0] <= 8'b0000_0000;
//	end
//	else
//	begin
////		Ph1Sha_Ring[7:0] <= 8'b0000_0000;
//	end
//end







//always@(negedge CLK_i or negedge Reset_ni)
//begin
//	if(~Reset_ni)
//	begin
//		Ph0Ent_Status[3:0] <= 4'b0000;
//	end
//	else
//	begin
//		case(Ph1Sha_Status[3:0])
//		4'd0:begin//wait empty to be low
//			if(~FIFO_Empty_i)
//			begin
//				Ph0Ent_FIFO_REn<=1'b1;
//				Ph0Ent_Status[3:0] <= Ph1Sha_Status[3:0]+1'b1;
//			end
//			else
//			begin
//				Ph0Ent_FIFO_REn<=1'b0;
//				Ph0Ent_Status[3:0] <= Ph1Sha_Status[3:0];
//			end
//		end
		
//		4'd1:begin//send Addr
//			Ph0Ent_FIFO_REn <=1'b0;
//			Ph0Ent_Data[7:0]<=
//			if(FIFO_RData_i[71:64]==8'b1111_1111)
//			begin
//				Ph0Ent_Status[3:0] <= Ph1Sha_Status[3:0]+1'b1;
//			end
//			else
//			begin
//				Ph0Ent_Status[3:0] <= Ph1Sha_Status[3:0];
//			end
//		end
		
//		endcase
//	end
//end

		
//always@(posedge CLK_i or negedge Reset_ni)
//begin
//	if(~Reset_ni)
//	begin
//		CRC16_o[15:0] <= 16'b1111_1111_1111_1111;
//	end
//	else
//	begin
//		if(BitValid_i)
//		begin
//			CRC16_o[15:0] <= {CRC16_shadow[14:12],INV3,CRC16_shadow[10:5],INV2,CRC16_shadow[3:0],INV1};
//		end
//		else
//		begin
//			CRC16_o[15:0] <= CRC16_shadow;
//		end
//	end
//end
		
//endmodule




//module CRC16Pall(
//input       CLK_i,
//input       ENA_i,
//input [7:0] Data_i,
//input       DataValid_i,

//output[15:0] CRC16_o
//);

//reg [15:0] Ph1Ent_CRC16;
//reg [15:0] Ph2Sha_CRC16;

//assign CRC16_o[15:0]=Ph1Ent_CRC16[15:0];

//wire[3:0] X ;
//wire[3:0] Y ;
//wire[3:0] Z ;

//assign X[3]=Data_i[7] ^ Data_i[3];
//assign X[2]=Data_i[6] ^ Data_i[2];
//assign X[1]=Data_i[5] ^ Data_i[1];
//assign X[0]=Data_i[4] ^ Data_i[0];

//assign Y[3]=Ph2Sha_CRC16[15] ^ Ph2Sha_CRC16[11];
//assign Y[2]=Ph2Sha_CRC16[14] ^ Ph2Sha_CRC16[10];
//assign Y[1]=Ph2Sha_CRC16[13] ^ Ph2Sha_CRC16[9];
//assign Y[0]=Ph2Sha_CRC16[12] ^ Ph2Sha_CRC16[8];

//assign Z[3]=Data_i[7] ^ Ph2Sha_CRC16[15];
//assign Z[2]=Data_i[6] ^ Ph2Sha_CRC16[14];
//assign Z[1]=Data_i[5] ^ Ph2Sha_CRC16[13];
//assign Z[0]=Data_i[4] ^ Ph2Sha_CRC16[12];


//always@(negedge CLK_i)
//begin
//	if(~ENA_i)
//	begin
//		Ph2Sha_CRC16[15:0] <= 16'b1111_1111_1111_1111;
//	end
//	else
//	begin
//		Ph2Sha_CRC16[15:0] <= Ph1Ent_CRC16[15:0];
//	end
//end

		
//always@(posedge CLK_i)
//begin
//	if(~ENA_i)
//	begin
//		Ph1Ent_CRC16[15:0] <= 16'b1111_1111_1111_1111;
//	end
//	else
//	begin
//		if(DataValid_i)
//		begin
//			Ph1Ent_CRC16[15] <= X[3] ^ Y[3] ^        Ph2Sha_CRC16[7];
//			Ph1Ent_CRC16[14] <= X[2] ^ Y[2] ^        Ph2Sha_CRC16[6];
//			Ph1Ent_CRC16[13] <= X[1] ^ Y[1] ^        Ph2Sha_CRC16[5];
//			Ph1Ent_CRC16[12] <= X[0] ^ Y[0] ^ Z[3] ^ Ph2Sha_CRC16[4];
			
//			Ph1Ent_CRC16[11] <=               Z[2] ^ Ph2Sha_CRC16[3];
//			Ph1Ent_CRC16[10] <=               Z[1] ^ Ph2Sha_CRC16[2];
//			Ph1Ent_CRC16[9]  <=               Z[0] ^ Ph2Sha_CRC16[1];
//			Ph1Ent_CRC16[8]  <= X[3] ^ Y[3]        ^ Ph2Sha_CRC16[0];
			
//			Ph1Ent_CRC16[7]  <= X[2] ^ Y[2] ^ Z[3];
//			Ph1Ent_CRC16[6]  <= X[1] ^ Y[1] ^ Z[2];
//			Ph1Ent_CRC16[5]  <= X[0] ^ Y[0] ^ Z[1];
//			Ph1Ent_CRC16[4]  <=               Z[0];
			
//			Ph1Ent_CRC16[3]  <= X[3] ^ Y[3];
//			Ph1Ent_CRC16[2]  <= X[2] ^ Y[2];
//			Ph1Ent_CRC16[1]  <= X[1] ^ Y[1];
//			Ph1Ent_CRC16[0]  <= X[0] ^ Y[0];			
//		end
//		else
//		begin
//			Ph1Ent_CRC16[15:0] <= Ph2Sha_CRC16[15:0];
//		end
//	end
//end
		
//endmodule








//module PulseTrim_ClkNegedge(
//input  CLK_i,
//input  IN_i,
//output OUT_o
//);

//reg Ph1Ent_IN;
//reg Ph2Sha_IN;

//assign OUT_o=IN_i & Ph2Sha_IN;

//always@(negedge CLK_i)
//begin
//	Ph2Sha_IN<=Ph1Ent_IN;
//end

		
//always@(posedge CLK_i)
//begin
//	Ph1Ent_IN<=~IN_i;
//end

		
//endmodule








module SCA_ByteTXnew(
input        Reset_ni,
input        CLK_i,

input [9:0]  FIFO_RData_i,
input        FIFO_Empty_i,

output FIFO_REn_o,
output EmptyGate_o,
output BitStream_o,
output BitValid_o
);

wire [15:0] CRC_Value;

reg Ph0Ent_FIFO_REn    ;

reg Ph1Ent_Busy    ;
reg Ph2Sha_Busy    ;

reg Ph1Ent_CRC_ENA   ;
reg Ph2Sha_CRC_ENA   ;

reg Ph2Ent_BusyRst   ;

reg [7:0]  Ph1Ent_DataPipe;
reg [7:0]  Ph2Sha_DataPipe;
reg [15:0] Ph1Ent_CRCPipe;
reg [15:0] Ph2Sha_CRCPipe;

reg [7:0]  Ph2Ent_Data;
reg [6:0]  Ph3Sha_Data;

reg Ph2Ent_BitValid;
reg Ph2Ent_EmptyGate;
reg Ph3Sha_EmptyGate;

reg        Ph2Ent_IsStop;
reg        Ph3Sha_IsStop;

reg        Ph2Ent_SendData;
reg        Ph3Sha_SendData;
reg        Ph2Ent_SendCRC;
reg        Ph3Sha_SendCRC;

assign FIFO_REn_o  =Ph0Ent_FIFO_REn;
assign EmptyGate_o =Ph2Ent_EmptyGate;
assign BitStream_o =Ph2Ent_Data[7];
assign BitValid_o  =Ph2Ent_BitValid;

//////******Ph0Ent_FIFO_REn******//////
always@(negedge CLK_i or negedge Reset_ni)
begin
	if(~Reset_ni)
	begin
		Ph0Ent_FIFO_REn <=1'b0;
	end
	else
	begin
		if({Ph1Ent_Busy,FIFO_Empty_i}==2'b00)
		begin
			Ph0Ent_FIFO_REn <=1'b1;
		end
		else
		begin
			Ph0Ent_FIFO_REn <=1'b0;
		end
	end
end


//////******Ph1Ent_DataPipe******//////
always@(negedge CLK_i or negedge Reset_ni)
begin
	if(~Reset_ni)
	begin
		Ph2Sha_DataPipe[7:0] <=8'b0000_0000;
	end
	else
	begin
		Ph2Sha_DataPipe[7:0] <=Ph1Ent_DataPipe[7:0];
	end
end
always@(posedge CLK_i or negedge Reset_ni)
begin
	if(~Reset_ni)
	begin
		Ph1Ent_DataPipe[7:0] <=8'b0000_0000;
	end
	else
	begin
		Ph1Ent_DataPipe[7:0] <={Ph2Sha_DataPipe[6:0],Ph0Ent_FIFO_REn};
	end
end

//////******Ph1Ent_CRCPipe******//////
always@(negedge CLK_i or negedge Reset_ni)
begin
	if(~Reset_ni)
	begin
		Ph2Sha_CRCPipe[15:0] <=16'b0000_0000_0000_0000;
	end
	else
	begin
		Ph2Sha_CRCPipe[15:0] <=Ph1Ent_CRCPipe[15:0];
	end
end
always@(posedge CLK_i or negedge Reset_ni)
begin
	if(~Reset_ni)
	begin
		Ph1Ent_CRCPipe[15:0] <=16'b0000_0000_0000_0000;
	end
	else
	begin
		if(Ph2Ent_IsStop)
		begin
			Ph1Ent_CRCPipe[15:0] <={Ph2Sha_CRCPipe[14:0],Ph2Sha_DataPipe[7]};
		end
		else
		begin
			Ph1Ent_CRCPipe[15:0] <=16'b0000_0000_0000_0000;
		end
	end
end


//////******Ph2Ent_Data******//////
//////******Ph2Ent_IsStop******//////
always@(posedge CLK_i or negedge Reset_ni)
begin
	if(~Reset_ni)
	begin
		Ph3Sha_Data[6:0] <=7'b000_0000;
		Ph3Sha_IsStop<=1'b0;
	end
	else
	begin
		Ph3Sha_Data[6:0] <=Ph2Ent_Data[6:0];
		Ph3Sha_IsStop<=Ph2Ent_IsStop;
	end
end
always@(negedge CLK_i or negedge Reset_ni)
begin
	if(~Reset_ni)
	begin
		Ph2Ent_Data[7:0] <=8'b0000_0000;
		Ph2Ent_IsStop<=1'b0;
	end
	else
	begin
		case({Ph1Ent_CRCPipe[8],Ph1Ent_CRCPipe[0],Ph1Ent_DataPipe[0]})
		3'b001:begin
			Ph2Ent_Data[7:0] <=FIFO_RData_i[7:0];
			Ph2Ent_IsStop<=FIFO_RData_i[9];
		end
		3'b010:begin
			Ph2Ent_Data[7:0] <=CRC_Value[15:8];
			Ph2Ent_IsStop<=Ph3Sha_IsStop;
		end
		3'b100:begin
			Ph2Ent_Data[7:0] <=CRC_Value[7:0];
			Ph2Ent_IsStop<=Ph3Sha_IsStop;
		end
		default:begin
			Ph2Ent_Data[7:0] <={Ph3Sha_Data[6:0],1'b0};
			Ph2Ent_IsStop<=Ph3Sha_IsStop;
		end
		endcase
	end
end







//////******Ph2Ent_SendData******//////
always@(posedge CLK_i or negedge Reset_ni)
begin
	if(~Reset_ni)
	begin
		Ph3Sha_SendData <=1'b0;
	end
	else
	begin
		Ph3Sha_SendData <=Ph2Ent_SendData;
	end
end
always@(negedge CLK_i or negedge Reset_ni)
begin
	if(~Reset_ni)
	begin
		Ph2Ent_SendData <=1'b0;
	end
	else
	begin
		case({Ph1Ent_DataPipe[7],Ph1Ent_DataPipe[0]})
		2'b00:begin
			Ph2Ent_SendData <=Ph3Sha_SendData;
		end
		2'b01:begin
			Ph2Ent_SendData <=1'b1;
		end
		2'b10:begin
			Ph2Ent_SendData <=1'b0;
		end
		2'b11:begin
			Ph2Ent_SendData <=1'b0;
		end
		endcase
	end
end



//////******Ph2Ent_SendCRC******//////
always@(posedge CLK_i or negedge Reset_ni)
begin
	if(~Reset_ni)
	begin
		Ph3Sha_SendCRC <=1'b0;
	end
	else
	begin
		Ph3Sha_SendCRC <=Ph2Ent_SendCRC;
	end
end
always@(negedge CLK_i or negedge Reset_ni)
begin
	if(~Reset_ni)
	begin
		Ph2Ent_SendCRC <=1'b0;
	end
	else
	begin
		case({Ph1Ent_DataPipe[7],Ph1Ent_CRCPipe[15]})
		2'b00:begin
			Ph2Ent_SendCRC <=Ph3Sha_SendCRC;
		end
		2'b01:begin
			Ph2Ent_SendCRC <=1'b0;
		end
		2'b10:begin
			Ph2Ent_SendCRC <=Ph3Sha_IsStop;
		end
		2'b11:begin
			Ph2Ent_SendCRC <=1'b0;
		end
		endcase
	end
end



//////******Ph2Ent_BusyRst******//////
always@(negedge CLK_i or negedge Reset_ni)
begin
	if(~Reset_ni)
	begin
		Ph2Ent_BusyRst <=1'b0;
	end
	else
	begin
		if(Ph3Sha_IsStop)
		begin
			Ph2Ent_BusyRst<=Ph1Ent_CRCPipe[15];
		end
		else
		begin
			Ph2Ent_BusyRst<=Ph1Ent_DataPipe[7];
		end
	end
end






//////******Ph1Ent_Busy******//////
always@(negedge CLK_i or negedge Reset_ni)
begin
	if(~Reset_ni)
	begin
		Ph2Sha_Busy<=1'b0;
	end
	else
	begin
		Ph2Sha_Busy<=Ph1Ent_Busy;
	end
end
always@(posedge CLK_i or negedge Reset_ni)
begin
	if(~Reset_ni)
	begin
		Ph1Ent_Busy<=1'b0;
	end
	else
	begin
		case({Ph2Ent_BusyRst,Ph0Ent_FIFO_REn})
		2'b00:begin
			Ph1Ent_Busy<=Ph2Sha_Busy;
		end
		2'b01:begin
			Ph1Ent_Busy<=1'b1;
		end
		2'b10:begin
			Ph1Ent_Busy<=1'b0;
		end
		2'b11:begin
			Ph1Ent_Busy<=1'b0;
		end
		endcase
	end
end






//////******Ph1Ent_CRC_ENA******//////
always@(negedge CLK_i or negedge Reset_ni)
begin
	if(~Reset_ni)
	begin
		Ph2Sha_CRC_ENA<=1'b0;
	end
	else
	begin
		Ph2Sha_CRC_ENA<=Ph1Ent_CRC_ENA;
	end
end
always@(posedge CLK_i or negedge Reset_ni)
begin
	if(~Reset_ni)
	begin
		Ph1Ent_CRC_ENA<=1'b0;
	end
	else
	begin
		case({Ph2Sha_CRCPipe[10],Ph0Ent_FIFO_REn})
		2'b00:begin
			Ph1Ent_CRC_ENA<=Ph2Sha_CRC_ENA;
		end
		2'b01:begin
			Ph1Ent_CRC_ENA<=1'b1;
		end
		2'b10:begin
			Ph1Ent_CRC_ENA<=1'b0;
		end
		2'b11:begin
			Ph1Ent_CRC_ENA<=1'b0;
		end
		endcase
	end
end





//////******Ph2Ent_BitValid******//////
always@(negedge CLK_i or negedge Reset_ni)
begin
	if(~Reset_ni)
	begin
		Ph2Ent_BitValid <=1'b0;
	end
	else
	begin
		Ph2Ent_BitValid<=(Ph1Ent_DataPipe[0] |Ph3Sha_SendData |Ph3Sha_SendCRC);
	end
end



//////******Ph2Ent_EmptyGate******//////
always@(posedge CLK_i or negedge Reset_ni)
begin
	if(~Reset_ni)
	begin
		Ph3Sha_EmptyGate<=1'b0;
	end
	else
	begin
		Ph3Sha_EmptyGate<=Ph2Ent_EmptyGate;
	end
end
always@(negedge CLK_i or negedge Reset_ni)
begin
	if(~Reset_ni)
	begin
		Ph2Ent_EmptyGate<=1'b0;
	end
	else
	begin
		case({Ph1Ent_CRCPipe[15],Ph1Ent_DataPipe[0]})
		2'b00:begin
			Ph2Ent_EmptyGate<=Ph3Sha_EmptyGate;
		end
		2'b01:begin
			Ph2Ent_EmptyGate<=1'b1;
		end
		2'b10:begin
			Ph2Ent_EmptyGate<=1'b0;
		end
		2'b11:begin
			Ph2Ent_EmptyGate<=1'b0;
		end
		endcase
	end
end


SCA_CRC16Pall  SCA_CRC16Pall_inst1(
.CLK_i      (CLK_i),
.ENA_i      (Ph2Sha_CRC_ENA),
.Data_i     (Ph2Ent_Data[7:0]),
.DataValid_i(Ph2Sha_DataPipe[0]),
.CRC16_o    (CRC_Value)
);


endmodule







module SCA_CRC16Pall(
input       CLK_i,
input       ENA_i,
input [7:0] Data_i,
input       DataValid_i,

output[15:0] CRC16_o
);

reg [15:0] Ph1Ent_CRC16;
reg [15:0] Ph2Sha_CRC16;

assign CRC16_o[15:0]=Ph1Ent_CRC16[15:0];

wire[3:0] X ;
wire[3:0] Y ;
wire[3:0] Z ;

assign X[3]=Data_i[7] ^ Data_i[3];
assign X[2]=Data_i[6] ^ Data_i[2];
assign X[1]=Data_i[5] ^ Data_i[1];
assign X[0]=Data_i[4] ^ Data_i[0];

assign Y[3]=Ph2Sha_CRC16[15] ^ Ph2Sha_CRC16[11];
assign Y[2]=Ph2Sha_CRC16[14] ^ Ph2Sha_CRC16[10];
assign Y[1]=Ph2Sha_CRC16[13] ^ Ph2Sha_CRC16[9];
assign Y[0]=Ph2Sha_CRC16[12] ^ Ph2Sha_CRC16[8];

assign Z[3]=Data_i[7] ^ Ph2Sha_CRC16[15];
assign Z[2]=Data_i[6] ^ Ph2Sha_CRC16[14];
assign Z[1]=Data_i[5] ^ Ph2Sha_CRC16[13];
assign Z[0]=Data_i[4] ^ Ph2Sha_CRC16[12];


always@(negedge CLK_i)
begin
	if(~ENA_i)
	begin
		Ph2Sha_CRC16[15:0] <= 16'b1111_1111_1111_1111;
	end
	else
	begin
		Ph2Sha_CRC16[15:0] <= Ph1Ent_CRC16[15:0];
	end
end

		
always@(posedge CLK_i)
begin
	if(~ENA_i)
	begin
		Ph1Ent_CRC16[15:0] <= 16'b1111_1111_1111_1111;
	end
	else
	begin
		if(DataValid_i)
		begin
			Ph1Ent_CRC16[15] <= X[3] ^ Y[3] ^        Ph2Sha_CRC16[7];
			Ph1Ent_CRC16[14] <= X[2] ^ Y[2] ^        Ph2Sha_CRC16[6];
			Ph1Ent_CRC16[13] <= X[1] ^ Y[1] ^        Ph2Sha_CRC16[5];
			Ph1Ent_CRC16[12] <= X[0] ^ Y[0] ^ Z[3] ^ Ph2Sha_CRC16[4];
			
			Ph1Ent_CRC16[11] <=               Z[2] ^ Ph2Sha_CRC16[3];
			Ph1Ent_CRC16[10] <=               Z[1] ^ Ph2Sha_CRC16[2];
			Ph1Ent_CRC16[9]  <=               Z[0] ^ Ph2Sha_CRC16[1];
			Ph1Ent_CRC16[8]  <= X[3] ^ Y[3]        ^ Ph2Sha_CRC16[0];
			
			Ph1Ent_CRC16[7]  <= X[2] ^ Y[2] ^ Z[3];
			Ph1Ent_CRC16[6]  <= X[1] ^ Y[1] ^ Z[2];
			Ph1Ent_CRC16[5]  <= X[0] ^ Y[0] ^ Z[1];
			Ph1Ent_CRC16[4]  <=               Z[0];
			
			Ph1Ent_CRC16[3]  <= X[3] ^ Y[3];
			Ph1Ent_CRC16[2]  <= X[2] ^ Y[2];
			Ph1Ent_CRC16[1]  <= X[1] ^ Y[1];
			Ph1Ent_CRC16[0]  <= X[0] ^ Y[0];			
		end
		else
		begin
			Ph1Ent_CRC16[15:0] <= Ph2Sha_CRC16[15:0];
		end
	end
end
		
endmodule
