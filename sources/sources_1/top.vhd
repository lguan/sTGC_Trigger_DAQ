--- VERSION -1
--- version -0.1 ethernet works 


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use work.bus_types.all;

entity top is
port
(
    -- System Signals
    CLK200_P                                : in   std_logic;
    CLK200_N                                : in   std_logic;
    SW_N                                    : in   std_logic;
    SW_S                                    : in   std_logic;
    SW_E                                    : in   std_logic;
    SW_W                                    : in   std_logic;
    SW_C                                    : in   std_logic;
    LED_0                                   : out  std_logic;
    LED_1                                   : out  std_logic;
    LED_2                                   : out  std_logic;
    LED_3                                   : out  std_logic;
    LED_4                                   : out  std_logic;
    LED_5                                   : out  std_logic;
    LED_6                                   : out  std_logic;
    LED_7                                   : out  std_logic;

    -- Ethernet Signals
    refclk125_p                             : in   std_logic;
    refclk125_n                             : in   std_logic;
    txp                                     : out  std_logic;
    txn                                     : out  std_logic;
    rxp                                     : in   std_logic;
    rxn                                     : in   std_logic;
    phy_int                                 : out  std_logic;
    phy_rstn                                : out  std_logic;
    
    --CTF INPUT
    CTF_TRIG_IN_P                                : in   std_logic;
    CTF_TRIG_IN_N                                : in   std_logic;
    CTF_CKBC_IN_P                                : in   std_logic;
    CTF_CKBC_IN_N                                : in   std_logic;
    CTF_BCR_IN_P                                 : in   std_logic;
    CTF_BCR_IN_N                                 : in   std_logic;
    
    -- FEB ELINK Interface
    to_FEB_CKBC_P                             : out std_logic_vector(7 downto 0);
    to_FEB_CKBC_N                             : out std_logic_vector(7 downto 0);
    to_FEB_TTC_DATA_P                         : out std_logic_vector(7 downto 0);
    to_FEB_TTC_DATA_N                         : out std_logic_vector(7 downto 0);
    to_FEB_SCA_CLK_P                          : out std_logic_vector(7 downto 0);
    to_FEB_SCA_CLK_N                          : out std_logic_vector(7 downto 0);
    to_FEB_SCA_DATA_P                         : out std_logic_vector(7 downto 0);
    to_FEB_SCA_DATA_N                         : out std_logic_vector(7 downto 0);
    from_FEB_SCA_DATA_P                       : in  std_logic_vector(7 downto 0);
    from_FEB_SCA_DATA_N                       : in  std_logic_vector(7 downto 0);
    
    -- TDS Signals
    Q3_CLK1_GTREFCLK_PAD_N_IN               : in  std_logic;
    Q3_CLK1_GTREFCLK_PAD_P_IN               : in  std_logic;
    RXN_IN                                  : in  std_logic_vector(3 downto 0);
    RXP_IN                                  : in  std_logic_vector(3 downto 0);
    TXN_OUT                                 : out std_logic_vector(3 downto 0);
    TXP_OUT                                 : out std_logic_vector(3 downto 0) 
);
end top;


architecture RTL of top is
--**************************Component Declarations*****************************

component vio_rx_mux 
PORT (
clk : IN STD_LOGIC;

probe_out0 : OUT STD_LOGIC;
probe_out1 : OUT STD_LOGIC_vector(1 downto 0)
);
END component;


---------------------------------------------------

component daq_clk_wiz_0
    port (
        clk_in1_p           : in  std_logic;
        clk_in1_n           : in  std_logic;
        clk320_pha0         : out std_logic;
        clk320_pha180       : out std_logic;
        clk160_pha0         : out std_logic;
        clk160_pha180       : out std_logic;
        clk80_pha0          : out std_logic;
        clk80_pha180        : out std_logic;
        clk40               : out std_logic;
        reset               : in  std_logic;
        locked              : out std_logic
);
end component;




component gtcore_init
port (
    Q3_CLK1_GTREFCLK_PAD_N_IN   : in  std_logic;
    Q3_CLK1_GTREFCLK_PAD_P_IN   : in  std_logic;
    DRPCLK_IN                   : in  std_logic;
    RXN_IN                      : in std_logic_vector(3 downto 0);
    RXP_IN                      : in std_logic_vector(3 downto 0);
    TXN_OUT                     : out std_logic_vector(3 downto 0);
    TXP_OUT                     : out std_logic_vector(3 downto 0);
    RST_CHECKER                 : in  std_logic;
    GTX_RST                     : in  std_logic;
    CLK_FIFO                    : in  std_logic
);
end component;


component ethernet_wrapper
port (
    clk_80                   : in std_logic;
    clk_200                 : in  std_logic;
    clk_125                 : out std_logic;
    glbl_rst                : in  std_logic;

    -- Signals to the tx MUX
    to_eth_tx               : in  TX_mux2eth;
    eth_tx_hold             : out std_logic;

    -- Tranceiver Interface
    -----------------------
    refclk125_p             : in  std_logic;                     -- Differential +ve of reference clock for tranceiver: 125MHz, very high quality
    refclk125_n             : in  std_logic;                     -- Differential -ve of reference clock for tranceiver: 125MHz, very high quality
    txp                     : out std_logic;                     -- Differential +ve of serial transmission from PMA to PMD.
    txn                     : out std_logic;                     -- Differential -ve of serial transmission from PMA to PMD.
    rxp                     : in  std_logic;                     -- Differential +ve for serial reception from PMD to PMA.
    rxn                     : in  std_logic;                     -- Differential -ve for serial reception from PMD to PMA.
    phy_int                 : out std_logic;
    phy_rstn_out             : out std_logic;
    
   -- data received 
    rxr_fifo_rd_en          : in  std_logic;
    rxr_fifo_empty          : out std_logic;
    rxr_fifo_dout           : out std_logic_vector(8 downto 0);
    rxr_fifo_valid          : out std_logic;

    rxr_fifo_full_rd_en     : in std_logic;
    rxr_fifo_full_empty     : out std_logic;
    
    txt_fifo_full_info      : out std_logic_vector(1 downto 0)
);
end component;

component rx_mux
port (
    clk                     : in  std_logic;
    reset                   : in  std_logic;
    
    -- data from ethernet 
    rxr_fifo_rd_en          : out  std_logic;
    rxr_fifo_empty          : in std_logic;
    rxr_fifo_dout           : in std_logic_vector(8 downto 0);
    rxr_fifo_valid          : in std_logic;

    rxr_fifo_full_rd_en     : out std_logic;
    rxr_fifo_full_empty     : in std_logic;
    
    -- interface between txmux
    from_rxmux_tx         : out TX_mod2mux;
    grant_rxmux           : in  std_logic;
    
   ------ interface between sca
   cfgFIFO_wren        : out  std_logic;    
   cfgFIFO_din         : out  std_logic_vector(7 downto 0);
   cfgFIFO_data_length : out  std_logic_vector(7 downto 0);
   cfgFIFO_wr_done     : out  std_logic;    
   cfg_busy            : in   std_logic    
);
end component;

component tx_mux
Port (
    clk                     : in  std_logic;
    reset                   : in  std_logic;

    -- Signals to the ethernet components
    to_eth_tx               : out TX_mux2eth;
    hold                    : in  std_logic;

    -- Signals to each module
    from_rxmux_tx                : in  TX_mod2mux;
    grant_rxmux               : out std_logic;
    from_SCA_tx             : in  TX_mod2mux;
    grant_SCA               : out std_logic;
    
    
    txt_fifo_full_info          : in std_logic_vector(1 downto 0)
);
end component;


component sca_drive
port (
    clk80                   : in  std_logic;
    ElinkRX_i               : in  std_logic;
    ElinkTX_o               : out std_logic;
    ElinkCk_o               : out std_logic;
    sca_port_mux            : out std_logic_vector(2 downto 0);
 
    send_request        : out std_logic;
    send_data           : out std_logic_vector(31 downto 0);
    send_end_packet     : out std_logic;
    send_wr_en          : out std_logic;
    send_grant          : in  std_logic;
    
    
    cfgFIFO_wren        : in  std_logic;    
    cfgFIFO_din         : in  std_logic_vector(7 downto 0);
    cfgFIFO_data_length : in  std_logic_vector(7 downto 0);
    cfgFIFO_wr_done     : in  std_logic;    
    cfg_busy            : out  std_logic    
);
end component;


component sca_mux
port (
    sca_port_mux         : in std_logic_vector(2 downto 0);
    SCA_ElinkCk_o        : in std_logic;
    SCA_ElinkTX_o        : in std_logic;
    SCA_ElinkRX_i        : out std_logic;
    to_FEB_SCA_CLK      : out std_logic_vector(7 downto 0);
    to_FEB_SCA_DATA     : out std_logic_vector(7 downto 0);
    from_FEB_SCA_DATA   : in std_logic_vector(7 downto 0)    
);
end component;


component ttc_encode
port (
     CLK40               : in std_logic;
     CLK320              : in std_logic;
     EC0R                : in std_logic;
     SCA_RST             : in std_logic;
     TRIG_i              : in std_logic;
     BCR_OCR             : in std_logic;
     ECR                 : in std_logic;
     VMM_TESTPULSE_ena   : in std_logic;
     ROC_SoftRST         : in std_logic;
     TTC_ena             : in std_logic;
     TTC_STREAM_o         : out std_logic
    );
end component;


-- Custom Signal Declarations (CLOCKS)
-- from 200 MHz clock
signal  clk_200             : std_logic;

-- 125 MHz for Ethernet 
signal  clk_125             : std_logic;

-- from CTF module (for FEB trigger and readout)
signal clk320_pha0          : std_logic;
signal clk320_pha180        : std_logic;
signal clk160_pha0          : std_logic;
signal clk160_pha180        : std_logic;
signal clk80_pha0           : std_logic;
signal clk80_pha180         : std_logic;
signal clk40                : std_logic;

signal to_eth_tx            : TX_mux2eth;
signal eth_tx_hold          : std_logic;

-- reply control (to be sent via Ethernet to Host PC) for various modules 
signal from_sca_tx          : TX_mod2mux;
signal grant_SCA            : std_logic;
signal from_rxmux_tx        : TX_mod2mux;
signal grant_rxmux          : std_logic;


-- Signals for SCA
signal cfgFIFO_wren        : std_logic;
signal cfgFIFO_din         : std_logic_vector(7 downto 0);
signal cfgFIFO_data_length : std_logic_vector(7 downto 0);
signal cfgFIFO_wr_done     : std_logic;
signal cfg_busy            : std_logic;

signal SCA_ElinkRX_i            : std_logic;
signal SCA_ElinkTX_o            : std_logic;
signal SCA_ElinkCk_o            : std_logic;
signal sca_port_mux         : std_logic_vector(2 downto 0);
signal to_FEB_SCA_CLK       : std_logic_vector(7 downto 0);
signal to_FEB_SCA_DATA      : std_logic_vector(7 downto 0);
signal from_FEB_SCA_DATA    : std_logic_vector(7 downto 0);

-- Signals for TTC to FEB
signal EC0R                 : std_logic;
signal SCA_RST              : std_logic;
signal TRIG_i               : std_logic;
signal BCR_OCR              : std_logic;
signal ECR                  : std_logic;
signal VMM_TESTPULSE_ena    : std_logic;
signal ROC_SoftRST          : std_logic;
signal TTC_ena              : std_logic;
signal to_FEB_TTC_DATA      : std_logic;

-- Signals for CKBC to FEB

signal to_FEB_CKBC          : std_logic;

-- Signals for Deubug/Display
signal  LED_counter         : unsigned(22 downto 0);

-- Signals for UDP RX/TX Mux
signal rxr_fifo_rd_en       : std_logic;
signal rxr_fifo_dout        : std_logic_vector(8 downto 0);
signal rxr_fifo_empty       : std_logic;
signal rxr_fifo_valid       : std_logic;
signal rxr_fifo_full_rd_en  : std_logic;
signal rxr_fifo_full_empty  : std_logic;
  
signal sca_busy_fake        : std_logic;
signal txt_fifo_full_fake       : std_logic_vector(1 downto 0);
signal txt_fifo_full_info       : std_logic_vector(1 downto 0);



--**************************** Main Body of Code *******************************
begin

--vio_rx_mux_i: vio_rx_mux 
--PORT map(
--clk        => clk80_pha0,

--probe_out0 => sca_busy_fake,
--probe_out1 => txt_fifo_full_fake
--);


daq_clk_wiz_0_inst: daq_clk_wiz_0
port map (
    clk_in1_p             => CTF_CKBC_IN_P,
    clk_in1_n             => CTF_CKBC_IN_N,
    clk320_pha0           => clk320_pha0,
    clk320_pha180         => clk320_pha180,
    clk160_pha0           => clk160_pha0,
    clk160_pha180         => clk160_pha180,
    clk80_pha0            => clk80_pha0,
    clk80_pha180          => clk80_pha180,
    clk40                 => clk40,
    reset               => '0',
    locked              => open
);


-- CKBC assignment
to_FEB_CKBC <= clk40;

-- LED blinking
LED_1 <= not(LED_counter(22));
LED_2 <= LED_counter(22);
LED_3 <= not(LED_counter(22));
LED_4 <= LED_counter(22);
LED_5 <= not(LED_counter(22));
LED_6 <= LED_counter(22);
LED_7 <= not(LED_counter(22));

LED_count_proc: process(clk40)
begin
    if rising_edge(clk40) then -- counter for LEDs
        if (SW_N = '1' or SW_C = '1' or SW_W = '1') then
            LED_counter <= (others => '0');
        else
            LED_counter <= LED_counter + 1;
        end if;
    end if;
end process;


gtcore: gtcore_init
port map (
    Q3_CLK1_GTREFCLK_PAD_N_IN   => Q3_CLK1_GTREFCLK_PAD_N_IN,
    Q3_CLK1_GTREFCLK_PAD_P_IN   => Q3_CLK1_GTREFCLK_PAD_P_IN,
    DRPCLK_IN                   => clk80_pha0,
    RXN_IN                      => RXN_IN,
    RXP_IN                      => RXP_IN,
    TXN_OUT                     => TXN_OUT,
    TXP_OUT                     => TXP_OUT,
    RST_CHECKER                 => '0',
    GTX_RST                     => '0',
    CLK_FIFO                    => clk80_pha0
);


-- Ethernet module
ethernet_wrap: ethernet_wrapper
port map (
    clk_80              => clk80_pha0,
    clk_200             => clk_200,
    clk_125             => clk_125,
    glbl_rst            => SW_C,

    -- Signals to the tx MUX
    to_eth_tx           => to_eth_tx,
    eth_tx_hold         => eth_tx_hold,
    
    -- Tranceiver Interface
    refclk125_p         => refclk125_p,
    refclk125_n         => refclk125_n,
    txp                 => txp,
    txn                 => txn,
    rxp                 => rxp,
    rxn                 => rxn,
    phy_int             => phy_int,
    phy_rstn_out        => phy_rstn,
    
    rxr_fifo_rd_en      =>rxr_fifo_rd_en,
    rxr_fifo_empty      =>rxr_fifo_empty,
    rxr_fifo_dout       =>rxr_fifo_dout,
    rxr_fifo_valid      =>rxr_fifo_valid,  
    rxr_fifo_full_rd_en =>rxr_fifo_full_rd_en,
    rxr_fifo_full_empty =>rxr_fifo_full_empty,
    txt_fifo_full_info  =>txt_fifo_full_info
);


rx_eth_mux: rx_mux
port map (
    clk                 => clk80_pha0,
    reset               => '0',

    --data from ethernet 
    rxr_fifo_rd_en      =>rxr_fifo_rd_en,
    rxr_fifo_empty      =>rxr_fifo_empty,
    rxr_fifo_dout       =>rxr_fifo_dout,
    rxr_fifo_valid      =>rxr_fifo_valid,
    rxr_fifo_full_rd_en =>rxr_fifo_full_rd_en,
    rxr_fifo_full_empty =>rxr_fifo_full_empty,
    
    -- interface between txmux
    from_rxmux_tx         => from_rxmux_tx,
    grant_rxmux           => grant_rxmux,
    
    -- interface between SCA
    cfgFIFO_wren        =>  cfgFIFO_wren,
    cfgFIFO_din         =>  cfgFIFO_din,
    cfgFIFO_data_length =>  cfgFIFO_data_length,
    cfgFIFO_wr_done     =>  cfgFIFO_wr_done,
    cfg_busy            =>  cfg_busy 
    --cfg_busy            =>  sca_busy_fake
);

tx_eth_mux: tx_mux
port map (
    clk                 => clk80_pha0,
    reset               => '0',
    to_eth_tx           => to_eth_tx,
    hold                => eth_tx_hold,

    from_SCA_tx         => from_SCA_tx,
    grant_SCA           => grant_SCA,
    
    from_rxmux_tx         => from_rxmux_tx,
    grant_rxmux           => grant_rxmux,
    
    txt_fifo_full_info    => txt_fifo_full_info
    --txt_fifo_full_info    => txt_fifo_full_fake
);

sca_inst: sca_drive
port map (
    clk80               => clk80_pha0,
    ElinkRX_i           => SCA_ElinkRX_i,
    ElinkTX_o           => SCA_ElinkTX_o,
    ElinkCk_o           => SCA_ElinkCk_o,
    sca_port_mux        => sca_port_mux,
    
    -- Ethernet signals

    send_request        => from_SCA_tx.request,
    send_data           => from_SCA_tx.data_in,
    send_end_packet     => from_SCA_tx.end_packet,
    send_wr_en          => from_SCA_tx.wr_en,
    send_grant          => grant_SCA,
    
    cfgFIFO_wren        =>  cfgFIFO_wren,
    cfgFIFO_din         =>  cfgFIFO_din,
    cfgFIFO_data_length =>  cfgFIFO_data_length,
    cfgFIFO_wr_done     =>  cfgFIFO_wr_done,
    cfg_busy            =>  cfg_busy 
    
);


sca_mux_inst: sca_mux
port map (
    sca_port_mux         => sca_port_mux,
    SCA_ElinkCk_o        => SCA_ElinkCk_o,
    SCA_ElinkRX_i        => SCA_ElinkRX_i,
    SCA_ElinkTX_o        => SCA_ElinkTX_o,
    to_FEB_SCA_CLK      => to_FEB_SCA_CLK,
    to_FEB_SCA_DATA     => to_FEB_SCA_DATA,
    from_FEB_SCA_DATA   => from_FEB_SCA_DATA

);


ttc_encode_inst: ttc_encode
port map (
     CLK40               => clk40,
     CLK320              => clk320_pha0,
     EC0R                => EC0R,
     SCA_RST             => SCA_RST,
     TRIG_i              => TRIG_i,
     BCR_OCR             => BCR_OCR,
     ECR                 => ECR,
     VMM_TESTPULSE_ena   => VMM_TESTPULSE_ena,
     ROC_SoftRST         => ROC_SoftRST,
     TTC_ena             => TTC_ena,
     TTC_STREAM_o         => to_FEB_TTC_DATA
);





-------##### DIFFERENTIAL OUTPUT and INPUT BUFFER #####--------------




   IBUFDS_SGMII_INDEP_CLK_inst : IBUFDS
   generic map (
      DIFF_TERM => FALSE, -- Differential Termination 
      IBUF_LOW_PWR => TRUE, -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
      IOSTANDARD => "DEFAULT")
   port map (
      O => clk_200,  -- Buffer output
      I => CLK200_P,  -- Diff_p buffer input (connect directly to top-level port)
      IB =>CLK200_N -- Diff_n buffer input (connect directly to top-level port)
   );


   IBUFDS_CTF_TRIG_inst : IBUFDS
   generic map (
      DIFF_TERM => FALSE, -- Differential Termination 
      IBUF_LOW_PWR => TRUE, -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
      IOSTANDARD => "DEFAULT")
   port map (
      O => TRIG_i,  -- Buffer output
      I => CTF_TRIG_IN_P,  -- Diff_p buffer input (connect directly to top-level port)
      IB => CTF_TRIG_IN_N -- Diff_n buffer input (connect directly to top-level port)
   );

   IBUFDS_CTF_BCR_inst : IBUFDS
   generic map (
      DIFF_TERM => FALSE, -- Differential Termination 
      IBUF_LOW_PWR => TRUE, -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
      IOSTANDARD => "DEFAULT")
   port map (
      O => BCR_OCR,  -- Buffer output
      I => CTF_BCR_IN_P,  -- Diff_p buffer input (connect directly to top-level port)
      IB => CTF_BCR_IN_N -- Diff_n buffer input (connect directly to top-level port)
   );

    
to_FEB_SCA_CLK_inst: for i in 0 to 7 generate
  begin
  obuf_to_feb_sca_clk_inst: OBUFDS
  generic map (
      IOSTANDARD          => "LVDS",
      SLEW                => "FAST"
  )
  port map (
      O                   => to_FEB_SCA_CLK_P(i),
      OB                  => to_FEB_SCA_CLK_N(i),
      I                   => to_FEB_SCA_CLK(i)
  );

 end generate;

to_FEB_SCA_DATA_inst: for i in 0 to 7 generate
  begin
  obuf_to_feb_sca_data_inst: OBUFDS
  generic map (
      IOSTANDARD          => "LVDS",
      SLEW                => "FAST"
  )
  port map (
      O                   => to_FEB_SCA_DATA_P(i),
      OB                  => to_FEB_SCA_DATA_N(i),
      I                   => to_FEB_SCA_DATA(i)
  );

 end generate;

from_FEB_SCA_DATA_inst: for i in 0 to 7 generate
  begin
  ibuf_from_feb_sca_data_inst: IBUFDS
  generic map (
      DIFF_TERM => FALSE, -- Differential Termination 
      IOSTANDARD          => "LVDS"
  )
  port map (
      O                  => from_FEB_SCA_DATA(i),
      I                  => from_FEB_SCA_DATA_P(i),
      IB                 => from_FEB_SCA_DATA_N(i)
  );

 end generate;

to_FEB_CKBC_inst: for i in 0 to 7 generate
  begin
  obuf_to_feb_sca_clk_inst: OBUFDS
  generic map (
      IOSTANDARD          => "LVDS",
      SLEW                => "FAST"
  )
  port map (
      O                   => to_FEB_CKBC_P(i),
      OB                  => to_FEB_CKBC_N(i),
      I                   => to_FEB_CKBC
  );

 end generate;

to_FEB_TTC_DATA_inst: for i in 0 to 7 generate
  begin
  obuf_to_feb_sca_clk_inst: OBUFDS
  generic map (
      IOSTANDARD          => "LVDS",
      SLEW                => "FAST"
  )
  port map (
      O                   => to_FEB_TTC_DATA_P(i),
      OB                  => to_FEB_TTC_DATA_N(i),
      I                   => to_FEB_TTC_DATA
  );

 end generate;

end RTL;