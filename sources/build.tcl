# Vivado (TM) v2016.2 (64-bit)
#
# build.tcl: Tcl script for re-creating vivado project
#
#*****************************************************************************************
#################################  README FIRST ##########################################
#*****************************************************************************************
#
# This generic .tcl script points to the source files needed to re-create the project
# 'xxx'. The user may change the project name at the #Set project name
#  field as he wishes. (e.g. set projectname "myproject"). 
#
# In order to execute the tcl script and build the project, run Vivado and go to: 
# Tools -> Run Tcl Script...
#
# An alternative way would be to open a terminal, and run this command:
# vivado -mode batch -source <PATH>/build.tcl
#
# For more info on how to make further changes to the script, see: 
# http://xillybus.com/tutorials/vivado-version-control-packaging
# 
#*****************************************************************************************
########################## Reid Pinkham, reidpinkham@gmail.com ##########################
#*****************************************************************************************
# Set part type
set thepart "xc7k325tffg900-2"

# Set project name
set projectname "STGC_TRIG_DAQ"

# Set the reference directory for source file relative paths (by default the value is script directory path)
set origin_dir [file dirname [info script]]

# Create project (Force Overwrite Created Project Folder)
create_project $projectname $origin_dir/$projectname -force



## Use origin directory path location variable, if specified in the tcl shell
#if { [info exists ::origin_dir_loc] } {
#  set origin_dir $::origin_dir_loc
#}

variable script_file
set script_file "build.tcl"

# Help information for this script
proc help {} {
  variable script_file
  puts "\nDescription:"
  puts "Recreate a Vivado project from this script. The created project will be"
  puts "functionally equivalent to the original project for which this script was"
  puts "generated. The script contains commands for creating a project, filesets,"
  puts "runs, adding/importing sources and setting properties on various objects.\n"
  puts "Syntax:"
  puts "$script_file"
  puts "$script_file -tclargs \[--origin_dir <path>\]"
  puts "$script_file -tclargs \[--help\]\n"
  puts "Usage:"
  puts "Name                   Description"
  puts "-------------------------------------------------------------------------"
  puts "\[--origin_dir <path>\]  Determine source file paths wrt this path. Default"
  puts "                       origin_dir path value is \".\", otherwise, the value"
  puts "                       that was set with the \"-paths_relative_to\" switch"
  puts "                       when this script was generated.\n"
  puts "\[--help\]               Print help information for this script"
  puts "-------------------------------------------------------------------------\n"
  exit 0
}

if { $::argc > 0 } {
  for {set i 0} {$i < [llength $::argc]} {incr i} {
    set option [string trim [lindex $::argv $i]]
    switch -regexp -- $option {
      "--origin_dir" { incr i; set origin_dir [lindex $::argv $i] }
      "--help"       { help }
      default {
        if { [regexp {^-} $option] } {
          puts "ERROR: Unknown option '$option' specified, please type '$script_file -tclargs --help' for usage info.\n"
          return 1
        }
      }
    }
  }
}

# Set the directory path for the original project from where this script was exported
set orig_proj_dir "[file normalize "$origin_dir"]"

# Set the directory path for the new project
set proj_dir [get_property directory [current_project]]

# Reconstruct message rules
# None

# Set project properties
set obj [get_projects $projectname]
set_property "default_lib" "xil_defaultlib" $obj
set_property "generate_ip_upgrade_log" "0" $obj
set_property "part" $thepart $obj
set_property "sim.ip.auto_export_scripts" "1" $obj
set_property "simulator_language" "VHDL" $obj
set_property "target_language" "VHDL" $obj
set_property "xpm_libraries" "XPM_CDC XPM_MEMORY" $obj

# # Create 'sources_1' fileset (if not found)
if {[string equal [get_filesets -quiet sources_1] ""]} {
  create_fileset -srcset sources_1
}

# Set 'sources_1' fileset object
# set obj [get_filesets sources_1]
set obj [get_filesets sources_1]   
set files [list \
 "[file normalize "$origin_dir/sources_1/top.vhd"]"\
 "[file normalize "$origin_dir/sources_1/bus_types.vhd"]"\
 "[file normalize "$origin_dir/sources_1/rx_mux.vhd"]"\
 "[file normalize "$origin_dir/sources_1/tx_mux.vhd"]"\
 "[file normalize "$origin_dir/sources_1/SCA/SCA_DRIVE.v"]"\
 "[file normalize "$origin_dir/sources_1/SCA/sca_mux.v"]"\
 "[file normalize "$origin_dir/sources_1/SCA/HDLC_DWORD_PREP.v"]"\
 "[file normalize "$origin_dir/sources_1/SCA/HDLC_FRMGEN.v"]"\
 "[file normalize "$origin_dir/sources_1/SCA/SCAnew.v"]"\
 "[file normalize "$origin_dir/sources_1/SCA/SCA_RX.v"]"\
 "[file normalize "$origin_dir/sources_1/SCA/SCA_RX_DWORD.v"]"\
 "[file normalize "$origin_dir/sources_1/GT/gtcore_init.v"]"\
 "[file normalize "$origin_dir/sources_1/GT/gtcore_gt_frame_gen.v"]"\
 "[file normalize "$origin_dir/sources_1/GT/gtcore_gt_frame_check.v"]"\
 "[file normalize "$origin_dir/sources_1/GT/tds_packet_formater.v"]"\
 "[file normalize "$origin_dir/sources_1/GT/gt_data2fifo.v"]"\
 "[file normalize "$origin_dir/sources_1/GT/strip_mode.v"]"\
 "[file normalize "$origin_dir/sources_1/GT/strip_descrambler.v"]"\
 "[file normalize "$origin_dir/sources_1/GT/Any_PRBS_Checker.v"]"\
 "[file normalize "$origin_dir/sources_1/TTC/ttc_encode.v"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/ethernet_wrapper.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/ETH_RX.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/FIFO2UDP.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/imports/arp_REQ.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/imports/arp.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/imports/arp_RX.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/imports/axi.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/imports/temac_10_100_1000_config_vector_sm.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/imports/arp_STORE_br.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/imports/IP_complete_nomac.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/imports/tx_arbitrator.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/imports/arp_SYNC.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/imports/IPv4_RX.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/imports/UDP_Complete_nomac.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/imports/arp_SYNC.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/imports/arp_TX.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/imports/IPv4_TX.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/imports/UDP_RX.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/imports/arp_types.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/imports/ipv4_types.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/imports/UDP_TX.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/imports/arpv2.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/imports/IPv4.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/imports/gig_ethernet_wrapper.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/imports/sgmii_10_100_1000/phy_reset.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/imports/sgmii_10_100_1000/temac_10_100_1000/temac_10_100_1000_fifo_block.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/imports/sgmii_10_100_1000/temac_10_100_1000/temac_10_100_1000_block.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/imports/sgmii_10_100_1000/temac_10_100_1000/axi_ipif/temac_10_100_1000_ipif_pkg.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/imports/sgmii_10_100_1000/temac_10_100_1000/fifo/temac_10_100_1000_rx_client_fifo.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/imports/sgmii_10_100_1000/temac_10_100_1000/fifo/temac_10_100_1000_tx_client_fifo.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/imports/sgmii_10_100_1000/temac_10_100_1000/fifo/temac_10_100_1000_ten_100_1g_eth_fifo.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/imports/sgmii_10_100_1000/temac_10_100_1000/common/temac_10_100_1000_reset_sync.vhd"]"\
 "[file normalize "$origin_dir/sources_1/Ethernet/imports/sgmii_10_100_1000/temac_10_100_1000/common/temac_10_100_1000_sync_block.vhd"]"\
 "[file normalize "$origin_dir/sources_1/ip/gig_ethernet_pcs_pma_0.xcix"]"\
 "[file normalize "$origin_dir/sources_1/ip/temac_10_100_1000.xcix"]"\
 "[file normalize "$origin_dir/sources_1/ip/daq_clk_wiz_0.xcix"]"\
 "[file normalize "$origin_dir/sources_1/ip/gt_clk_wiz_1.xcix"]"\
 "[file normalize "$origin_dir/sources_1/ip/gtcore_tds4.xcix"]"\
 "[file normalize "$origin_dir/sources_1/ip/fifo_gt_channel_data.xcix"]"\
 "[file normalize "$origin_dir/sources_1/ip/fifo_eth_tx_data.xcix"]"\
 "[file normalize "$origin_dir/sources_1/ip/fifo_eth_tx_packet_length.xcix"]"\
 "[file normalize "$origin_dir/sources_1/ip/fifo_eth_rx_data.xcix"]"\
 "[file normalize "$origin_dir/sources_1/ip/fifo_eth_rx_full.xcix"]"\
 "[file normalize "$origin_dir/sources_1/ip/fifo_sca_cfgdata.xcix"]"\
 "[file normalize "$origin_dir/sources_1/ip/fifo_sca_hdlc_1b.xcix"]"\
 "[file normalize "$origin_dir/sources_1/ip/fifo_sca_hdlc_10b.xcix"]"\
 "[file normalize "$origin_dir/sources_1/ip/debug/ila_rx_mux.xcix"]"\
 "[file normalize "$origin_dir/sources_1/ip/debug/ila_tx_mux.xcix"]"\
 "[file normalize "$origin_dir/sources_1/ip/debug/ila_eth_tx_fifo2udp.xcix"]"\
 "[file normalize "$origin_dir/sources_1/ip/debug/ila_eth_rx_udp2fifo.xcix"]"\
 "[file normalize "$origin_dir/sources_1/ip/debug/ila_sca_port.xcix"]"\
 "[file normalize "$origin_dir/sources_1/ip/debug/ila_sca_cfgFIFO2BRAM.xcix"]"\
 "[file normalize "$origin_dir/sources_1/ip/debug/ila_sca_HDLC_dword_prep.xcix"]"\
 "[file normalize "$origin_dir/sources_1/ip/debug/ila_sca_rx_dword.xcix"]"\
 "[file normalize "$origin_dir/sources_1/ip/debug/ila_gt_stripTDS.xcix"]"\
 "[file normalize "$origin_dir/sources_1/ip/debug/ila_gt_data2fifo.xcix"]"\
 "[file normalize "$origin_dir/sources_1/ip/debug/ila_gt_channelFIFO_dout.xcix"]"\
 "[file normalize "$origin_dir/sources_1/ip/debug/ila_gt_prbs.xcix"]"\
 "[file normalize "$origin_dir/sources_1/ip/debug/vio_rx_mux.xcix"]"\
 "[file normalize "$origin_dir/sources_1/ip/debug/vio_gt_fifo_ctrl.xcix"]"\
]
add_files -norecurse -fileset $obj $files

# Set 'sources_1' fileset file properties for remote files
set file "$origin_dir/sources_1/top.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
set_property "file_type" "VHDL" $file_obj

# Set 'sources_1' fileset file properties for local files
# None

# Set 'sources_1' fileset properties
set obj [get_filesets sources_1]
set_property "top" "top" $obj

# Set 'sources_1' fileset object
#set obj [get_filesets sources_1]
#set files [list \
# "[file normalize "$origin_dir/sources_1/ip/ip_Core.xcix"]"\
#]
#add_files -norecurse -fileset $obj $files


# Set 'sources_1' fileset file properties for remote files
# None

# Set 'sources_1' fileset file properties for local files
# None

# Create 'constrs_1' fileset (if not found)
if {[string equal [get_filesets -quiet constrs_1] ""]} {
  create_fileset -constrset constrs_1
}

# Set 'constrs_1' fileset object
set obj [get_filesets constrs_1]

# Add/Import constrs file and set constrs file properties

set file "[file normalize "$origin_dir/constrs_1/constraint_SYSTEM.xdc"]"
set file_added [add_files -norecurse -fileset $obj $file]
set file "$origin_dir/constrs_1/constraint_SYSTEM.xdc"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets constrs_1] [list "*$file"]]
set_property "file_type" "XDC" $file_obj

set file "[file normalize "$origin_dir/constrs_1/constraint_ETH.xdc"]"
set file_added [add_files -norecurse -fileset $obj $file]
set file "$origin_dir/constrs_1/constraint_ETH.xdc"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets constrs_1] [list "*$file"]]
set_property "file_type" "XDC" $file_obj

set file "[file normalize "$origin_dir/constrs_1/constraint_ELINK.xdc"]"
set file_added [add_files -norecurse -fileset $obj $file]
set file "$origin_dir/constrs_1/constraint_ELINK.xdc"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets constrs_1] [list "*$file"]]
set_property "file_type" "XDC" $file_obj

set file "[file normalize "$origin_dir/constrs_1/constraint_GTX.xdc"]"
set file_added [add_files -norecurse -fileset $obj $file]
set file "$origin_dir/constrs_1/constraint_GTX.xdc"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets constrs_1] [list "*$file"]]
set_property "file_type" "XDC" $file_obj

# Set 'constrs_1' fileset properties
set obj [get_filesets constrs_1]

# Create 'sim_1' fileset (if not found)
if {[string equal [get_filesets -quiet sim_1] ""]} {
  create_fileset -simset sim_1
}

# # Set 'sim_1' fileset object
# set obj [get_filesets sim_1]
# set files [list \
#  "[file normalize "$origin_dir/sims/packet_formation_tester_top_tb.vhd"]"\
# ]
# add_files -norecurse -fileset $obj $files

# # Set 'sim_1' fileset file properties for remote files
# set file "$origin_dir/sims/packet_formation_tester_top_tb.vhd"
# set file [file normalize $file]
# set file_obj [get_files -of_objects [get_filesets sim_1] [list "*$file"]]
# set_property "file_type" "VHDL" $file_obj


# Set 'sim_1' fileset file properties for local files
# None

# Set 'sim_1' fileset properties
# set obj [get_filesets sim_1]
# set_property "top" "packet_formation_tester_top_tb" $obj
# set_property "transport_int_delay" "0" $obj
# set_property "transport_path_delay" "0" $obj
# set_property "xelab.nosort" "1" $obj
# set_property "xelab.unifast" "" $obj

# Create 'synth_1' run (if not found)
if {[string equal [get_runs -quiet synth_1] ""]} {
  create_run -name synth_1 -part $thepart -flow {Vivado Synthesis 2016} -strategy "Vivado Synthesis Defaults" -constrset constrs_1
} else {
  set_property strategy "Vivado Synthesis Defaults" [get_runs synth_1]
  set_property flow "Vivado Synthesis 2016" [get_runs synth_1]
}
set obj [get_runs synth_1]
set_property "part" "$thepart" $obj

# set the current synth run
current_run -synthesis [get_runs synth_1]

# Create 'impl_1' run (if not found)
if {[string equal [get_runs -quiet impl_1] ""]} {
  create_run -name impl_1 -part $thepart -flow {Vivado Implementation 2016} -strategy "Vivado Implementation Defaults" -constrset constrs_1 -parent_run synth_1
} else {
  set_property strategy "Vivado Implementation Defaults" [get_runs impl_1]
  set_property flow "Vivado Implementation 2016" [get_runs impl_1]
}
set obj [get_runs impl_1]
set_property "part" $thepart $obj
set_property "steps.write_bitstream.args.readback_file" "0" $obj
set_property "steps.write_bitstream.args.verbose" "0" $obj

# set the current impl run
current_run -implementation [get_runs impl_1]

puts "INFO: Project created:$projectname"
