proc start_step { step } {
  set stopFile ".stop.rst"
  if {[file isfile .stop.rst]} {
    puts ""
    puts "*** Halting run - EA reset detected ***"
    puts ""
    puts ""
    return -code error
  }
  set beginFile ".$step.begin.rst"
  set platform "$::tcl_platform(platform)"
  set user "$::tcl_platform(user)"
  set pid [pid]
  set host ""
  if { [string equal $platform unix] } {
    if { [info exist ::env(HOSTNAME)] } {
      set host $::env(HOSTNAME)
    }
  } else {
    if { [info exist ::env(COMPUTERNAME)] } {
      set host $::env(COMPUTERNAME)
    }
  }
  set ch [open $beginFile w]
  puts $ch "<?xml version=\"1.0\"?>"
  puts $ch "<ProcessHandle Version=\"1\" Minor=\"0\">"
  puts $ch "    <Process Command=\".planAhead.\" Owner=\"$user\" Host=\"$host\" Pid=\"$pid\">"
  puts $ch "    </Process>"
  puts $ch "</ProcessHandle>"
  close $ch
}

proc end_step { step } {
  set endFile ".$step.end.rst"
  set ch [open $endFile w]
  close $ch
}

proc step_failed { step } {
  set endFile ".$step.error.rst"
  set ch [open $endFile w]
  close $ch
}

set_msg_config -id {HDL 9-1061} -limit 100000
set_msg_config -id {HDL 9-1654} -limit 100000

start_step init_design
set rc [catch {
  create_msg_db init_design.pb
  set_property design_mode GateLvl [current_fileset]
  set_param project.singleFileAddWarning.threshold 0
  set_property webtalk.parent_dir /home/liang/work/sTGC_Trigger_DAQ/sources/STGC_TRIG_DAQ/STGC_TRIG_DAQ.cache/wt [current_project]
  set_property parent.project_path /home/liang/work/sTGC_Trigger_DAQ/sources/STGC_TRIG_DAQ/STGC_TRIG_DAQ.xpr [current_project]
  set_property ip_repo_paths /home/liang/work/sTGC_Trigger_DAQ/sources/STGC_TRIG_DAQ/STGC_TRIG_DAQ.cache/ip [current_project]
  set_property ip_output_repo /home/liang/work/sTGC_Trigger_DAQ/sources/STGC_TRIG_DAQ/STGC_TRIG_DAQ.cache/ip [current_project]
  set_property XPM_LIBRARIES {XPM_CDC XPM_MEMORY} [current_project]
  add_files -quiet /home/liang/work/sTGC_Trigger_DAQ/sources/STGC_TRIG_DAQ/STGC_TRIG_DAQ.runs/synth_1/top.dcp
  add_files -quiet /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/temac_10_100_1000/temac_10_100_1000.dcp
  set_property netlist_only true [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/temac_10_100_1000/temac_10_100_1000.dcp]
  add_files -quiet /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_gt_channel_data/fifo_gt_channel_data.dcp
  set_property netlist_only true [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_gt_channel_data/fifo_gt_channel_data.dcp]
  add_files -quiet /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/debug/ila_gt_stripTDS/ila_gt_stripTDS.dcp
  set_property netlist_only true [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/debug/ila_gt_stripTDS/ila_gt_stripTDS.dcp]
  add_files -quiet /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/debug/ila_gt_data2fifo/ila_gt_data2fifo.dcp
  set_property netlist_only true [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/debug/ila_gt_data2fifo/ila_gt_data2fifo.dcp]
  add_files -quiet /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/debug/ila_gt_channelFIFO_dout/ila_gt_channelFIFO_dout.dcp
  set_property netlist_only true [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/debug/ila_gt_channelFIFO_dout/ila_gt_channelFIFO_dout.dcp]
  add_files -quiet /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/debug/vio_gt_fifo_ctrl/vio_gt_fifo_ctrl.dcp
  set_property netlist_only true [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/debug/vio_gt_fifo_ctrl/vio_gt_fifo_ctrl.dcp]
  add_files -quiet /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/gig_ethernet_pcs_pma_0/gig_ethernet_pcs_pma_0.dcp
  set_property netlist_only true [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/gig_ethernet_pcs_pma_0/gig_ethernet_pcs_pma_0.dcp]
  add_files -quiet /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/gt_clk_wiz_1/gt_clk_wiz_1.dcp
  set_property netlist_only true [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/gt_clk_wiz_1/gt_clk_wiz_1.dcp]
  add_files -quiet /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_eth_tx_data/fifo_eth_tx_data.dcp
  set_property netlist_only true [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_eth_tx_data/fifo_eth_tx_data.dcp]
  add_files -quiet /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_eth_tx_packet_length/fifo_eth_tx_packet_length.dcp
  set_property netlist_only true [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_eth_tx_packet_length/fifo_eth_tx_packet_length.dcp]
  add_files -quiet /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_eth_rx_data/fifo_eth_rx_data.dcp
  set_property netlist_only true [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_eth_rx_data/fifo_eth_rx_data.dcp]
  add_files -quiet /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_eth_rx_full/fifo_eth_rx_full.dcp
  set_property netlist_only true [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_eth_rx_full/fifo_eth_rx_full.dcp]
  add_files -quiet /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_sca_cfgdata/fifo_sca_cfgdata.dcp
  set_property netlist_only true [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_sca_cfgdata/fifo_sca_cfgdata.dcp]
  add_files -quiet /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/debug/ila_sca_cfgFIFO2BRAM/ila_sca_cfgFIFO2BRAM.dcp
  set_property netlist_only true [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/debug/ila_sca_cfgFIFO2BRAM/ila_sca_cfgFIFO2BRAM.dcp]
  add_files -quiet /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/debug/ila_sca_HDLC_dword_prep/ila_sca_HDLC_dword_prep.dcp
  set_property netlist_only true [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/debug/ila_sca_HDLC_dword_prep/ila_sca_HDLC_dword_prep.dcp]
  add_files -quiet /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/gtcore_tds4/gtcore_tds4.dcp
  set_property netlist_only true [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/gtcore_tds4/gtcore_tds4.dcp]
  add_files -quiet /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_sca_hdlc_1b/fifo_sca_hdlc_1b.dcp
  set_property netlist_only true [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_sca_hdlc_1b/fifo_sca_hdlc_1b.dcp]
  add_files -quiet /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_sca_hdlc_10b/fifo_sca_hdlc_10b.dcp
  set_property netlist_only true [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_sca_hdlc_10b/fifo_sca_hdlc_10b.dcp]
  add_files -quiet /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/daq_clk_wiz_0/daq_clk_wiz_0.dcp
  set_property netlist_only true [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/daq_clk_wiz_0/daq_clk_wiz_0.dcp]
  read_xdc -prop_thru_buffers -ref temac_10_100_1000 -cells U0 /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/temac_10_100_1000/synth/temac_10_100_1000_board.xdc
  set_property processing_order EARLY [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/temac_10_100_1000/synth/temac_10_100_1000_board.xdc]
  read_xdc -ref temac_10_100_1000 -cells U0 /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/temac_10_100_1000/synth/temac_10_100_1000.xdc
  set_property processing_order EARLY [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/temac_10_100_1000/synth/temac_10_100_1000.xdc]
  read_xdc -ref fifo_gt_channel_data -cells U0 /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_gt_channel_data/fifo_gt_channel_data/fifo_gt_channel_data.xdc
  set_property processing_order EARLY [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_gt_channel_data/fifo_gt_channel_data/fifo_gt_channel_data.xdc]
  read_xdc -ref ila_gt_stripTDS -cells U0 /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/debug/ila_gt_stripTDS/ila_v6_1/constraints/ila.xdc
  set_property processing_order EARLY [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/debug/ila_gt_stripTDS/ila_v6_1/constraints/ila.xdc]
  read_xdc -ref ila_gt_data2fifo -cells U0 /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/debug/ila_gt_data2fifo/ila_v6_1/constraints/ila.xdc
  set_property processing_order EARLY [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/debug/ila_gt_data2fifo/ila_v6_1/constraints/ila.xdc]
  read_xdc -ref ila_gt_channelFIFO_dout -cells U0 /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/debug/ila_gt_channelFIFO_dout/ila_v6_1/constraints/ila.xdc
  set_property processing_order EARLY [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/debug/ila_gt_channelFIFO_dout/ila_v6_1/constraints/ila.xdc]
  read_xdc -ref vio_gt_fifo_ctrl /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/debug/vio_gt_fifo_ctrl/vio_gt_fifo_ctrl.xdc
  set_property processing_order EARLY [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/debug/vio_gt_fifo_ctrl/vio_gt_fifo_ctrl.xdc]
  read_xdc -prop_thru_buffers -ref gig_ethernet_pcs_pma_0 -cells U0 /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/gig_ethernet_pcs_pma_0/gig_ethernet_pcs_pma_0_board.xdc
  set_property processing_order EARLY [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/gig_ethernet_pcs_pma_0/gig_ethernet_pcs_pma_0_board.xdc]
  read_xdc -ref gig_ethernet_pcs_pma_0 -cells U0 /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/gig_ethernet_pcs_pma_0/synth/gig_ethernet_pcs_pma_0.xdc
  set_property processing_order EARLY [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/gig_ethernet_pcs_pma_0/synth/gig_ethernet_pcs_pma_0.xdc]
  read_xdc -prop_thru_buffers -ref gt_clk_wiz_1 -cells inst /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/gt_clk_wiz_1/gt_clk_wiz_1_board.xdc
  set_property processing_order EARLY [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/gt_clk_wiz_1/gt_clk_wiz_1_board.xdc]
  read_xdc -ref gt_clk_wiz_1 -cells inst /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/gt_clk_wiz_1/gt_clk_wiz_1.xdc
  set_property processing_order EARLY [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/gt_clk_wiz_1/gt_clk_wiz_1.xdc]
  read_xdc -ref fifo_eth_tx_data -cells U0 /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_eth_tx_data/fifo_eth_tx_data/fifo_eth_tx_data.xdc
  set_property processing_order EARLY [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_eth_tx_data/fifo_eth_tx_data/fifo_eth_tx_data.xdc]
  read_xdc -ref fifo_eth_tx_packet_length -cells U0 /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_eth_tx_packet_length/fifo_eth_tx_packet_length/fifo_eth_tx_packet_length.xdc
  set_property processing_order EARLY [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_eth_tx_packet_length/fifo_eth_tx_packet_length/fifo_eth_tx_packet_length.xdc]
  read_xdc -ref fifo_eth_rx_data -cells U0 /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_eth_rx_data/fifo_eth_rx_data/fifo_eth_rx_data.xdc
  set_property processing_order EARLY [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_eth_rx_data/fifo_eth_rx_data/fifo_eth_rx_data.xdc]
  read_xdc -ref fifo_eth_rx_full -cells U0 /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_eth_rx_full/fifo_eth_rx_full/fifo_eth_rx_full.xdc
  set_property processing_order EARLY [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_eth_rx_full/fifo_eth_rx_full/fifo_eth_rx_full.xdc]
  read_xdc -ref fifo_sca_cfgdata -cells U0 /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_sca_cfgdata/fifo_sca_cfgdata/fifo_sca_cfgdata.xdc
  set_property processing_order EARLY [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_sca_cfgdata/fifo_sca_cfgdata/fifo_sca_cfgdata.xdc]
  read_xdc -ref ila_sca_cfgFIFO2BRAM -cells U0 /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/debug/ila_sca_cfgFIFO2BRAM/ila_v6_1/constraints/ila.xdc
  set_property processing_order EARLY [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/debug/ila_sca_cfgFIFO2BRAM/ila_v6_1/constraints/ila.xdc]
  read_xdc -ref ila_sca_HDLC_dword_prep -cells U0 /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/debug/ila_sca_HDLC_dword_prep/ila_v6_1/constraints/ila.xdc
  set_property processing_order EARLY [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/debug/ila_sca_HDLC_dword_prep/ila_v6_1/constraints/ila.xdc]
  read_xdc -ref gtcore_tds4 -cells U0 /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/gtcore_tds4/gtcore_tds4.xdc
  set_property processing_order EARLY [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/gtcore_tds4/gtcore_tds4.xdc]
  read_xdc -ref fifo_sca_hdlc_1b -cells U0 /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_sca_hdlc_1b/fifo_sca_hdlc_1b/fifo_sca_hdlc_1b.xdc
  set_property processing_order EARLY [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_sca_hdlc_1b/fifo_sca_hdlc_1b/fifo_sca_hdlc_1b.xdc]
  read_xdc -ref fifo_sca_hdlc_10b -cells U0 /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_sca_hdlc_10b/fifo_sca_hdlc_10b/fifo_sca_hdlc_10b.xdc
  set_property processing_order EARLY [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_sca_hdlc_10b/fifo_sca_hdlc_10b/fifo_sca_hdlc_10b.xdc]
  read_xdc -prop_thru_buffers -ref daq_clk_wiz_0 -cells inst /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/daq_clk_wiz_0/daq_clk_wiz_0_board.xdc
  set_property processing_order EARLY [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/daq_clk_wiz_0/daq_clk_wiz_0_board.xdc]
  read_xdc -ref daq_clk_wiz_0 -cells inst /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/daq_clk_wiz_0/daq_clk_wiz_0.xdc
  set_property processing_order EARLY [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/daq_clk_wiz_0/daq_clk_wiz_0.xdc]
  read_xdc /home/liang/work/sTGC_Trigger_DAQ/sources/constrs_1/constraint_SYSTEM.xdc
  read_xdc /home/liang/work/sTGC_Trigger_DAQ/sources/constrs_1/constraint_ETH.xdc
  read_xdc /home/liang/work/sTGC_Trigger_DAQ/sources/constrs_1/constraint_ELINK.xdc
  read_xdc /home/liang/work/sTGC_Trigger_DAQ/sources/constrs_1/constraint_GTX.xdc
  read_xdc -ref temac_10_100_1000 -cells U0 /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/temac_10_100_1000/synth/temac_10_100_1000_clocks.xdc
  set_property processing_order LATE [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/temac_10_100_1000/synth/temac_10_100_1000_clocks.xdc]
  read_xdc -ref fifo_gt_channel_data -cells U0 /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_gt_channel_data/fifo_gt_channel_data/fifo_gt_channel_data_clocks.xdc
  set_property processing_order LATE [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_gt_channel_data/fifo_gt_channel_data/fifo_gt_channel_data_clocks.xdc]
  read_xdc -ref gt_clk_wiz_1 -cells inst /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/gt_clk_wiz_1/gt_clk_wiz_1_late.xdc
  set_property processing_order LATE [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/gt_clk_wiz_1/gt_clk_wiz_1_late.xdc]
  read_xdc -ref fifo_eth_tx_data -cells U0 /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_eth_tx_data/fifo_eth_tx_data/fifo_eth_tx_data_clocks.xdc
  set_property processing_order LATE [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_eth_tx_data/fifo_eth_tx_data/fifo_eth_tx_data_clocks.xdc]
  read_xdc -ref fifo_eth_tx_packet_length -cells U0 /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_eth_tx_packet_length/fifo_eth_tx_packet_length/fifo_eth_tx_packet_length_clocks.xdc
  set_property processing_order LATE [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_eth_tx_packet_length/fifo_eth_tx_packet_length/fifo_eth_tx_packet_length_clocks.xdc]
  read_xdc -ref fifo_eth_rx_data -cells U0 /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_eth_rx_data/fifo_eth_rx_data/fifo_eth_rx_data_clocks.xdc
  set_property processing_order LATE [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_eth_rx_data/fifo_eth_rx_data/fifo_eth_rx_data_clocks.xdc]
  read_xdc -ref fifo_eth_rx_full -cells U0 /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_eth_rx_full/fifo_eth_rx_full/fifo_eth_rx_full_clocks.xdc
  set_property processing_order LATE [get_files /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/fifo_eth_rx_full/fifo_eth_rx_full/fifo_eth_rx_full_clocks.xdc]
  link_design -top top -part xc7k325tffg900-2
  write_hwdef -file top.hwdef
  close_msg_db -file init_design.pb
} RESULT]
if {$rc} {
  step_failed init_design
  return -code error $RESULT
} else {
  end_step init_design
}

start_step opt_design
set rc [catch {
  create_msg_db opt_design.pb
  read_xdc -unmanaged /home/liang/work/sTGC_Trigger_DAQ/sources/sources_1/ip/gtcore_tds4/tcl/v7ht.tcl
  opt_design 
  write_checkpoint -force top_opt.dcp
  report_drc -file top_drc_opted.rpt
  close_msg_db -file opt_design.pb
} RESULT]
if {$rc} {
  step_failed opt_design
  return -code error $RESULT
} else {
  end_step opt_design
}

start_step place_design
set rc [catch {
  create_msg_db place_design.pb
  implement_debug_core 
  place_design 
  write_checkpoint -force top_placed.dcp
  report_io -file top_io_placed.rpt
  report_utilization -file top_utilization_placed.rpt -pb top_utilization_placed.pb
  report_control_sets -verbose -file top_control_sets_placed.rpt
  close_msg_db -file place_design.pb
} RESULT]
if {$rc} {
  step_failed place_design
  return -code error $RESULT
} else {
  end_step place_design
}

start_step route_design
set rc [catch {
  create_msg_db route_design.pb
  route_design 
  write_checkpoint -force top_routed.dcp
  report_drc -file top_drc_routed.rpt -pb top_drc_routed.pb
  report_timing_summary -warn_on_violation -max_paths 10 -file top_timing_summary_routed.rpt -rpx top_timing_summary_routed.rpx
  report_power -file top_power_routed.rpt -pb top_power_summary_routed.pb -rpx top_power_routed.rpx
  report_route_status -file top_route_status.rpt -pb top_route_status.pb
  report_clock_utilization -file top_clock_utilization_routed.rpt
  close_msg_db -file route_design.pb
} RESULT]
if {$rc} {
  step_failed route_design
  return -code error $RESULT
} else {
  end_step route_design
}

start_step write_bitstream
set rc [catch {
  create_msg_db write_bitstream.pb
  catch { write_mem_info -force top.mmi }
  write_bitstream -force top.bit 
  catch { write_sysdef -hwdef top.hwdef -bitfile top.bit -meminfo top.mmi -file top.sysdef }
  catch {write_debug_probes -quiet -force debug_nets}
  close_msg_db -file write_bitstream.pb
} RESULT]
if {$rc} {
  step_failed write_bitstream
  return -code error $RESULT
} else {
  end_step write_bitstream
}

