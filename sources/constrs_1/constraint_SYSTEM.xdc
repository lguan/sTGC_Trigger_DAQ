
# On the KC705 board, bank 0 and the CFGBVS pin are connected to a 2.5v supply. 
# 
# Configuration voltage supplied to bank 0
set_property CONFIG_VOLTAGE 2.5 [current_design]
#
# Configuration Bank Voltage Selection (CFGBVS)
set_property CFGBVS VCCO [current_design]


##================ SYSTEM CLOCK  ===================##
## LOC constrain for SYSTEM CLOCK (200 MHz LVDS on KC705, RefDes: U51)
set_property PACKAGE_PIN AD12  [get_ports  CLK200_P]
set_property IOSTANDARD LVDS [get_ports CLK200_P]
set_property PACKAGE_PIN AD11 [get_ports  CLK200_N] 
set_property IOSTANDARD LVDS [get_ports CLK200_N]
##================ Push Buttons, Switches and LEDs ===================##
set_property PACKAGE_PIN AB8 [get_ports LED_0]
set_property IOSTANDARD LVCMOS15 [get_ports LED_0]
set_property PACKAGE_PIN AA8 [get_ports LED_1]
set_property IOSTANDARD LVCMOS15 [get_ports LED_1]
set_property PACKAGE_PIN AC9 [get_ports LED_2]
set_property IOSTANDARD LVCMOS15 [get_ports LED_2]
set_property PACKAGE_PIN AB9 [get_ports LED_3]
set_property IOSTANDARD LVCMOS15 [get_ports LED_3]
set_property PACKAGE_PIN AE26 [get_ports LED_4]
set_property IOSTANDARD LVCMOS25 [get_ports LED_4]
set_property PACKAGE_PIN G19 [get_ports LED_5]
set_property IOSTANDARD LVCMOS25 [get_ports LED_5]
set_property PACKAGE_PIN E18 [get_ports LED_6]
set_property IOSTANDARD LVCMOS25 [get_ports LED_6]
set_property PACKAGE_PIN F16 [get_ports LED_7]
set_property IOSTANDARD LVCMOS25 [get_ports LED_7]
#PUSH BUTTONS
set_property PACKAGE_PIN AB12 [get_ports SW_S]
set_property IOSTANDARD LVCMOS15 [get_ports SW_S]
set_property PACKAGE_PIN AA12 [get_ports SW_N]
set_property IOSTANDARD LVCMOS15 [get_ports SW_N]
set_property PACKAGE_PIN G12 [get_ports SW_C]
set_property IOSTANDARD LVCMOS25 [get_ports SW_C]
set_property PACKAGE_PIN AG5 [get_ports SW_E]
set_property IOSTANDARD LVCMOS15 [get_ports SW_E]
set_property PACKAGE_PIN AC6 [get_ports SW_W]
set_property IOSTANDARD LVCMOS15 [get_ports SW_W]



##================ CTF Module INPUT  ===================##

set_property PACKAGE_PIN H21 [get_ports CTF_TRIG_IN_P]
set_property IOSTANDARD LVDS_25 [get_ports CTF_TRIG_IN_P]
set_property PACKAGE_PIN H22 [get_ports CTF_TRIG_IN_N]
set_property IOSTANDARD LVDS_25 [get_ports CTF_TRIG_IN_N]

set_property PACKAGE_PIN D27 [get_ports CTF_CKBC_IN_P]
set_property IOSTANDARD LVDS_25 [get_ports CTF_CKBC_IN_P]
set_property PACKAGE_PIN C27 [get_ports CTF_CKBC_IN_N]
set_property IOSTANDARD LVDS_25 [get_ports CTF_CKBC_IN_N]

set_property PACKAGE_PIN D21 [get_ports CTF_BCR_IN_P]
set_property IOSTANDARD LVDS_25 [get_ports CTF_BCR_IN_P]
set_property PACKAGE_PIN C21 [get_ports CTF_BCR_IN_N]
set_property IOSTANDARD LVDS_25 [get_ports CTF_BCR_IN_N]


# GTX Reference Clock 160 MHz
#set_property PACKAGE_PIN E8 [get_ports HPC_GBTCLK1_M2C_P]
#set_property IOSTANDARD LVDS_25 [get_ports HPC_GBTCLK1_M2C_P]
#set_property PACKAGE_PIN E7 [get_ports HPC_GBTCLK1_M2C_N]
#set_property IOSTANDARD LVDS_25 [get_ports HPC_GBTCLK1_M2C_N]






