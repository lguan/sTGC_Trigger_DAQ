##================ Ethernet Signals ===================##
set_property PACKAGE_PIN G8 [get_ports refclk125_p]
set_property PACKAGE_PIN G7 [get_ports refclk125_n]

set_property PACKAGE_PIN H5 [get_ports rxn]
set_property PACKAGE_PIN H6 [get_ports rxp]
set_property PACKAGE_PIN J3 [get_ports txn]
set_property PACKAGE_PIN J4 [get_ports txp]


set_property PACKAGE_PIN N30 [get_ports phy_int]
set_property IOSTANDARD LVCMOS25 [get_ports phy_int]
set_property PACKAGE_PIN L20 [get_ports phy_rstn]
set_property IOSTANDARD LVCMOS25 [get_ports phy_rstn]
