# FEB-0  J1 on mSAS_FMC4S

set_property PACKAGE_PIN   D26         [get_ports {to_FEB_CKBC_P[0]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_CKBC_P[0]}]
set_property PACKAGE_PIN   C26         [get_ports {to_FEB_CKBC_N[0]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_CKBC_N[0]}]

set_property PACKAGE_PIN   B28         [get_ports {to_FEB_TTC_DATA_P[0]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_TTC_DATA_P[0]}]
set_property PACKAGE_PIN   A28         [get_ports {to_FEB_TTC_DATA_N[0]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_TTC_DATA_N[0]}]



set_property PACKAGE_PIN   H30         [get_ports {to_FEB_SCA_CLK_P[0]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_CLK_P[0]}]
set_property PACKAGE_PIN   G30         [get_ports {to_FEB_SCA_CLK_N[0]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_CLK_N[0]}]

set_property PACKAGE_PIN   G29         [get_ports {to_FEB_SCA_DATA_P[0]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_DATA_P[0]}]
set_property PACKAGE_PIN   F30         [get_ports {to_FEB_SCA_DATA_N[0]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_DATA_N[0]}]

set_property PACKAGE_PIN   B30         [get_ports {from_FEB_SCA_DATA_P[0]}]
set_property IOSTANDARD    LVDS_25     [get_ports {from_FEB_SCA_DATA_P[0]}]
set_property PACKAGE_PIN   A30         [get_ports {from_FEB_SCA_DATA_N[0]}]
set_property IOSTANDARD    LVDS_25     [get_ports {from_FEB_SCA_DATA_N[0]}]







# FEB-1   J2 on mSAS_FMC4S
set_property PACKAGE_PIN   L16         [get_ports {to_FEB_CKBC_P[1]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_CKBC_P[1]}]
set_property PACKAGE_PIN   K16         [get_ports {to_FEB_CKBC_N[1]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_CKBC_N[1]}]

set_property PACKAGE_PIN   C15         [get_ports {to_FEB_TTC_DATA_P[1]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_TTC_DATA_P[1]}]
set_property PACKAGE_PIN   B15         [get_ports {to_FEB_TTC_DATA_N[1]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_TTC_DATA_N[1]}]



set_property PACKAGE_PIN   H24         [get_ports {to_FEB_SCA_CLK_P[1]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_CLK_P[1]}]
set_property PACKAGE_PIN   H25         [get_ports {to_FEB_SCA_CLK_N[1]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_CLK_N[1]}]

set_property PACKAGE_PIN   H26         [get_ports {to_FEB_SCA_DATA_P[1]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_DATA_P[1]}]
set_property PACKAGE_PIN   H27         [get_ports {to_FEB_SCA_DATA_N[1]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_DATA_N[1]}]

set_property PACKAGE_PIN   C25         [get_ports {from_FEB_SCA_DATA_P[1]}]
set_property IOSTANDARD    LVDS_25     [get_ports {from_FEB_SCA_DATA_P[1]}]
set_property PACKAGE_PIN   B25         [get_ports {from_FEB_SCA_DATA_N[1]}]
set_property IOSTANDARD    LVDS_25     [get_ports {from_FEB_SCA_DATA_N[1]}]



# FEB-2   J3 on mSAS_FMC4S

set_property PACKAGE_PIN   G13         [get_ports {to_FEB_CKBC_P[2]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_CKBC_P[2]}]
set_property PACKAGE_PIN   F13         [get_ports {to_FEB_CKBC_N[2]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_CKBC_N[2]}]

set_property PACKAGE_PIN   K14         [get_ports {to_FEB_TTC_DATA_P[2]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_TTC_DATA_P[2]}]
set_property PACKAGE_PIN   J14         [get_ports {to_FEB_TTC_DATA_N[2]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_TTC_DATA_N[2]}]


set_property PACKAGE_PIN   A11         [get_ports {to_FEB_SCA_CLK_P[2]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_CLK_P[2]}]
set_property PACKAGE_PIN   A12         [get_ports {to_FEB_SCA_CLK_N[2]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_CLK_N[2]}]

set_property PACKAGE_PIN   J16         [get_ports {to_FEB_SCA_DATA_P[2]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_DATA_P[2]}]
set_property PACKAGE_PIN   H16         [get_ports {to_FEB_SCA_DATA_N[2]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_DATA_N[2]}]

set_property PACKAGE_PIN   D14         [get_ports {from_FEB_SCA_DATA_P[2]}]
set_property IOSTANDARD    LVDS_25     [get_ports {from_FEB_SCA_DATA_P[2]}]
set_property PACKAGE_PIN   C14         [get_ports {from_FEB_SCA_DATA_N[2]}]
set_property IOSTANDARD    LVDS_25     [get_ports {from_FEB_SCA_DATA_N[2]}]


# FEB-3   J4 on mSAS_FMC4S
set_property PACKAGE_PIN   F21         [get_ports {to_FEB_CKBC_P[3]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_CKBC_P[3]}]
set_property PACKAGE_PIN   E21         [get_ports {to_FEB_CKBC_N[3]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_CKBC_N[3]}]

set_property PACKAGE_PIN   B22         [get_ports {to_FEB_TTC_DATA_P[3]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_TTC_DATA_P[3]}]
set_property PACKAGE_PIN   A22         [get_ports {to_FEB_TTC_DATA_N[3]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_TTC_DATA_N[3]}]


set_property PACKAGE_PIN   G22         [get_ports {to_FEB_SCA_CLK_P[3]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_CLK_P[3]}]
set_property PACKAGE_PIN   F22         [get_ports {to_FEB_SCA_CLK_N[3]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_CLK_N[3]}]

set_property PACKAGE_PIN   D22         [get_ports {to_FEB_SCA_DATA_P[3]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_DATA_P[3]}]
set_property PACKAGE_PIN   C22         [get_ports {to_FEB_SCA_DATA_N[3]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_DATA_N[3]}]

set_property PACKAGE_PIN   D16         [get_ports {from_FEB_SCA_DATA_P[3]}]
set_property IOSTANDARD    LVDS_25     [get_ports {from_FEB_SCA_DATA_P[3]}]
set_property PACKAGE_PIN   C16         [get_ports {from_FEB_SCA_DATA_N[3]}]
set_property IOSTANDARD    LVDS_25     [get_ports {from_FEB_SCA_DATA_N[3]}]


# FEB-4   J5 on mSAS_FMC4S
set_property PACKAGE_PIN   D12         [get_ports {to_FEB_CKBC_P[4]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_CKBC_P[4]}]
set_property PACKAGE_PIN   D13         [get_ports {to_FEB_CKBC_N[4]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_CKBC_N[4]}]

set_property PACKAGE_PIN   F15         [get_ports {to_FEB_TTC_DATA_P[4]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_TTC_DATA_P[4]}]
set_property PACKAGE_PIN   E16         [get_ports {to_FEB_TTC_DATA_N[4]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_TTC_DATA_N[4]}]



set_property PACKAGE_PIN   C12         [get_ports {to_FEB_SCA_CLK_P[4]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_CLK_P[4]}]
set_property PACKAGE_PIN   B12         [get_ports {to_FEB_SCA_CLK_N[4]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_CLK_N[4]}]

set_property PACKAGE_PIN   D11         [get_ports {to_FEB_SCA_DATA_P[4]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_DATA_P[4]}]
set_property PACKAGE_PIN   C11         [get_ports {to_FEB_SCA_DATA_N[4]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_DATA_N[4]}]

set_property PACKAGE_PIN   B14         [get_ports {from_FEB_SCA_DATA_P[4]}]
set_property IOSTANDARD    LVDS_25     [get_ports {from_FEB_SCA_DATA_P[4]}]
set_property PACKAGE_PIN   A15         [get_ports {from_FEB_SCA_DATA_N[4]}]
set_property IOSTANDARD    LVDS_25     [get_ports {from_FEB_SCA_DATA_N[4]}]


# FEB-5   J6 on mSAS_FMC4S
set_property PACKAGE_PIN   E28         [get_ports {to_FEB_CKBC_P[5]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_CKBC_P[5]}]
set_property PACKAGE_PIN   D28         [get_ports {to_FEB_CKBC_N[5]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_CKBC_N[4]}]

set_property PACKAGE_PIN   C29         [get_ports {to_FEB_TTC_DATA_P[5]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_TTC_DATA_P[5]}]
set_property PACKAGE_PIN   B29         [get_ports {to_FEB_TTC_DATA_N[5]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_TTC_DATA_N[5]}]



set_property PACKAGE_PIN   G28         [get_ports {to_FEB_SCA_CLK_P[5]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_CLK_P[5]}]
set_property PACKAGE_PIN   F28         [get_ports {to_FEB_SCA_CLK_N[5]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_CLK_N[5]}]

set_property PACKAGE_PIN   E29         [get_ports {to_FEB_SCA_DATA_P[5]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_DATA_P[5]}]
set_property PACKAGE_PIN   E30         [get_ports {to_FEB_SCA_DATA_N[5]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_DATA_N[5]}]

set_property PACKAGE_PIN   G27         [get_ports {from_FEB_SCA_DATA_P[5]}]
set_property IOSTANDARD    LVDS_25     [get_ports {from_FEB_SCA_DATA_P[5]}]
set_property PACKAGE_PIN   F27         [get_ports {from_FEB_SCA_DATA_N[5]}]
set_property IOSTANDARD    LVDS_25     [get_ports {from_FEB_SCA_DATA_N[5]}]


# FEB-6   J7 on mSAS_FMC4S
set_property PACKAGE_PIN   E19         [get_ports {to_FEB_CKBC_P[6]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_CKBC_P[6]}]
set_property PACKAGE_PIN   D19         [get_ports {to_FEB_CKBC_N[6]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_CKBC_N[6]}]

set_property PACKAGE_PIN   G18         [get_ports {to_FEB_TTC_DATA_P[6]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_TTC_DATA_P[6]}]
set_property PACKAGE_PIN   F18         [get_ports {to_FEB_TTC_DATA_N[6]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_TTC_DATA_N[6]}]


set_property PACKAGE_PIN   A16         [get_ports {to_FEB_SCA_CLK_P[6]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_CLK_P[6]}]
set_property PACKAGE_PIN   A17         [get_ports {to_FEB_SCA_CLK_N[6]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_CLK_N[6]}]

set_property PACKAGE_PIN   C17         [get_ports {to_FEB_SCA_DATA_P[6]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_DATA_P[6]}]
set_property PACKAGE_PIN   B17         [get_ports {to_FEB_SCA_DATA_N[6]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_DATA_N[6]}]

set_property PACKAGE_PIN   C20         [get_ports {from_FEB_SCA_DATA_P[6]}]
set_property IOSTANDARD    LVDS_25     [get_ports {from_FEB_SCA_DATA_P[6]}]
set_property PACKAGE_PIN   B20         [get_ports {from_FEB_SCA_DATA_N[6]}]
set_property IOSTANDARD    LVDS_25     [get_ports {from_FEB_SCA_DATA_N[6]}]



# FEB-7   J8 on mSAS_FMC4S
set_property PACKAGE_PIN   H14         [get_ports {to_FEB_CKBC_P[7]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_CKBC_P[7]}]
set_property PACKAGE_PIN   G14         [get_ports {to_FEB_CKBC_N[7]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_CKBC_N[7]}]

set_property PACKAGE_PIN   F20         [get_ports {to_FEB_TTC_DATA_P[7]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_TTC_DATA_P[7]}]
set_property PACKAGE_PIN   E20         [get_ports {to_FEB_TTC_DATA_N[7]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_TTC_DATA_N[7]}]


set_property PACKAGE_PIN   L11         [get_ports {to_FEB_SCA_CLK_P[7]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_CLK_P[7]}]
set_property PACKAGE_PIN   K11         [get_ports {to_FEB_SCA_CLK_N[7]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_CLK_N[7]}]

set_property PACKAGE_PIN   L12         [get_ports {to_FEB_SCA_DATA_P[7]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_DATA_P[7]}]
set_property PACKAGE_PIN   L13         [get_ports {to_FEB_SCA_DATA_N[7]}]
set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_DATA_N[7]}]

set_property PACKAGE_PIN   H15         [get_ports {from_FEB_SCA_DATA_P[7]}]
set_property IOSTANDARD    LVDS_25     [get_ports {from_FEB_SCA_DATA_P[7]}]
set_property PACKAGE_PIN   G15         [get_ports {from_FEB_SCA_DATA_N[7]}]
set_property IOSTANDARD    LVDS_25     [get_ports {from_FEB_SCA_DATA_N[7]}]




##FPGA 
## # KC705 onboard SMA
##set_property PACKAGE_PIN   L25         [get_ports ELINK_CLK_P]
##set_property IOSTANDARD    LVDS_25     [get_ports ELINK_CLK_P]
##set_property PACKAGE_PIN   K25         [get_ports ELINK_CLK_N]
##set_property IOSTANDARD    LVDS_25     [get_ports ELINK_CLK_N]

##set_property PACKAGE_PIN   Y23         [get_ports ELINK_TX_P]
##set_property IOSTANDARD    LVDS_25     [get_ports ELINK_TX_P]
##set_property PACKAGE_PIN   Y24         [get_ports ELINK_TX_N]
##set_property IOSTANDARD    LVDS_25     [get_ports ELINK_TX_N]




## FEB-0
## KC705 with NSW Versatile Adapter (SMA)
##set_property PACKAGE_PIN   F20         [get_ports {to_FEB_SCA_CLK_P[0]} ]
##set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_CLK_P[0]} ]
##set_property PACKAGE_PIN   E20         [get_ports {to_FEB_SCA_CLK_N[0]}]
##set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_CLK_N[0]}]

##set_property PACKAGE_PIN   B22         [get_ports {to_FEB_SCA_DATA_P[0]}]
##set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_DATA_P[0]}]
##set_property PACKAGE_PIN   A22         [get_ports {to_FEB_SCA_DATA_N[0]}]
##set_property IOSTANDARD    LVDS_25     [get_ports {to_FEB_SCA_DATA_N[0]}]

##set_property PACKAGE_PIN   B18         [get_ports {from_FEB_SCA_DATA_P[0]}]
##set_property IOSTANDARD    LVDS_25     [get_ports {from_FEB_SCA_DATA_P[0]}]
##set_property PACKAGE_PIN   A18         [get_ports {from_FEB_SCA_DATA_N[0]}]
##set_property IOSTANDARD    LVDS_25     [get_ports {from_FEB_SCA_DATA_N[0]}]
