# NSW sTGC Trigger Path miniDAQ Firmware

# Table of Contents
* [Introduction](#intro)
* [How to Use](#useage)
* [History](#history)
* [Developer](#devel)
* [Contact](#cont)

## Introduction
This project contains the FPGA firmware for the slow control and readout of
ATLAS NSW sTGC chambers using pFEB/sFEB v2.1 with VMM3, TDS2 and GBT-SCA ASIC 
and an FPGA emulating the ROC ASIC.

## How to Use
1. Download the firmware source code, bitstream using the follow commmand
```
git clone ssh://git@gitlab.cern.ch:7999/lguan/sTGC_SCA.git
```
2. setup KC705 board Ethernet PHY chip to operate at SGMII mode: Short pin 2 and of both J29 and J30 connectors using jumper header 
3. download bitstream to KC705 via JTAG
3. load debug probe file if needed
4. set IP and configure ethernet port for DAQ PC (fixed to 192.168.0.16 now)
```
Host PC IP:       192.168.0.16
Host PC  Port:    7201
Dest(KC705) IP:   192.168.0.1
Dest(KC705) Port: 6008
```
Make sure the UDP port for communicating with the KC705 FPGA board is open. For CENT OS 7,
login as super user and use the following command:
```
firewall-cmd --permanent --zone=public --add-port=6008/udp
firewall-cmd --permanent --zone=public --query-port=6008/udp
firewall-cmd --reload
```
4. use the simple python script in the **Scripts** folder to send configruation command and monitor the reply message. It is also possible to use the WireShark software to sniff the traffic.

## History
* 26 May 2017: Initial Creation -- demo the Ethernet and GT cores coexist
* 13 June 2017: Updated with 80 MHz processing logic. Updated ASIC Control via SCA.

## Developer
Liang Guan (UM)  
Liang Yu (UM)  
Miao Peng (USTC)  
Jinhong Wang (UM)  
Reid Pinkham (UM)  
Xiong Xiao (UM)

## License
## License
CERN OHL

## Contact
For any question,please use the following contact to get into touch with the developler
**Liang Guan**: <lguan@cern.ch>

## Acknowledgement
The Ethernet UDP Core is adapted from the firmware used by the ATLAS NSW MICROMEGAS FEB which is developed, integrated by Panagiotis Gkountoumis et al. of the ATHENS group. 




	





   

  
